<!DOCTYPE html>
<html lang="it">
   <head>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8">
      <title>ISERVE INVOICE</title>
      <!--
         COLORE INTENSE  #9C010F
         COLORE LIGHT #EDE8DA
         
         TESTO LIGHT #3F3D33
         TESTO INTENSE #ffffff 
         
         
          -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
<!--      <style type="text/css">#ko_twocolumnBlock_6 .textsmallintenseStyle a, #ko_twocolumnBlock_6 .textsmallintenseStyle a:link, #ko_twocolumnBlock_6 .textsmallintenseStyle a:visited, #ko_twocolumnBlock_6 .textsmallintenseStyle a:hover {color: #fff;color: ;text-decoration: none;text-decoration: none;font-weight: bold;}
         #ko_twocolumnBlock_6 .textsmalllightStyle a:visited, #ko_twocolumnBlock_6 .textsmalllightStyle a:hover {color: #3f3d33;color: ;text-decoration: none;text-decoration: none;font-weight: bold;}
      </style>-->
      
   </head>
   <body style="margin: 0; padding: 0;" bgcolor="#f9f9f9" align="center">
   <style type="text/css">
	  hr {
    margin: 0px;
    background: #b3b3b3;
    opacity: 0.5;
}
body {    
    font-family: 'Open Sans', sans-serif;
}
td {
    font-family: 'Open Sans' !important;
}

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'Models/config.php';
require_once 'Models/ConDBServices.php';
require_once 'Models/MPDF56/mpdf.php';

function getOut() {
    echo "<h1>You are on a wrong page, please get back.</h1>";
    exit();
}
function convertToHoursMins($time, $format = '%d:%d') {
    settype($time, 'integer');
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}

if (isset($_REQUEST['apntId'])) {
    $db = new ConDBServices();
    $apptData = $db->mongo->selectCollection("bookings")->findOne(array("bid" => (string) $_REQUEST['apntId']));
//    if ($apptData['status'] != '7')
//        getOut();

        if ($apptData['status'] == '7')
            $status = 'Appointment completed';
        else if ($apptData['status'] == '4')
            $status = 'Appointment cancelled';
        $type = 2;
        if ($type == 1) {
            $first_name = $apptData['customer']['fname'];
            $last_name = $apptData['customer']['lname'];
            $mobile = $apptData['customer']['mobile'];
            $profile_pic = $apptData['customer']['pic'];
            $usertype = "CUSTOMER";
        } else {
            $first_name = $apptData['provider']['fname'];
            $last_name = $apptData['provider']['lname'];
            $mobile = $apptData['provider']['mobile'];
            $profile_pic = $apptData['provider']['pic'];
            $usertype = "PROVIDER";
        }
        if($apptData['payment_type'] == "2"){
            $pt ="CARD";
        }else{
            $pt ="CASH";
        }

        $ServicesStr = "";
        $ServicePrice = 0;
        foreach ($apptData['services'] as $service) {
            $ServicesStr .= $service['sname'] . " , ";
            $ServicePrice = $ServicePrice + $service['sprice'];
        }


        $createdPdfHtml = '';
        $remHtml = '';

//        new invoice template added  here
        $bookingType = "Now";
        if ($apptData['booking_type'] == '2')
            $bookingType = "Later";
$startdate =date("d-m-Y H:i A", strtotime($apptData['job_start_time']));
        $enddate =date("d-m-Y H:i A", strtotime($apptData['job_end_time']));
        $html = ' <body style="margin: 0px; padding: 0px;" bgcolor="#f9f9f9" align="center" data-gr-c-s-loaded="true">
   <style type="text/css">
	  hr {
    margin: 0px;
    background: #b3b3b3;
    opacity: 0.5;
}
body {    
    font-family: "Open Sans", sans-serif;
}
td {
    font-family: "Open Sans" !important;
}

<style type="text/css">
	  hr {
    margin: 0px;
    background: #b3b3b3;
    opacity: 0.5;
}
         /* CLIENT-SPECIFIC STYLES */
         #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
         .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height: 100%;} /* Force Hotmail to display normal line spacing */
         body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
         table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
         img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */
         /* RESET STYLES */
         body{margin:0; padding:0;}
         img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
         table{border-collapse:collapse !important;}
         body{height:100% !important; margin:0; padding:0; width:100% !important;}
         /* iOS BLUE LINKS */
         .appleBody a{color:#68440a; text-decoration: none;}
         .appleFooter a{color:#999999; text-decoration: none;}
         /* MOBILE STYLES */
         @media screen and (max-width: 625px) {
         /* ALLOWS FOR FLUID TABLES */
         table[class="wrapper"]{
         width:100% !important;
         min-width:0px !important;
         }
         td.mobile_disturb {
    margin-top: 12px !important;
}
         span.lasttrip {
    display: none;
}
span.left_col_field {    
    margin-left: 0px !important;
}
p.left_col_head{
    margin-left:0px !important;
}

td.align-rt {    
    font-size: 13px;  
}

         /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
         td[class="mobile-hide"]{
         display:none;}
         img[class="mobile-hide"]{
         display: none !important;
         }
         img[class="img-max"]{
        width: 60% !important;
    margin-left: 21%;
    max-width: 100% !important;
    height: auto !important;
         }
         /* FULL-WIDTH TABLES */
         table[class="responsive-table"]{
         width:100%!important;
         }
         /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
         td[class="padding"]{
         padding: 10px 5% 15px 5% !important;
         }
         td[class="padding-copy"]{
         padding: 10px 5% 10px 5% !important;
         text-align: center;
         }
         td[class="padding-meta"]{
         padding: 30px 5% 0px 5% !important;
         text-align: center;
         }
         td[class="no-pad"]{
         padding: 0 0 0px 0 !important;
         }
         td[class="no-padding"]{
         padding: 0 !important;
         }
         td[class="section-padding"]{
         padding: 10px 15px 10px 15px !important;
         }
         td[class="section-padding-bottom-image"]{
         padding: 10px 15px 0 15px !important;
         }
         /* ADJUST BUTTONS ON MOBILE */
         td[class="mobile-wrapper"]{
         padding: 10px 5% 15px 5% !important;
         }
         table[class="mobile-button-container"]{
         margin:0 auto;
         width:100% !important;
         }
         a[class="mobile-button"]{
         width:80% !important;
         padding: 15px !important;
         border: 0 !important;
         font-size: 16px !important;
         }
         }
		 table#end_block tbody tr td {
    font-size: 12px!important;
    color: #232323!important;
    border: 1px solid #bfbfbf;
    padding: 3%!important;
}
table tr:last-child td:first-child {
    border-bottom-left-radius: 10px;
}

table tr:last-child td:last-child {
    border-bottom-right-radius: 10px;
}
p.left_col_head {
    font-size: 11px;
    color: #999999;
    font-family: sans-serif;
	margin-top:11px;
	margin-bottom:11px;
	margin-left:5px
}
tr.bottom_border {
    border-bottom: solid 1px #efe7e7;
}
.rt_col_head {
      font-family: sans-serif;
    font-size: 15px;
    text-align: center;
    padding-bottom: 7px;
    border-bottom: 1px solid #e0dcdc;
}
td.rt_col_head2 {
    font-family: sans-serif;
    color: #999999;
    font-size: 14px;
    font-weight: 500;
	width:78%
}
td.bold_txt {
    font-family: sans-serif;
    font-weight: 600;
    font-size: 15px;
}
span.left_col_field {
   
    font-size: 14px;
	margin-top:11px;
	margin-bottom:11px;
	margin-left:5px
}
td#app_det {
    font-size: 16px;
    font-family: sans-serif;
    text-align: center;
}
td.align-rt {
    text-align: right;
	font-size:15px;
    font-weight:400;
}
span.lasttrip {   
    font-size: 16px;
    font-weight: 400;
    text-align: right;
    float: right;
    /* position: relative; */
    margin-bottom: 18px;
}
span.class_name {   
    margin-left: 14px !important;    
}
table.responsive-table {
    border-radius: 10px 10px 0px 0px;
}
      </style>   
      <!-- PREHEADER -->
      <table border="0" cellpadding="0" cellspacing="0" width="100%" id="ko_imageBlock_5">
         <tbody>
            <tr class="row-a">
               <td bgcolor="#f9f9f9" align="center" class="no-pad" style="padding-top: 0px; padding-left: 15px; padding-bottom: 0px; padding-right: 15px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="700" class="responsive-table">
                     <tbody>
                        <tr>
                           <td>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                 <tbody>
                                    <tr>
                                       <td>
                                          <!-- HERO IMAGE -->
                                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                             <tbody>
                                                <tr>
                                                   <td class="no-padding">
                                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                         <tbody>
                                                            <tr>
                                                               <td>
                                                                    <img width="600" border="0" alt="" class="img-max" style="display: block; padding: 0; color: #3F3D33; 
																  text-decoration: none; font-family: Helvetica, Arial, sans-serif; font-size: 16px; width: 187px;height:25px;margin-top:40px;margin-bottom:20px" src="http://iserve.ind.in/pics/invoice_logo.png">
                                                               </td>
                                                               <td><br><br>
                                                                    <span class="lasttrip " style="FLOAT: RIGHT;">Your Last Appoinment On : <b>' . $startdate . ' </b></span>
                                                                </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      <table border="0" cellpadding="0" cellspacing="0" width="100%" id="ko_twocolumnBlock_6">
         <tbody>
            <tr class="row-a">
               <td bgcolor="#f9f9f9" align="center" class="section-padding" style="padding-top: 0px; padding-left: 15px; padding-bottom: 0px; padding-right: 15px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="700" class="responsive-table">
                     <tbody>
                        <tr>
                           <td>
                              <!-- TWO COLUMNS -->
							  <table cellpadding="0" cellspacing="0" border="0" width="700" align="left" class="responsive-table" style="background:#fff;padding:2% ;border: 1px solid #e6e3e3;">
								<tbody><tr><td style="font-size: 28px; letter-spacing:1px; padding: 10px;font-family: fantasy; font-weight: 400;width:47%">'. CURRENCY . ' ' . $apptData['invoiceData']['billed_amount'] . '</td>
								    <td class="mobile_disturb" style="float: right; color: #b2b2b2; margin-top: 18px; margin-right: 10px;font-family: sans-serif;letter-spacing: 1px;font-size: 13px;">Thanks for using ' . APP_NAME . '</td>
									
								</tr>
							  </tbody></table>
							
                              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                 <tbody>
                                    <tr>
                                       <td valign="top" style="padding: 2%;background:#fff; border: 1px solid #e6e3e3; border-top: 0px;" class="mobile-wrapper">
                                          <!-- LEFT COLUMN -->
                                          <table cellpadding="5" cellspacing="0" border="0" width="48%" align="left" class="responsive-table" style="background:#fff; border: 1px solid #e6e3e3;">
                                             <tbody>
                                               
												<tr class="bottom_border">
												  <td colspan="3" id="app_det" style="border-bottom: 1px solid #e6e3e3;font-size: 16px;text-align:center;">
												    Appointment details
												  </td>
												</tr>
												<tr class="bottom_border">
												  <td  colspan="3" style="border-bottom: 1px solid #e6e3e3";>
												    <p class="left_col_head">JOB ID</p>
													<span class="left_col_field adjust">' . $apptData['invoiceData']['invoice_id'] . '</span>
												  </td>
												</tr><tr class="bottom_border">

												  <td  colspan="3" style="border-bottom: 1px solid #e6e3e3;">
												    <p class="left_col_head">START TIME</p>
													<span class="left_col_field adjust">' .$startdate . '</span>
												  </td>
                                                  </tr><tr class="bottom_border">

												  <td  colspan="3" style="border-bottom: 1px solid #e6e3e3";>
												    <p class="left_col_head">END TIME</p>
													<span class="left_col_field adjust">' . $enddate . '</span>
												  </td>
												</tr>
												<tr class="bottom_border">
												  <td colspan="3" style="border-bottom: 1px solid #e6e3e3";>
												    <p class="left_col_head">JOB LOCATION</p>
													<span class="left_col_field">
                                                                                                       ' . $apptData['address1'] . '
													</span>
												  </td>
												</tr>
												<tr class="bottom_border">
												  <td colspan="3" style="border-bottom: 1px solid #e6e3e3";>
												    <p class="left_col_head">' . $usertype . '  DETAILS</p>
													<span><img src="' . $profile_pic . '" style="margin-bottom:15px;float:left;margin-right:5px;height:70px;width:70px;border-radius:50px"></span><span class="left_col_field name_section" style="margin-left:0px">
													<span class="class_name">' . $first_name .' '. $last_name . '<br>
													<span class="class_name">' . $mobile . '
													</span>
												  </span></span></td>
												</tr>
											    <tr>
												  <td colspan="3" style="border-bottom: 1px solid #e6e3e3";>
												    <p class="left_col_head" style="text-align:center">Issued on behalf of ' . $first_name . '</p>
												  </td>
												</tr>
                                             </tbody>
                                          </table>
                                          <!-- RIGHT COLUMN -->
                                          <table cellpadding="5" cellspacing="0" border="0" width="47%" align="right" class="responsive-table">
                                             <tbody>
											  <tr>
											    <td class="rt_col_head" colspan="3" style="font-size: 16px;text-align: center;border-bottom: 1px solid #e6e3e3;">RECEIPT</td>
											  </tr>
                                                <tr>
												  <td class="rt_col_head2">
												   Job Type
												  </td>
												  <td class="align-rt" style="text-align:right;">
												   ' . $apptData['cat_name'] . '
												  </td>
                                                </tr>
												<tr>
                                                                                                  </tr>
                                                <tr>
												  <td class="rt_col_head2">
												   Services
												  </td>
												  <td class="align-rt" style="text-align:right;">
												   ' . $ServicesStr . '
												  </td>
                                                </tr>
                                                <tr>
												  <td class="rt_col_head2">
												   Services Price
												  </td>
												  <td class="align-rt" style="text-align:right;">
												  '. CURRENCY . ' ' . $ServicePrice . '
												  </td>
                                                </tr>
												<tr>
												  <td class="rt_col_head2">
												    Booking Type
												  </td>
												  <td class="align-rt" style="text-align:right;">
                                                                                                  ' . $bookingType . '</td>
                                                </tr>
												<tr>
												  <td class="rt_col_head2">
												    Visit fee
												  </td>
												  <td class="align-rt" style="text-align:right;">
												      '. CURRENCY . ' ' . $visitfee . '
												  </td>
                                                </tr>
												<tr>
												  <td class="rt_col_head2">
												    Hourly fee
												  </td>
												  <td class="align-rt" style="text-align:right;">
												   '. CURRENCY . ' ' . $timefee . '
												  </td>
                                                </tr>
												<tr>
												  <td class="rt_col_head2">
												    Misc fee
												  </td>
												  <td class="align-rt" style="text-align:right;">
												    '. CURRENCY . ' ' . $miscfee . '
												  </td>
                                                </tr>
												<tr>
												  <td class="rt_col_head2">
												    Material fee
												  </td>
												  <td class="align-rt" style="text-align:right;">
												       '. CURRENCY . ' ' . $matfee . '
												  </td>
                                                </tr>
												<tr>
												  <td class="rt_col_head2">
												   Provider discount
												  </td>
												  <td class="align-rt" style="text-align:right;">
												   ' . CURRENCY . ' '. $prodis . '
												  </td>
                                                </tr>
												<tr>
												  <td class="rt_col_head2">
												    App discount
												  </td>
												  <td class="align-rt" style="text-align:right;">
												    '. CURRENCY . ' ' . $appdis . '
												  </td>
                                                </tr>
												<tr class="bottom_border">
												  <td class="bold_txt" style="font-size:16px">
												     Total
												  </td>
												  <td class="bold_txt" style="text-align:right">
												     <strong>  '. CURRENCY . ' ' . $apptData['invoiceData']['billed_amount'] . '</strong>
												  </td>
                                                </tr>
												<tr>
												  <td class="rt_col_head2" style="font-size: 10px;line-height: 9px;margin-bottom: 0px;">CHARGED</td>
                                                </tr>
												<tr class="bottom_border">
												  <td class="bold_txt" style="font-size:17px">
												    <span><img src="http://iserve.ind.in/pics/pp/cash.png" style=" float: left;  padding-top: 2px; margin-right: 5px;"></span> '.$pt.'
												  </td>
												  <td class="bold_txt" style="font-size:15px;text-align:right">
												     '. CURRENCY . ' ' . $apptData['invoiceData']['billed_amount'] . '
												  </td>
                                                </tr>
												<tr>
												   <td>
												    <p class="left_col_head">Notes:</p> ' . $apptData['comp']['complete_note'] . ' 
												   </td>
												</tr>
                                                 <tr>
                                                 <td>
                                                     <p class="left_col_head">SIGNATURE:</p>
                                                     <img src=" ' . $apptData['comp']['sign_url'] . ' " style="width:210px;height: 38px;">
                                                     </td>
                                                 </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>   
      </table>
      <table border="0" cellpadding="0" cellspacing="0" width="100%" id="ko_titleBlock_7">
         <tbody>
            <tr class="row-a">
               <td bgcolor="#f9f9f9" align="center" class="section-padding" style="padding: 30px 15px 15px 15px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="700" style="padding: 0 0 20px 0;" class="responsive-table" id="end_block">
                     <!-- TITLE -->
                     <tbody>
                        <tr>
                           <td align="center" class="padding-copy" colspan="2" style="padding: 0 0 10px 0px; color:#000; font-size: 25px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #232323!important;">This is an electronically generated invoice.<br> All Taxes are included.  </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      <!-- FOOTER -->
   </body>';



        
    echo $html;
} else {
    getOut();
}
?>
