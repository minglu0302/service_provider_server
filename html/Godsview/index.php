<?php

$sess = unserialize($_COOKIE['ci_session']);
if ($sess['table'] == '') {
    header("Location: ../dashboard/");
}
require_once '../Models/config.php';
//require_once 'Models/config.php';
//include("../../Models/config.php");
 
?>
<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<title>iServe</title>
<meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
<script src='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.js'></script>
<link href='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.css' rel='stylesheet' />

<style>
    .leaflet-bottom{
        display: none !important;
    }
</style>
</head>
<body>
<script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/leaflet.markercluster.js'></script>
<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.css' rel='stylesheet' />
<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.Default.css' rel='stylesheet' />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lato:400,100,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/vis/4.15.0/vis.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
         <link href='<?= APP_SERVER_HOST ?>Godsview/simple.css' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="<?= APP_SERVER_HOST ?>admin/theme/assets/img/demo/favicon.png" />
        <link href='<?= APP_SERVER_HOST ?>Godsview/default.css' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="<?= LogoURL?>" />
        <title>iServe</title>
        <link rel="apple-touch-icon" sizes="76x76" href="<?= LogoURL?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= LogoURL?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= LogoURL?>">

<!-- Example data. -->
<script src="https://www.mapbox.com/mapbox.js/assets/data/realworld.388.js"></script>
<body id="body">
        <div class="conatiner-fluid">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <h2><?= APP_NAME ?> GODS VIEW</h2>
                    <ul class="nav navbar-nav navbar-right" style="margin-top:1px;">
                                        <li>
                                            <select class="selectpicker" id="city">
                                            <option value="">Select City</option>
                                        </select>
                                        </li>
               
                </div>
            </nav>
        </div>
        <div id='bs-example'>
            <nav role="navigation" class="navbar navbar-default">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="modal" data-target="#myModal" id="left" style='float: left !important;color: black;'>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </button>
                    <button type="button" data-target="#collapse1" data-toggle="collapse" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <div id="collapse1" class="panel-collapse collapse">
                        <div class="col-xs-12">
                            <a href="#" onclick="change_state(5,5);"><img src="http://iserve.ind.in/Godsview/icons/red.png" />
                                <h2><button class="btn btn-default" role="button">On The Way</button></h2>
                            </a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#" onclick="change_state(5,21);"><img src="http://iserve.ind.in/Godsview/icons/blue.png" />
                                <h2><button class="btn btn-default" role="button"> Arrived</button></h2>
                            </a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#" onclick="change_state(5,6);"><img src="http://iserve.ind.in/Godsview/icons/yellow.png" />
                                <h2> <button class="btn btn-default" role="button">Started</button></h2>
                            </a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#" onclick="change_state(3,0);" class="active1"><img src="http://iserve.ind.in/Godsview/icons/green.png" />
                                <h2><button class="btn btn-default " role="button"> Available</button></h2>
                            </a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#" onclick="change_state(0,0);"><img src="" style="opacity:0;" />
                                <h2> <button class="btn btn-default raised" role="button">All</button></h2>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- Collection of nav links and other content for toggling -->
                <div id="navbarCollapse" class="collapse navbar-collapse" style="margin-top: 3px;">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#" onclick="change_state(5,5);"><img src="http://iserve.ind.in/Godsview/icons/red.png" />
                                <h2><button class="btn btn-default" role="button">On The Way <span id="six"></span></button></h2>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="change_state(5,21);"><img src="http://iserve.ind.in/Godsview/icons/blue.png" />
                                <h2><button class="btn btn-default" role="button"> Arrived <span id="seven"></span></button></h2>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="change_state(5,6);"><img src="http://iserve.ind.in/Godsview/icons/yellow.png" />
                                <h2> <button class="btn btn-default" role="button">Started <span id="eight"></span></button></h2>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="change_state(3,0);" class="active1"><img src="http://iserve.ind.in/Godsview/icons/green.png" />
                                <h2><button class="btn btn-default " role="button"> Available <span id="four"></span></button></h2>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="change_state(0,0);"><img src="" style="opacity:0;" />
                                <h2> <button class="btn btn-default raised" role="button">All <span></span></button></h2>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- Modal -->
        <div class="modal left fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="margin-top: 191px;">
                <div class="modal-content">

                    <div class="modal-body" style="padding: 0px;">
                        <ul class="nav nav-tabs noborder aligncenter" role="tablist">
                            <li role="presentation" class="active widht60percent"><a href="#avilable" onclick="show_data(3);" aria-controls="available" role="tab" data-toggle="tab">AVAILABLE</a></li>
                            <li role="presentation" class="widht40percent"><a href="#busy" onclick="show_data(5);" aria-controls="busy" role="tab" data-toggle="tab" class="styletab2">BUSY</a></li>
                        </ul>
                        <table class="table table-hover search-table" id="drivertable">
                            <tbody class="driverlist"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog" role="document" style="height: auto;top: 15vh;">
                <div class="modal-content" style="border-bottom: 10px solid #006873;padding: 0px;">
                    <div class="modal-body" style="padding:0px;">
                        <div id="driver"></div>
                        <div id="jobid"></div>
                        <div id="slave"></div>
                        <div id="route"></div>
                    </div>

                </div>
            </div>
        </div>
        <!-- modal -->

        <div class="row restSpace" id="mainContent">
            <div class="col-sm-3 col-md-2 nopadding restSpace" id="drivercontainer">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs noborder aligncenter" role="tablist">
                    <li role="presentation" class="active widht60percent"><a href="#avilable" onclick="show_data(3);" aria-controls="available" role="tab" data-toggle="tab">AVAILABLE</a></li>
                    <li role="presentation" class="widht40percent"><a href="#busy" onclick="show_data(5);" aria-controls="busy" role="tab" data-toggle="tab" class="styletab2">BUSY</a></li>
                    <li role="presentation" style="text-align: center;width: 100%;border: 1px solid red;">
                                <a href="#location_issue" onclick="show_data(0);" aria-controls="location_issue" role="tab" data-toggle="tab" class="styletab2" aria-expanded="false">LOCATION ISSUE</a>
                            </li>
                </ul>
                <table class="table table-hover search-table" id="drivertableNewOne">
                    <tbody class="drivertableNewOne"></tbody>
                </table>
            </div>
            <div class="col-sm-9 col-md-10 nopadding restSpace">
                <input id="pac-input" class="controls" type="text" placeholder="Search Location" style="width: 15.5em;
    margin-left: 50px;
    z-index: 6;
    position: absolute;
    left: 0px;
    top: 0px;">
            <!--<input id="pac-input" class="controls" type="text" placeholder="Search Location" style="width: 15.5em;margin-left: 50px;">-->
                <div id='map'></div>
            </div>
        </div>
        <div style='position:fixed;bottom:0;height:3vh;width:100vw;background-color:#006873;'>
        </div>

        

    </body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
        <!--<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCHAAF-2jzmx-G06WCapwMZK0VG3pIC3yY&libraries=places,geometry"></script>-->
         
        <script src="http://cdn.pubnub.com/pubnub-3.15.1.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone-with-data.js"></script>
        
<script>
var baseurl = "http://iserve.ind.in/Godsview/";
var appurl = 'http://iserve.ind.in/';
var baseurl1 = 'http://iserve.ind.in/superadmin/index.php/Godsview';
var serverurl = 'http://iserve.ind.in:8080';
var scoketurl = 'http://iserve.ind.in:9999';
var allDriver = [];
var allMarkers = [];
var status = 0;
var appstatus = 0;
var tab_state = 0;
var timestamp = 0;
var citymarker, cityobj;
var cityobj = [];
var x_mapmarker = L.marker([34.052234, -118.243685]);
L.mapbox.accessToken = 'pk.eyJ1IjoiYXNoaXNoM2VtYmVkIiwiYSI6ImNpZzgxajNzejA4cDN2ZmtvbDNiN3R4ancifQ.jMkHGe2fgsgMnclPWrWElQ';//'sk.eyJ1IjoiYXNoaXNoM2VtYmVkIiwiYSI6ImNpd25jcXZyYTAwMTQyc210d2FqZGI5cHYifQ.ybjGWR_zGpVSc8mHGQbR4w';
    var map = L.mapbox.map('map', 'mapbox.streets')
        .setView([12.9716, 77.5946], 14);

    var markers = new L.MarkerClusterGroup();
    function calculateDistance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var radlon1 = Math.PI * lon1 / 180
    var radlon2 = Math.PI * lon2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") {
        dist = dist * 1.609344
    }
    if (unit == "N") {
        dist = dist * 0.8684
    }
    return dist
}
function shownearestDoctor() {
    
//        console.log(cityobj);
    for (var ind in allDriver) {
        flag = 1;
        var a = 0;
        var colorcode = "";
        var markericong = '';
        var val = allDriver[ind];
        var adata = allDriver[ind];
        var d = calculateDistance(val.lat, val.lng, cityobj[0].City_Lat, cityobj[0].City_Long, 'K');
        if (d < 150) {
            switch (val.status) {
                 case 3:
                        a = 3;
                        colorcode = "#78ac2c";
                        markericong = baseurl + 'icons/green.png';
                        break;
                    case 5:
                        switch (val.appstatus) {
                            case 5:
                                a = 5;
                                colorcode = '#be0411';
                                markericong = baseurl + 'icons/red.png';
                                break;
                            case 21:
                                a = 5;
                                colorcode = "#4d93c7";
                                markericong = baseurl + 'icons/blue.png';
                                break;
                            case 6:
                                a = 5;
                                colorcode = "#ffd50b";
                                markericong = baseurl + 'icons/yellow.png';
                                break;
                        }
                        break;
            }

             var html = '<td> \n\
                            <div onclick="get_driver_data(' + adata.user + ');popupmodel();">\n\
                                <p> \n\
                                    <span class="col-xs-height col-middle">\n\
                                        <span class="thumbnail-wrapper d32 circular bg-success"> \n\
                                            <img src="' + adata.proPic + '" onerror="this.src = \'' + appurl + 'pics/user.jpg\'">\n\
                                        </span>\n\
                                        <img width="12" height="12" class="position" src="' + markericong + '"/> \n\
                                    </span>\n\
                                </p>\n\
                                <p class="p-l-10 col-xs-height col-middle" style="width: 80%">\n\
                                    <span class="text-master" style="padding-right: 30px;">\n\
                                         ' + adata.name + '&nbsp;(ID:' + adata.user + ')<br/>' + adata.mobile + '<br/><b class="sec_D">Last Upd. : ' + (moment().unix() - adata.lastTs).toString() + ' sec.</b><font style="float:right;"></font>\n\
                                    </span>\n\
                                </p>\n\
                            </div>\n\
                        </td>';
                if(a != 0){
                    if (a == tab_state) {
                        if($('#D_' + adata.user).length == 0) {
                            $('#D_' + adata.user).remove();
                            $('.drivertableNewOne').append('<tr id="D_' + adata.user + '">' + html + '</tr>');
                        }else{
                            $('#D_' + adata.user).html(html);
                        }
                    } else {
                        $('#D_' + adata.user).remove();
                    }
                    if(adata.status == 4){
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {

                        } else {
                            markers.removeLayer(allMarkers['D_' + adata.user]);
                            delete allMarkers['D_' + adata.user];
                        }
                    } else if ((status == 0 && appstatus == 0) || (adata.status == status && appstatus == 0) || (adata.status == status && adata.appstatus == appstatus)) {
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {
                            allMarkers['D_' + adata.user] = L.marker(L.latLng(parseFloat(adata.lat), parseFloat(adata.lng)), {
                                icon: L.mapbox.marker.icon({
                                    'marker-color': colorcode
                                }), data: adata
                            })
                            .bindPopup('<b>'+ adata.name +'</b>')
                    markers.addLayer(allMarkers['D_' + adata.user]);
                            
                        } else {
                            markers.removeLayer(allMarkers['D_' + adata.user]);
                            allMarkers['D_' + adata.user] = L.marker(L.latLng(parseFloat(adata.lat), parseFloat(adata.lng)), {
                                icon: L.mapbox.marker.icon({
                                    'marker-color': colorcode
                                }), data: adata
                            })
                            .bindPopup('<b>'+ adata.name +'</b>')
                             markers.addLayer(allMarkers['D_' + adata.user]);
                        }
                    } else {
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {

                        } else {
                            markers.removeLayer(allMarkers['D_' + adata.user]);
                            delete allMarkers['D_' + adata.user];
                        }
                    }
                }else{
                    if($('#D_' + adata.user).length != 0) 
                        $('#D_' + adata.user).remove();
                    if (typeof (allMarkers['D_' + adata.user]) != 'undefined'){
                        markers.removeLayer(allMarkers['D_' + adata.user]);
                        delete allMarkers['D_' + adata.user];
                    }
                }
        } else {
            if ($('#D_' + adata.user).length != 0)
                $('#D_' + adata.user).remove();
            if (typeof (allMarkers['D_' + adata.user]) != 'undefined') {
                markers.removeLayer(allMarkers['D_' + adata.user]);
                delete allMarkers['D_' + adata.user];
            }
        }
    }
}
function update_status_no(){
    var status_3 = 0;
    var status_6 = 0;
    var status_7 = 0;
    var status_8 = 0;
    
    for (var ind in allDriver){
        var val = allDriver[ind];
        console.log(val);
        var adata = val;
        var d =0;
        if( cityobj[0])
            d = calculateDistance(adata.lat, adata.lng, cityobj[0].City_Lat, cityobj[0].City_Long, 'K');

        if (d < 150) {
//    $.each(allDriver, function (ind, val) {
        switch (val.status) {
            case 3:
                status_3++;
                break;
            case 5:
                switch (val.appstatus) {
                    case 5:
                        status_6++;
                        break;
                    case 21:
                        status_7++;
                        break;
                    case 6:
                        status_8++;
                        break;
                }
                break;
        }
    }
//    });
    }
    $("#four").html(status_3);
    $("#six").html(status_6);
    $("#seven").html(status_7);
    $("#eight").html(status_8);
}
function initMap() {

    var input = (document.getElementById('pac-input'));
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
        console.log(place);
        map.removeLayer(x_mapmarker);
        //map pointer in mapbox
        x_mapmarker = L.marker([place.geometry.location.lat(), place.geometry.location.lng()]).addTo(map);
        map.addLayer(x_mapmarker);
        map.setView([place.geometry.location.lat(), place.geometry.location.lng()], 13);



    });
}
function change_state(flg, act) {
    status = flg;
    appstatus = act;

    for (var key in allMarkers)
    {
        var val = allMarkers[key].options;
        if ((flg == 0 && act == 0) || (val.data.status == flg && act == 0) || (val.data.status == flg && val.data.appstatus == act)) {
            allMarkers[key].addTo(map);
        } else {
            markers.removeLayer(allMarkers[key]);
        }
    }

}
function get_driver_data(id) {
//    clearCircle();
    document.getElementById("driver").innerHTML = "";
    document.getElementById("jobid").innerHTML = "";
    document.getElementById("slave").innerHTML = "";
    document.getElementById("route").innerHTML = "";

    var ddata = '';
    for (var ind in allDriver){
        var val = allDriver[ind];
//    $.each(allDriver, function (ind, val) {
        if (val.user == id) {
            $("#driver").append('<h5 style="padding-left: 10px;color: white;">Driver Details</h5>');
            $("#driver").append('<h5><p style="margin-bottom: 0px;padding: 10px;">\
                                    Driver Name: &nbsp;' + val.name + '<br>Phone No: &nbsp;' + val.mobile + '\
                                </p></h5>');
            map.setView([parseFloat(val.lat), parseFloat(val.lng)], 18);
            allMarkers['D_' + val.user].openPopup();
            ddata = val;
        }
//    });
    }
    if (ddata.status == 5) {
        $.getJSON(baseurl1 + "/getApptDetails/" + id, function (response) {
            if (response.flg == 0) {
                var data = response.data;
                $("#jobid").append('<h5 style="padding-left: 10px;color: white;">Booking Deatils</h5>');
                
                var type = 'Card';
                if (data.payment_type == 2) {
                    type = 'Cash';
                }

                $('#jobid').append('<h5 style="padding-left: 10px;color: white;">Booking ID : ' + data._id + '</h5>');
                $("#jobid").append('<p>Booking Type : ' + type + '</p>')
                $("#jobid").append('<h5><p style="padding: 10px;">Customer Name: ' + data.slave_id.SlaveName + '<br>Phone No: ' + data.slave_id.SlavePone + '</p></h5>');
                $("#jobid").append('<hr><p style="margin-bottom: 0px;"><i class="fa fa-map-marker" style="color: green;font-size: 20px;"></i><font size="3" style="padding-left: 20px;">Pickup Address</font><br/>');
                $("#jobid").append('<span class="col-xs-height col-middle" style="padding-left: 30px;">' + data.appointment_address + '</p><hr>');
                $("#jobid").append('<hr><p style="margin-bottom: 0px;"><i class="fa fa-map-marker" style="color: red;font-size: 20px;"></i><font size="3" style="padding-left: 20px;">Drop Address</font><br/>');
                $("#jobid").append('<span class="col-xs-height col-middle" style="padding-left: 30px;">' + ((data.Drop_address == "")?"----------":data.Drop_address) + '</p><hr>');
//                }
            }
        });
    }

}
function update_data(updated) {
    if (typeof cityobj == 'undefined') {
        cityobj = [];
        cityobj[0].City_Lat = 12.971599;
        cityobj[0].City_Long = 77.594563;
    }
    $.each(updated, function (index, dr_data) {
        var flag = 0;
        var id = dr_data.id;
        
        for (var ind in allDriver){
            var val = allDriver[ind];
//        $.each(allDriver, function (ind, val) {
            if (val.id == id) {
                flag = 1;
                var data = dr_data;
                allDriver[ind].status = (typeof (data.status) != 'undefined') ? data.status : allDriver[ind].status;
                allDriver[ind].appstatus = (typeof (data.appstatus) != 'undefined') ? parseInt(data.appstatus) : allDriver[ind].appstatus;
                allDriver[ind].lat = (typeof (data.location) != 'undefined') ? parseFloat(data.location.latitude) : allDriver[ind].lat;
                allDriver[ind].lng = (typeof (data.location) != 'undefined') ? parseFloat(data.location.longitude) : allDriver[ind].lng;
                allDriver[ind].lastTs = (typeof (data.lastTs) != 'undefined') ? parseFloat(data.lastTs) : allDriver[ind].lastTs;
                var d =0;
                console.log("cityobj[0]",cityobj[0]);
                if(cityobj[0])
                    d = calculateDistance(allDriver[ind].lat, allDriver[ind].lng, cityobj[0].City_Lat, cityobj[0].City_Long, 'K');
                  
                if (d < 150) {
                var a = 0;
                var colorcode = "";
                var markericong = '';
                val = allDriver[ind];
//                console.log(val);

                switch (val.status) {
                    case 3:
                        a = 3;
                        colorcode = "#78ac2c";
                        markericong = baseurl + 'icons/green.png';
                        break;
                    case 5:
                        switch (val.appstatus) {
                            case 5:
                                a = 5;
                                colorcode = '#be0411';
                                markericong = baseurl + 'icons/red.png';
                                break;
                            case 21:
                                a = 5;
                                colorcode = "#4d93c7";
                                markericong = baseurl + 'icons/blue.png';
                                break;
                            case 6:
                                a = 5;
                                colorcode = "#ffd50b";
                                markericong = baseurl + 'icons/yellow.png';
                                break;
                        }
                        break;
                }
                var adata = allDriver[ind];
                var html = '<td> \n\
                            <div onclick="get_driver_data(' + adata.user + ');popupmodel();">\n\
                                <p> \n\
                                    <span class="col-xs-height col-middle">\n\
                                        <span class="thumbnail-wrapper d32 circular bg-success"> \n\
                                            <img src="' + adata.proPic + '" onerror="this.src = \'' + appurl + 'pics/user.jpg\'">\n\
                                        </span>\n\
                                        <img width="12" height="12" class="position" src="' + markericong + '"/> \n\
                                    </span>\n\
                                </p>\n\
                                <p class="p-l-10 col-xs-height col-middle" style="width: 80%">\n\
                                    <span class="text-master" style="padding-right: 30px;">\n\
                                         ' + adata.name + '&nbsp;(ID:' + adata.user + ')<br/>' + adata.mobile + '<br/><b class="sec_D">Last Upd. : ' + (moment().unix() - adata.lastTs).toString() + ' sec.</b><font style="float:right;"></font>\n\
                                    </span>\n\
                                </p>\n\
                            </div>\n\
                        </td>';
                if(a != 0){
                    if (a == tab_state) {
                        if($('#D_' + adata.user).length == 0) {
                            $('#D_' + adata.user).remove();
                            $('.drivertableNewOne').append('<tr id="D_' + adata.user + '">' + html + '</tr>');
                        }else{
                            $('#D_' + adata.user).html(html);
                        }
                    } else {
                        $('#D_' + adata.user).remove();
                    }
                    if(adata.status == 4){
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {

                        } else {
                            markers.removeLayer(allMarkers['D_' + adata.user]);
                            delete allMarkers['D_' + adata.user];
                        }
                    } else if ((status == 0 && appstatus == 0) || (adata.status == status && appstatus == 0) || (adata.status == status && adata.appstatus == appstatus)) {
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {
                            allMarkers['D_' + adata.user] = L.marker(L.latLng(parseFloat(adata.lat), parseFloat(adata.lng)), {
                                icon: L.mapbox.marker.icon({
                                    'marker-color': colorcode
                                }), data: adata
                            })
                            .bindPopup('<b>'+ adata.name +'</b>')
                            markers.addLayer(allMarkers['D_' + adata.user]);
                        } else {
                            markers.removeLayer(allMarkers['D_' + adata.user]);
                            allMarkers['D_' + adata.user] = L.marker(L.latLng(parseFloat(adata.lat), parseFloat(adata.lng)), {
                                icon: L.mapbox.marker.icon({
                                    'marker-color': colorcode
                                }), data: adata
                            })
                            .bindPopup('<b>'+ adata.name +'</b>')
                            markers.addLayer(allMarkers['D_' + adata.user]);
                        }
                    } else {
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {

                        } else {
                            markers.removeLayer(allMarkers['D_' + adata.user]);
                            delete allMarkers['D_' + adata.user];
                        }
                    }
                }else{
                    if($('#D_' + adata.user).length != 0) 
                        $('#D_' + adata.user).remove();
                    if (typeof (allMarkers['D_' + adata.user]) != 'undefined'){
                        markers.removeLayer(allMarkers['D_' + adata.user]);
                        delete allMarkers['D_' + adata.user];
                    }
                }
                } else {
                    flag = 1;
                    if ($('#D_' + adata.user).length != 0)
                        $('#D_' + adata.user).remove();
                    if (typeof (allMarkers['D_' + adata.user]) != 'undefined') {
                        markers.removeLayer(allMarkers['D_' + adata.user]);
                        delete allMarkers['D_' + adata.user];
                    }
                }
            }
//        });
        }
        if (flag == 0) {
            $.getJSON(baseurl1 + "/getSpecificDriver/" + id, function (response) {
                if (response.flg == 0) {
                    var val = response.data;
                    var adata = {
                        "id": val._id.$id,
                        "name": val.name + " " + val.lname,
                        "proPic":   val.image,
                        "bid": '',
                        "email": val.email,
                        "mobile": val.mobile,
                        "user": val.user,
                        "status": val.status,
                        "appstatus": val.apptStatus,
                        "lat": parseFloat(val.location.latitude),
                        "lng": parseFloat(val.location.longitude)
                    };
                    allDriver[val.user] = adata;
//                    allDriver.push(adata);
                    var a = 0;
                    var colorcode = "";
                    var markericong = '';
                    switch (adata.status) {
                        case 3:
                            a = 3;
                            colorcode = "#78AC2C";
                            markericong = baseurl + 'icons/green.png';
                            break;
                        case 5:
                            switch (adata.appstatus) {
                                case 5:
                                    a = 5;
                                    colorcode = "#BE0411";
                                    markericong = baseurl + 'icons/red.png';
                                    break;
                                case 21:
                                    a = 5;
                                    colorcode = "#4D93C7";
                                    markericong = baseurl + 'icons/blue.png';
                                    break;
                                case 6:
                                    a = 5;
                                    colorcode = "#FFD50B";
                                    markericong = baseurl + 'icons/yellow.png';
                                    break;
                            }
                            break;
                    }
//                    var adata = val;
                    var html = '<tr id="D_' + adata.user + '">\
                            <td> \n\
                                <div onclick="get_driver_data(' + adata.user + ');popupmodel();">\n\
                                    <p> \n\
                                        <span class="col-xs-height col-middle">\n\
                                            <span class="thumbnail-wrapper d32 circular bg-success"> \n\
                                                <img src="' + adata.proPic + '" onerror="this.src = \'' + appurl + 'pics/user.jpg\'">\n\
                                            </span>\n\
                                            <img width="12" height="12" class="position" src="' + markericong + '"/> \n\
                                        </span>\n\
                                    </p>\n\
                                    <p class="p-l-10 col-xs-height col-middle" style="width: 80%">\n\
                                        <span class="text-master" style="padding-right: 30px;">\n\
                                             ' + adata.name + '&nbsp;(ID:' + adata.user + ')<br/>' + adata.mobile + '<br/><b class="sec_D">Last Upd. : ' + (moment().unix() - adata.lastTs).toString() + ' sec.</b><font style="float:right;"></font>\n\
                                        </span>\n\
                                    </p>\n\
                                </div>\n\
                            </td>\n\
                        </tr>';
                    if(a != 0){
                        if (a == tab_state || tab_state == 0) {
                            $('#D_' + adata.user).remove();
                            $('.drivertableNewOne').append(html);
                        }
                        if ((status == 0 && appstatus == 0) || (adata.status == status && appstatus == 0) || (adata.status == status && adata.appstatus == appstatus)) {
                            allMarkers['D_' + adata.user] = L.marker([parseFloat(adata.lat), parseFloat(adata.lng)], {
                                icon: L.mapbox.marker.icon({
                                    'marker-color': colorcode
                                }), data: adata
                            })
                            .bindPopup('<b>'+ adata.name +'</b>')
                           markers.addLayer(allMarkers['D_' + adata.user]);
                        }
                    }
                }
            });
        }
    });
}
 //right side bar ,its displays driver and jobs details
function popupmodel() {
    $('#myModal2').modal('show');
}
    $(document).ready(function () {
     var socket = io(scoketurl);
    socket.on('godzviewChn', function (m) {
        
        if (m) {
            if (m.op == "u") {
                var id = m.o2._id;
                var data = m.o.$set;
                if (typeof data != "undefined") {
                    var updated = [];
                    //            $.each(data.all, function (ind, val) {
                    console.log(data);
                    var adata = {
                        "id": id,
                        "bid": '',
                        "status": data.status,
                        "appstatus": data.apptStatus,
                        "location": data.location,
                        "lastTs": data.lastUpdated,
                    };
                    updated.push(adata);
                    update_data(updated);
                    update_status_no();
                }
            }
        }
        
             
        });
    $.getJSON(baseurl1 + "/get_all_drivers", function (response) {
        if (response.flg == 0) {
            timestamp = response.time;
            var data = response.data;
            $("#four").html(data.four.length);
            $("#six").html(data.six.length);
            $("#seven").html(data.seven.length);
            $("#eight").html(data.eight.length);
            $.each(data.all, function (ind, val) {
                var adata = {
                    "id": val._id.$id,
                    "name": val.name ,
                    "proPic":   val.image,
                    "bid": '',
                    "email": val.email,
                    "mobile": val.mobile,
                    "user": val.user,
                    "booked": val.booked,
                    "status": val.status,
                    "lastTs": val.lastUpdated,
                    "appstatus":parseInt(val.apptStatus),
                    "lat": parseFloat(val.location.latitude),
                    "lng": parseFloat(val.location.longitude)
                };
                allDriver[val.user] = adata;
//                allDriver.push(adata);
            });
            show_data(3, 0);
        }
    });
    $.getJSON(baseurl1 + "/citydata", function (response) {
//        response = $(response).sort(sortCityName);
        citydata = response;
        $.each(citydata, function (ind, val) {
            $('#city').append('<option value="' + val.City_Id + '">' + camelize(val.City_Name) + '</option>');
        });
    });
   
    //based on click  adding css class to statusbar enroute,pickup,journey,all 
$(".btn").click(function () {
    $(".btn-default").removeClass("raised");
    $(this).addClass("raised");
});
$('#city').on('change', function () {
    $('.driverlist').html('');
    var value = $(this).val();
    console.log(value);
    if (value != '') {
        //based on city lat long focusing map to particular location        
        cityobj = $.grep(citydata, function (item) {
            if (item.City_Id == value) {
                return item;
            }
        });
        console.log(cityobj);
//        if (citymarker != undefined) {
//            citymarker.setMap(null);
//        }
        map.setView([parseFloat(cityobj[0].City_Lat), parseFloat(cityobj[0].City_Long)], 12);
        searchflag = 1;
        shownearestDoctor();
    } else {
        city = '';
        searchflag = 0;
    }
    update_status_no();
});

});
setInterval(function () {
    $('.sec_D').each(function(){
        var sec = $(this).html();
        var sec_arr = sec.split(" ");
        sec_arr[3] = parseInt(sec_arr[3]) + 1;
        
        if (sec_arr[3] >120  && tab_state == 3)
            $(this).closest('tr').remove();
        else {
            sec = sec_arr.join(" ");
            $(this).html(sec);
        }
    });
}, 1000);
function camelize(inStr) {
    return inStr.replace(/\w\S*/g, function (tStr) {
        return tStr.charAt(0).toUpperCase() + tStr.substr(1).toLowerCase();
    });
}
function show_data(flg, flg2 = 1) {
    var html = '';
    tab_state = flg;
    var bound = [];
    for (var ind in allDriver){
        var val = allDriver[ind];
//    $.each(allDriver, function (ind, val) {
        var a = 0;
        var colorcode = "";
        var markericong = '';
         
        switch (val.status) {
            case 3:
                if(val.booked == 0)
                    a = 3;
                    colorcode = "#78ac2c";
                    markericong = baseurl + 'icons/green.png';
                    break;
            case 5:
                switch (val.appstatus) {
                    case 5:
                        a = 5;
                        colorcode = '#be0411';
                        markericong = baseurl + 'icons/red.png';
                        break;
                    case 21:
                        a = 5;
                        colorcode = "#4d93c7";
                        markericong = baseurl + 'icons/blue.png';
                        break;
                    case 6:
                        a = 5;
                        colorcode = "#ffd50b";
                        markericong = baseurl + 'icons/yellow.png';
                        break;
                }
                break;
        }
         var adata = val;
        if(typeof cityobj == undefined || cityobj == "")
             var  d=0;
        else
            d = calculateDistance(adata.lat, adata.lng, cityobj[0].City_Lat, cityobj[0].City_Long, 'K');
        console.log("aaaa" + adata);
        if (d < 150) {
        if (a != 0) {
            var lastTs = (moment().unix() - adata.lastTs);
                if (a == flg || (flg == 0 && lastTs>120 && a == 3)) {
                html += '<tr id="D_' + adata.user + '">\
                            <td> \n\
                                <div onclick="get_driver_data(' + adata.user + ');popupmodel();">\n\
                                    <p> \n\
                                        <span class="col-xs-height col-middle">\n\
                                            <span class="thumbnail-wrapper d32 circular bg-success"> \n\
                                                <img src="' + adata.proPic + '" onerror="this.src = \'' + appurl + 'pics/user.jpg\'">\n\
                                            </span>\n\
                                            <img width="12" height="12" class="position" src="' + markericong + '"/> \n\
                                        </span>\n\
                                    </p>\n\
                                    <p class="p-l-10 col-xs-height col-middle" style="width: 80%">\n\
                                        <span class="text-master" style="padding-right: 30px;">\n\
                                             ' + adata.name + '&nbsp;(ID:' + adata.user + ')<br/>' + adata.mobile + '<br/><b class="sec_D">Last Upd. : ' + (lastTs).toString() + ' sec.</b><font style="float:right;"><font style="float:right;"></font>\n\
                                        </span>\n\
                                    </p>\n\
                                </div>\n\
                            </td>\n\
                        </tr>';
            }
            if (flg2 == 0) {
                allMarkers['D_' + adata.user] = new L.marker(L.latLng(parseFloat(adata.lat), parseFloat(adata.lng)), {
                    icon: L.mapbox.marker.icon({
                        'marker-color': colorcode
                    }), data: adata
                })
                .bindPopup('<b>'+ adata.name +'</b>')
                markers.addLayer(allMarkers['D_' + adata.user]);
            }
        }
    }
//    });
    }
    $(".drivertableNewOne").html(html);
    
     map.addLayer(markers);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJ1oXlqQZRC5VqUsqwk22f2Fu0PvlwXAI&libraries=places&callback=initMap"
async defer></script>
<script src="http://iserve.ind.in:9999/socket.io/socket.io.js"></script>
</body>
</html>