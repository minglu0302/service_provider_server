<?php
//echo date_default_timezone_get();
date_default_timezone_set('Asia/Kolkata');
//exit();
$this->load->library('mongo_db');
$db = $this->mongo_db->db;

$collection = $db->createCollection("mycol");


$locs = $db->selectCollection('masterLocations');
$slot = $db->selectCollection('slotbooking');
$getcollection = $slot->find(array("doc_id" => (int) $this->session->userdata('admin_session')['LoginId']));
$data = array();


$locsCur = $locs->find(array('user' => (int) $this->session->userdata('admin_session')['LoginId']));
$locations_array = array();
if ($locsCur->count() > 0) {
    foreach ($locsCur as $loc) {
        $locations_array[] = $loc;
    }
}
?>

<style>
    .form-group .control-label:after{
        content: '*';
        color: red;
    }

    .fc-center{
        color: black;
    }


    #quickview{
        display: none;
    }
    .globlenot{
        display: none;
    }
    .rejectbutn{
        display: none;
    }
    .pac-container {
        background-color: #FFF;
        z-index: 20;
        position: fixed;
        display: inline-block;
        float: left;
    }
    .datepicker{z-index:1151 !important;}
    /*.modal{*/
    /*z-index: 20;*/
    /*}*/
    /*.modal-backdrop{*/
    /*z-index: 10;*/
    /*}​*/
    .pac-container {
        /* put Google geocomplete list on top of Bootstrap modal */
        z-index: 9999;
    }

</style>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>-->

<!--<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>-->
<link rel='stylesheet' href='<?php echo base_url() ?>theme/lib/cupertino/jquery-ui.min.css' />
<link href='<?php echo base_url() ?>theme/calendercss/fullcalendar.css' rel='stylesheet' />
<link href='<?php echo base_url() ?>theme/calendercss/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?php echo base_url() ?>theme/lib/moment.min.js'></script>
<!--<script src='lib/jquery.min.js'></script>-->
<script src='<?php echo base_url() ?>theme/lib/fullcalendar.min.js'></script>

<script src='http://ubilabs.github.io/geocomplete/jquery.geocomplete.js'></script>



<style type="text/css">
    #cover{
        position:fixed; top:0; left:0; background:rgba(0,0,0,0.6); z-index:5; width:100%; height:100%; display:none; }
    #loginScreen:target, #loginScreen:target + #cover{ display:block; opacity:2; }
    .cancel { display:block; position:absolute; top:3px; right:2px; background:rgb(245,245,245); color:black; height:30px; width:35px; font-size:30px; text-decoration:none; text-align:center; font-weight:bold; }
</style>

<script>

    var componentForm = {
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    function initMap() {
        var input = document.getElementById('proaddress');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            console.log(place);
            for (var component in componentForm) {
                document.getElementById(component).value = '';
            }
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }


        });
    }
</script>
<script>
    function ChangeOption(ob)
    {
        var option = ob.value;
        var sdate = document.getElementById('sdate');
        if (option == 2)
        {
            $('.repeat').show();
            sdate.style.display = 'none';
        }
        if (option == 1)
        {
            $('.repeat').hide();
            sdate.style.display = 'block';
        }
    }

    $(function () {
    });

    function timezone() {
        var offset = new Date().getTimezoneOffset();
        return offset;
//        var minutes = Math.abs(offset);
////        var hours = Math.floor(minutes / 60);
//        var prefix = offset < 0 ? "+" : "-";
//        return prefix + minutes;
    }
    $(document).ready(function () {
     $('.datepicker-components').on('changeDate', function () {
            $(this).datepicker('hide');
        });
        var date = new Date();
        $('.datepicker-components').datepicker({
            startDate: date
        });
        var offset = timezone();

        $('.showredmark').removeClass('bubble');
        $('.showredmark').html('');


        $('.schedule').addClass('active open');


        $('.schedule .icon-thumbnail').addClass('bg-success');
//     $('#close_location_popup').click(function(){
//         $('#refresh').trigger('click');
//     });

//        alert('jw');
        $('#slots_window').click(function () {



            $.ajax({
                type: 'post',
                url: '<?php echo base_url('index.php/masteradmin/get_location_list') ?>',
                dataType: 'json',
                success: function (result) {
//alert(result.html);
                    $('#list_all_location').html(result.html);
//

                }, error: function () {
                    alert('error');
                }


            });
            $("#numslot").val("");
            $("#slotprice").val("");
            $("#duration").val("");
            $("#somedatepicker").val("");
            $('#modal_default_3').modal('show');


        });

        $(".pg-close,#cover").click(function () {

            $(".cancel")[0].click();
            $("#quickview").hide();
        });

        $('#map_view').click(function () {
            var latlong = $(this).attr('alt').split(',');
            initialize(latlong[0], latlong[1], 'show_direction');
            $('#model_for_map_view').modal('show');

        });
        $('#acancelapt').click(function (){
//           $("#newslotdetails").css('display', 'none ');
            $('#newslotdetails').modal('hide');
        })
//alert('uptyo');
        $('#calendar').fullCalendar({
            theme: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: Date(),
            editable: true,
            eventLimit: true, // allow "more" link when too many events

            eventRender: function (event, element) {

                $(this).css('float', 'left');
                $(this).css('margin-top', '6px');
                element.append('<div style="margin-left:85%;margin-top: -16px"><button class="btn btn-danger btn-xs" id="devent">X</button></div>');
                element.find("#devent").click(function () {

//                        alert(event.slot);
                    $('#calendar').fullCalendar('removeEvents', event._id);

                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url('index.php/masteradmin/delete_schedule') ?>',
                        data: {event: 2, val: event.mongoid},
                        dataType: 'json',
                        success: function (result) {
                            if (result.val)
                                alert('Deleted successfully');
                            else
                                alert('Not Deleted');

                        }


                    });




                });
            },
            events: function (start, end, timezone, callback) {
//            var offset = new Date().getTimezoneOffset();
                $.ajax({
                    url: 'GetEvents',
                    dataType: 'json',
                    data: {offset: offset},
                    success: function (data) {
                        callback(data.html);
                    }
                });
            },
            eventClick: function (calEvent, jsEvent, view) {
                if (calEvent.slot == 1) {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url('index.php/masteradmin/get_new_slot_details') ?>',
                        data: {offset: offset, slot_id: calEvent.mongoid},
                        dataType: 'json',
                        success: function (result) {
                            result = result.slot_data;
                            $('#slot_start').html(result.start_dt);
                            $('#slot_end').html(result.end_dt);
                            $("#slot_rad").html(result.radius);
                            $("#slot_loc_name").html(result.locName);
                            $('#slot_location').html(result.address);
                            $('#slot_price').html(result.slot_price);
                            $('#newslotdetails').modal('show');
                            $(".custohide").css('display', 'none ');
                            $('#acancelapt').hide();
                        }
                    });
                } else if (calEvent.slot == 3) {
                        $.ajax({
                        type: 'post',
                        url: '<?php echo base_url('index.php/masteradmin/get_new_slot_details') ?>',
                        data: {offset: offset, slot_id: calEvent.mongoid},
                        dataType: 'json',
                        success: function (result) {
                            var bdata = result.bdata;
                            result = result.slot_data;
                            $('#slot_start').html(result.start_dt);
                            $('#slot_end').html(result.end_dt);
                            $("#slot_rad").html(result.radius);
                            $("#slot_loc_name").html(result.locName);
                            $('#slot_location').html(result.address);
                            $('#slot_price').html(result.slot_price);
                            $('#custName').html(bdata.customer.fname +' '+ bdata.customer.lname);
                            $('#newslotdetails').modal('show');
                            $('#acancelapt').show();
                        }
                    });
//                    $.ajax({
//                        type: 'post',
//                        url: '<?php echo base_url('index.php/masteradmin/get_schedule_details') ?>',
//                        data: {offset: offset, slot_id: calEvent.mongoid},
//                        dataType: 'json',
//                        success: function (result) {
//                            $('#patientname').html(result.pname);
//                            $('#phone').html(result.phone);
//                            if (result.profile_pic)
//                                $('#patientimage').attr('src', '<?php echo pics_url; ?>' + result.profile_pic);
//                            else
//                                $('#patientimage').attr('src', 'http://iserve.ind.in/pics/aa_default_profile_pic.jpg');
//
//                            $('#datentime').html(result.datetime);
////                           $('#datentime').html(date('d-M-Y h : i a', strtotime(result.datetime)));
//
//                            $('#notes').html(result.notes);
//
//                            $('#apploc').html(result.applocation);
//
//                            $("#quickview").css('display', 'block ');
//                            $("#quickview").show();
//
//                            if (result.status == 1) {
//                                $("#acceptapt").show();
//                                $('#cancelapt').hide();
//                                $('#rejectbutn').show();
//                            } else if (result.status == 2) {
//                                $("#acceptapt").hide();
//                                $('#cancelapt').show();
//                                $('#rejectbutn').hide();
//
//                            } else if (result.status == 8) {
////                                $("#acceptapt").hide();
//                                $('#cancelapt').hide();
//                                $('#rejectbutn').hide();
//                                $("#acceptapt").html('This appointment is Expired ! ');
//                                $("#acceptapt").show();
//
//                            } else {
//                                $("#acceptapt").show();
//                                $('#cancelapt').show();
//                                $('#rejectbutn').hide();
//
//                            }
//                            $('#map_view').attr('alt', calEvent.lat + ',' + calEvent.long);
//                            $('#email').val(result.email);
//                            $('#ent_appnt_dt').val(result.appdate);
//                            $('#ent_slot_id').val(calEvent.mongoid);
//                            $('#ent_pat_email').val(result.email);
//                            $(".abt")[0].click();
//                        }
//
//
//                    });
                }

            }
        });

        $('#add_slot').click(function () {
            var option = $('input[name="nonre"]:checked').val();
            if ($('#numslot').val() == '') {
                $('#error_message1').text('Please Enter NUmber Of Slot !');
                return false;
            } else if ($('#duraion').val() == '') {
                $('#error_message1').text('Please Enter Duration !');
                return false
            } else if ($('#slotprice').val() == '') {
                $('#error_message1').text('Please Enter Price !');
                return false;
            } else if ($("select[name='location']").val() == '') {
                $('#error_message1').text('Select Location !');
                return false;
            } else if ($('#start_time').val() == '') {
                $('#error_message1').text('Select Start Time !');
                return false;
            } else if (option == '') {
                $('#error_message1').text('Select Option!');
                return false;
            } else {
                if (option == 1) {
                    var date = new Date($('#somedatepicker').val());
                    var datetosend = serialize(date);
                    var currentTime = new Date().getTime();
                    var selectedStartTime = toTimestamp(datetosend + ' ' + $('#start_time').val() + ':00:00 ' + $('#start_time_am_pm').val());
                    $('#error_message1').text('');
                    if (datetosend == '') {
                        $('#error_message1').text('Fields with * are mandatory');
                    } else if (currentTime > selectedStartTime) {
                        $('#error_message1').text('Please select date time correctly');
                    } else {
                         $('#add_slot').prop('disabled', true);
                        $.ajax({
                            type: "POST",
                            dataType: "JSON",
                            url: "addslot",
                            data: {
                                location: $("select[name='location']").val(),
                                start_time: $('#start_time').val(),
                                start_time_am_pm: $('#start_time_am_pm').val(),
                                dates: datetosend,
                                ent_radius: $('#Radius').val(),
                                ent_num_slot: $('#numslot').val(),
                                ent_option: option,
                                ent_duration: $('#duration').val(),
                                ent_start_time_am_pm: $('#start_time_am_pm').val(),
                                ent_price: $('#slotprice').val()},
                            success: function (result) {
                                $('#add_slot').prop('disabled', false);
                                if (result.errFlag == '0') {
                                    $('#close_location_popup').trigger('click');
                                    $('#close_slots').trigger('click');
                                    alert(result.errMsg);
                                    $('#calendar').fullCalendar('refetchEvents');
                                } else {
                                    $('#error_message1').text(result.errMsg);
                                }
                            }
                        });
                    }
                } else if (option == 2) {
                    var dd = $('#repeatday').val();
                    var sdate = new Date($('#startdatepicker').val());
                    var edate = new Date($('#enddatepicker').val());
                    var start_date = serialize(sdate);
                    var end_date = serialize(edate);
                    var currentTime = new Date().getTime();
                    var selectedStartTime = toTimestamp(start_date + ' ' + $('#start_time').val() + ':00:00 ' + $('#start_time_am_pm').val());
//                    var selectedEndTime = toTimestamp(start_date + ' ' + $('#end_time').val() + ':00:00 ' + $('#end_time_am_pm').val());
                    $('#error_message1').text('');
                    if (start_date == '' || $('#start_time').val() == '' || $('#end_time').val() == '') {
                        $('#error_message1').text('Fields with * are mandatory');
                    } else if (currentTime > selectedStartTime) {
                        $('#error_message1').text('Please select date time correctly');
                    } else if (sdate >= edate) {
                        $('#error_message1').text('please make sure start date and end date are correct !');
                    } else if (dd == 0) {
                        $('#error_message1').text('please select Day');
                    } else {
                        $.ajax({
                            type: "POST",
                            dataType: "JSON",
                            url: "addslot",
                            data: {
                                location: $("select[name='location']").val(),
                                start_time: $('#start_time').val(),
                                start_time_am_pm: $('#start_time_am_pm').val(),
                                dates: start_date,
                                ent_radius: $('#Radius').val(),
                                ent_num_slot: $('#numslot').val(),
                                ent_option: option,
                                ent_duration: $('#duration').val(),
                                ent_price: $('#slotprice').val(),
                                ent_repeatday: $('#repeatday').val(),
                                ent_start_time_am_pm: $('#start_time_am_pm').val(),
                                ent_end_date: end_date
                            },
                            success: function (result) {
                                if (result.errFlag == '0') {
                                    $('#close_location_popup').trigger('click');
                                    $('#close_slots').trigger('click');
                                    alert(result.errMsg);
                                    $('#calendar').fullCalendar('refetchEvents');
                                } else {
                                    $('#error_message1').text(result.errMsg);
                                }
                            }
                        });
                    }
                }
            }
        });
        $('#Remove_slots').click(function () {
            $('#modal_default_4').modal('show');

        });

        $('#removeSlot').click(function () {


            var date = new Date($('#r_somedatepicker').val());
            var datetosend = serialize(date);


            var currentTime = new Date().getTime();

            var selectedStartTime = toTimestamp(datetosend + ' ' + $('#r_start_time').val() + ':00:00 ' + $('#r_start_time_am_pm').val());
            var selectedEndTime = toTimestamp(datetosend + ' ' + $('#r_end_time').val() + ':00:00 ' + $('#r_end_time_am_pm').val());



            $('#error_message2').text('');
//            alert('locationl'+$("select[name='location']").val() + 'st time'+$('#start_time').val()+"end time"+$('#end_time').val());
            if ($('#r_start_time').val() == '' || $('#r_end_time').val() == '') {
                $('#error_message2').text('Fields with * are mandatory');
            } else if (currentTime > selectedStartTime || selectedStartTime >= selectedEndTime) {
                $('#error_message2').text('Please select date time correctly');
            } else {
                $.ajax({
                    type: "POST",
                    url: "removeSlots",
                    data: {
                        offset: offset,
                        start_time: $('#r_start_time').val(),
                        end_time: $('#r_end_time').val(),
                        start_time_am_pm: $('#r_start_time_am_pm').val(),
                        end_time_am_pm: $('#r_end_time_am_pm').val(),
                        dates: datetosend},
                    dataType: "JSON",
                    success: function (result) {

                        if (result.f == '0') {
                            $('#close_Remove_slots').trigger('click');
                            alert(result.error);
                            $('#calendar').fullCalendar('refetchEvents');
//                            if (confirm(result.error))
//                                $('#refresh').trigger('click');
//                            $('#refresh').trigger('click');

                        } else {
                            $('#error_message2').text('Please check date Once.');
                        }
                    }
                });
            }

        });
//                            < !--<?php
//$data['pagename'] = "master/schedule";
?>//////

$('.hideContent').hide();
         $('#location_show').click(function(){
             $('.hideContent').toggle();
         })
        $('#add_location').click(function () {
//                alert('called');
            $('#error_message').text('');
            if ($('#location_name').val() == '' || $('#location_addr1').val() == '' || $('#location_addr2').val() == '' || $('#loc_lat').val() == '' || $('#loc_lat').val() == '') {
                $('#error_message').text('Fields with * are mandatory');
                alert('Fields with * are mandatory');
            } else {
                var dataString = $('#location_form').serialize();
//                        alert('yahatak call');
                $.ajax({
                    type: "POST",
                    url: "add_location",
                    data: dataString,
                    dataType: "JSON",
                    success: function (result) {
//                        $('#close_location_popup').trigger('click');

                        $('#locations_manage').html(result.table);
                        $('#locations_select').html(result.options);
                        $('#overlay_div').toggle();
//                                alert('added');
                        alert(result.message);
                    }
                });
                //                $('#location_form').submit();
            }
        });
        function toTimestamp(strDate) {
            var datum = Date.parse(strDate);
            return datum;
        }
        $('#model_2').click(function () {
            plotmap(34.069858, -118.395195);
        });
        $('.mandatory').blur(function () {
            $('#error_message').text('');
        });
        $('.delete_location').live('click', function () {
            var dats = $(this).attr('id');
//            alert(dats);
            if (confirm('Are you confirm to remove this location ?')) {
                $.ajax({
                    type: "POST",
                    url: "removelocation",
                    data: {item_list: dats},
                    dataType: "JSON",
                    success: function (result) {
                        if (result.flag == 0) {
//                            $('#error_message').text(result.message);
                            $('#location-' + dats).remove();
                        } else {
                            $('#error_message').text('Location deletion failed');
                        }
                    }
                });
            }
        });

    });</script>
<script type="text/javascript">
    function serialize(date) {
        return '' + date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    }

    function call_service() {

        $.ajax({
            type: 'POST',
            url: '<?php echo SERVICE_URL ?>abortAppointment',
            data: $('#call_form').serialize(),
            success: function (result) {
                if (result.errNum == 42) {

                    alert(result.errMsg);
                    $('#calendar').fullCalendar('refetchEvents');
                    $('#reson_box').modal('hide');
                }
            }
        });
    }



    function done_booking() {
        $.ajax({
            type: 'POST',
            url: '<?php echo SERVICE_URL ?>abortAppointment',
            data: $('#call_form').serialize(),
            dataType: "json",
            success: function (result) {

                if (result.errNum == '40') {
                    alert(result.errMsg);
//                    alert('Appoitnment is accepted successfully');
                    $('#calendar').fullCalendar('refetchEvents');
                    $('#cover').trigger('click');
                } else {
                    alert('Appoitnment is Not Updated.');
                }

            }
        });
    }
</script>

<script>
    function initialize(lat, long, divid) {
        var map;
        var marker;
        var myLatlng = new google.maps.LatLng(20.268455824834792, 85.84099235520011);
        var geocoder = new google.maps.Geocoder();
        var infowindow = new google.maps.InfoWindow();
        var myLatlng = '';
        if (lat != '' || long != '')
        {
            myLatlng = new google.maps.LatLng(lat, long);
        } else {
            myLatlng = new google.maps.LatLng(20.268455824834792, 85.84099235520011);
        }


        var mapOptions = {
            zoom: 18,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById(divid), mapOptions);
        marker = new google.maps.Marker({
            map: map,
            position: myLatlng,
            draggable: true
        });
        google.maps.event.addListenerOnce(map, 'idle', function () {
            google.maps.event.trigger(map, 'resize');
        });
    }
    function callMap() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }


        function showPosition(position) {
//            x.innerHTML = "Latitude: " + position.coords.latitude +
//            "<br>Longitude: " + position.coords.longitude;
            initialize(position.coords.latitude, position.coords.longitude, 'myMap1');
        }
        $("#location_addr1").geocomplete({
            map: "#myMap",
            details: "form",
            detailsAttribute: "data-geo",
            types: ["geocode", "establishment"],
            markerOptions: {
                draggable: true
            }
        });



        $("#location_addr1").geocomplete()
                .bind("geocode:result", function (event, result) {
//                $.log("Result: " + result.formatted_address);
                    $('#location_addr1').val(result.formatted_address);
//                alert('address 1' + result.formatted_address);
//                alert('address 1' + result.lat);
                })
                .bind("geocode:error", function (event, status) {
                    $.log("ERROR: " + status);
                })
                .bind("geocode:multiple", function (event, results) {
                    $.log("Multiple: " + results.length + " results found");
                })
                .bind("geocode:dragged", function (event, latLng) {
                    $("input[name=loc_lat]").val(latLng.lat());
                    $("input[name=loc_long]").val(latLng.lng());
                });

        google.maps.event.trigger($('#geoComplete').geocomplete('map'), 'resize');

        //        }
    }

  var prevdate = '';
    function go_to_date() {

        if ($('#datetogo').val() != ''){
            $( 'td[ data-date='+prevdate+']' ).removeClass( 'ui-state-highlight' );
            $('#calendar').fullCalendar('gotoDate', $('#datetogo').val());
            var mydate = $('#datetogo').val();
            var x = mydate.split('/');
            var y = x[2]+'-'+x[0]+'-'+x[1];
            $( 'td[ data-date='+y+']' ).addClass( 'ui-state-highlight' );
            prevdate = y;
        }
    }
     
</script>




<div>
    <div id="notificationpop" class="pgn-wrapper" data-position="top-right" style="top: 3px;">
    </div>
    <div id="notificationsound"></div>
</div>

<div id="loginScreen" style="margin-top: 73px;">
    <a href="#" class="cancel">&times;</a>

    <div id="quickview" class="quickview-wrapper open" data-pages="quickview" style="max-height: 500px;
         margin-top: 79px;
         right: 181px;
         width: 331px;border: 5px solid #cccccc;
         border-radius: 10px;">

        <ul class="nav nav-tabs" style="padding: 0 14px;z-index: 50;">
            <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                <span class="col-xs-height col-middle">
                    <span class="thumbnail-wrapper d32 circular bg-success">
                        <img width="34" height="34" id="patientimage" src="" class="col-top">
                    </span>
                </span>
                <p class="p-l-20 col-xs-height col-middle col-xs-12">
                    <span class="text-master" style="color: #ffffff !important;" id="patientname"></span>
                    <span class="block text-master hint-text fs-12" style="color: #ffffff !important;" id="phone"></span>
                </p>
            </a>

            <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close cancel" ></i></a>
        </ul>


        <div class="tab-content" style="top: 21px !important;">
            <div class="list-view-group-container" style="overflow: auto;">
                <ul>
                    <li class="chat-user-list clearfix">
                        <div class="form-control">
                            <label class="col-sm-5 control-label">DATE</label>
                            <label class="col-sm-7 control-label" id="datentime"></label>
                        </div>
                    </li>
                </ul>
                <div class="list-view-group-container">
                    <div class="list-view-group-header text-uppercase" style="background-color: #f0f0f0;padding: 10px;">
                        APPOINTMENT LOCATION <div class="pull-right" style="color: green;cursor: pointer" id="map_view" alt=""> Map View</div></div>
                    <div style="height: auto;overflow: auto;background: #fff;">
                        <ul style="margin-top: 15px;">
                            <li class="chat-user-list clearfix">
                                <div class="item share share-self col1" data-social="item" style="border: 2px solid #e5e8e9;">
                                    <div class="item-description" style="">
                                        <ul>
                                            <li class="chat-user-list clearfix">
                                                <div class="" style="border: 1px solid rgba(0, 0, 0, 0.07);">
                                                    <p style="padding: 8px;" id="apploc"></p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="list-view-group-container">
                    <div class="list-view-group-header text-uppercase" style="background-color: #f0f0f0;padding: 10px;">
                        NOTES </div>
                    <div style="height: auto;overflow: auto;background: #fff;">
                        <ul style="margin-top: 15px;">
                            <li class="chat-user-list clearfix">
                                <div class="item share share-self col1" data-social="item" style="border: 2px solid #e5e8e9;">
                                    <div class="item-description" style="">
                                        <ul>
                                            <li class="chat-user-list clearfix">
                                                <div class="" style="border: 1px solid rgba(0, 0, 0, 0.07);">
                                                    <p style="padding: 8px;" id="notes"></p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div></div>
                <div class="col-md-12" style="margin-top: 10px;">
                    <div class="col-md-12" id="acceptapt">
                        <button class="btn btn-success btn-cons m-b-10 m-t-20 m-l-30" type="button" onclick="done_booking()">
                            <span class="bold">Accept Appointment</span>
                        </button>
                    </div>
                    <div class="col-md-12" id="cancelapt" >
                        <button class="btn btn-danger btn-cons m-t-10  m-l-30"  type="button" onclick="Show_Reson_box(1)">
                            <span class="bold">Cancel Appointment </span>
                        </button>
                    </div>
                    <div class="col-md-12" id="rejectbutn" >
                        <button class="btn btn-danger btn-cons m-t-10  m-l-30"  type="button" onclick="Show_Reson_box(2)">
                            <button class="btn btn-danger btn-cons m-t-10  m-l-30"  type="button" onclick="done_booking()">
                                <span class="bold">Reject Appointment11 </span>
                            </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div id="cover" > </div>
<a href="#loginScreen" class="button abt"></a>
<div class="page-content-wrapper">


    <!-- START PAGE CONTENT -->
    <div class="content" style="padding-top: 0px;">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->

                    <div class="col-md-12 breadcrumb" style="margin-top: 1%;    margin-left: -1.4%;">
                        <div class="col-md-4" style="margin-top: -15px;">
                            <div class="">
                                <h3>
                                    <a href="#modal_default_2" data-toggle="modal" style='color: black;' id="model_2">
                                        <span class="icon-plus-sign">
                                            <button type="button" class="btn btn-theme" onclick="callMap();">Manage Location</button>
                                    </a>
                                    <button type="button" id="slots_window" class="btn btn-theme02">Add Slot</button>
                                    <button type="button" id="Remove_slots" class="btn btn-theme03">Remove Slot</button>
                                </h3>

                            </div>



                        </div>
                        <div class="col-md-8" >
                            <div id="datepicker-component" class="input-group date col-md-5 pull-left">
                                <span id="showerror"> </span>
                                <input type="text" class="form-control" id="datetogo" placeholder="Go To Date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                            </div>

                            <div  class="input-group date col-md-7 pull-right">
                                <button class="btn btn-primary btn-cons" onclick="go_to_date()" style="margin-left: 6px;
                                        min-width: 85px;">Go</button>
                                <div class="pull-right">
                                    <!--<span class="badge" style="background: #64FE2E">New</span>-->
                                    <span class="badge badge-success">FREE</span>
                                    <span class="badge btn-primary" style="background: #6d5cae;color: white">ACCEPTED</span>
                                    <span class="badge badge-important">COMPLETED</span>

                                </div>

                            </div>

                        <!--                        <span class="badge badge-warning">8</span>-->
                        </div>

                    </div>


                    <!-- END BREADCRUMB -->
                </div>

                <a href="refreshshedule" ><button id="refresh" style="display: none"></button></a>
                <div class="panel panel-transparent">

                    <div class="panel-body">


                        <div id="calendar" ></div>




                    </div>
                </div>




            </div>


        </div>
        <!-- END JUMBOTRON -->

        <div class="modal fade stick-up in" id="modal_default_3" tabindex="-1" role="dialog" aria-hidden="false" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                        <h5>Add slots <span class="semi-bold">Information</span></h5>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="form-group" style="padding: 14px;">
                                <label for="option" class="col-sm-5 control-label">Option</label>
                                <div class="col-sm-7" id="list_all_option">
                                    <input onchange="ChangeOption(this)" type="radio" name="nonre" value="1" checked="checked"/>Non Repeat
                                    &nbsp; &nbsp; &nbsp; &nbsp;                                    
                                    <input onchange="ChangeOption(this)" type="radio" name="nonre" value="2"/>Repeat
                                </div>
                            </div>
                            <div class="form-group" style="padding: 14px;">
                                <label for="fname" class="col-sm-5 control-label">Select locations</label>
                                <div class="col-sm-7" id="list_all_location">
                                </div>
                            </div>
                              <br>
                            <div class="form-group" style="padding: 14px;">
                                <label for="fname" class="col-sm-5 control-label">Radius &nbsp;(<?php echo "KM"?>)</label>
                                <div class="col-sm-7">
                                    <select  name='Radius' id="Radius" class="form-control">
                                        <?php
                                        for ($i = 1; $i <= 50; $i++) {
                                            echo "<option value='" . $i . "'>" . $i . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div id="sdate" class="form-group" style="padding: 14px;">
                                <label for="fname" class="col-sm-5 control-label">Date</label>
                                <div class="col-sm-7">
                                    <input type="" class="form-control datepicker-components" id="somedatepicker"  autocomplete="off">
<!--                                    <div id="datepicker-component" class="input-group date">
                                        <input type="text" id='somedatepicker' class="form-control"><span class="input-group-addon "><i class="fa fa-calendar"></i></span>
                                         
                                    </div>-->
                                </div>
                            </div>
                            <br>
                            <div class="form-group" style="padding: 14px;">
                                <label for="duration" class="col-sm-5 control-label">Slots Duration</label>
                                <div class="col-sm-7">
                                    <!--<input type="text" id='duration' class="form-control" >-->
                                     <select id="duration" class="form-control">
                                            <option value>Select Duration</option>
                                            <option value="30">30 Mins</option>
                                            <option value="60">60 Mins</option>     
                                            <option value="90">90 Mins</option>     
                                        </select>
                                </div>
                            </div><br>
                            <div class="form-group" style="padding: 14px;">
                                <label for="numslot" class="col-sm-5 control-label">Number Of Slot</label>
                                <div class="col-sm-7">
                                    <input type="number" id='numslot' class="form-control" >
                                </div>
                            </div>
                              
                            
                            <!--Start Repeat Funtionality-->
                            <div class="form-group repeat" style="padding: 14px; display: none ">
                                <br>
                                <label for="fname" class="col-sm-5 control-label">Select Days</label>
                                <div class="col-sm-7">
                                    <select id="repeatday" name='repeatday' id="every" class="form-control">
                                        <option value="0">Select Days</option> 
                                        <option value="1">Every Day</option>
                                        <option value="2">Week Day</option>
                                        <option value="3">Week end</option>                                        
                                    </select>
                                </div>
                            </div>
                                 
                            <div class="form-group repeat" style="padding: 14px; display: none">
                                <br>
                                <label for="startdatepicker" class="col-sm-5 control-label">Start Date</label>
                                <div class="col-sm-7">
                                    <input type="" class="form-control datepicker-components" id="startdatepicker"  autocomplete="off">
<!--                                    <div id="datepicker-component" class="input-group date">
                                        <input type="text" id='startdatepicker' class="form-control"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>-->
                                </div>
                            </div>
                                <br>
                            <div class="form-group repeat" style="padding: 14px; display: none">
                                <br>
                                <label for="enddatepicker" class="col-sm-5 control-label">End Date</label>
                                
                                <div class="col-sm-7">
                                    <input type="" class="form-control datepicker-components" id="enddatepicker"  autocomplete="off">
<!--                                    <div id="datepicker-component" class="input-group date">
                                        <input type="text" id='enddatepicker' class="form-control"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>-->
                                </div>
                            </div>
                                <br>
                            <!--End Repeat Functinality-->
                            <div class="form-group" style="padding: 14px;">
                                <label for="fname" class="col-sm-5 control-label">Slots Price [<?php echo CURRENCY; ?>]</label>
                                <div class="col-sm-7">
                                    <input type="number" id='slotprice' class="form-control" >
                                </div>
                            </div>
                            <br>

                            <div class="form-group repeat" style="padding: 14px;margin-top: 20px;">

                                <div class="row" style="    margin-left: 15px;">
                                    <!--<div class="col-sm-5">-->
                                    <label for="starttime" class="col-sm-5 control-label">Start Time</label>                                        
                                    <!--</div>-->
                                    <div class="col-sm-7">
                                        <div class="col-md-12">
                                            <!--<input  type="hidden" class="timepicker form-control" value="<?php echo date('h:s', time()); ?>" name="start_time" id="start_time" style="margin-bottom: 16px;"/>-->
                                            <div class="col-sm-8" style='float:left;'>
                                                <select style='width:90%;' name='start_time' id="start_time" class="form-control">
                                                    <option value='01'>01</option>
                                                    <option value='02'>02</option>
                                                    <option value='03'>03</option>
                                                    <option value='04'>04</option>
                                                    <option value='05'>05</option>
                                                    <option value='06'>06</option>
                                                    <option value='07'>07</option>
                                                    <option value='08'>08</option>
                                                    <option value='09'>09</option>
                                                    <option value='10'>10</option>
                                                    <option value='11'>11</option>
                                                    <option value='12'>12</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4" style='float:left;'>
                                                <select  name='start_time_am_pm' id="start_time_am_pm" class="form-control">
                                                    <option value='am'>am</option>
                                                    <option value='pm'>pm</option>
                                                </select>
                                            </div>
                                            <div style='clear:both;'></div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 m-t-10 sm-m-t-10">
                                    <span id="error_message1" style="color:red;"></span><br/>
                                    <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id="close_slots">Close</button>
                                    <button type="button" class="btn btn-success btn-clean" id="add_slot">Add slots</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>





        <div class="modal fade stick-up in" id="modal_default_4" tabindex="-1" role="dialog" aria-hidden="false" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                        <h5>Remove slots </h5>

                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="form-group" style="padding: 14px;">
                                <label for="fname" class="col-sm-5 control-label">Date</label>
                                <div class="col-sm-7">
                                    <div id="datepicker-component" class="input-group date">
                                        <input type="text" id='r_somedatepicker' class="form-control"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group" style="padding: 14px;margin-top: 20px;">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="col-md-12">
                                            <!--<input  type="hidden" class="timepicker form-control" value="<?php echo date('h:s', time()); ?>" name="start_time" id="start_time" style="margin-bottom: 16px;"/>-->
                                            <div class="col-sm-8" style='float:left;'>
                                                <select style='width:90%;' name='r_start_time' id="r_start_time" class="form-control">
                                                    <option value='01'>01</option>
                                                    <option value='02'>02</option>
                                                    <option value='03'>03</option>
                                                    <option value='04'>04</option>
                                                    <option value='05'>05</option>
                                                    <option value='06'>06</option>
                                                    <option value='07'>07</option>
                                                    <option value='08'>08</option>
                                                    <option value='09'>09</option>
                                                    <option value='10'>10</option>
                                                    <option value='11'>11</option>
                                                    <option value='12'>12</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4" style='float:left;'>
                                                <select  name='r_start_time_am_pm' id="r_start_time_am_pm" class="form-control">
                                                    <option value='am'>am</option>
                                                    <option value='pm'>pm</option>
                                                </select>
                                            </div>
                                            <div style='clear:both;'></div>
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-md-12">
                                            <!--<input  type="hidden" class="timepicker form-control" value="<?php echo date('h:s', time()); ?>" name="end_time" id="end_time" style="margin-bottom: 16px;"/>-->
                                            <div class="col-sm-8" style='float:left;'>
                                                <select style='width:90%;' name='r_end_time' id="r_end_time" class="form-control">
                                                    <option value='01'>01</option>
                                                    <option value='02'>02</option>
                                                    <option value='03'>03</option>
                                                    <option value='04'>04</option>
                                                    <option value='05'>05</option>
                                                    <option value='06'>06</option>
                                                    <option value='07'>07</option>
                                                    <option value='08'>08</option>
                                                    <option value='09'>09</option>
                                                    <option value='10'>10</option>
                                                    <option value='11'>11</option>
                                                    <option value='12'>12</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4" style='float:left;'>
                                                <select  name='r_end_time_am_pm' id="r_end_time_am_pm" class="form-control">
                                                    <option value='am'>am</option>
                                                    <option value='pm' selected>pm</option>
                                                </select>
                                            </div>
                                            <div style='clear:both;'></div>
                                        </div>

                                    </div>

                                </div>
                            </div>



                            <div class="row">
                                <div class="col-sm-4 m-t-10 sm-m-t-10">
                                    <span id="error_message2" style="color:red;"></span><br/>
                                    <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id="close_Remove_slots">Close</button>
                                    <button type="button" class="btn btn-success btn-clean" id="removeSlot">Remove</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>



        <div class="modal fade stick-up in" id="newslotdetails" tabindex="-1" role="dialog" aria-hidden="false" >
            <div class="modal-dialog">
                <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        
                        <h6 style="font-weight: 700; font-size: 12px;color: #00b3f4;">SLOT DETAIL</h6>
                    </div>
                 
                    <div class="modal-body">
                        <div class="row" style="border: 1px solid moccasin;">
                        <div class="form-group" style="padding: 14px;">
                            <label class="col-sm-3">Start Time</label>
                            <label class="col-sm-9" id="slot_start"></label>
                        </div>
                        <div class="form-group" style="padding: 14px;">
                            <label class="col-sm-3">End Time</label>
                            <label class="col-sm-9" id="slot_end"></label>
                        </div>
                        <div class="form-group" style="padding: 14px;">
                            <label class="col-sm-3">Slot Radius</label>
                            <label class="col-sm-9" id="slot_rad"></label>
                        </div>
                        <div class="form-group" style="padding: 14px;">
                            <label class="col-sm-3">Location name</label>
                            <label class="col-sm-9" id="slot_loc_name"></label>
                        </div>
                        
                        <div class="form-group" style="padding: 14px;">
                            <label class="col-sm-3">Slot Price</label>
                            <label class="col-sm-9" id="slot_price"></label>
                        </div>

                        <div class="form-group" style="padding: 14px;">
                            <label class="col-sm-3">Address</label>
                            <label class="col-sm-9" id="slot_location"></label>
                        </div>
                        <div class="form-group custohide" style="padding: 14px;">
                            <label class="col-sm-3">Book by</label>
                            <label class="col-sm-9" id="custName"></label>
                        </div>
                        <div class="form-group" style="padding: 14px;" id="acancelapt" >
                        <button class="btn btn-danger btn-cons"  type="button" onclick="Show_Reson_box(1)">
                            <span class="bold">Cancel Appointment </span>
                        </button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal" id="modal_default_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        
                        <h6 style="font-weight: 700; font-size: 12px;">MANAGE LOCATION</h6>
                    </div>
                    <form action="" method="post" id="location_form">
                        <div class="modal-body clearfix">

                            <h6 style="color:#00b3f4;font-weight: 700; font-size: 12px;">Your Saved Addresses</h6>
                            <div id="locations_manage" style="margin-bottom: 10px;margin-top: 19px;" >
                                <?php
                                if (count($locations_array) > 0) {
                                    ?>
                                <table style="border: 1px solid moccasin; width: 100%;">
                                        <tr>
                                            <td style="padding:5px; font-weight: 600;text-transform: uppercase;">No.</td>
                                            <td style="padding:5px;font-weight: 600;text-transform: uppercase;">Name</td>
                                            <td style="padding:5px;font-weight: 600;text-transform: uppercase;">Address</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <?php
                                        $i = 1;
                                        foreach ($locations_array as $loc) {
                                            ?>
                                            <tr id="location-<?php echo (string) $loc['_id']; ?>">
                                                <td  valign="top" style="padding:5px;"><?php echo $i; ?></td>
                                                <td  valign="top" style="padding:5px;"><?php echo (string) $loc['locName']; ?></td>
                                                <td  valign="top" style="padding:5px;"><?php echo $loc['address1'] . ' ' . $loc['address2']; ?></td>
                                                <td><button style="    margin-left: -12%;" class="btn btn-danger btn-xs delete_location" id="<?php echo (string) $loc['_id']; ?>" value="Delete" type="button"><i class="fa fa-trash-o "></i></button></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </table>
                                    <?php
                                } else {
                                    echo "Seems like you don't have any saved addresses.";
                                }
                                ?>
                            </div>
 <button type="button" class="btn btn-primary" id="location_show"><i class="fa fa-plus text-white"></i> Add</button>
 <div class="hideContent">
 <div class="controls" style="height: 165px;
    border-width: 100%;
    margin-top: 10px;
    border: 1px solid moccasin;">


                                <div class="form-group" style="padding: 18px;">
                                    <label for="fname" class="col-sm-5 control-label">Address Type</label>
                                    <div class="col-sm-7">

                                        <input type="text" class="form-control mandatory" name="location_name" id="location_name" placeholder="Address Type: Home/Office/Clinic" />

                                    </div>
                                </div>
                                <div class="form-group" style="padding: 18px;">
                                    <label for="fnamef" class="col-sm-5 control-label">House No.</label>
                                    <div class="col-sm-7">

                                        <input type="text" class="form-control" name="add2" id="add2" placeholder="House No./Suite Number" />

                                    </div>
                                </div>
                                <div class="form-group" style="padding: 18px;">
                                    <label for="fname" class="col-sm-5 control-label">Address</label>
                                    <div class="col-sm-7">

                                        <input type="text" class="form-control mandatory" name="location_addr1" id="location_addr1"  placeholder="Enter your address" />

                                    </div>
                                </div>

                                <div class="form-row" style="margin-left: 10px;display: none">
                                    <div class="col-md-12">
                                        <!--                                    <label for="fname" class="col-sm-5 control-label">Locate exact address here:</label>-->
                                        <!--                                    <div style="width:50%;display: inline;text-align: left;"><label>Locate exact address here: </label></div><div style="width:50%;display: inline;text-align: right;"><span id="latlong_text"></span></div>-->
                                        <div class="content np" style="padding-top: 5px;padding-bottom: 11px;">
                                            <!--                                                                <div id="google_search_map" style="width: 100%; height: 300px;"></div>-->
                                            <div id="myMap" style="width: 593px; height: 300px; right: 38px;margin-left: -10px;"></div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                         
                        <div class="modal-footer">
                            <input type="hidden" id="loc_lat" name="loc_lat"  data-geo="lat"/>
                            <input type="hidden" id="loc_long" name="loc_long" data-geo="lng"/>
                            <span id="error_message" style="color:red;"></span>
                            <button type="button" class="btn btn-default btn-clean" data-dismiss="modal" id='close_location_popup'>Close</button>
                            <button type="button" class="btn btn-success btn-clean" id="add_location">Add Location</button>
                        </div>
                </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <!-- END PLACE PAGE CONTENT HERE -->
        </div>
        <!-- END CONTAINER FLUID -->

    </div>
    <!-- END PAGE CONTENT -->
    <!-- START FOOTER -->

    <!-- END FOOTER -->
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJ1oXlqQZRC5VqUsqwk22f2Fu0PvlwXAI&libraries=places&callback=initMap"
async defer></script>




