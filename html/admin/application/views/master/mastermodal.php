<?php

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");

require_once 'StripeModule.php';

class Mastermodal extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
//        $this->load->model('mastermodal');
        $this->load->database();
    }

    function getTransectionData($status) {
        if ($status != '7')
            $status = "1,2,3,4,5,6,7,8,9";
        else
            $status = "7,9";

        $query = $this->db->query("select ap.pg_commision,ap.doc_amount,ap.app_commision,ap.address_line1,ap.appointment_dt,ap.appointment_id,ap.inv_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status IN(" . $status . ") order by ap.appointment_id DESC")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function get_all_data($stdate, $enddate, $status) {
        if ($status != '7')
            $status = '1,2,3,4,5,6,7,8,9';
        else
            $status = "7,9";

        if ($stdate || $enddate) {
            $query = $this->db->query("select ap.pg_commision,ap.doc_amount,ap.app_commision,ap.address_line1,ap.last_modified_dt,ap.appt_long,ap.appt_lat,ap.address_line1,ap.address_line2,ap.appointment_dt as appointment_date,ap.appointment_id as Bookin_Id,ap.inv_id,ap.status,ap.amount,d.email as Driver_email,d.first_name as Driver_First_Name,d.last_name as Driver_Last_Name,p.email as Passenger_email,p.first_name as Passenger_fname,p.last_name as Passenger_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($stdate)) . "' AND '" . date('Y-m-d', strtotime($enddate)) . "' and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status IN(" . $status . ") order by ap.appointment_id DESC");
        } else {
            $query = $this->db->query("select ap.pg_commision,ap.doc_amount,ap.app_commision,ap.address_line1,ap.last_modified_dt,ap.appt_long,ap.appt_lat,ap.address_line1,ap.address_line2,ap.appointment_dt as appointment_date,ap.appointment_id as Bookin_Id,ap.inv_id,ap.status,ap.amount,d.email as Driver_email,d.first_name as Driver_First_Name,d.last_name as Driver_Last_Name,p.email as Passenger_email,p.first_name as Passenger_fname,p.last_name as Passenger_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and  ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status IN(" . $status . ") order by ap.appointment_id DESC");
        }


        foreach ($query->result_array() as $row) {

            if ($row['status'] == '1')
                $status = 'Appointment requested';
            else if ($row['status'] == '2')
                $status = 'Doctor accepted.';
            else if ($row['status'] == '3')
                $status = 'Doctor rejected.';
            else if ($row['status'] == '4')
                $status = 'Student has cancelled.';

            else if ($row['status'] == '5')
                $status = 'Doctor is on the way.';
            else if ($row['status'] == '6')
                $status = 'Appointment started.';
            else if ($row['status'] == '7')
                $status = 'Doctor Arrived';
            else if ($row['status'] == '8')
                $status = 'Appointment completed.';
            else if ($row['status'] == '9')
                $status = 'Appointment Timed out.';
            else
                $status = 'Status unavailable.';

            $now = new DateTime($row['last_modified_dt']);
            $ref = new DateTime($row['start_dt']);
            $diff = $now->diff($ref);



            $data[] = array(
                'Session_Id' => $row['Bookin_Id'],
                'appointment_Date' => $row['appointment_date'],
                'Amount' => '$' . $row['amount'],
                'Doctor_Earning' => '$' . (float) ($row['doc_amount']),
                'Session_Status' => $status,
                'session_start_date' => $row['start_dt'],
                'session_complete_date' => $row['last_modified_dt'],
                'Doctor_Name' => $row['Driver_First_Name'],
                'Appointment_latitude' => $row['appt_lat'],
                'Appointment_longitude' => $row['appt_long'],
                'patient_Name' => $row['Passenger_fname'],
            );
        }

        return $data;
    }

    function getDatafromTransectionstatus($tstatus = '') {

        $query = $this->db->query("select ap.pg_commision,ap.doc_amount,ap.app_commision,ap.address_line1,ap.appointment_dt,ap.appointment_id,ap.inv_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status IN(" . $tstatus . ") order by ap.appointment_id DESC")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function getDatafromdate($stdate, $enddate, $status) {
        if ($status != '7')
            $status = '1,2,3,4,5,6,7,8,9';
        else
            $status = "7,9";

        $query = $this->db->query("select ap.pg_commision,ap.doc_amount,ap.app_commision,ap.address_line1,ap.appointment_dt,ap.appointment_id,ap.inv_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($stdate)) . "' AND '" . date('Y-m-d', strtotime($enddate)) . "' and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status IN(" . $status . ") order by ap.appointment_id DESC")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function getDataSelected($selectdval, $status) {
        if ($status != '7')
            $status = '1,2,3,4,5,6,7,8,9';
        else
            $status = "7,9";

        $query = $this->db->query("select ap.address_line1,ap.appointment_dt,ap.payment_type,ap.appointment_id,ap.inv_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and ap.payment_type = '" . $selectdval . "' and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status IN(" . $status . ") order by ap.appointment_id DESC")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function getuserinfo($param = '') {

//        $query = $this->db->query("SELECT * FROM doctor where doc_id='" . $this->session->userdata("LoginId") . "' ")->row();
        if ($param == '')
            $query = $this->db->query("select d.amount,d.board_certificate,d.medical_license_num,d.address_line1,d.address_line2,d.zipcode,d.cityName,d.countryName,(select type_name from doctor_type where type_id = d.type_id) as typename,d.board_certificate,d.board_certification_expiry_dt,d.medical_license_pic, d.doc_id,d.expertise,d.first_name,d.profile_pic,d.last_name,d.about,d.mobile,d.tax_num,d.medical_license_num,d.email,d.created_dt,d.profile_pic,d.status from doctor d where  d.doc_id=" . $this->session->userdata("LoginId") . "")->row();
        else
            $query = $this->db->query("select d.amount,d.board_certificate,d.medical_license_num,d.address_line1,d.address_line2,d.zipcode,d.cityName,d.countryName,(select type_name from doctor_type where type_id = d.type_id) as typename,d.board_certificate,d.board_certification_expiry_dt,d.medical_license_pic, d.doc_id,d.expertise,d.first_name,d.profile_pic,d.last_name,d.about,d.mobile,d.tax_num,d.medical_license_num,d.email,d.created_dt,d.profile_pic,d.status from doctor d where  d.doc_id=" . $param . "")->row();


        return $query;
    }

    function getuseraddress($param = '') {


        if ($param == '')
            $query = $this->db->query("select w.phone,w.address_line1,w.address_line2,w.city,w.country,(select City_Name from city where City_Id = w.city) as cityName,(select Country_Name from country where Country_Id = w.country) as countryName from workplace w where workplace_id= (select workplace_id from doctor where doc_id=" . $this->session->userdata("LoginId") . ")")->row();
        else
            $query = $this->db->query("select w.phone,w.address_line1,w.address_line2,w.city,w.country,(select City_Name from city where City_Id = w.city) as cityName,(select Country_Name from country where Country_Id = w.country) as countryName from workplace w where workplace_id= (select workplace_id from doctor where doc_id=" . $param . ")")->row();

        return $query;
    }

    function deletegallerydata() {

        $this->db->where('image', $this->input->post('url'));
        $this->db->delete('images');

        echo json_encode(array('name' => $this->input->post('url')));
    }

    function getgalleryimage($param = '') {
        if ($param == '')
            $query = $this->db->query("SELECT * from images WHERE  doc_id='" . $this->session->userdata("LoginId") . "' ")->result();
        else {
            $query = $this->db->query("SELECT * from images WHERE  doc_id='" . $param . "' ")->result();
        }
        return $query;
    }

    function getPassangerBooking() {
        $query = $this->db->query("select a.appointment_id,a.last_modified_dt,a.amount,a.inv_id,a.distance,a.appointment_dt,a.drop_addr1,a.drop_addr2,a.doc_id,a.patient_id,d.first_name as doc_firstname,d.profile_pic as doc_profile,d.last_name as doc_lastname,p.first_name as patient_firstname,p.last_name as patient_lastname,a.address_line1,a.address_line2,a.status from appointment a,doctor d,patient p where a.patient_id=p.patient_id and d.doc_id=a.doc_id  and a.patient_id='" . $this->session->userdata("LoginId") . "' order by a.appointment_id desc")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function geteducationdata($param = '') {

        if ($param == '')
            $query = $this->db->query("select * from doctor_education WHERE doc_id='" . $this->session->userdata("LoginId") . "' ")->result();
        else {
            $query = $this->db->query("select * from doctor_education WHERE doc_id='" . $param . "' ")->result();
        }

        return $query;
    }

    function updateeducation() {


        $education = $this->input->post('education');
        if (!empty($education)) {

            if ($education) {
                for ($i = 0; $i < count($education); $i++) {

                    $data[$i] = array('degree' => $education[$i]['degree'], 'end_year' => $education[$i]['end_year'], 'start_year' => $education[$i]['start_year'], 'institute' => $education[$i]['institute'], 'doc_id' => $this->session->userdata("LoginId"));

                    if ($education[$i]['degree'] != '' || $education[$i]['end_year'] != 'null' || $education[$i]['start_year'] != 'null' || $education[$i]['institute'] != '')
                        $this->db->insert('doctor_education', $data[$i]);
                }
            }
        }

        $educationupdate = $this->input->post('educationupdate');
        if ($educationupdate) {
            for ($i = 0; $i < count($educationupdate); $i++) {

                $data[$i] = array('degree' => $educationupdate[$i]['degree'], 'end_year' => $educationupdate[$i]['end_year'], 'start_year' => $educationupdate[$i]['start_year'], 'institute' => $educationupdate[$i]['institute']);

                $id = $educationupdate[$i]['ed_id'];
                $this->db->update('doctor_education', $data[$i], array('ed_id' => $id));
            }
        }
    }

    function addservices() {
        $data = $this->input->post('servicedata');
        $this->db->insert('services', $data);
    }

    function updateservices($table = '') {
        $formdataarray = $this->input->post('editservicedata');
        $id = $this->input->post('id');
        $this->db->update($table, $formdataarray, array('service_id' => $id));
    }

    function deleteservices($table = '') {
        $id = $this->input->post('id');
        $this->db->where('service_id', $id);
        $this->db->delete($table);
    }

    function getActiveservicedata() {
        $query = $this->db->query("select * from services")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function delete_education() {

        $this->db->where('ed_id', $this->input->post('idtodelete'));
        $this->db->delete('doctor_education');

        echo json_encode(array('name' => $this->input->post('idtodelete')));
    }

    function week_start_end_by_date($date, $format = 'Y-m-d') {

        //Is $date timestamp or date?
        if (is_numeric($date) AND strlen($date) == 10) {
            $time = $date;
        } else {
            $time = strtotime($date);
        }

        $week['week'] = date('W', $time);
        $week['year'] = date('o', $time);
        $week['year_week'] = date('oW', $time);
        $first_day_of_week_timestamp = strtotime($week['year'] . "W" . str_pad($week['week'], 2, "0", STR_PAD_LEFT));
        $week['first_day_of_week'] = date($format, $first_day_of_week_timestamp);
        $week['first_day_of_week_timestamp'] = $first_day_of_week_timestamp;
        $last_day_of_week_timestamp = strtotime($week['first_day_of_week'] . " +6 days");
        $week['last_day_of_week'] = date($format, $last_day_of_week_timestamp);
        $week['last_day_of_week_timestamp'] = $last_day_of_week_timestamp;

        return $week;
    }

    function get_schedule_details() {
        $ca = 0;
        if ($this->input->post('slot_id') != '') {
            $ca = 1;
            $query = $this->db->query("select a.status,a.appt_lat,a.appt_long,a.slot_id,p.email,p.first_name,p.phone,p.profile_pic,p.last_name,a.appointment_dt,a.address_line1,a.address_line2,a.extra_notes from patient p,appointment a,doctor ms WHERE a.slot_id='" . $this->input->post('slot_id') . "'  and a.patient_id = p.patient_id and a.doc_id='" . $this->session->userdata('LoginId') . "'")->result();
        } else if ($this->input->post('app_id') != '') {
            $ca = 2;
            $query = $this->db->query("select distinct  a.status,a.appt_lat,a.appt_long,a.slot_id,p.email,p.first_name,p.phone,p.profile_pic,p.last_name,a.appointment_dt,a.address_line1,a.address_line2,a.extra_notes from patient p,appointment a,doctor ms WHERE a.appointment_id=" . $this->input->post('app_id') . "  and a.patient_id = p.patient_id")->result();
        }

        foreach ($query as $result) {
            $pname = $result->first_name . " " . $result->last_name;
            $datetime = date("F jS, Y h a", strtotime($result->appointment_dt) - $this->input->post('offset'));
            $applocation = $result->address_line1 . $result->address_line2;
            $notes = $result->extra_notes;
            $profile_pic = $result->profile_pic;
            $phone = $result->phone;
            $email = $result->email;
            $appdate = $result->appointment_dt;
            $slot_id = $result->slot_id;
            $lat = $result->appt_lat;
            $long = $result->appt_long;
            $status = $result->status;
            $phone = $result->phone;
        }

        echo json_encode(array('slot_id' => $slot_id, 'lat' => $lat, 'long' => $long, 'pname' => $pname, 'datetime' => $datetime, 'applocation' => $applocation, 'notes' => $notes, 'profile_pic' => $profile_pic, 'phone' => $phone, 'email' => $email, 'appdate' => $appdate, 'status' => $status, 'phone' => $phone));
    }

    function updateDataProfile() {

        $formdataarray = $this->input->post('fdata');
        $formdataarray['board_certification_expiry_dt'] = date('Y-m-d', strtotime(str_replace('-', '/', $formdataarray['board_certification_expiry_dt'])));

        $this->db->update('doctor', $formdataarray, array('doc_id' => $this->session->userdata("LoginId")));

        $this->session->set_userdata(array(
            'profile_pic' => $formdataarray['profile_pic'],
            'first_name' => $formdataarray['first_name'],
            'last_name' => $formdataarray['last_name']
        ));

        $formdataarrayaddress = $this->input->post('useraddress');
        $this->db->query('update workplace set address_line1="' . $formdataarrayaddress['address_line2'] . '",address_line2="' . $formdataarrayaddress['address_line2'] . '" where workplace_id = (select workplace_id from doctor where doc_id ="' . $this->session->userdata("LoginId") . '" )'); //'doctor', $formdataarrayaddress, array('doc_id' => $this->session->userdata("LoginId")));


        $con = new Mongo();
        $PriveMd = $con->PriveMd;

        $location = $PriveMd->selectCollection('location');
        $location->update(array('user' => (int) $this->session->userdata("LoginId")), array('$set' => array('name' => $formdataarray['first_name'], 'lname' => $formdataarray['last_name'], 'image' => $formdataarray['profile_pic'], 'radius' => $this->input->post('radius'), 'amount' => $formdataarray['amount'])));
    }

    function updategallery() {

        $images = $this->input->post('newimage');
        if ($images)
            $this->db->insert('images', $images);
    }

    function updateMasterBank() {
//        $this->load->library('StripeModule');
        $stripe = new StripeModule();

        return array('flag' => 0);
    }

    function Getdashboarddata() {
        $currTime = time();
        // today completed booking count
        $today = date('Y-m-d', $currTime);
        $todayone = $this->db->query("SELECT ap.appointment_id  FROM appointment ap,doctor d ,patient p WHERE ap.patient_id = p.patient_id and ap.doc_id = d.doc_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.appointment_dt like '" . date('Y-m-d') . "%' and ap.status = 7 ");
//        $today
        //this week completed booking
        $weekArr = $this->week_start_end_by_date($currTime);
        $week = $this->db->query("SELECT ap.appointment_id  FROM appointment ap,doctor d ,patient p WHERE ap.patient_id = p.patient_id and ap.doc_id = d.doc_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status = 7 and DATE(ap.appointment_dt) >= '" . $weekArr['first_day_of_week'] . "'");


        // this month completed booking

        $currMonth = date('n', $currTime);
        $month = $this->db->query("SELECT ap.appointment_id  FROM appointment ap,doctor d ,patient p WHERE ap.patient_id = p.patient_id and ap.doc_id = d.doc_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status = 7 and  MONTH(ap.appointment_dt) = '" . $currMonth . "' ");


        // lifetime completed booking
        $lifetime = $this->db->query("SELECT ap.appointment_id  FROM appointment ap,doctor d ,patient p WHERE ap.patient_id = p.patient_id and ap.doc_id = d.doc_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status = 7 ");

//         $todayone = $this->db->query("SELECT ap.*  FROM appointment ap, doctor d,patient p WHERE  ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and  doc_id='" . $this->session->userdata("LoginId") . "' and appointment_dt like '" . date('Y-m-d') . "%' and status = 7 ");
////        $today
//        //this week completed booking
//        $weekArr = $this->week_start_end_by_date($currTime);
//        $week = $this->db->query("SELECT ap.*  FROM appointment ap, doctor d,patient p WHERE ap.patient_id = p.patient_id ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and  doc_id='" . $this->session->userdata("LoginId") . "' and status = 7 and DATE(appointment_dt) >= '" . $weekArr['first_day_of_week'] . "'");
//
//
//        // this month completed booking
//
//        $currMonth = date('n', $currTime);
//        $month = $this->db->query("SELECT ap.*  FROM appointment ap, doctor d,patient p WHERE  ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and  doc_id='" . $this->session->userdata("LoginId") . "' and status = 7 and  MONTH(appointment_dt) = '" . $currMonth . "' ");
//
//
//        // lifetime completed booking
//        $lifetime = $this->db->query("SELECT ap.*  FROM appointment ap, doctor d,patient p WHERE  ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and d.doc_id='" . $this->session->userdata("LoginId") . "' and status = 7 ");

//        ap.doc_id = d.doc_id and ap.patient_id = p.patient_id 
        // total booking uptodate
        $totaluptodate = $this->db->query("SELECT ap.appointment_id  FROM appointment ap,doctor d ,patient p WHERE ap.patient_id = p.patient_id and ap.doc_id = d.doc_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status = 7");



        //today earnings
//
        $todayearning = $this->db->query("SELECT sum(ap.amount) as totamount FROM appointment ap,doctor d ,patient p WHERE ap.patient_id = p.patient_id and ap.doc_id = d.doc_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.appointment_dt like '" . date('Y-m-d') . "%' and ap.status = 7");

//
//
//        //this week completed booking
//
        $weekearning = $this->db->query("SELECT sum(ap.amount) as totamount  FROM appointment ap,doctor d ,patient p WHERE ap.patient_id = p.patient_id and ap.doc_id = d.doc_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status = 7 and DATE(ap.appointment_dt) >= '" . $weekArr['first_day_of_week'] . "'");
//
//
//        // this month completed booking
//
//
        $monthearning = $this->db->query("SELECT sum(ap.amount) as totamount  FROM appointment ap,doctor d ,patient p WHERE ap.patient_id = p.patient_id and ap.doc_id = d.doc_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status = 7 and  MONTH(ap.appointment_dt) = '" . $currMonth . "' ");
//
//
//        // lifetime completed booking
        $lifetimeearning = $this->db->query("SELECT sum(ap.amount) as totamount FROM appointment ap,doctor d ,patient p WHERE ap.patient_id = p.patient_id and ap.doc_id = d.doc_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status = 7 ");
//
//        // total booking uptodate
        $totaluptodateearning = $this->db->query("SELECT  sum(ap.amount)  FROM appointment ap,doctor d ,patient p WHERE ap.patient_id = p.patient_id and ap.doc_id = d.doc_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status = 7");


        $t = $todayearning->row();
        $w = $weekearning->row();
        $m = $monthearning->row();
        $l = $lifetimeearning->row();
        $te = $totaluptodateearning->row();


        $data = array('today' => $todayone->num_rows(), 'week' => $week->num_rows(), 'month' => $month->num_rows(), 'lifetime' => $lifetime->num_rows(), 'total' => $totaluptodate->num_rows(),
            'todayearning' => (float) (($t->totamount - ($t->totamount * (10 / 100)) - (float) (($t->totamount * (2.9 / 100)) + 0.3))), 'weekearning' => (float) (($w->totamount - ($w->totamount * (10 / 100)) - (float) (($w->totamount * (2.9 / 100)) + 0.3))), 'monthearning' => (float) (($m->totamount - ($m->totamount * (10 / 100)) - (float) (($m->totamount * (2.9 / 100)) + 0.3))), 'lifetimeearning' => (float) (($l->totamount - ($l->totamount * (10 / 100)) - (float) (($l->totamount * (2.9 / 100)) + 0.3))), 'totalearning' => $te
        );
        return $data;
    }

    function updateData($IdToChange = '', $databasename = '', $db_field_id_name = '') {
        $formdataarray = $this->input->post('fdata');
        $this->db->update($databasename, $formdataarray, array($db_field_id_name => $IdToChange));
    }

    function LoadAdminList() {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;
        $cursor = $collection->find(array('Role' => "SubAdmin"));
        $db->close();
        return $cursor;
    }

    function issessionset() {

        $this->load->library('nativesession');
        if ($this->session->userdata('emailid') && $this->session->userdata('password')) {

            return true;
        } elseif ($_SESSION['admin'] == "super") {

            return true;
        }
        return false;
    }

    function server_pagination() {

        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
//    $aColumns = array('address_line1','appointment_dt');
// DB table to use
//    $sTable = 'country';
//

        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
        $iSortingCols = $this->input->get_post('iSortingCols', true);
        $sSearch = $this->input->get_post('sSearch', true);
        $sEcho = $this->input->get_post('sEcho', true);

// Paging
        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }

// Ordering
        if (isset($iSortCol_0)) {
            for ($i = 0; $i < intval($iSortingCols); $i++) {
                $iSortCol = $this->input->get_post('iSortCol_' . $i, true);
                $bSortable = $this->input->get_post('bSortable_' . intval($iSortCol), true);
                $sSortDir = $this->input->get_post('sSortDir_' . $i, true);

                if ($bSortable == 'true') {
                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        if (isset($sSearch) && !empty($sSearch)) {
            for ($i = 0; $i < count($aColumns); $i++) {
                $bSearchable = $this->input->get_post('bSearchable_' . $i, true);

                // Individual column filtering
                if (isset($bSearchable) && $bSearchable == 'true') {
                    $this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
                }
            }
        }

// Select Data
//    $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
//    $rResult = $this->db->get($sTable);
        $rResult = $this->db->query("select ap.address_line1,ap.appointment_dt,ap.appointment_id,ap.inv_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and ap.doc_id='" . $this->session->userdata("LoginId") . "' and ap.status IN(7) order by ap.appointment_id DESC Limit " . $iDisplayLength . " ");

// Data set length after filtering
//    $this->db->select('FOUND_ROWS() AS found_rows');
//    $iFilteredTotal = $this->db->get()->row()->found_rows;

        $iFilteredTotal = $rResult->num_rows();
// Total data set length
//    $iTotal = $this->db->count_all($rResult);

        $iTotal = $rResult->num_rows();

// Output
        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => array()
        );
//
//    if u want spacific data then above a columns array has created please fill that to field
//    foreach($rResult->result_array() as $aRow)
//    {
//        $row = array();
//
//        foreach($aColumns as $col)
//        {
//            $row[] = $aRow[$col];
//        }
//
//        $output['aaData'][] = $row;
//    }

        foreach ($rResult->result_array() as $aRow) {
            $row = array();

            foreach ($aRow as $col) {
                $row[] = $col;
            }

            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

}

?>
