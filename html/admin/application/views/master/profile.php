<?php
error_reporting(0);
$this->load->library('session');
$msg = $this->session->userdata('admin_session')['msg'];
if ($msg != '') {
    ?> 
    <script>alert("<?php echo $msg; ?>");</script>
    <?php
}
$meta = array('msg' => '');
$this->session->set_userdata($meta);
?>
<style>
    #imagePreview{
        background:url(https://oodt.apache.org/images/grid.jpg)no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        border-radius:5px;
        width: 124px;
        height: 135px;
    }
    #imgbox{
        margin-top: 38px;
    }



    #closbtn{
        margin-left: 112px;
        margin-top: 35PX;
        position: absolute;
        width: 18px;
        cursor: pointer;
    }
    .panel-controls{
        display: none;
    }
    #plus{
        cursor: pointer;
    }
    .add_way_point {
        font-size: 16px;
        background-image: url(http://app.bringg.com/images/860876be.add_driver_icon.png);
        background-position: center left;
        background-repeat: no-repeat;
        padding-left: 22px;
        cursor: pointer;
    }
    .job_img{
        display: -webkit-inline-box;
    }
</style>
<script>
     function isNumberKey(evt)
    {
        $("#mobify").text("");
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 45 || charCode > 57)) {
            //    alert("Only numbers are allowed");
            $("#mobify").text(<?php echo json_encode(LIST_COMPANY_MOBIFY); ?>);
            return false;
        }
        return true;
    }
    var componentForm = {
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    function initMap() {
        var input = (document.getElementById('address'));
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            console.log(place);
            for (var component in componentForm) {
                document.getElementById(component).value = '';
            }
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }


        });
    }
</script>
<script>
    $(document).ready(function () {
        $('#big_table_processing').hide();
//        var date = new Date();
//        $('.datepicker-components').datepicker({
//            format: "yyyy-mm-dd",
//            startDate: date,
//            autoclose: true
//        });
 $('.datepicker-components').on('changeDate', function () {
            $(this).datepicker('hide');
        });
        var date = new Date();
        $('.datepicker-components').datepicker({
            startDate: date
        });
        $('#nav_user_img').attr('src', '<?php echo $userinfo->profile_pic; ?>');
        $('#board,#medicalc').click(function () {

            $('#gal').attr('alt', $(this).attr('alt'));
            if ($(this).attr('alt') == '1')
            {
                $('#show_board').css({
                    "width": "50px",
                    "height": "47px"
                });
                $('#show_board').attr('src', "<?php echo base_url(); ?>theme/assets/img/progressBar4.gif");
            } else if ($(this).attr('alt') == '2')
            {

                $('#show_medical').css({
                    "width": "50px",
                    "height": "47px"
                });
                $('#show_medical').attr('src', "<?php echo base_url(); ?>theme/assets/img/progressBar4.gif");
            }


            $('#gal').trigger('click');
        });
        $('#profile_img_upload_click').click(function () {

            $('#poponclick').trigger('click');
        });
        $('#poponclick').change(function () {


            var DivId = 0;
            var emailId = $('#emailId').val();
            var formElement = $('#poponclick').prop('files')[0]; //document.getElementById("files_upload_form");
            var form_data = new FormData();
            form_data.append('myfile', formElement);
            form_data.append('uploadType', 'profile');
            form_data.append('emailId', emailId);
            form_data.append('type', 'master');
            $.ajax({
                url: "<?php echo base_url() ?>/application/views/master/uploadprofile.php",
                type: "POST",
                data: form_data,
                dataType: "JSON",
                mimeType: "multipart/form-data",
                async: false,
                success: function (result) {
                    if (result.flag == 0) {
                        alert('Invalid formate please select jpeg or jpg');
                    } else {

                        $('#profile_pic_to_save').val(result.fileName);
//                        $('#uimg,#nav_user_img').attr('src', 'http://www.privemd.com/pics/' + result.fileName);
                        $('#uimg,#nav_user_img').attr('src', result.fileName);
                    }



                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
        $('#profile_img_upload_click_photo').click(function () {
             $('#big_table_processing').css("display", "block");
            $('#poponclick_photo').trigger('click');
        });
        $('#poponclick_photo').change(function () {

            $('#big_table_processing').show();
            var DivId = 0;
            var emailId = $('#doc_id_photo').val();
            console.log('<?php echo $userinfo->num_of_job_images; ?>');
            var noofjob = $('#noofjob').val();

            var formElement = $('#poponclick_photo').prop('files')[0]; //document.getElementById("files_upload_form");
            var form_data = new FormData();
            form_data.append('myfile', formElement);
            form_data.append('uploadType', 'profile');
            form_data.append('emailId', emailId);
            form_data.append('type', 'master');
            form_data.append('job', noofjob);
            $.ajax({
                url: "<?php echo base_url() ?>/application/views/master/uploadprofile_photo.php",
                type: "POST",
                data: form_data,
                dataType: "JSON",
                mimeType: "multipart/form-data",
                async: false,
                success: function (result) {
                    $('#big_table_processing').hide();
                    console.log(result);
                    if (result.flag == 0) {
                        alert('Invalid formate please select jpeg or jpg');
                    } else {
                        $('#noofjob').val(result.job);
                        $('#jobImageUrl').val(result.fileName);
                        var html = "<div class='job_img'><span style='position: absolute;' onclick='deletejobPhoto(this)'><i class='fa fa-close' style='color:rgba(0, 179, 244, 0.55);font-size: 20px;margin-left: 110px;margin-top: -3px;'></i></span>\n\
                                <img style='width: 126px;height: 127px;' class='img' id='uimg_photo' src='" + result.fileName + "'>\n\
                                <input type='hidden' name='imgarr[]' value='" + result.fileName + "'></div>";
                        $('#prof_img_photo').append(html);
                        $('#profile_pic_to_save_photo').val(result.fileName);
//                        $('#uimg,#nav_user_img').attr('src', 'http://www.privemd.com/pics/' + result.fileName);
                        $('#uimg,#nav_user_img_photo').attr('src', result.fileName);
                    }



                },
                cache: false,
                contentType: false,
                processData: false
            });
             $('#big_table_processing').hide();
        });
        $('#upload1').click(function () {

            $('#positionone').trigger('click');
        });
        $('#positionone').change(function () {
            $('#path').html($('#positionone').val());
        });
        $('#upload2').click(function () {

            $('#positiontwo').trigger('click');
        });
//var Divid = 0 ;
        $("#gal").on("change", function ()
        {

//            if($(this).attr('alt') == )
//            alert($(this).attr('alt'));

            var formElement = $('#gal').prop('files')[0]; //document.getElementById("files_upload_form");
            var form_data = new FormData();
            form_data.append('myfile', formElement);
            form_data.append('uploadType', 'gallery');
            form_data.append('type', '<?php echo $this->session->userdata("first_name") ?>');
            $.ajax({
                url: "<?php echo base_url() ?>/application/views/master/upload_images_on_local.php",
                type: "POST",
                data: form_data,
                dataType: "JSON",
                async: false,
                success: function (result) {
                    var arrayData = ["jpg", "png", "gif", "jpeg"];
                    var defaultImg = "<?php echo base_url(); ?>theme/assets/img/certificatedft.png";
                    if (arrayData.indexOf(result.ext) != -1)
                        defaultImg = '<?php echo base_url() ?>../pics/' + result.fileName;
//                     var code = '<div class="col-sm-3 m-b-15" id="Divid'+Divid+'"> <img src="<?php //echo base_url()                              ?>////theme/assets/img/close.png"  onclick="closebtn(this)"  id="closbtn" alt="'+Divid+'" /><div class="bg-white m-t-125 " id="imgbox"><img src="https://s3.amazonaws.com/' +result.fileName +' " id="imagePreview" ><input type="hidden" name="newimage[image]" value="https://s3.amazonaws.com/'+result.fileName+'"><input type="hidden" name="newimage[mas_id]" value="<?php //echo $this->session->userdata("LoginId")                              ?>//"></div></div>';
//
//                        $('#divid').append(code);
                    if (result.flag == 0) {
                        alert('Invalid formate please select pdf or jpeg');
                        $('#show_medical').attr('src', defaultImg);
                    } else {

                        if ($('#gal').attr('alt') == '1')
                        {

                            $('#show_board').css({
                                "width": "130px",
                                "height": "108px"
                            });


                            $('#show_board').attr('src', defaultImg);
                            $('#board_certificate').val(result.fileName);
                        } else if ($('#gal').attr('alt') == '2') {
                            $('#show_medical').css({
                                "width": "130px",
                                "height": "108px"
                            });
                            $('#medical_certificate').val(result.fileName);
                            $('#show_medical').attr('src', defaultImg);
                        }
                    }

                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
        var counttoadded = 0;
        $('#otheeducation').click(function () {


            var code = '<div class="col-sm-12" style="border: 1px solid #e6e6e6;" id="div' + counttoadded + '"> <div class="pull-right" style="margin-right: -14px;" ><button alt="div' + counttoadded + '" class="btn btn-danger  m-b-10" type="button" id="temp" onclick="deleteeducation(this)"> \n\
          <i class="fa fa-trash-o"></i> </button> </div><br> <div class="form-group"> \n\
        <label for="name" class="col-sm-3 control-label">DEGREE</label>\n\
    <div class="col-sm-9"> \n\
     <input type="text" class="form-control " id="position"  placeholder="Education degree" name="education[' + counttoadded + '][degree]"  aria-required="true" aria-invalid="true" maxlength="100"> \n\
          </div><br><br></div> <div class="form-group"> <label for="name" class="col-sm-3 control-label">Duration</label> <div class="col-sm-9"> \n\
      <div class=""> <div id="datepicker-range"> <div class="" aria-required="true">\n\
      <div class="col-sm-4 no-padding"> \n\
      <select class="form-control" name="education[' + counttoadded + '][start_year]"><option value="null">Select...</option>\n\
<?php for ($i = 1950; $i < 2017; $i++) { ?>\n\
                                                                                                                              <option value="<?php echo $i ?>"><?php echo $i ?></option>\n\
<?php } ?>\n\
      </select>\n\
      </div>\n\
      <div class="col-sm-4 no-padding">\n\
      <select class="form-control" name="education[' + counttoadded + '][end_year]"><option value="null">Select...</option>\n\
<?php for ($i = 1950; $i < 2017; $i++) { ?>\n\
                                                                                                                              <option value="<?php echo $i ?>"><?php echo $i ?></option>\n\
<?php } ?>\n\
      </select>\n\
      </div>\n\
      </div></div> </div> \n\
      </div></div><br><br><div class="form-group"> <label for="name" class="col-sm-3 control-label">INSTITUTE</label> <div class="col-sm-9"> <textarea class="form-control" name="education[' + counttoadded + '][institute]"></textarea> </div> </div>';
            $('#addexperiencedyanamic').append(code);
            counttoadded++;
        });
    });
    function deleteeducation(val) {
        if (val.id == 'temp') {

            $("#" + $(val).attr('alt')).remove();
        } else {

            $.ajax({
                url: "delete_education",
                type: "POST",
                data: {idtodelete: val.id},
                dataType: "JSON",
                success: function (result) {


                    $('#div' + val.id).remove();
                }
            });
        }



    }

    function showMessage() {
        if (confirm("Are you sure to update!")) {
            $('#form-work').submit();
        }
        var fname = $('#fname').val();
        var c_code = $('#c_code').val();
        var position = $('#position').val();
        var emailId = $('#emailId').val();

//        if(fname == "")
//        {
//            $('#profile_val').text("Please enter first name");
//        }else if(c_code){
//            $('#profile_val').text("Please enter country code");
//        }else if(position){
//            $('#profile_val').text("Please enter phone number");
//        }else if(emailId){
//            $('#profile_val').text("Please enter email address");
//        }else if (confirm("Are you sure to update!")) {
//            $('#form-work').submit();
//        }
    }
    function closebtn(val) {

        $('#Divid' + $(val).attr('alt')).remove();
    }
    function closebtntodelete(val) {
        var url = $('.oldimage' + ($(val).attr('alt'))).attr('src');
        $.ajax({
            url: "deletegallerydata",
            type: "POST",
            data: {url: url},
            dataType: "JSON",
            success: function (result) {


            }
        });
        $('#Divid' + $(val).attr('alt')).remove();
    }

    function deletejobPhoto(imgid)
    {
        $(imgid).closest('.job_img').remove();
//        alert(photo);
//         $.ajax({
//            url: "<?php echo base_url() ?>index.php/masteradmin/deletejobPhoto",
//            type: "POST",
//            data: {photo: photo},
//            dataType: "JSON",
//            success: function (result) {
//                console.log(result);
//location.reload();
//            }
//        });
    }

</script>

<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content" style="padding-top: 0px;">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <ul class="breadcrumb" style="margin-left: 20px;font-size: 16px;color:#0090d9;">
                <li><a style="color:#0090d9;" href="" class="">Profile</a>
                <li><a href="#" class="active"> </a>
                </li>
            </ul>


            <!-- END BREADCRUMB -->
        </div>
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="panel panel-transparent">

                    <div class="panel-body">
                        <div class="col-md-1 sm-no-padding"></div>
                        <div class="col-md-10 sm-no-padding">

                            <div class="panel panel-transparent">
                                <div class="panel-body no-padding">
                                    <div id="portlet-advance" >
                                        <div class="panel-heading ">

                                            <div class="panel-controls">
                                                <ul>
                                                    <li>
                                                        <div class="dropdown">
                                                            <a id="portlet-settings" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" Role="button" aria-expanded="false">
                                                                <i class="portlet-icon portlet-icon-settings "></i>
                                                            </a>
                                                            <ul class="dropdown-menu pull-right" Role="menu" aria-labelledby="portlet-settings">
                                                                <li><a href="#">API</a>
                                                                </li>
                                                                <li><a href="#">Preferences</a>
                                                                </li>
                                                                <li><a href="#">About</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="#" class="portlet-collapse" data-toggle="collapse"><i class="portlet-icon portlet-icon-collapse"></i></a>
                                                    </li>
                                                    <li><a href="#" class="portlet-refresh" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                                    </li>
                                                    <li><a href="#" class="portlet-maximize" data-toggle="maximize"><i class="portlet-icon portlet-icon-maximize"></i></a>
                                                    </li>
                                                    <li><a href="#" class="portlet-close" data-toggle="close"><i class="portlet-icon portlet-icon-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                         <form id="multipleformdata">
                                        <!--                                    <form  action="--><?php //echo base_url()                             ?><!--index.php/upload/do_upload" enctype="multipart/form-data">-->
                                        <input type="file"  id="gal" style="display: none" alt="1" multiple>

                                    </form>
                                        <form enctype="multipart/form-data" id="uploadFile">
                                            <input type="file" style="display: none" name="myfile_upload" id="poponclick">
                                        </form>
                                        <form enctype="multipart/form-data" id="uploadFile_photo">
                                            <input type="file" style="display: none" name="myfile_upload_photo" id="poponclick_photo">
                                        </form>
                                        <form id="form-work" class="form-horizontal" method="post" Role="form" autocomplete="off" novalidate="novalidate" action="<?php echo base_url(); ?>index.php/masteradmin/udpadedataProfile">
                                        <div id="portlet-advance" class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">

                                                <div class="col-sm-12">
                                                        <input type="hidden" id="profile_pic_to_save" name="fdata[profile_pic]" value="<?php echo $userinfo->profile_pic; ?>">

                                                        <div class="col-sm-4">
                                                            <center>
                                                                <div class="col_2">
                                                                    <div class="img" id="prof_img">
                                                                        <?php if ($userinfo->profile_pic) { ?>
                                                                            <img style="-webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);width: 126px;height: 127px;"   class="img-circle" id="uimg" src="<?php echo $userinfo->profile_pic; ?>" enctype="multipart/form-data">
                                                                        <?php } else { ?>
                                                                            <img style="-webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);width: 126px;height: 127px;"   class="img-circle" id="uimg" src=" http://104.236.41.101/tutree/pics/aa_default_profile_pic.gif" enctype="multipart/form-data">
                                                                        <?php } ?>

                                                                    </div>
                                                                    <div class="img" id="prof_img" style="margin-top: 28px;">

                                                                        <button class="btn btn-success btn-cons m-b-10" type="button" id="profile_img_upload_click"><i class="fa fa-cloud-upload"></i> <span class="bold">Upload</span>
                                                                        </button>
                                                                    </div>
                                                                </div>



                                                            </center>
                                                        </div>


                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label for="fname" class="col-sm-3">First Name<span style="color:red;font-size: 18px">*</span></label>
                                                                <div class="col-sm-8">
                                                                    <input type="text" class="form-control" id="fname" placeholder="Full name"  value="<?php echo $userinfo->first_name; ?>" name="fdata[first_name]" required="" aria-required="true" >
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="fname" class="col-sm-3">Last Name</label>
                                                                <div class="col-sm-8">
                                                                    <input type="text" class="form-control" id="fname" placeholder="Last name" value="<?php echo $userinfo->last_name; ?>" name="fdata[last_name]" required="" aria-required="true">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="position" class="col-sm-3 ">Mobile<span style="color:red;font-size: 18px">*</span></label>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <div class="col-sm-3">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">+</span>
                                                                                <input type="text"  id="c_code" name="fdata[country_code]" required="required" class="form-control" value="<?php echo ltrim($userinfo->country_code, "+") ?>" onkeypress="return isNumberKey(event)" readonly="true">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="form-control " id="position" value="<?php echo $userinfo->mobile; ?>" placeholder="Mobile" name="fdata[mobile]"  aria-required="true" aria-invalid="true" onkeypress="return isNumberKey(event)" readonly="true">
                                                                        </div>
                                                                    </div>

                                                                    <!--<label id="position-error" class="error" for="position">This field is required.</label>-->
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="name" class="col-sm-3 ">Email<span style="color:red;font-size: 18px">*</span></label>
                                                                <div class="col-sm-8">
                                                                    <input type="text" class="form-control" id="emailId" value="<?php echo $userinfo->email; ?>" placeholder="Email"  name="fdata[email]"   aria-required="true" aria-invalid="true" readonly="true">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="fname" class="col-sm-3">Service Category<span style="color:red;font-size: 18px">*</span></label>
                                                                                                                <div class="col-sm-9">
                                                                    <?php
                                                                    foreach ($catlist as $cat) {

                                                                        if($cat['status'] == 1){
                                                                        ?>
                                                                        <input type="checkbox" name="fdata[]" value="" disabled="" checked/> 
                                                                        <?php
                                                                         echo $cat['cat_name'];
                                                                        }else {?>
                                                                        <input type="checkbox" name="fdata[]" value="" disabled="" /> 
                                                                        <?php
                                                                        echo $cat['cat_name'];
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="portlet-advance" class="panel panel-default">
                                        <div class="panel-body"> 
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-6">Office Address</label>
                                                        <input type="text" class="form-control " id="address" placeholder="address" name="fdata[address_line1]" value="<?php echo $userinfo->address_line1; ?>"  aria-required="true" aria-invalid="true" >
                                                    </div> 
                                                </div>
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-6 ">Country</label>
                                                        <input type="text" class="form-control " id="country" value="<?php echo $userinfo->countryName; ?>" name="fdata[countryName]"  aria-required="true" aria-invalid="true" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-6">State</label>
                                                        <input type="text" class="form-control " id="administrative_area_level_1" value="<?php echo $userinfo->cityName; ?>"  name="fdata[cityName]"   aria-required="true" aria-invalid="true" >
                                                    </div> 
                                                </div>
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-6">Zip<span style="color:red;font-size: 18px"></span> </label>
                                                        <input type="text" class="form-control " id="postal_code" value="<?php echo $userinfo->zipcode; ?>" placeholder="Zipcode" name="fdata[Zipcode]"  aria-required="true" aria-invalid="true">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="portlet-advance" class="panel panel-default">
                                        <div class="panel-body"> 
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-3">TAX NUMBER</label>
                                                        <input type="text" class="form-control " id="position" name="fdata[tax_num]" value="<?php echo $userinfo->tax_num; ?>" placeholder="Tax number"  aria-required="true" aria-invalid="true" >
                                                    </div> 
                                                </div>
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-9">RADIUS TO ACCEPT JOB IN THE RANGE OF (<?php echo DISTANCE?>)</label>
                                                        <?php
//                                                        $this->load->database();
//                                                        $this->load->library('mongo_db');
//                                                        $priveMd = $this->mongo_db->db;
//                                                        $location = $priveMd->selectCollection('location');
//                                                        $proid = (int) $this->session->userdata('admin_session')['LoginId'];
//                                                        $masData = $location->findOne(array('user' => $proid));
                                                        ?>
                                                        <select class="form-control" name="radius" id="radius">
                                                            <?php
                                                            for ($i = 5; $i < 30; $i++) {
                                                                if ($userdata['radius'] == $i) {
                                                                    ?>
                                                                    <option value="<?php echo $i ?>" selected><?php echo $i ?></option>
                                                                <?php } else { ?>
                                                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-6">Licence Expiry</label>
                                                        <input type="" class="form-control datepicker-components" id="position" name="fdata[board_certification_expiry_dt]" value="<?php echo $userinfo->board_certification_expiry_dt ?>" autocomplete="off">
                                                    </div> 
                                                </div>
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-6">Licence No </label>
                                                        <input type="text" class="form-control " id="position" value="<?php echo $userinfo->medical_license_num; ?>" placeholder=""   name="fdata[medical_license_num]" aria-required="true" aria-invalid="true" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <?php
//                                                $this->load->database();
//                                                $this->load->library('mongo_db');
//                                                $priveMd = $this->mongo_db->db;
//                                                $location = $priveMd->selectCollection('location');
//                                                $proid = (int) $this->session->userdata('admin_session')['LoginId'];
//                                                $masData = $location->findOne(array('user' => $proid));
//                                                $procat = $priveMd->selectCollection('Category');
                                                ?>
                                                <?php
                                                foreach ($catlist as $cat) {

//                                                    $catname = $procat->findOne(array('_id' => new MongoId($cat['cid'])));
                                                    if ($cat['price_set_by'] != 'Admin') {
                                                      
                                                         
                                                        ?>
                                                        <div class="col-sm-5">
                                                            <div class="form-group">
                                                                <label for="name" class="col-sm-6">Price p/m (<?php echo $cat['cat_name']; ?>)</label>

                                                                <input type="text" class="form-control "  name="amt[priceset][]" id="positionn" value="<?php echo $cat['amount'] ?>" placeholder="Price per minute"   aria-required="true" aria-invalid="true"  >
                                                                <input type="hidden" id="dsd" name="amt[catid][]" value="<?php echo $cat['cid'] ?>" />  
                                                            </div>
                                                        </div>  


                                                        <?php
                                                        
                                                    }
                                                }
                                                ?>


                                            </div>



                                        </div>
                                    </div>

                                    <div id="portlet-advance" class="panel panel-default">
                                        <div class="panel-body"> 
                                            <div class="form-group">
                                                <label for="name" class="col-sm-3">About</label>
                                                <div class="col-sm-9">
                                                    <textarea type="text" class="form-control " id="position" placeholder="About" name="fdata[about]"  aria-required="true" aria-invalid="true" ><?php echo $userinfo->about; ?></textarea>
                                                </div>
                                            </div>
                                            <br>
                                            <br>

                                            <div class="form-group">
                                                <label for="name" class="col-sm-3">Expertise</label>
                                                <div class="col-sm-9">
                                                    <input type="text" value="<?php echo $userinfo->expertise; ?>" name="fdata[expertise]"  data-Role="tagsinput" id="addexperties">
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="form-group">
                                                <label for="name" class="col-sm-3 control-label">Languages Known</label>
                                                <div class="col-sm-9">
                                                    <input type="text" value="<?php echo $userinfo->languages; ?>" name="fdata[languages]"  data-Role="tagsinput" id="addlanguages">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div id="portlet-advance" class="panel panel-default">
                                        <div class="panel-body"> 
                                            <div class="form-group">
                                                <label for="name" class="col-sm-3">Practice Certificate</label>

                                                <div class="col-sm-9 ">
                                                    <div data-pages="portlet" class="panel panel-default" id="portlet-basic">
                                                        <div class="panel-heading ">
                                                            <div class="panel-title" style="opacity: .42;">
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="col-sm-6 ">
                                                                <div style="padding-bottom: 12px;padding-top: 21px;">
                                                                    <button class="btn btn-primary btn-cons" type="button" id="board" alt="1">Upload</button>
                                                                    <input type="hidden" value="<?php echo $userinfo->medical_license_pic ?>" id="board_certificate" name="fdata[medical_license_pic]">
                                                                </div>
                                                                <div>

                                                                    <?php
                                                                    $file_formats = array("jpg", "png", "gif", "jpeg");
                                                                    if ($userinfo->medical_license_pic) {

                                                                        $d_img = APP_SERVER_HOST.'pics/' . $userinfo->medical_license_pic;
                                                                    } else
                                                                        $d_img = base_url() . 'theme/assets/img/certificatedft.png';
                                                                    ?>
                                                                    <?php if ($userinfo->medical_license_pic) { ?>
                                                                        <a href = "<?php echo $d_img; ?>" target = "_blank" ><button class="btn btn-success btn-cons" type = "button" > View</button ></a >
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 ">
                                                                <?php
                                                                $ext = substr($d_img, strrpos($d_img, '.') + 1);
                                                                if (!in_array($ext, $file_formats)) {
                                                                    $d_img = base_url() . 'theme/assets/img/certificatedft.png';
                                                                }
                                                                ?>
                                                                <img src="<?php echo $d_img ?>" style="width: 130px;height: 108px;border-radius: 8px;" id="show_board">
                                                            </div>

                                                        </div>
                                                        <!--<img src="pages/img/progress/progress-circle-master.svg" style="display:none"></div>-->

                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" id='doc_id_photo' value="<?php echo $userinfo->doc_id ?>">
                                            <div class="form-group">
                                                <label for="name" class="col-sm-3">JOBS PHOTO</label>
                                                <div class="col-sm-9">
                                                    <div class="col_4">
                                                        <div class="img" id="prof_img_photo">
                                                            <?php
                                                            $jobPhoto = array();
                                                            foreach ($jobsImages as $data) {
                                                                foreach ($data as $imge) {
                                                                    ?>
                                                                    <div class="job_img">
                                                                        <span style="position: absolute;" onclick="deletejobPhoto(this)" ><i class="fa fa-close" style="color:rgba(0, 179, 244, 0.55);font-size: 20px;margin-left: 110px;margin-top: -3px;"></i></span>
                                                                        <img style="width: 126px;height: 127px;"   class="img" id="uimg_photo" src="<?php echo $imge; ?> ">
                                                                        <input type="hidden" name="imgarr[]" value="<?php echo $imge; ?>">
                                                                    </div>
                                                                <?php }
                                                            } ?>
                                                                <!--<img style="-webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);width: 126px;height: 127px;"   class="img-circle" id="uimg_photo" src=" http://104.236.41.101/tutree/pics/aa_default_profile_pic.gif" enctype="multipart/form-data">-->

                                                        </div>
                                                        <div class="img" id="prof_img_photo" style="margin-top: 28px;">
                                                            <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif"></div>
                                                            <button class="btn btn-success btn-cons m-b-10" type="button" id="profile_img_upload_click_photo"><i class="fa fa-cloud-upload"></i> <span class="bold">Upload</span>
                                                            </button>
                                                            
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <br>

                                            <!--    education-->

<!--
                                            <div class="form-group">
                                                <label for="name" class="col-sm-3">Education Information </label>
                                                <div class="col-sm-9">
                                                    <div id="addexperience" >
                                                        <?php
                                                        $count = 0;
                                                        foreach ($education as $result) {
                                                            ?>
                                                            <div id="div<?php echo $result->ed_id ?>" style="border: 1px solid #e6e6e6;" class="col-sm-12">
                                                                <div class="pull-right" style="margin-right: -14px;" ><button class="btn btn-danger  m-b-10" type="button" id="<?php echo $result->ed_id ?>" onclick="deleteeducation(this)">
                                                                        <i class="fa fa-trash-o"></i>
                                                                    </button>
                                                                </div><br>
                                                                <div class="form-group">
                                                                    <label for="name" class="col-sm-3 control-label">DEGREE</label>
                                                                    <div class="col-sm-9">
                                                                        <input type="text" class="form-control " id="position" value="<?php echo $result->degree; ?>" placeholder="Education degree" name="educationupdate[<?php echo $count ?>][degree]"  aria-required="true" aria-invalid="true" maxlength="100">
                                                                    </div>
                                                                    <br><br>
                                                                </div>
                                                                <input type="hidden" name="educationupdate[<?php echo $count ?>][ed_id]"  value="<?php echo $result->ed_id ?>">

                                                                <div class="form-group">
                                                                    <label for="name" class="col-sm-3 control-label">Duration</label>
                                                                    <div class="col-sm-9">


                                                                        <div class="" aria-required="true">


                                                                            <div class="col-sm-4">
                                                                                <select class="form-control" name="educationupdate[<?php echo $count ?>][start_year]">
                                                                                    <?php
                                                                                    for ($i = 1950; $i <= 2016; $i++) {
                                                                                        if ($result->start_year == $i) {
                                                                                            ?>
                                                                                            <option value="<?php echo $i ?>" selected><?php echo $i ?></option>
                                                                                        <?php } else { ?>
                                                                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <select class="form-control" name="educationupdate[<?php echo $count ?>][end_year]">
                                                                                    <?php
                                                                                    for ($i = 1950; $i <= 2016; $i++) {
                                                                                        if ($result->end_year == $i) {
                                                                                            ?>
                                                                                            <option value="<?php echo $i ?>" selected><?php echo $i ?></option>
                                                                                        <?php } else { ?>
                                                                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br><br>
                                                                <div class="form-group">
                                                                    <label for="name" class="col-sm-3 control-label">INSTITUTE</label>
                                                                    <div class="col-sm-9">
                                                                        <textarea class="form-control" name="educationupdate[<?php echo $count ?>][institute]"><?php echo $result->institute; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <?php
                                                            $count++;
                                                        }
                                                        ?>


                                                    </div>
                                                    <div  id="addexperiencedyanamic" >
                                                    </div>

                                                    <a id="otheeducation" class="add_way_point"> <?php
                                                        if (count($education) > 0)
                                                            echo "Add another Education to this.";
                                                        else
                                                            echo "Click to add Education.";
                                                        ?>
                                                    </a>

                                                </div>
                                            </div>-->




                                        </div>
                                    </div>

                                    <div id="portlet-advance" class="panel panel-default">
                                        <div class="panel-body"> 
                                            <center><div class="col-sm-12 error-box" id=profile_val></div></center>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                </div>
                                                <div class="col-sm-6">
                                                    <button class="btn btn-success" type="button" style="width: 229px;" onclick="showMessage()">Submit</button>
                                                    <!--                                                                <button class="btn btn-default"><i class="pg-close"></i> Clear</button>-->
                                                </div>
                                            </div>
                                         
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                 

                                
                            </div>
                        </div>


                    </div>
                    <div class="col-md-1 sm-no-padding"></div>
                </div>
            </div>
        </div>


    </div>
    <!-- END JUMBOTRON -->

    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

</div>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJ1oXlqQZRC5VqUsqwk22f2Fu0PvlwXAI&libraries=places&callback=initMap"
async defer></script>