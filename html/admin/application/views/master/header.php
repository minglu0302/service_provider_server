<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>iServe - Admin Dashboard UI Kit</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
         <link rel="shortcut icon" href="<?php echo base_url() ?>theme/assets/img/demo/favicon.png" />
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>theme/pages/ico/60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>theme/pages/ico/76.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>theme/pages/ico/120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>theme/pages/ico/152.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- BEGIN Vendor CSS-->
        <link href="<?php echo base_url(); ?>theme/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />

        
        <!-- BEGIN Pages CSS-->
        <link href="<?php echo base_url(); ?>theme/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
        <link class="main-stylesheet" href="<?php echo base_url(); ?>theme/pages/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet" type="text/css" media="screen" />
         
        <link href="<?php echo base_url(); ?>theme/assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
        <link href="<?php echo base_url(); ?>theme/assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />

        <link href="<?php echo base_url(); ?>theme/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
<script src="<?php echo base_url(); ?>theme/assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
        <!--[if lte IE 9]>
            <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
        <![endif]-->

        <script type="text/javascript">
            window.onload = function() {
                // fix for windows 8
                if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                    document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/pages/css/windows.chrome.fix.css" />'
            }
        </script>

    </head>

    <body class="fixed-header">
        <div class="page-sidebar" data-pages="sidebar">
            <div id="appMenu" class="sidebar-overlay-slide from-top">
            </div>
            <div class="sidebar-header">
                <div>
                    <img   src="<?php echo base_url(); ?>../pics/on_the_way_admin_logo.png" alt="logo" class="brand" data-src="<?php echo base_url(); ?>../pics/on_the_way_admin_logo.png" data-src-retina="<?php echo base_url(); ?>../pics/on_the_way_admin_logo.png" width="279" height="58" style="margin-left:-33px">
                </div>
            </div>
            <div class="sidebar-menu">
                <ul class="menu-items">
                    <li class="dashboard">
                        <a href="<?php echo base_url(); ?>index.php/masteradmin/Dashboard">
                            <span class="title">DASHBOARD</span>
                        </a>
                        <span class="icon-thumbnail"><img src="<?php echo base_url()?>/theme/pages/img/icons/binu/dashbord.png"></span>
                    </li>
                    
                    
                    <li class="myprofile">
                        <a href="<?php echo base_url(); ?>index.php/masteradmin/profile">
                            <span class="title">MY PROFILE</span>
                        </a>
                        <span class="icon-thumbnail"><img src="<?php echo base_url()?>/theme/pages/img/icons/binu/acc.png"></span>
                    </li>

                    <li class="schedule">
                        <a href="<?php echo base_url(); ?>index.php/masteradmin/schedule">
                            <span class="title">SCHEDULE</span>
                        </a>
                        <span class="icon-thumbnail"><img src="<?php echo base_url()?>/theme/pages/img/icons/binu/workweek.png"></span>
                    </li>


                    <li class="accounting">
                        <a href="<?php echo base_url(); ?>index.php/masteradmin/accounting">
                            <span class="title">ACCOUNTING</span>
                        </a>
                        <span class="icon-thumbnail"><img src="<?php echo base_url()?>/theme/pages/img/icons/binu/acc.png"></span>
                    </li>

                    <li class="allbooking">
                        <a href="<?php echo base_url(); ?>index.php/masteradmin/allbooking">
                            <span class="title">ALL BOOKINGS</span>
                        </a>
                        <span class="icon-thumbnail"><i class="pg-grid"></i></span>
                    </li>

                    <li class="paycycle">
                        <a href="<?php echo base_url(); ?>index.php/masteradmin/paycycle">
                            <span class="title">PAYMENT CYCLE</span>
                        </a>
                        <span class="icon-thumbnail">
                            <img src="<?php echo base_url()?>/theme/pages/img/icons/binu/banking.png">
                        </span>
                    </li>
                    
<!--                    <li class="bank">
                        <a href="<?php echo base_url(); ?>index.php/masteradmin/Bank">
                            <span class="title">BANKING</span>
                        </a>
                        <span class="icon-thumbnail">
                            <img src="<?php echo base_url()?>/theme/pages/img/icons/binu/banking.png">
                        </span>
                    </li>-->

                </ul>
                <div class="clearfix"></div>
            </div>
        </div>


        
        
        
        