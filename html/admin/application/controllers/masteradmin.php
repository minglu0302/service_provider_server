<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Masteradmin extends CI_Controller {

    var $serviceUrl = "";

    public function __construct() {
        parent::__construct();
	//echo  "Master controller";
        $this->load->helper('url');
        $this->load->model("mastermodal");
        $this->load->library('mongo_db');
        $this->load->library('session');
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        $this->serviceUrl = SERVICE_URL;
    }

    public function index($loginerrormsg = NULL) {

        $data['loginerrormsg'] = $loginerrormsg;
        $this->load->view('login', $data);
    }

    public function AuthenticateUser($doc = '') {
        $email = $this->input->post("email");
        $password = $this->input->post("password");

        $status = $this->mastermodal->ValidateSuperAdmin($doc);
         
        if (is_array($status)) {

            $this->index($status['Message']);
        } else if ($status) {
//             if ($this->session->userdata('Role') == 'master')
            if ($this->session->userdata('admin_session')['Role'] == 'master') {
                redirect(base_url() . "index.php/masteradmin/Dashboard");
            }
//              if ($this->session->userdata('Role') == 'super')
            if ($this->session->userdata('admin_session')['Role'] == 'super') {
                redirect(base_url() . "index.php/masteradmin/profile");
            }
        } else {
            $loginerrormsg = "invalid email or password";
            $this->index($loginerrormsg);
        }
    }

    public function Dashboard() {
        $sessionsetornot = $this->mastermodal->issessionset();
        if ($sessionsetornot) {
            $data['todaybooking'] = $this->mastermodal->Getdashboarddata();
            $data['pagename'] = "master/Dashboard";
            $this->load->view("master", $data);
        } else {
            redirect(base_url() . "index.php");
        }
    }

    public function schedule() {
        $sessionsetornot = $this->mastermodal->issessionset();
        if ($sessionsetornot) {
            $data['pagename'] = "master/schedule";
            $this->load->view("master", $data);
        } else {
            redirect(base_url() . "index.php");
        }
    }

    public function GetEvents() {
//        date_default_timezone_set('UTC');
        $this->load->database();
        $this->load->library('mongo_db');
        $priveMd = $this->mongo_db->db;
        $slot = $priveMd->selectCollection('slotbooking');

        $offset = $this->input->get_post('offset');
        $cond = array("doc_id" => (int) $this->session->userdata("admin_session")['LoginId']);

        $getcollection = $slot->find($cond);
        $code = array();
        foreach ($getcollection as $obj) {
            $s_date = date('Y-m-d H:i:s', (int) $obj['start']);
            $e_date = date('Y-m-d H:i:s', (int) $obj['end']);
            $color = 'black';
            if ($obj['booked'] == 1) {
                $ecolor = '#10CFBD';
            } elseif ($obj['booked'] == 2) {
                $ecolor = '#64FE2E';
                $color = 'black';
            } elseif ($obj['booked'] == 3) {
                $ecolor = '#6d5cae';
            } elseif ($obj['booked'] == 4) {
                $ecolor = '#f55753';
            }

            $query = $priveMd->selectCollection('bookings')->findOne(array('slot_id' => (string) $obj['_id']));
            // $query = $this->db->query("select * from bookings where slot_id='" . (string) $obj['_id'] . "'")->row_array();

            $code[] = array(
                'mongoid' => (string) $obj['_id'],
                'slot' => $obj['booked'],
                'title' => $obj['locName'] . ' / ' . $obj['slot_price'] . '$',
                'start' => $s_date,
                'end' => $e_date,
                'color' => $ecolor,
                'lat' => $query['appt_lat'],
                'long' => $query['appt_long'],
                'textColor' => $color
            );
        }

        echo json_encode(array('html' => $code, 't' => $cond));
    }

    public function addslot() {

        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;
        $locs = $db->selectCollection('masterLocations');
        $get_latlng = $locs->findOne(array("_id" => new MongoId($this->input->post('location'))));

       $startTime = $this->input->post('start_time');
      
        $ampm = $this->input->post('start_time_am_pm');
        if ($ampm == "pm") {
            if($startTime == "12")
            {
               $startTime =$startTime;
            }else{
                 $startTime = $startTime + 12;
            }
           
        }else{
             if($startTime == "12")
            {
                $startTime="00";
               $startTime = $startTime;
            }
        }
        
//print_r($startTime);die;
        $startDateTime = $this->input->post('dates') . " " . $startTime . ":00:00";

        $url = $this->serviceUrl . 'addslot';

        $fields = array(
            'ent_pro_id' => (int) $get_latlng['user'],
            'ent_location' => (string) $get_latlng['_id'],
            'ent_option' => $this->input->post('ent_option'),
            'ent_start_time' => $startDateTime,
            'ent_num_slot' => $this->input->post('ent_num_slot'),
            'ent_duration' => $this->input->post('ent_duration'),
            'ent_radius' => $this->input->post('ent_radius'),
            'ent_price' => $this->input->post('ent_price'),
            'ent_repeatday' => $this->input->post('ent_repeatday'),
            'ent_end_date' => $this->input->post('ent_end_date'),
        );

        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
//        print_r($result);// $result;
//        return $result;
    }

    public function addSlots() {

        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;
        $startTime = $this->input->post('start_time') . $this->input->post('start_time_am_pm');

        $startTimeArr = explode(':', date('H:i:s', strtotime($startTime)));
//        echo "<pre>";
//        print_r($startTimeArr);
        $endTime = $this->input->post('end_time') . $this->input->post('end_time_am_pm');
        $endTimeArr = explode(':', date('H:i:s', strtotime($endTime)));
        if ($this->input->post('end_time') <= 12 && $this->input->post('end_time_am_pm') == 'am' && $this->input->post('start_time_am_pm') == 'pm') {
            if ($this->input->post('end_time_am_pm') == 'am') {

                $enddates = date('Y-m-d', strtotime('+1 day', strtotime($this->input->post('dates'))));
            }
        } else {
            $enddates = $this->input->post('dates');
        }
        $dateArr = explode('-', $this->input->post('dates'));

        $enddatesarra = explode('-', $enddates);
        $from_ts = mktime($startTimeArr[0], $startTimeArr[1], 0, $dateArr[1], $dateArr[2], $dateArr[0]);
        $to_ts = mktime($endTimeArr[0], $endTimeArr[1], 0, $enddatesarra[1], $enddatesarra[2], $enddatesarra[0]);
        $locs = $db->selectCollection('masterLocations');
        $ltlng = $db->selectCollection('slotbooking');



        $total_min_slots = round(abs($to_ts - $from_ts) / 60, 2);
        $number_of_slot = round($total_min_slots / 60);
        $get_latlng = $locs->findOne(array("_id" => new MongoId($this->input->post('location'))));

//        echo "from_ts" . $from_ts;
//        echo "to_ts" . $to_ts;
//        echo "total_min_slots" . $total_min_slots;
//        echo "number_of_slot" . $number_of_slot;
//        echo "<br>";

        if ($number_of_slot > 0) {
            $cond = array('doc_id' => (int) $get_latlng['user'], 'start' => array('$gte' => $from_ts), 'end' => array('$lte' => $to_ts));
            $checkOtherSlots = $ltlng->find($cond)->count();
            if ($checkOtherSlots > 0) {
                echo json_encode(array('f' => 1, 'error' => 'Some slots are already created, check the start and end time once again.', 't' => $cond));
            } else {
                $insertQry = array();
                $start = $from_ts;
                $final = $to_ts;
                while ($start <= $final) {
                    $next = $start + (60 * 60);
                    $insertQry = array("doc_id" => (int) $get_latlng['user'],
                        'locId' => (string) $get_latlng['_id'], "locName" => $get_latlng['locName'],
                        "start" => $start, "end" => $next, "loc" => array("longitude" => (double) $get_latlng['lng'], "latitude" => (double) $get_latlng['lat']), 'booked' => 1, 'radius' => (int) $this->input->post('radius'), 'status' => 0, 'slot_price' => $this->input->post('priceforslot'));
                    $ltlng->insert($insertQry);
//                    echo "<pre>";
//                    print_r($insertQry);
                    $start = $start + (60 * 60);
                }
                echo json_encode(array('f' => 0, 'error' => 'Slots added succesfully.'));
            }
        } else {
            echo json_encode(array('f' => 1, 'error' => 'Error in adding slots.' . date('H:i:s', strtotime($startTime)) . '.' . date('H:i:s', strtotime($endTime))));
        }
    }

    public function addRepeatSlots() {

        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;
        $locs = $db->selectCollection('masterLocations');
        $ltlng = $db->selectCollection('slotbooking');

// am pm selection for slot
        $startTime = $this->input->post('start_time') . $this->input->post('start_time_am_pm');
        $startTimeArr = explode(':', date('H:i:s', strtotime($startTime)));
        $endTime = $this->input->post('end_time') . $this->input->post('end_time_am_pm');
        $endTimeArr = explode(':', date('H:i:s', strtotime($endTime)));

// check what Repeation selected everyday ,
        $option = $this->input->post('repeatday');
        if ($option == 1) {
            $dayarr = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
        } else if ($option == 2) {
            $dayarr = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri');
        } else if ($option == 3) {
            $dayarr = array('Sat', 'Sun');
        } else {
            $dayarr = array();
        }
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $end_date = date('Y-m-d H:i:s', strtotime($end_date . ' +1 day'));
        $begin = new DateTime($start_date);
        $end = new DateTime($end_date);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $flag = 0;
        foreach ($period as $dt) {
            $sd = $dt->format("Y-m-d");
            $day = date("D", strtotime($sd));
            if (in_array($day, $dayarr)) {
                if ($this->input->post('end_time') <= 12 && $this->input->post('end_time_am_pm') == 'am' && $this->input->post('start_time_am_pm') == 'pm') {
                    if ($this->input->post('end_time_am_pm') == 'am') {
                        $enddates = date('Y-m-d', strtotime('+1 day', strtotime($sd)));
                    }
                } else {
                    $enddates = $sd;
                }
                $dateArr = explode('-', $sd);
                $enddatesarra = explode('-', $enddates);
                $from_ts = mktime($startTimeArr[0], $startTimeArr[1], 0, $dateArr[1], $dateArr[2], $dateArr[0]);
                $to_ts = mktime($endTimeArr[0], $endTimeArr[1], 0, $enddatesarra[1], $enddatesarra[2], $enddatesarra[0]);
                $total_min_slots = round(abs($to_ts - $from_ts) / 60, 2);
                $number_of_slot = round($total_min_slots / 60);
                $get_latlng = $locs->findOne(array("_id" => new MongoId($this->input->post('location'))));
                if ($number_of_slot > 0) {
                    $cond = array('doc_id' => (int) $get_latlng['user'], 'start' => array('$gte' => $from_ts), 'end' => array('$lte' => $to_ts));
                    $checkOtherSlots = $ltlng->find($cond)->count();
                    if ($checkOtherSlots > 0) {
                        echo json_encode(array('f' => 1, 'error' => 'Some slots are already created, check the start and end time once again.', 't' => $cond));
                    } else {
                        $insertQry = array();
                        $start = $from_ts;
                        $final = $to_ts;
                        while ($start <= $final) {
                            $next = $start + (60 * 60);
                            $insertQry = array("doc_id" => (int) $get_latlng['user'], 'locId' => (string) $get_latlng['_id'], "locName" => $get_latlng['locName'], "start" => $start, "end" => $next, "loc" => array("longitude" => (double) $get_latlng['lng'], "latitude" => (double) $get_latlng['lat']), 'booked' => 1, 'radius' => (int) $this->input->post('radius'), 'status' => 0, 'slot_price' => $this->input->post('priceforslot'));
                            $ltlng->insert($insertQry);
                            $start = $start + (60 * 60);
                        }
                        $flag = 1;
                    }
                } else {
                    $flag = 0;
                }
            }
        }
        if ($flag == 1)
            echo json_encode(array('f' => 0, 'error' => 'Slots added succesfully.'));
        else
            echo json_encode(array('f' => 1, 'error' => 'Error in adding slots.' . date('H:i:s', strtotime($startTime)) . '.' . date('H:i:s', strtotime($endTime))));
    }

    public function removeSlots() {

        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;


        $startTime = $this->input->post('start_time') . $this->input->post('start_time_am_pm');

        $startTimeArr = explode(':', date('H:i:s', strtotime($startTime)));

        $endTime = $this->input->post('end_time') . $this->input->post('end_time_am_pm');

        $endTimeArr = explode(':', date('H:i:s', strtotime($endTime)));


        if ($this->input->post('end_time') <= 12 && $this->input->post('end_time_am_pm') == 'am' && $this->input->post('start_time_am_pm') == 'pm') {
            if ($this->input->post('end_time_am_pm') == 'am') {

                $enddates = date('Y-m-d', strtotime('+1 day', strtotime($this->input->post('dates'))));
            }
        } else {
            $enddates = $this->input->post('dates');
        }

        $dateArr = explode('-', $this->input->post('dates'));
        $enddatesarra = explode('-', $enddates);

//        $from_ts = mktime($startTimeArr[0], $startTimeArr[1], 0, $dateArr[1], $dateArr[2], $dateArr[0]) + ($this->input->post('offset') * 60);
//        $to_ts = mktime($endTimeArr[0], $endTimeArr[1], 0, $enddatesarra[1], $enddatesarra[2], $enddatesarra[0]) + ($this->input->post('offset') * 60);

        $from_ts = mktime($startTimeArr[0], $startTimeArr[1], 0, $dateArr[1], $dateArr[2], $dateArr[0]);
        $to_ts = mktime($endTimeArr[0], $endTimeArr[1], 0, $enddatesarra[1], $enddatesarra[2], $enddatesarra[0]);

        $locs = $db->selectCollection('masterLocations');

        $ltlng = $db->selectCollection('slotbooking');

        $total_min_slots = round(abs($to_ts - $from_ts) / 60, 2);
        $number_of_slot = round($total_min_slots / 60);

        if ($number_of_slot > 0) {

            $deleterec = array("doc_id" => (int) $this->session->userdata("admin_session")['LoginId'], 'start' => array('$gte' => $from_ts), 'end' => array('$lte' => $to_ts));
            $res = $ltlng->remove($deleterec);
            echo json_encode(array('f' => 0, 'error' => 'Slots Deleted succesfully.', 'res' => $res));
        } else {
            echo json_encode(array('f' => 1, 'error' => 'Error in Deleting slots.' . date('H:i:s', strtotime($startTime)) . '.' . date('H:i:s', strtotime($endTime))));
        }
    }

    function refreshshedule() {
        $data['pagename'] = "master/schedule";
        $this->load->view("master", $data);
    }

    public function patientDetails($mas_id = '') {

        $data['pagename'] = 'master/patientDetails';
        $data['mas_id'] = $mas_id;
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('BOOKING ID', 'BOOKING DATE', 'PROVIDER EARNING (' . CURRENCY . ')', 'PAYMENT STATUS');
        $this->load->view("master", $data);
    }

    public function patientDetails_ajax($mas_id = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->mastermodal->patientDetails($mas_id);
    }

    function add_location() {
        $this->load->library('mongo_db');
        $priveMd = $this->mongo_db->db;

        $locs = $priveMd->selectCollection('masterLocations');

        $document = array('user' => (int) $this->session->userdata('admin_session')['LoginId'], 'locName' => $this->input->post("location_name"),
            'address1' => $this->input->post("location_addr1"),
            'address2' => $this->input->post("add2"),
            'lat' => (double) $this->input->post("loc_lat"),
            'lng' => (double) $this->input->post("loc_long")
        );

        if ($locs->insert($document)) {
            $message = ' LOCATION WAS ADDED SUCESSFULLY';
        } else {
            $message = 'Location adding failed';
        }

        $locsCur = $locs->find(array('user' => (int) $this->session->userdata('admin_session')['LoginId']));
        if ($locsCur->count() > 0) {

            $options = '<lable style="color: white;">Select one of your locations *</lable>';
            $options .= '<select class="form-control" id="location" name="location">';

            $table = '<table><tr><th style="padding:5px;">No.</th><th style="padding:5px;">Name</th><th style="padding:5px;">Address</th><th>&nbsp;</th></tr>';

            $i = 1;
            foreach ($locsCur as $loc) {
                $options .= '<option value="' . (string) $loc['_id'] . '"> ' . $loc['locName'] . '</option>';
                $table .= '<tr id="location-' . (string) $loc['_id'] . '"><td style="padding:5px;">' . $i . '</td><td style="padding:5px;"> ' . $loc['locName'] . '</td><td style="padding:5px;">' . $loc['address1'] . ' ' . $loc['address2'] . '</td><td><button class="btn btn-danger btn-xs delete_location" id="' . $loc['_id'] . '" value="Delete" type="button"><i class="fa fa-trash-o "></i></button></td></tr>';
                $i++;
            }
            $options .= '</select>';
            $table .= '</table>';
        } else {
            $options = '<span style="color:red;">* Please add a location to create slots</span><input type="hidden" id="location" value=""/>';
            $table = 'Locations not available currently, add one below.';
        }
        echo json_encode(array('message' => $message, 'options' => $options, 'table' => $table));
    }

    function removelocation() {
        $this->load->library('mongo_db');
        $priveMd = $this->mongo_db->db;
        $locs = $priveMd->selectCollection('masterLocations');
        $res = $locs->remove(array('_id' => new MongoId($this->input->post('item_list'))));
        echo json_encode(array('flag' => 0, 'message' => 'Location deleted'));
    }

    function get_location_list() {

        $this->load->library('mongo_db');
        $priveMd = $this->mongo_db->db;
        $locs = $priveMd->selectCollection('masterLocations');
        $locsCur = $locs->find(array('user' => (int) $this->session->userdata('admin_session')['LoginId']));
        $locations_array = array();
        $code = '';
        if ($locsCur->count() > 0) {


            $code = '<select class="form-control" id="location" name="location">';

            foreach ($locsCur as $loc) {
                $locations_array[] = $loc;
                $code.="<option value='" . (string) $loc['_id'] . "'>" . $loc['locName'] . "</option>";
            }

            $code.='</select>';
        } else {
            $code.='<span style="color:red;">* Please add a location to create slots</span><input type="hidden" id="location" value=""/>';
        }
        echo json_encode(array('html' => $code));
    }

    function multiexplode($delimiters, $string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return $launch;
    }

    public function transection_data_ajax() {

        $this->mastermodal->getTransectionData();
    }

    public function getBilledAmount() {
        $bid = $this->input->post('bid');
        $ser = $this->mastermodal->getBilledAmount($bid);
        echo json_encode($ser);
    }

    public function accounting() {

        $data['gat_way'] = "2";
        $data['pagename'] = "master/accounting";

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->table->clear();
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('JOB ID', 'CUST ID', 'CUST NAME', 'DATE', 'CATEGORY', 'TYPE', 'PRICE MODEL', 'DISCOUNT ("' . CURRENCY . '")', 'BILLED AMOUNT', 'PRO EARNING (' . CURRENCY . ')', 'APP EARNING (' . CURRENCY . ')', 'PG COMMISSION(' . CURRENCY . ')', 'PAYMENT TXN ID', 'STATUS', 'PAYMENT STATUS', 'PAYMENT TYPE', 'INVOICE');
//        $this->table->set_heading('Job Id', 'Provider Id', 'Pro Name', 'Cust Id', 'Cust Name', 'Date', 'Category', 'Type', 'invoice');
        $this->load->view("master", $data);
    }

    public function Transection($status = '') {
        $sessionsetornot = $this->mastermodal->issessionset();
        if ($sessionsetornot) {
            $data['transection_data'] = $this->mastermodal->getBookingData($status);
            $data['$stmsg'] = '0';
            if ($status == '7')
                $data['pagename'] = "master/Transection";
            else
                $data['pagename'] = "master/AllTransection";
            $this->load->view("master", $data);
        } else {
            redirect(base_url() . "index.php");
        }
    }

    public function allbooking() {

        $data['pagename'] = "master/allbooking";
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->table->clear();
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('JOB ID', 'CUST ID', 'CUST NAME', 'DATE', 'CATEGORY', 'APPT TYPE', 'ADDRESS', 'STATUS');
        $this->load->view("master", $data);
    }

    public function allbooking_data_ajax() {
        $this->mastermodal->allbooking_data_ajax();
    }

    public function paycycle() {
        $id = $this->session->userdata('admin_session')['LoginId'];
        $data['duebalance'] = $this->mastermodal->Provider_Pay($id);
//        $data['driverdata'] = $this->mastermodal->Driver_pay($id);
        $data['payrolldata'] = $this->mastermodal->get_payrolldata($id);
//        $data['totalamountpaid'] = $this->mastermodal->Totalamountpaid($id);
        $data['mas_id'] = $id;
        $data['pagename'] = 'master/paycycle';
        $this->load->view("master", $data);
    }

    public function callExel($status = '', $stdate = '', $enddate = '') {
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $data = $this->mastermodal->get_all_data($stdate, $enddate, $status);

//        print_r( array (new ArrayObject (array ('name'=> 'ashish','call' => '123') )) );
        $this->excel->stream('Transaction.xls', $data);
    }

    public function Get_dataformdate($status = '', $stdate = '', $enddate = '') {
//        $data = $this->mastermodal->get_all_data();
        $data['transection_data'] = $this->mastermodal->getDatafromdate($stdate, $enddate, $status);
        $data['stdate'] = $stdate;
        $data['endate'] = $enddate;
        if ($status == '7')
            $data['pagename'] = "master/Transection";
        else
            $data['pagename'] = "master/AllTransection";
        $this->load->view("master", $data);
    }

    public function setOffset() {

        $offset = -($this->input->post('offset'));
        $this->session->set_userdata(array('offset' => $offset));

        echo json_encode(array('offset' => $offset));
    }

    function Get_TransectionDataBy_selected_option($Transectionstatus = '') {
        $data['stmsg'] = $Transectionstatus;
        $data['transection_data'] = $this->mastermodal->getDatafromTransectionstatus($Transectionstatus);
        $data['pagename'] = "master/AllTransection";
        $this->load->view("master", $data);
    }

    public function search_by_select($selectdval = '', $status = '') {
        $data['transection_data'] = $this->mastermodal->getDataSelected($selectdval, $status);
        $data['pagename'] = "master/Transection";
        $this->load->view("master", $data);
    }

    public function profile() {
        $sessionsetornot = $this->mastermodal->issessionset();
        if ($this->session->userdata('admin_session')['LoginId'] == "")
            redirect(base_url() . "index.php");
        if ($sessionsetornot) {
            $data['userinfo'] = $this->mastermodal->getuserinfo();
//            $data['useraddress'] = $this->mastermodal->getuseraddress();
//            $data['gallerydata'] = $this->mastermodal->getgalleryimage();
            $data['education'] = $this->mastermodal->geteducationdata();

//$data['location'] = $this->mastermodal->getuserlocation();
            $data['ser_cat'] = $this->mastermodal->GetAllCatForProvider();
            $data['jobsImages'] = $this->mastermodal->GetjobImages();
            $data['catlist'] = $this->mastermodal->getCatlist();
            $data['userdata']=  $this->mastermodal->GetUserFromMongo();
            $data['pagename'] = "master/profile";
            $this->load->view("master", $data);
        } else {
            redirect(base_url() . "index.php");
        }
    }
    public function GetUserFromMongo()
    {
       
    }

    public function get_new_slot_details() {
        $this->mastermodal->get_new_slot_details();
    }
    
    public function get_schedule_details() {
        $this->mastermodal->get_schedule_details();
    }

    public function services() {

        $sessionsetornot = $this->mastermodal->issessionset();
        if ($sessionsetornot) {

            $data['service'] = $this->mastermodal->getActiveservicedata();
            $data['pagename'] = "master/Addservice";
            $this->load->view("master", $data);
        } else {
            redirect(base_url() . "index.php");
        }
    }

    public function updateservices($tablename = '') {

        $this->mastermodal->updateservices($tablename);
        redirect(base_url() . "index.php/masteradmin/services");
    }

    function deleteservices($tablename = "") {
        $this->mastermodal->deleteservices($tablename);
        redirect(base_url() . "index.php/masteradmin/services");
    }

    function Banking() {
        $sessionsetornot = $this->mastermodal->issessionset();
        if ($sessionsetornot) {

//            $data['service'] = $this->mastermodal->getActiveservicedata();
            $data['pagename'] = "master/banking";
            $this->load->view("master", $data);
        } else {
            redirect(base_url() . "index.php");
        }
    }

    function Bank() {
        $sessionsetornot = $this->mastermodal->issessionset();
        if ($sessionsetornot) {
            $data['pagename'] = "master/bank";
            $data['Bank_Arr'] = $this->mastermodal->GetStripeIdForDoctor();
            $this->load->view("master", $data);
        } else {
            redirect(base_url() . "index.php");
        }
    }

    function Bank_ajax() {
        $sessionsetornot = $this->mastermodal->issessionset();
        if ($sessionsetornot) {
            $Bank_Arr = $this->mastermodal->GetStripeIdForDoctor();
            echo json_encode($Bank_Arr);
        } else {
            redirect(base_url() . "index.php");
        }
    }

    public function AddRecipient() {
        $res = $this->mastermodal->AddRecipient();
        echo json_encode($res);
    }

    public function DeleteRecipient() {
        $res = $this->mastermodal->DeleteRecipient();
        echo json_encode($res);
    }

    public function MakeDefaultRecipient() {
        $res = $this->mastermodal->MakeDefaultRecipient();
        echo json_encode($res);
    }

    public function addservices() {

        $data['service'] = $this->mastermodal->addservices();
        redirect(base_url() . "index.php/masteradmin/services");
    }

    public function booking() {
        $sessionsetornot = $this->madmin->issessionset();
        if ($sessionsetornot) {
            $data['bookinlist'] = $this->madmin->getPassangerBooking();
            $data['pagename'] = "booking";
            $this->load->view("index", $data);
        } else {
            redirect(base_url() . "index.php");
        }
    }

    function Logout() {
        $this->session->unset_userdata('admin_session');
//         print_r($this->session->unset_userdata('admin_session'));die;   
//        $this->session->sess_destroy(   );
        redirect(base_url() . "index.php");
    }

    function udpadedataProfile() {

        $this->mastermodal->updateDataProfile();
        $this->mastermodal->updategallery();
        $this->mastermodal->updateeducation();

        $meta = array('msg' => "Doctor profile has been updated successfully");
        $this->session->set_userdata($meta);

        redirect(base_url() . "index.php/masteradmin/profile");
    }

    function deletegallerydata() {
        $this->mastermodal->deletegallerydata();
    }
    function deletejobPhoto() {
        $res = $this->mastermodal->deletejobPhoto();
        echo json_encode($res);
    }

    function delete_education() {
        $this->mastermodal->delete_education();
    }

    function udpadedata($IdToChange = '', $databasename = '', $db_field_id_name = '') {

        $this->madmin->updateData($IdToChange, $databasename, $db_field_id_name);
        redirect(base_url() . "index.php/masteradmin/profile");
    }

    public function updateMasterBank() {
//        return;
        $ret = $this->mastermodal->updateMasterBank();
        $data['error'] = $ret['flag'];
        $data['pagename'] = "master/banking";
        $this->load->view("master", $data);
    }

    function ForgotPassword() {
        $this->load->database();
        $useremail = $this->input->post('resetemail');

//        header('Content-Type: application/json');
        $resetlink = '';

        $query = $this->db->query("SELECT * FROM doctor where email='" . $useremail . "' ");

        foreach ($query->result() as $row) {
            $name = $row->first_name;
        }

        $this->db->where('email', $useremail);
        $this->db->update('doctor', array('resetData' => $resetlink));

        header('Content-Type: application/json');

        if ($query->num_rows() > 0) {
            $rlink = md5(mt_rand());
            $resetlink = base_url() . "index.php/masteradmin/VerifyResetLink/" . $rlink;
            $template = "<h3> Click below link to reset your password</h3><br><a href=" . $resetlink . ">" . $resetlink . "</a>";
            $to[] = array(
                'email' => $useremail,
                'name' => $name,
                'type' => "to");

            $from = "noreply@privemd.com";
            $subject = "Reset Password Link";
            $this->sendMail($template, $to, $from, $subject);


            $this->db->where('email', $useremail);
            $this->db->update('doctor', array('resetData' => $resetlink));


            echo json_encode(array('succ' => 'Verification link  has been sent to your email !', 'succchk' => '1'));
//            return;
        } else
            echo json_encode(array('err' => 'Invalid email Id', 'errchk' => '2'));
//        return;
    }

    function VerifyResetLink($vlink) {

        $this->load->database();


        $query = $this->db->query("select * from doctor where resetData = '" . base_url() . "index.php/masteradmin/VerifyResetLink/" . $vlink . "'");

        if ($query->num_rows() <= 0) {

            $data['vlink'] = $vlink;
            $this->load->view('master/sessionExp', $data);
        } else {
            $data['vlink'] = $vlink;
            $this->load->view('master/forgotpassword', $data);
        }
    }

    function varifyandsave($vlink) {

        $this->load->database();

//        $this->db->where('resetData', base_url() . "index.php/masteradmin/VerifyResetLink/" . $vlink);
//        $val = $this->db->update('doctor', array('password' => $this->input->post('password')));

        $val = $this->db->query("update doctor set password  = md5('" . $this->input->post('password') . "'),resetData= '0' where resetData = '" . base_url() . "index.php/masteradmin/VerifyResetLink/" . $vlink . "'");

        if ($val) {
            redirect(base_url() . "index.php/masteradmin");
        }
    }
  public function editmasterpassword() {

     
        $this->mastermodal->editmasterpassword();
    }
    function sendMail($template, $to, $from, $subject) {
        require("src/Mandrill.php");

        try {

            $mandrill = new Mandrill('c-weErLVtPsaQaRx1awwhg');
            $message = array(
                'html' => ($template),
                'subject' => $subject,
                'from_email' => $from,
                'from_name' => Appname . 'App',
                'to' => $to,
                'headers' => array('Reply-To' => "noreply@privemd.com"),
                'important' => false,
                'track_opens' => null,
                'track_clicks' => null,
                'auto_text' => null,
                'auto_html' => null,
                'inline_css' => null,
                'url_strip_qs' => null,
                'preserve_recipients' => null,
                'view_content_link' => null,
                'tracking_domain' => null,
                'signing_domain' => null,
                'return_path_domain' => null,
                'merge' => true,
                'merge_language' => 'mailchimp',
                'metadata' => array('website' => 'www.' . Appname . '.com'),
            );

            $async = false;
            $ip_pool = 'Main Pool';
            $result = $mandrill->messages->send($message, $async, $ip_pool);
            $result['flag'] = 0;
            $result['message'] = $message;


            return true;
        } catch (Mandrill_Error $e) {
            return false;
        }
    }

    function delete_schedule() {

        $this->mongo_db->delete('slotbooking', array('_id' => new MongoId($this->input->post('val'))));
        echo json_encode(array('val' => $this->input->post('val')));
    }

    function test_mongo() {

//        $cursor = $this->mongo_db->get_where('location',array('email' => "apriyankaraooo@gmail.com"));
        try {
            $this->load->library('mongo_db');
            $mdb_instance = $this->mongo_db->db;
            $tutree = $mdb_instance->selectCollection('location');
            $cursor = $tutree->find(array('email' => "anu@yahoo.com"));
            foreach ($cursor as $c) {
                echo $c['email'];
            }
        } catch (Exception $e) {
            print_r($e);
        }

        echo 'hi' . $mdb_instance . $tutree;
    }

    function test_dates() {

        error_reporting(0);
        $start = "4/27/2015 11:00";
        $time_end = "4/27/2015 13:00";

        $time_start = strtotime($start);
        $time_end = strtotime($time_end);

        echo "start time - " . $time_start;
        echo "start time - " . $time_end;

        while ($time_start <= $time_end) {
//    $time_end = strtotime('+60 minutes',$time_start);
            $someArr[] = array('start' => $time_start, 'end' => $time_end);
            $time_start = strtotime('+60 minutes', $time_start);

//   $time_end = $time_start +(60*60);
//   $time_end = strtotime('+60 minutes',$time_start);
        }
        print '<pre>';
        print_r($someArr);
        print '</pre>';
    }

    function server_pagination() {


        return $this->mastermodal->server_pagination();
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
