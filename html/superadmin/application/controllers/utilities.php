<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class utilities extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("utilmodal");
        $this->load->library('session');
//        $this->load->library('excel');
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
    }

    function show_help($cat_id, $lan = 0, $scat_id = '') {
        $return['helpText'] = $this->utilmodal->get_cat_hlpText($cat_id);
        $return['lan'] = $lan;
        $this->load->view("show_help", $return);
    }

    function submit_help($lan = '0', $sid = '0') {
        $slaveInfo = $this->utilmodal->get_SlaveDetails($sid);
        $desc = implode("\n", $this->input->post('fDesc'));

        if ($sid > 0) {
            // CREATE JSON FORMATTED VARIABLE TO PASS AS PARAMETER TO API  
            $create = json_encode(
                    array(
                'ticket' => array(
                    'requester' => array(
                        'name' => $slaveInfo->first_name,
                        'email' => $slaveInfo->email
                    ),
                    'group_id' => $this->input->post('zGroup'),
//                'assignee_id' => $arr['new_tick_assignee'],
                    'subject' => $this->input->post('fTitle'),
                    'description' => $desc
                )
                    ), JSON_FORCE_OBJECT
            );

            $data = $this->curlWrap("/tickets.json", $create, "POST");
            $return['msg'] = 'Your requested successfully submitted.';
            $return['flag'] = 0;

            $bookingId = $this->input->post('bid');
            if ($bookingId != '') {
                $this->load->library('mongo_db');
                $this->mongo_db->update('appointments', array('UpdateReview' => 0),array('_id' => (int)$bookingId));
            }

            $this->load->view("submit_help", $return);
        } else {
            $return['msg'] = 'Something went wrong try again.';
            $return['flag'] = 1;
            $this->load->view("submit_help", $return);
        }
    }

    public function curlWrap($url, $json, $action) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_URL, ZDURL . $url);
        curl_setopt($ch, CURLOPT_USERPWD, ZDUSER . "/token:" . ZDAPIKEY);
        switch ($action) {
            case "POST":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                break;
            case "GET":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                break;
            case "PUT":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                break;
            case "DELETE":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;
            default:
                break;
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $output = curl_exec($ch);
        curl_close($ch);
        $decoded = json_decode($output);
        return $decoded;
    }

    public function helpText() {
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }

        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['group'] = $this->utilmodal->get_grp_hlpText();
        $return['helpText'] = $this->utilmodal->get_cat_hlpText();
        error_reporting(0);

        $return['pagename'] = "company/helpText";
        $this->load->view("company", $return);
    }

    function get_subcat() {
        error_reporting(0);
        $this->load->library('mongo_db');
        $res = $this->mongo_db->get_one('hlp_txt', array('cat_id' => (int) $this->input->post('cat_id')));
        echo json_encode($res);
    }

    function grp_action($param = '') {
        error_reporting(0);
        if ($param == 'del') {
            $this->load->library('mongo_db');
            $this->mongo_db->delete('group_hlp', array('grp_id' => (int) $this->input->post('id')));
            echo json_encode(array('msg' => '1'));
            die;
        }
        $this->utilmodal->grp_action();
    }

    function help_cat($param = '', $param2 = '') {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['group'] = $this->utilmodal->get_grp_hlpText();
        $return['helpText'] = '';
        if ($param == 0) {
            $return['edit_id'] = '';
            $return['cat_id'] = $param2;
            $return['helpText'] = $this->utilmodal->get_cat_hlpText($param2, 'add');
        } else {
            
        }

        $return['pagename'] = "company/add_help_cat";
        $this->load->view("company", $return);
    }

    function help_edit($param = '', $param2 = '') {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['group'] = $this->utilmodal->get_grp_hlpText();
//        if($param2 == ''){
        $return['edit_id'] = $param;
        $return['scat_id'] = $param2;
//        }else{
//        }
        $return['helpText'] = $this->utilmodal->get_cat_hlpText($param);
        $return['pagename'] = "company/edit_help_cat";
        $this->load->view("company", $return);
    }

    function help_cat_action($param = '', $param2 = '') {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        if ($param == 'del') {
            $this->load->library('mongo_db');
            if ($param2 == '') {
                $this->mongo_db->delete('hlp_txt', array('cat_id' => (int) $this->input->post('id')));
                echo json_encode(array('msg' => '1'));
                die;
            } else {
                $this->mongo_db->updatewithpull('hlp_txt', array('sub_cat' => array('scat_id' => new MongoId($this->input->post('id')))), array('sub_cat.scat_id' => new MongoId($this->input->post('id'))));
                echo json_encode(array('msg' => '1'));
                die;
            }
        }
        $return['language'] = $this->utilmodal->help_cat_action();

        redirect(base_url() . "index.php/utilities/helpText");
    }

    function lan_help() {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['pagename'] = "company/hlp_language";
        $this->load->view("company", $return);
    }

    function lan_action($param = '') {
        error_reporting(0);
        if($param == 'del'){
            $this->load->library('mongo_db');
            $this->mongo_db->delete('lang_hlp',array('lan_id' => (int)$this->input->post('id')));
            echo json_encode(array('msg' => '1'));die;
        }
        $this->utilmodal->lan_action();
    }

    function cancellation() {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['reasons'] = $this->utilmodal->get_can_reasons();
        $return['pagename'] = "company/cancell_reasons";
        $this->load->view("company", $return);
    }

    function cancell_act($param = '') {
        error_reporting(0);
        if ($param == 'del') {
            $this->load->library('mongo_db');
            $this->mongo_db->delete('can_reason', array('res_id' => (int) $this->input->post('id')));
            echo json_encode(array('msg' => '1'));
            die;
        }
        $this->utilmodal->cancell_act();
    }
    public function checkLanName() {
                
        $cityid = $this->input->post('cid');
        $servicelist = $this->utilmodal->checkcategorynamebycityid($cityid);
        echo json_encode($servicelist);
    }
    function gaurantee($param = '') {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $this->load->model("financemodal");
        $return['gaurantee'] = $this->utilmodal->get_gaurantee($param);
        $return['pagename'] = "company/gaurantee";
        $return['cycleid'] = $param;
        $return['cycle_data'] = $this->financemodal->get_pay_cycle($param)[0];
        $this->load->view("company", $return);
    }

    function get_gaurantee($param = '') {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $data = $this->utilmodal->get_gaurantee('',$param);
        echo json_encode(array('msg' => '1', 'data' => $data));
        die;
    }

    function gaurantee_act($param = '') {
        error_reporting(0);
        if ($param == 'del') {
            $this->load->library('mongo_db');
            $this->mongo_db->delete('gaurantee', array('_id' => new MongoId($this->input->post('id'))));
            echo json_encode(array('msg' => '1'));
            die;
        }
        $this->utilmodal->gaurantee_act($param);
    }
    
    function gaurantee_details($param = '', $param2 = '', $param3 = '') {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" class="table table-hover demo-table-search">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th>',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );

        $this->table->set_template($tmpl);
        $this->table->set_heading('Captain Id', 'Captain Name', 'Number of trips', 'Acceptance rate', 'duty hours (Minutes)', 'Average Rating', 'Total Captain earnings', 'Date', 'Amount credited to driver wallet');
        
        $this->load->model("financemodal");
        $return['gaurantee'] = $param2;
        $return['pagename'] = "company/gaurantee_detail";
        $return['cycleid'] = $param;
        $return['pgtitle'] = ($param3 == 1)?'Guarantee' : 'Bonus';
        $return['cycle_data'] = $this->financemodal->get_pay_cycle($param)[0];
        $this->load->view("company", $return);
    }

    public function datatable_gaurantee_details($param = '', $param2 = '') {
        $this->utilmodal->datatable_gaurantee_details($param, $param2);
    }
    
    function driver_trips($param = '', $param2 = '', $param3 = '') {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" class="table table-hover demo-table-search">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th>',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );

        $this->table->set_template($tmpl);
        $this->table->set_heading('Trip Id', 'Trip Date & Time', 'Payment Method', 'Billed Amount', 'Captain Earning', 'Average Rating');
        
        $return['gaurantee'] = $param2;
        $return['pagename'] = "company/gau_driver_trips";
        $return['cycleid'] = $param;
        $return['txn_id'] = $param3;
        $this->load->library('mongo_db');
        $return['gua_data'] = $gua_data = $this->mongo_db->get_one('gaurantee', array('_id' => new MongoId($param2)));
        $return['pgtitle'] = ($gua_data['actionFor'] == '1')?'Guarantee' : 'Bonus';
        $this->load->model("financemodal");
        $return['cycle_data'] = $this->financemodal->get_pay_cycle($param)[0];
        $return['mas_name'] = $this->db->query('SELECT CONCAT(m.first_name," ",m.last_name) as cap_name FROM master m where mas_id in(select mas_id from DriverTxn where id='.$param3.')')->row()->cap_name;
        $this->load->database();
        $this->load->view("company", $return);
    }
    
    public function datatable_driver_trips($param = '', $param2 = '') {
        $this->utilmodal->datatable_driver_trips($param, $param2);
    }
    
    public function supportText() {
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }

        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['suppText'] = $this->utilmodal->get_cat_support();
        error_reporting(0);

        $return['pagename'] = "company/supportText";
        $this->load->view("company", $return);
    }

    function support_cat($param2 = '', $param = '') {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['group'] = $this->utilmodal->get_grp_hlpText();
        $return['helpText'] = '';
        if ($param2 == 0) {
            $return['edit_id'] = '';
            $return['cat_id'] = $param;
            $return['helpText'] = $this->utilmodal->get_cat_support($param, 'add');
        } else {
            
        }

        $return['pagename'] = "company/add_support_cat";
        $this->load->view("company", $return);
    }

    function support_edit($param = '', $param2 = '') {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        $return['language'] = $this->utilmodal->get_lan_hlpText();
        $return['group'] = $this->utilmodal->get_grp_hlpText();
//        if($param2 == ''){
        $return['edit_id'] = $param;
        $return['scat_id'] = $param2;
//        }else{
//        }
        $return['helpText'] = $this->utilmodal->get_cat_support($param);
        $return['pagename'] = "company/edit_support_text";
        $this->load->view("company", $return);
    }

    function get_subcat_support() {
        error_reporting(0);
        $this->load->library('mongo_db');
        $res = $this->mongo_db->get_one('support_txt', array('cat_id' => (int) $this->input->post('cat_id')));
        echo json_encode($res);
    }

    function support_action($param = '', $param2 = '') {
        error_reporting(0);
        if ($this->session->userdata('table') != 'company_info') {
            redirect(base_url());
        }
        if ($param == 'del') {
            $this->load->library('mongo_db');
            if ($param2 == '') {
                $this->mongo_db->delete('support_txt', array('cat_id' => (int) $this->input->post('id')));
                echo json_encode(array('msg' => '1'));
                die;
            } else {
                $this->mongo_db->updatewithpull('support_txt', array('sub_cat' => array('scat_id' => new MongoId($this->input->post('id')))), array('sub_cat.scat_id' => new MongoId($this->input->post('id'))));
                echo json_encode(array('msg' => '1'));
                die;
            }
        }
        $return['language'] = $this->utilmodal->support_action();

        redirect(base_url() . "index.php/utilities/supportText");
    }
}
