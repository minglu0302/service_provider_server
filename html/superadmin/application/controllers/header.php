<?php require_once 'language.php'; ?>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>../../pics/prive_link.png" />
        <title><?php echo Appname; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>theme/pages/ico/60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>theme/pages/ico/76.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>theme/pages/ico/120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>theme/pages/ico/152.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN Vendor CSS-->
        <link href="<?php echo base_url(); ?>theme/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
        <!--<link href="<?php echo base_url(); ?>theme/assets/cssextra/style.css" rel="stylesheet" type="text/css" media="screen"/>-->
        <!--<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />-->
        <!-- BEGIN Pages CSS-->
        <link rel="stylesheet" href="<?php echo base_url() ?>theme/assets/css/jquery-ui.css" type="text/css" media="screen"/>
        <link href="<?php echo base_url(); ?>theme/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/assets/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css" />
        <link class="main-stylesheet" href="<?php echo base_url(); ?>theme/pages/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet" type="text/css" media="screen" />

        <script src="<?php echo base_url(); ?>theme/assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
        <!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->


        <link href="<?php echo base_url(); ?>theme/assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>theme/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
        <link href="<?php echo base_url(); ?>theme/assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />

        <!--<link href="<?php echo base_url(); ?>theme/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">-->

        <!--[if lte IE 9]>
            <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
        <![endif]-->

        <script type="text/javascript">
            window.onload = function () {
                // fix for windows 8
                if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                    document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/pages/css/windows.chrome.fix.css" />'
            }
        </script>
        <style>
            .form-control{  height: 38px;
            }
            span .title{

                width: 100% !important;
            }
            .page-sidebar .sidebar-menu .menu-items > li > a {

                font-size: 11px;

            }
        </style>
        <script>
            $(document).ready(function () {
            var headerArr = {
                    'company/Dashboard': '.dashboard',
                    'company/cities': '.cities',
                    'company/addnewcity': '.cities',
                    'company/catogiriesservices': '.company_s',
//                    'company/catogiriesservices': '.company_s',
//                    'company/add_edit': '.company_s',
                    'company/vehicle_type': '.vehicle_type',
                    'company/vehicletype_addedit': '.vehicle_type',
                    'company/vehicle_models': '.vehicle_models',
                    'company/dispatchZones': '.dis_zones',
                    'company/zones': '.zones',
                    'company/vehicles': '.Vehicles',
                    'company/addnewvehicle': '.Vehicles',
                    'company/editvehicle': '.Vehicles',
                    'company/drivers': '.Drivers',
                    'company/addnewdriver': '.Drivers',
                    'company/editdriver': '.Drivers',
                    'company/aceptencerate': '.Drivers_arate',
                    'company/shiftDetails': '.Drivers_arate',
                    'company/passengers': '.passengers',
                    'company/onGoing_jobs': '.on_Going_jobs',
                    'company/completed_jobs': '.completed_jobs',
                    'company/trip_details': '.completed_jobs',
                    'company/bookings': '.bookings',
                    'customerwallet/CustomerList': '.bookings',
//                    'company/payroll' : '.payroll',
                    'company/Transection': '.transection',
                    'company/driver_review': '.driver_review',
                    'company/disputes': '.disputes',
                    'company/delete': '.delete',
                    'company/compaigns': '.compaigns',
                    'Coupons/zonesCoupons': '.campaigns_new',
                    'Coupons/refferal': '.campaigns_new',
                    'Coupons/promotions': '.campaigns_new',
                    'company/notificationConsole': '.notifyi',
                    'company/RediousPrice': '.radiousPrice',
                    'customerwallet/UserWalletList': '.driverRecharge',
                    'customerwallet/UsertransectionStatement': '.driverRecharge',
                    'customerwallet/UserWalletStatement': '.driverRecharge',
                    'company/payment_cycle': '.paycycle',
                    'company/operator_payouts': '.paycycle',
                    'company/drivers_payouts': '.paycycle',
                    'company/drivers_payDetails': '.paycycle',
                    'company/drivers_trips': '.paycycle',
                    'company/gaurantee': '.paycycle',
                    'company/gaurantee_detail': '.paycycle',
                    'company/gau_driver_trips': '.paycycle',
                    'company/operator_paylogs': '.paylogs',
                    'company/hlp_language': '.lang',
                    'company/helpText': '.helptext',
                    'company/add_help_cat': '.helptext',
                    'company/edit_help_cat': '.helptext',
                    'company/supportText': '.suptext',
                    'company/add_support_cat': '.suptext',
                    'company/edit_support_text': '.suptext',
                    'company/cancell_reasons': '.can_reason',
                    'company/configuration': '.configuration',
                    'company/manageRole': '.manageRole',
                    'godsview': '.godsview'
                };
                
                var pgname = '<?= $pagename ?>';
                
                var actcls = headerArr[pgname];
                if (typeof actcls !== "undefined") {
                    $(actcls).addClass('active');
                    var img = $(actcls + ' img').attr('src');
                    img = img.replace("off.png", "on.png");
                    $(actcls + ' img').attr('src', img);
                }
            });
        </script>
    </head>
    
    <body class="fixed-header">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar" data-pages="sidebar">
            <div id="appMenu" class="sidebar-overlay-slide from-top">
            </div>
            <!-- BEGIN SIDEBAR HEADER -->
            <div class="sidebar-header">
                <div>
                    <img   src="<?php echo base_url(); ?>../pics/on_the_way_admin_logo.png" alt="logo" class="brand" data-src="<?php echo base_url(); ?>../pics/on_the_way_admin_logo.png" data-src-retina="<?php echo base_url(); ?>../pics/on_the_way_admin_logo.png" width="279" height="58" style="margin-left:-33px">
                </div>
            </div>
            <!-- END SIDEBAR HEADER -->
            <!-- BEGIN SIDEBAR MENU -->
            <div class="sidebar-menu">
                <ul class="menu-items">
                    <?php $request_uri = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>


                    <li class="dashboard">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/Dashboard">
                            <span class="title"><?php echo NAV_DASHBOARD; ?></span>
                        </a>
                        <span class="icon-thumbnail <?php echo (base_url() . "index.php/superadmin/Dashboard" == $request_uri ? "bg-success" : ""); ?>">
                            <img src="<?php echo base_url(); ?>/theme/icon/dasboard_off.png" class="dashboard_thumb"></i></span>
                    </li>

                    <li class="cities">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/cities">
                            <span class="title"><?php echo NAV_CITIES; ?></span>
                        </a>
                        <span class="icon-thumbnail cities_thumb" >
                            <img src="<?php echo base_url(); ?>/theme/icon/cities_off.png" class="cities_thumb">
                        </span>
                    </li>

                    <li class="company_s">
                        <a href="<?php echo base_url(); ?>index.php/category/categories_services/1">
                            <span class="title"><?php echo NAV_COMPANYS; ?></span>
                        </a>
                        <span class="icon-thumbnail company_sthumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/service_categories_off.png" class="company_s"></span>
                    </li>

<!--                    <li class="sgroup">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/sgroup">
                            <span class="title"><?php echo NAV_SERVICES_GROUP; ?></span>
                        </a>
                        <span class="icon-thumbnail passengers_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/passanger_off.png" class="s">
                        </span>
                    </li>

                    <li class="company_s">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/cservices">
                            <span class="title"><?php echo NAV_SERVICES; ?></span>
                        </a>
                        <span class="icon-thumbnail company_sthumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/companies_off.png" class="company_s"></span>
                    </li>-->





                    <li class="Drivers">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/Drivers/my/1">
                            <span class="title"><?php echo NAV_DRIVERS; ?></span>
                        </a>
                        <span class="icon-thumbnail  driver_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/providers_off.png" class="Drivers">
                        </span>
                    </li>

                    <li class="passengers">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/patients/3">
                            <span class="title"><?php echo NAV_PASSENGERS; ?></span>
                            <!--<span class="details">Details</span>-->
                        </a>
                        <span class="icon-thumbnail passengers_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/passanger_off.png" class="passengers">
                        </span>
                    </li>

                    <li class="on_Going_jobs">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/onGoing_jobs/">
                            <span class="title">ON GOING JOBS</span>
                        </a>
                        <span class="icon-thumbnail passengers_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/ongoingjobs_off.png" class="on_Going_jobs">
                        </span>
                    </li>



                    <li class="bookings">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/bookings/11">
                            <span class="title"><?php echo NAV_BOOKINGS; ?></span>
                        </a>
                        <span class="icon-thumbnail booking_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/livebookingfeed_off.png" class="bookings">
                        </span>
                    </li>

                    <li class="Drivers_arate">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/aceptencerate">
                            <span class="title"><?php echo NAV_DRIVERS_ACCEPTENCE; ?></span>
                        </a>
                        <span class="icon-thumbnail  driver_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/provider_acceptence_off.png" class="Drivers_arate">
                        </span>
                    </li>

                    <!--                    <li class="dispatcher">
                                            <a target="_blank" href="<?php echo base_url(); ?>index.php/superadmin/dispatcher">
                                                <span class="title"><?php echo NAV_DISPATCHER; ?></span>
                                            </a>
                                            <span class="icon-thumbnail compaigns_thumb">
                                                <img src="<?php echo base_url(); ?>/theme/icon/godsview_off.png" class="godsview">
                                            </span>
                                        </li>-->

                    <li class="payroll">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/payroll">
                            <span class="title"><?php echo NAV_PAYROLL; ?></span>
                        </a>
                        <span class="icon-thumbnail payroll_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/payroll_off.png" class="payroll">
                        </span>
                    </li>

                    <!--                    <li class="wallet">
                                            <a href="<?php echo base_url(); ?>index.php/superadmin/ProRecharge">
                                                <span class="title"><?php echo NAV_WALLLET; ?></span>
                                            </a>
                                            <span class="icon-thumbnail payroll_thumb">
                                                <img src="<?php echo base_url(); ?>/theme/icon/wallet_icon.png" class="wallet">
                                            </span>
                                        </li>-->

                    <li class="commission">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/commission">
                            <span class="title"><?php echo NAV_COMMISSION; ?></span>
                        </a>
                        <span class="icon-thumbnail payroll_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/comission_off.png" class="payroll">
                        </span>
                    </li>


                    <li class="transection">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/transaction">
                            <span class="title"><?php echo NAV_ACCOUNTING; ?></span>
                        </a>
                        <span class="icon-thumbnail">
                            <img src="<?php echo base_url(); ?>/theme/icon/accounting_off.png" class="transection">
                        </span>
                    </li>


                    <li class="driver_review">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/doctor_review/1">
                            <span class="title"><?php echo NAV_DRIVERREVIEW; ?></span>
                        </a>
                        <span class="icon-thumbnail driver_review_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/driver review_off.png" class="driver_review">
                        </span>
                    </li>

                    <li class="disputes">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/disputes/1">
                            <span class="title"><?php echo NAV_DISPUTES; ?></span>
                        </a>
                        <span class="icon-thumbnail driver_review_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/dispuite_off.png" class="disputes">
                        </span>
                    </li>

                    <li class="compaigns">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/compaigns/1">
                            <span class="title"><?php echo NAV_COMPAIGNS; ?></span>
                        </a>
                        <span class="icon-thumbnail compaigns_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/campaigns_off.png" class="compaigns">
                        </span>
                    </li>

                    <li class="lang">
                        <a href="<?php echo base_url(); ?>index.php/utilities/lan_help">
                            <span class="title">LANGUAGE</span>
                        </a>
                        <span class="icon-thumbnail">
                            <img src="<?php echo base_url(); ?>/theme/icon/language_off.png" class="lang">
                        </span>
                    </li>

                    <!--                    <li class="helptext">
                                            <a href="<?php echo base_url(); ?>index.php/utilities/helpText">
                                                <span class="title">HELP TEXT</span>
                                            </a>
                                            <span class="icon-thumbnail">
                                                <img src="<?php echo base_url(); ?>/theme/icon/driver review_off.png" class="helptext">
                                            </span>
                                        </li>-->

                    <li class="suptext">
                        <a href="<?php echo base_url(); ?>index.php/utilities/supportText">
                            <span class="title">SUPPORT TEXT</span>
                        </a>
                        <span class="icon-thumbnail">
                            <img src="<?php echo base_url(); ?>/theme/icon/support_text_off.png" class="suptext">
                        </span>
                    </li>

                    <li class="can_reason">
                        <a href="<?php echo base_url(); ?>index.php/utilities/cancellation">
                            <span class="title">CANCELLATION REASONS</span>
                        </a>
                        <span class="icon-thumbnail">
                            <img src="<?php echo base_url(); ?>/theme/icon/cancellation_resaon_off.png.png" class="can_reason">
                        </span>
                    </li>

                    <li class="can_reason">
                        <a href="<?php echo base_url(); ?>index.php/superadmin/pgateway">
                            <span class="title">PAYMENT GATEWAY</span>
                        </a>
                        <span class="icon-thumbnail">
                            <img src="<?php echo base_url(); ?>/theme/icon/payment_gateway_off.png" class="p_gateway">
                        </span>
                    </li>

                    <li class="godsview">
                        <a target="_blank" href="<?php echo base_url(); ?>index.php/superadmin/godsview">
                            <span class="title">GODSVIEW</span>
                        </a>
                        <span class="icon-thumbnail compaigns_thumb">
                            <img src="<?php echo base_url(); ?>/theme/icon/godsview_off.png" class="godsview">
                        </span>
                    </li>




                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
















