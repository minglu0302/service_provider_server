<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("catmodal");
        $this->load->library('session');
//        $this->load->library('excel');
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
    }

    public function index($loginerrormsg = NULL) {
        $data['loginerrormsg'] = $loginerrormsg;

        $this->load->view('company/login', $data);
    }

    public function setcity_session() {

        $meta = array('city_id' => $this->input->post('city'), 'company_id' => $this->input->post('company'));
        $this->session->set_userdata($meta);
    }

                

   
    public function EditServiceGroup() {
       
        $this->catmodal->EditServiceGroup();
    }
 // Start-code Service Category page 
//----service category page---
    public function categories_services($status = '') {
       
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 35px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SI NO', 'CITY NAME','CURRENCY', 'NO OF CATEGORY');
        //data get
         
        $data['status'] = $status;
//        $data['entitylist'] = $this->catmodal->GetAllCategories();
//        $data['entitylist_products'] = $this->catmodal->GetAllProducts();
        $data['citys'] = $this->catmodal->city();
        $data['cityc'] = 'df';
        $data['pagename'] = "company/catogiriesservices";
        
        $this->load->view("company", $data);
    }
//end code for service category page
    
//Service category --datatable--
    function datatable_catogiries_services() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->catmodal->datatable_catogiries_services();
    }
//end data-table
    
// add new category in service category page
    public function AddNewCategory() {
       if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->catmodal->AddNewCategory();
        $city_id = $this->input->post("city_id");
        $CategoryId = $this->input->post("CategoryId");
        $addbygroup = $this->input->post("addbygroup");
        
        if($addbygroup != '')
        {
             redirect(base_url() . "index.php/category/show_cat/" .$city_id);
        }else if ($CategoryId != '') {
               redirect(base_url() . "index.php/category/show_cat/" .$city_id);
        } else {
               redirect(base_url() . "index.php/category/categories_services/1");
        }
       
    }            
//end code
// end-code Service Category page    

// Start-code Category page 
//category page     
    public function show_cat($cityid) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['business'] = array();
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 35px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SL NO', 'CATEGORY NAME', 'DESCRIPTION', 'IMAGE',  'PRICING MODEL','NO OF GROUPS','CHANGE ORDER', 'SELECT');
        $data['cityid'] = $cityid;
        $data['citylist'] = $this->catmodal->get_cities();
//        $data['catlist'] = $this->catmodal->GetAllServices();
                       
        $data['pagename'] = 'company/showCategory';
        $this->load->view("company", $data);
    }
    public function changeCatOrder() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->catmodal->changeCatOrder();
    }

//end category page 

// category page data-table    
    public function datatable_show_cat($cityid) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->catmodal->datatable_show_cat($cityid);
    }
//end data-table

// Add-service group in category page
    public function AddServiceGroup() {
        $this->catmodal->AddServiceGroup();
    }
//end-code
    
// Edit- Category in category page
    
    public function GetOneServices() {
        $sid = $this->input->post('sid');
        $ser = $this->catmodal->GetOneServices($sid);
        echo json_encode($ser);
    }
//end-code

//Delete Category in category page
    public function DeleteServices() {
        $status = $this->catmodal->DeleteServices();
    }
//end-code
// end-code Service Category page  

//Start-code Service group page
    
//service group page
    public function sgroup($parms = '') {

        $data = array();
        $data['catId'] = $parms;
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 35px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SI NO', 'GROUP NAME', 'NO OF SERVICES', 'SELECT');

        $data['citylist'] = $this->catmodal->get_cities();
        $result = $this->catmodal->get_citiesname($parms);
        $data['cityIdss'] = $result['city_id'];
        $data['cat_name'] = $result['cat_name'];
        $data['pagename'] = 'company/servicegroup';
        $this->load->view("company", $data);
    }
//end-code
//data-table for service group
    public function datatable_sgroup($parms = '') {
        $this->catmodal->datatable_sgroup($parms);
    }

//end-code data table
//Edit-get Service group

    public function EditGetServiceGroup() {
        $this->catmodal->EditGetServiceGroup();
    }
//end-code
// Delete- Service group 
    public function DeleteServiceGroup() {
        $data = $this->catmodal->DeleteServiceGroup();
    }

// end-code Service group page  

//Start-code Services page

 //sevice page
    public function show_services($parms) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $group = array();
        $data['gid']  = $parms;
        $data['business'] = array();
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 35px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SL NO', 'SERVICE NAME', 'DESCRIPTION', 'FIXED PRICE', 'OPTION');
//        $data['cityid'] = $cityid;
        $data['citylist'] = $this->catmodal->get_cities();
        
        $result=$this->catmodal->get_citiesnamebygroup($parms);
        $data['cityIdss'] =$result['city_id'] ;
        $data['cat_name'] =$result['cat_name']; 
        $group = $this->catmodal->get_groupName($parms);
        $data['group_name'] = $group['groupsname'];
        $data['catId'] = $group['catid'];
//        $data['catlist'] = $this->superadminmodal->GetAllServices();
//        $data['grouplist'] = $this->superadminmodal->GetGroupByCategory();
        $data['pagename'] = 'company/showservices';
        $this->load->view("company", $data);
    }
 
// services page data-table
    public function datatable_show_services($cityid) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->catmodal->datatable_show_services($cityid);
    }
//Add-Services
    public function AddService() {
        $this->catmodal->AddService();
    }
//Get Services
    public function GetOneSubServices() {
        $sid = $this->input->post('sid');
      
         $this->catmodal->GetOneSubServices($sid);
    }
 //Edit-service update
    public function EditServiceby() {
        $this->catmodal->EditServiceby();
    }
    
 //Delete- services
    public function DeleteSubCategory() {
        $status = $this->catmodal->DeleteSubCategory();
    }
 
// end-code Services page  
      

    public function checkcategorynamebycityid() {
                
        $cityid = $this->input->post('cid');
        $servicelist = $this->catmodal->checkcategorynamebycityid($cityid);
        echo json_encode($servicelist);
    }
  
    public function sessiondetails($value = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['value'] = $value;
        $data['session_details'] = $this->superadminmodal->get_sessiondetails($value);

        $data['pagename'] = "company/sessiondetails";
        $this->load->view("company", $data);
    }

    public function document($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['status'] = $status;

        $data['master'] = $this->superadminmodal->driver();

        $data['document_data'] = $this->superadminmodal->get_documentdata($status);

        $data['workname'] = $this->superadminmodal->get_workplace();


        $data['pagename'] = "company/document";
        $this->load->view("company", $data);
    }

    

    

    
 

    public function services() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $sessionsetornot = $this->superadminmodal->issessionset();
        if ($sessionsetornot) {

            $data['service'] = $this->superadminmodal->getActiveservicedata();
            $data['pagename'] = "company/Addservice";
            $this->load->view("company", $data);
        } else {
            redirect(base_url() . "index.php/superadmin");
        }
    }

    

                

    function Logout() {

        $this->session->sess_destroy();
        redirect(base_url() . "index.php/superadmin");
    }

                

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */