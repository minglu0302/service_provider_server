<?php

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");

require_once '/var/www/html/Models/StripeModule.php';
require_once '/var/www/html/Models/config.php';
require_once '/var/www/html/Models/sendAMail.php';
require_once 'StripeModuleNew.php';
require_once 'newstripe.php';
require_once('S3.php');
require 'aws.phar';
require_once '/var/www/html/Models/AwsPush.php';

class Superadminmodal extends CI_Model {

    var $serviceUrl = "";

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->database();
        $this->load->library('mongo_db');
        $this->serviceUrl = SERVICE_URL;
    }

    function validateSuperAdmin() {

        $email = $this->input->post("email");
        $password = $this->input->post("password");

        $this->load->library('mongo_db');
        $result = $this->mongo_db->get_one('admin_users', array('email' => $email, 'pass' => md5($password)));
        $role = 'Super Admin';
        if ($result['role'] != $role) {
            $role = $this->mongo_db->get_one('admin_roles', array('_id' => new MongoId($result['role'])));
            $role = $role['role_name'];
        }
        $_SESSION['admin'] = 'super';
        if (count($result) > 0) {
            $tablename = 'company_info';
            $LoginId = 'company_id';
            $sessiondata = array(
                'emailid' => $email,
                'password' => $password,
                'LoginId' => $LoginId,
                'profile_pic' => $result['logo'],
                'first_name' => $result['name'],
                'access_rights' => $result['access'],
                'table' => $tablename,
                'city_id' => $result['city'],
                'city' => $result['city'],
                'role' => $role,
                'company_id' => '0',
                'validate' => true,
                'superadmin' => '1',
                'mainAdmin' => $result['superadmin']
            );

            $this->session->set_userdata($sessiondata);
            return true;
        }

        return false;
    }

    function getallDispute() {
        $this->load->library('mongo_db');

        $catName = $this->mongo_db->get_one('config', array('_id' => (int) 1));


        return $catName;
    }

    function savedispute_confing() {


        $booking_second = $this->input->post('booking_second');
        $booking_second_later = $this->input->post('booking_seconds_later');
        $expire_now = $this->input->post('expire_now');
        $expire_later = $this->input->post('expire_later');
        $apikey1 = $this->input->post('apikey1');
        $apikey2 = $this->input->post('apikey2');
        $apikey3 = $this->input->post('apikey3');
        $apikey4 = $this->input->post('apikey4');
        $apikey5 = $this->input->post('apikey5');
        $apikey6 = $this->input->post('apikey6');
        $apikey7 = $this->input->post('apikey7');
        $apikey8 = $this->input->post('apikey8');
        $apikey9 = $this->input->post('apikey9');
        $apikey10 = $this->input->post('apikey10');
        $apikey11 = $this->input->post('apikey11');
        $apikey12 = $this->input->post('apikey12');
        $pt = $this->input->post('pt');
        $pul = $this->input->post('pul');

        $ios_pro_app_version = $this->input->post('ios_pro_app_version');
        $ios_cust_app_version = $this->input->post('ios_cust_app_version');
        $and_pro_app_version = $this->input->post('and_pro_app_version');
        $and_cust_app_version = $this->input->post('and_cust_app_version');
        $later_booking_direct_accepted = $this->input->post('later_booking_direct_accepted');

        $this->load->library('mongo_db');

        $update = array('ios_pro_app_version' => $ios_pro_app_version, 'later_booking_direct_accepted' => (int) $later_booking_direct_accepted, 'provider_timeout' => (int) $pt, 'provider_update_location' => (int) $pul,
            'ios_cust_app_version' => $ios_cust_app_version,
            'and_pro_app_version' => $and_pro_app_version,
            'and_cust_app_version' => $and_cust_app_version,
            'apikey1' => $apikey1, 'apikey2' => $apikey2, 'apikey3' => $apikey3, 'apikey4' => $apikey4, 'apikey5' => $apikey5, 'apikey6' => $apikey6, 'apikey7' => $apikey7, 'apikey8' => $apikey8, 'apikey9' => $apikey9, 'apikey10' => $apikey10, 'apikey11' => $apikey11, 'apikey12' => $apikey12,
            'booking_seconds' => (int) $booking_second, "booking_seconds_later" => (int) $booking_second_later, "expire_now" => (int) $expire_now, "expire_later" => (int) $expire_later);
        $data = $this->mongo_db->update('config', $update, array('_id' => (int) 1));
        echo json_encode(array("msg" => "your configuration setting is save!", "flag" => 1));
        return;
    }

    function validatePhone() {

        $query = $this->db->query("select doc_id from doctor where mobile='" . $this->input->post('mobile') . "'");
        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => '1'));
            return;
        } else {
            echo json_encode(array('msg' => '0'));
        }
    }

//    function validateSuperAdmin() {
//
//        $email = $this->input->post("email");
//        $password = $this->input->post("password");
//        $offset = $this->input->post("offset");
//
//        $queryforslave = $this->db->get_where('superadmin', array('username' => $email, 'password' => md5($password)));
//        $res = $queryforslave->row();
//
//        if ($queryforslave->num_rows > 0) {
//            $tablename = 'company_info';
//            $LoginId = 'company_id';
//            $sessiondata = $this->setsessiondata($tablename, $LoginId, $res, $email, $password, $offset);
//            $this->session->set_userdata($sessiondata);
//            $this->load->library('nativesession');
//            $_SESSION['admin'] = 'super';
//            return true;
//        }
//
//        return false;
//    }

    function updateeducation() {


        $education = $this->input->post('education');

        if (!empty($education)) {

            if ($education) {
                for ($i = 0; $i < count($education); $i++) {

                    $data[$i] = array('degree' => $education[$i]['degree'], 'end_year' => $education[$i]['end_year'], 'start_year' => $education[$i]['start_year'], 'institute' => $education[$i]['institute'], 'doc_id' => $this->session->userdata("admin_session")['LoginId']);

                    if ($education[$i]['degree'] != '' || $education[$i]['end_year'] != 'null' || $education[$i]['start_year'] != 'null' || $education[$i]['institute'] != '')
                        $this->db->insert('doctor_education', $data[$i]);
                }
            }
        }

        $educationupdate = $this->input->post('educationupdate');

        if ($educationupdate) {
            for ($i = 0; $i < count($educationupdate); $i++) {

                $data[$i] = array('degree' => $educationupdate[$i]['degree'], 'end_year' => $educationupdate[$i]['end_year'], 'start_year' => $educationupdate[$i]['start_year'], 'institute' => $educationupdate[$i]['institute']);

                $id = $educationupdate[$i]['ed_id'];
                $this->db->update('doctor_education', $data[$i], array('ed_id' => $id));
            }
        }
    }

    function datatable_operators() {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');

//        if ($this->input->post('sSearch') != '') {
//            $cond = array('operator.name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
//        } else {
//            $cond = array();
//        }
        $num = 0;
        $data = array();
        $opData = $this->mongo_db->get_where('operators', $cond);
        foreach ($opData as $row) {
            $num++;
            $prolist = count($row['prolist']);
            $row['oid'] = (string) $row['_id'];
            $data[] = array($num,
                "<a href='http://iserve.ind.in/operator/index.php/operatoradmin/AuthenticateUser/" . $row['oid'] . "'  target='_blank'><b>" . $row['operator_name'] . "</b></a>"
                , $row['operator_address'], $row['manager_name'],
                $row['operator_phone'], $row['operator_email'],
                "<a href='" . base_url() . "index.php/superadmin/oprdetails/" . $row['oid'] . " '><b>" . $prolist . "</b></a>",
                "<input type='checkbox' class='checkbox' data='" . $row['oid'] . "' value='" . $row['oid'] . "'>");
        }
        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = strtoupper($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(strtoupper($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function insertoperator() {
        $oname = $this->input->post('oname');
        $oaddress = $this->input->post('oaddress');
        $mname = $this->input->post('mname');
        $ophone = $this->input->post('ophone');
        $oemail = $this->input->post('oemail');
        $opassword = $this->input->post('opassword');

        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;
        $selecttb = $db->selectCollection('operators');

        $insert = array(
            "operator_name" => $oname,
            "operator_address" => $oaddress,
            "manager_name" => $mname,
            "operator_phone" => $ophone,
            "operator_email" => $oemail,
            "operator_password" => md5($opassword),
            "prolist" => array()
        );

        $selecttb->insert($insert);
        echo json_encode(array('msg' => "Operator added successfully", 'flag' => 1));
        return;
    }

    function editoperatorpass() {
        $this->load->library('mongo_db');
        $oid = $this->input->post('oid');
        $pass = $this->input->post('opasswordedit');

        $mongoArr = array(
            "operator_password" => md5($pass)
        );

        $this->mongo_db->update('operators', $mongoArr, array('_id' => new MongoId($oid)));
        echo json_encode(array('msg' => "Operator password updated successfully", 'flag' => 1));
        return;
    }

    function oprresetpassword() {
        $this->load->library('mongo_db');
        $oidArr = $this->input->post('val');
        $config = new config();
        $sendMail = new sendAMail($config->getHostUrl());
        foreach ($oidArr as $oid) {
            $oprdata = $this->mongo_db->get_one('operators', array('_id' => new MongoId($oid)));
            $randData = md5(mt_rand());

            $this->mongo_db->update('operators', array('resetData' => $randData), array('_id' => new MongoId($oid)));
            $status = $sendMail->forgotPasswordOperator($oprdata, $randData);
            print_r($status);
        }
        echo json_encode(array('msg' => "Reset Password mail Sent successfully", 'flag' => 1));
        return;

//         function ForgotPassword() {
//
//        $useremail = $this->input->post('resetemail');
//        $resetlink = '';
//        $query = $this->db->query("SELECT * FROM superadmin where username='" . $useremail . "' ");
//        foreach ($query->result() as $row) {
//            $name = $row->username;
//        }
//        $this->db->where('username', $useremail);
//        $this->db->update('superadmin', array('resetData' => $resetlink));
//
//        header('Content-Type: application/json');
//
//        if ($query->num_rows() > 0) {
//            $rlink = md5(mt_rand());
//            $resetlink = base_url() . "index.php/superadmin/VerifyResetLink/" . $rlink;
//            $template = "<h3> Click below link to reset your password</h3><br><a href=" . $resetlink . ">" . $resetlink . "</a>";
//            $to = array('laxman810@gmail.com' => 'Laxman');
//            $subject = "Reset Password Link";
//
//            $config = new config();
//            $sendMail = new sendAMail($config->getHostUrl());
//            $status = $sendMail->mailFun($to, $subject, $template);
////            print_r($status);
//
//            $this->db->where('username', $useremail);
//            $this->db->update('superadmin', array('resetData' => $resetlink));
//
//            return 1;
//        } else
//            return 0;
//    }
    }

    function editoperator() {
        $this->load->library('mongo_db');
        $oid = $this->input->post('oid');
        $oname = $this->input->post('oname');
        $oaddress = $this->input->post('oaddress');
        $mname = $this->input->post('mname');
        $ophone = $this->input->post('ophone');
        $oemail = $this->input->post('oemail');

        $mongoArr = array(
            "operator_name" => $oname,
            "operator_address" => $oaddress,
            "manager_name" => $mname,
            "operator_phone" => $ophone,
            "operator_email" => $oemail
        );

        $this->mongo_db->update('operators', $mongoArr, array('_id' => new MongoId($oid)));
        echo json_encode(array('msg' => "Operator updated successfully", 'flag' => 1));
        return;
    }

    function deleteoperator() {
        $this->load->library('mongo_db');
        $oidArr = $this->input->post('val');
        foreach ($oidArr as $oid)
            $this->mongo_db->delete('operators', array('_id' => new MongoId($oid)));
        echo json_encode(array('msg' => "Operator Deleted successfully", 'flag' => 1));
        return;
    }

    function GetOperatorDetails($oprid) {
        $opData = $this->mongo_db->get_one('operators', array('_id' => new MongoId($oprid)));
        return $opData['operator_name'];
    }

    function datatable_oprdetails($oprid) {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');

        if ($this->input->post('sSearch') != '') {
            $cond = array('operator.name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
        } else {
            $cond = array();
        }
        $num = 0;
        $data = array();
        $opData = $this->mongo_db->get_one('operators', array('_id' => new MongoId($oprid)));
        foreach ($opData['prolist'] as $pro) {
            $proid = $pro['proid'];
            $num++;
            $proData = $this->mongo_db->get_one('location', array('user' => (int) $proid));

            if ($proData['fee_group'] == "1") {
                $fee_group = "Fixed";
            } else if ($proData['fee_group'] == "2") {
                $fee_group = "Hourly";
            } else
                $fee_group = "Milage";

            $image = $proData['image'];
            if ($proData['image'] == "")
                $image = base_url('/../pics/user.jpg');

            $data[] = array($proData['user'], $proData['name'],
                $proData['lname'],
                "<img src='" . $image . "' class='imageborder' width='50px' height='50px'>",
                $proData['mobile'],
                $proData['email'],
                $fee_group,
                $proData['location']['latitude'] . "/" . $proData['location']['longitude']);
        }
        echo $this->datatables->commission_Data($data);
    }

    function geteducationdata($param = '') {

        if ($param == '')
            $query = $this->db->query("select * from doctor_education WHERE doc_id='" . $this->session->userdata('admin_session')['LoginId'] . "' ")->result();
        else {
            $query = $this->db->query("select * from doctor_education WHERE doc_id='" . $param . "' ")->result();
        }

        return $query;
    }

    function insertDispatcher() {
        $country = $this->input->post('country');
        $ccode = $this->input->post('ccode');
        $city = $this->input->post('city');
        $dname = $this->input->post('dname');
        $demail = $this->input->post('demail');
        $pwd = $this->input->post('pwd');
        $phone = $this->input->post('phone');
        $ins = array('country' => $country, 'city' => $city, 'dispatcher_name' => $dname, 'dispatcher_email' => $demail, 'password' => md5($pwd), 'country_code' => '+' . $ccode, 'mobile' => $phone);
        $res = $this->mongo_db->insert('dispatchers', $ins);
        echo json_encode(array('msg' => "dispatcher added successfully", 'flag' => 1));
        return;
    }

    function ForgotPassword() {

        $useremail = $this->input->post('resetemail');
        $resetlink = '';
        $query = $this->db->query("SELECT * FROM superadmin where username='" . $useremail . "' ");
        foreach ($query->result() as $row) {
            $name = $row->username;
        }
        $this->db->where('username', $useremail);
        $this->db->update('superadmin', array('resetData' => $resetlink));

        header('Content-Type: application/json');

        if ($query->num_rows() > 0) {
            $rlink = md5(mt_rand());
            $resetlink = base_url() . "index.php/superadmin/VerifyResetLink/" . $rlink;
            $template = "<h3> Click below link to reset your password</h3><br><a href=" . $resetlink . ">" . $resetlink . "</a>";
            $to = array('laxman810@gmail.com' => 'Laxman');
            $subject = "Reset Password Link";

            $sendMail = new sendAMail(APP_SERVER_HOST);
            $status = $sendMail->mailFun($to, $subject, $template);
//            print_r($status);

            $this->db->where('username', $useremail);
            $this->db->update('superadmin', array('resetData' => $resetlink));

            return 1;
        } else
            return 0;
    }

//* naveena models *//


    function dt_passenger($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');
        $_POST["sColumns"] = "rahul,cust_name,monum,em,s.created_dt,s.app_version,PROFILE PIC,OPTION,select";


        $this->datatables->select("s.patient_id as rahul,concat(s.first_name,' ',s.last_name) as cust_name,concat(s.country_code,'',s.phone) as monum,s.patient_id  as em,s.created_dt,s.app_version,s.profile_pic,"
                        . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' END) from user_sessions where oid = rahul and user_type = 2 order by oid DESC limit 0,1) as dtype", FALSE)
                ->edit_column('cust_name', '<a href=" ' . base_url() . '../superadmin/index.php/superadmin/cust_details/$1">$2</a>', 'rahul,cust_name')
                ->unset_column('dtype')
                ->unset_column('s.profile_pic')
                ->edit_column('s.created_dt', "get_formatedDate/$1", 's.created_dt')
                ->edit_column('em', "secureEmail/$1", 'em')
                ->add_column('PROFILE PIC', '<img src="$1" width="50px" class="imageborder">', 's.profile_pic')
//                ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="50px" >', 'dtype')
                ->add_column('OPTION', '<div class="btn-group">
                                 <button id="$1" onclick="editCustomer($1)" type="button" style="color: #ffffff !important;background-color: #10cfbd;" class="btn btn-success  pull-left"><i class="fa fa-pencil"></i>
                                  </button>
                                  
                              </div>', 'rahul')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value= "$1"/>', 'rahul')
                ->from('patient s')
                ->where('s.status', $status);
        // $this->db->order_by("rahul", "desc");

        echo $this->datatables->generate();
    }

    function EditGetCustomer() {
        $cid = $this->input->post('cid');

        $query = $this->db->query("SELECT * FROM patient where patient_id='" . $cid . "' ")->row_array();

        return $query;
    }

    function EditCustomer() {
        $cid = $this->input->post('cid');
        $fName = $this->input->post('fName');
        $lName = $this->input->post('lName');
        $c_code = '+' . $this->input->post('c_code');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        if (isset($_FILES["photos"])) {
            $pro_pic = $_FILES["photos"]["name"];
            $size = $_FILES['photos']['size'];
            $tmp = $_FILES['photos']['tmp_name'];
            $pro_pic_ext = substr($pro_pic, strrpos($pro_pic, '.') + 1);
            $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");
            $msg = false;
            $bucket = BUCKET_NAME;
            $s3 = new S3(AMAZON_AWS_ACCESS_KEY, AMAZON_AWS_AUTH_SECRET);
            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);

            if (strlen($pro_pic) > 0) {
                if (in_array($pro_pic_ext, $valid_formats)) {
                    if ($size < (1024 * 1024)) {
                        $actual_image_name = "ProfileImages/" . $email . "." . $pro_pic_ext;

                        if ($s3->putObjectFile($tmp, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
                            $msg = true;
                            $s3file = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
//                           print_r($s3file);die;
                        } else
                            $msg = false;
                    } else
                        $msg = false;
                } else
                    $msg = false;
            } else
                $msg = false;
//        print_r($cid);die;
        }
        $this->load->library('mongo_db');
        $this->mongo_db->update('customer', array('fname' => $fName, 'lname' => $lName,
            'email' => $email), array('cid' => (int) $cid));

        if ($s3file == "" || $s3file == NULL) {
            $inserPay = "update patient set first_name = '" . $fName . "'"
                    . ",last_name = '" . $lName . "'"
                    . ",country_code = '" . $c_code . "'"
                    . ",phone = '" . $mobile . "'"
                    . ",email = '" . $email . "'  where patient_id = '" . $cid . "' ";
        } else {
            $inserPay = "update patient set first_name = '" . $fName . "'"
                    . ",last_name = '" . $lName . "'"
                    . ",country_code = '" . $c_code . "'"
                    . ",phone = '" . $mobile . "'"
                    . ",profile_pic = '" . $s3file . "'"
                    . ",email = '" . $email . "'  where patient_id = '" . $cid . "' ";
        }
//        print_r($inserPay);die;
        $qres = $this->db->query($inserPay);
//        if (!$qres) {
//            return array("error" => "Error1", 'test' => $inserPay);
//        } else {
//
//
//            return array("msg" => "sucess", 'test' => $inserPay);
//        }
    }

    function get_joblogsdata($value = '') {
        $m = new MongoClient();
        $this->load->library('mongo_db');

        $db1 = $this->mongo_db->db;
        $logs = $db1->selectCollection('driver_log');
        $mas = $this->db->query("select email from master where mas_id = '" . $value . "'")->row();
        $getAllLogs = $logs->find(array('mas_email' => $mas->email))->sort(array("on_time" => 1));
        foreach ($getAllLogs as $l) {
            $minimumTimeStamp = $l['on_time'];
            break;
        }
        $currentDate = date('Y-m-d');
        $startDate = date('Y-m-d', $minimumTimeStamp);
        $diff = abs(strtotime($currentDate) - strtotime($startDate));
        $days = floor($diff / (60 * 60 * 24));

        $NextDay = $startDate;
        $totalData = 0;
        $dataByDay = array();
        for ($i = 0; $i <= $days; $i++) {


            $startTime = strtotime($NextDay . ' 00:00:01');
            $endTime = strtotime($NextDay . ' 23:23:59');
            $getAllTodayLogs = $logs->find(array('mas_email' => $mas->email,
                'on_time' => array('$gte' => $startTime),
                'off_time' => array('$lte' => $endTime)));
            $dataByDay[$i]['Date'] = $NextDay;
            $getData = array();
            $ii = 0;

            $lat1 = 0;
            $long1 = 0;
            $lat2 = 0;
            $long2 = 0;
            $dictance = 0;
            foreach ($getAllTodayLogs as $oneDay) {

                foreach ($oneDay['location'] as $latlngs) {
                    if ($lat1 == 0 && $long1 == 0) {
                        $lat1 = $latlngs['latitude'];
                        $long1 = $latlngs['longitude'];
                    } else {
                        $lat2 = $latlngs['latitude'];
                        $long2 = $latlngs['longitude'];
                        $dictance += (double) $this->distance($lat1, $long1, $lat2, $long2, 'M');
                    }
                }
                $ii++;
            }
            $dataByDay[$i]['Distance'] = $dictance;
            $dataByDay[$i]['total'] = $ii;
            $totalData = $i;
            $date1 = str_replace('-', '/', $NextDay);
            $NextDay = date('Y-m-d', strtotime($date1 . "+1 days"));
        }

        $getLogs = $logs->find(array('mas_email' => $mas->email))->sort(array("on_time" => -1));
        $count = 1;

        $data = array();

        for ($Count = $totalData; $Count >= 0; $Count--) {
            if ($dataByDay[$Count]['total'] > 0) {
                $sr = $Count + 1;
                $data[] = array('sr' => $sr, 'Date' => $dataByDay[$Count]['Date'], 'total' => $dataByDay[$Count]['total'], 'distance' => number_format($dataByDay[$Count]['Distance'], 2, '.', ','), 'view' => '<input type="button" value="Log" id="' . $value . '!' . $dataByDay[$Count]['Date'] . '" onclick="viewLog(this);">');
            }
        }

        return $data;
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    function get_sessiondetails($value = '') {

        $db1 = new ConDB();
        $logs = $db1->mongo->selectCollection('driver_log');
        $exp = explode('!', $_REQUEST['MasId']);
        $MasId = $exp[0];
        $Date = $exp[1];



        $startTime = strtotime($Date . ' 00:00:01');
        $endTime = strtotime($Date . ' 23:23:59');
        $getAllTodayLogs = $logs->find(array('mas_email' => $mas['email'],
            'on_time' => array('$gte' => $startTime),
            'off_time' => array('$lte' => $endTime)));

        $getLogs = $logs->find(array('mas_email' => $mas['email']))->sort(array("on_time" => -1));
        $count = 1;


        foreach ($getAllTodayLogs as $log) {
            $lat1 = 0;
            $long1 = 0;
            $lat2 = 0;
            $long2 = 0;
            $dictance = 0;
            echo '<tr>';
            echo '<td>' . $count . '</td>';
            echo '<td>' . date('Y, M dS g:i a', $log['on_time']) . '</td>';
            if ($log['off_time'] == '') {
                echo '<td>Online Now</td>';
                echo '<td>-</td>';
            } else {
                echo '<td>' . date('Y, M dS g:i a', $log['off_time']) . '</td>';
                $diff = $log['off_time'] - $log['on_time'];
                $tm = explode('.', number_format(($diff / 60), 2, '.', ','));
                if ($tm[1] > 60) {
                    $tm[0] ++;
                    $tm[1] = $tm[1] - 60;
                }
                if (strlen($tm[1]) == 1) {
                    $tm[1] = $tm[1] . '0';
                }
                echo '<td>' . $tm[0] . ':' . $tm[1] . ' Mins' . '</td>';
            }

//calculate distnce
            foreach ($log['location'] as $latlngs) {
                if ($lat1 == 0 && $long1 == 0) {
                    $lat1 = $latlngs['latitude'];
                    $long1 = $latlngs['longitude'];
                } else {
                    $lat2 = $latlngs['latitude'];
                    $long2 = $latlngs['longitude'];
                    $dictance += (double) distance($lat1, $long1, $lat2, $long2, 'M');
                }
            }
            echo '<td>' . number_format($dictance, 2, '.', ',') . ' Miles</td>';
            echo '</tr>';
            $count++;
        }
    }

    function addcountry() {

        $var = $this->input->post('data2');
        $string = strtoupper($var);

        $query = $this->db->query("select * from country where Country_Name= '" . $string . "'");
        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => "country already exists", 'flag' => 0));
            return;
        } else {


            $data2 = $this->input->post('data2');
            $string = strtoupper($data2);

            $this->db->query("insert into country(Country_Name)  values('" . $string . "')");

            $countryId = $this->db->insert_id();

            if ($countryId > 0) {
                echo json_encode(array('msg' => "country added successfully", 'flag' => 0, 'id' => $countryId));
                return;
            } else {
                echo json_encode(array('msg' => "Unable to add country", 'flag' => 1));
                return;
            }
        }
    }

    function deletecity() {
        $query = $this->input->post('val');
        foreach ($query as $rowid) {
            $this->db->query("select * from city_available where City_Id ='" . $rowid . "'");
            if ($this->db->affected_rows() > 0) {
                $this->db->query("delete from city_available where City_Id ='" . $rowid . "'");
                echo json_encode(array("msg" => "your selected cities deleted successfully", "flag" => 0));
                return;
            } else {
                echo json_encode(array("msg" => "your selected cities not deleted,retry!", "flag" => 1));
                return;
            }
        }
    }

    function deletedispatcher() {
        $this->load->library('mongo_db');

        $query = $this->input->post('val');
        foreach ($query as $rowid) {

            $this->mongo_db->delete('dispatchers', array("_id" => new MongoId($rowid)));
            echo json_encode(array("msg" => "your selected dispatcher deleted successfully", "flag" => 0));
        }
    }

    function get_vehivletype() {
        $query = $this->db->query("select * from workplace_types order by type_name")->result();
        return $query;
    }

    function datatable_catogiries_services() {
        $data = array();
        $city = $this->db->query("select City_Name,City_Id from city_available ORDER BY City_Name ASC ")->result();
        $count = 0;
        foreach ($city as $result) {
//                        $citycount = $col->aggregate(array(
//                            array('$match' => array('city_id' => $result->City_Id)),
//                            array('$group' => array('_id' => '$city_id', 'count' => array('$sum' => 1)))
//                        ));
            $no_of_city = 0;
//            if (count($citycount['result']) > 0) {
//                $no_of_city = $citycount['result'][0]['count'];
//            }
            $data[] = array($count, $result->City_Name, $no_of_city);
            $count++;
        }
        echo $this->datatables->commission_Data($data);
    }

    function GetServicesByCity($cityId) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('Category', array('city_id' => (string) $cityId));
        $type = array();
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $type[] = $cur;
        }
//        return $type;
        $Mileage = $Hourly = $Fixed = array();
        for ($i = 0; $i < count($type); $i++) {
            $catid = (string) $type[$i]['_id'];
            if ($type[$i]['fee_type'] == "Mileage")
                $Mileage[] = array('cat_id' => $catid, 'cat_name' => $type[$i]['cat_name']);
            else if ($type[$i]['fee_type'] == "Hourly")
                $Hourly[] = array('cat_id' => $catid, 'cat_name' => $type[$i]['cat_name']);
            else if ($type[$i]['fee_type'] == "Fixed")
                $Fixed[] = array('cat_id' => $catid, 'cat_name' => $type[$i]['cat_name']);
        }
        return array('Mileage' => $Mileage, 'Hourly' => $Hourly, 'Fixed' => $Fixed);
    }

    function getCategorybyfreetype() {
        $this->load->library('mongo_db');
        $cityId = $this->input->post('cid');
        $freetype = $this->input->post('type');
        $cursor = $this->mongo_db->get_where('Category', array('city_id' => (string) $cityId, 'fee_type' => $freetype));
        $type = array();
        foreach ($cursor as $cur) {
            $type[] = array('cat_id' => (string) $cur['_id'], 'cat_name' => $cur['cat_name']);
        }
        return $type;
    }

    function GetCategoryByCity($cityId) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderCategory', array('city_id' => (string) $cityId));
        $type = array();
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $type[] = $cur;
        }
        return $type;
    }

    function GetCurrncy($cityId) {
        $query = $this->db->query("select Currency  from city where City_Id= '" . $cityId . "'")->result();

        return $query;
    }

    function GetServicesByCategory($catId) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderSubCategory', array('cat_id' => (string) $catId));
        $slist = array();
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $slist[] = $cur;
        }
        return $slist;
    }

    function GetGroupByCategory($catId = "") {
        $this->load->library('mongo_db');
        if ($catId != "")
            $cursor = $this->mongo_db->get_where('ServiceGroup', array('cat_id' => $catId));
        else
            $cursor = $this->mongo_db->get_where('ServiceGroup', array());
        $glist = array();
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $glist[] = $cur;
        }
        return $glist;
    }

    function getcidbypro($pid) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('location', array('user' => (int) $pid));
        $catdata = array();
        foreach ($cursor as $cur) {
            $catdata[] = $cur;
        }

        return $catdata;
    }

    function getdocdetailes($pid) {

        return $this->db->query("select * from docdetail where driverid ='" . $pid . "' LIMIT 1 ")->row();
    }

    function DeleteServices() {
        $this->load->library('mongo_db');
        $ids = $this->input->post('val');
        $idar = explode(",", $ids);
        $iarrm = array();
        for ($i = 0; $i < count($idar); $i++) {
            array_push($iarrm, new MongoId($idar[$i]));
        }
        $b = $this->mongo_db->count_all_results('ProviderCategory', array());
        $return[] = $this->mongo_db->delete('ProviderCategory', array('_id' => array('$in' => $iarrm)));
        $a = $this->mongo_db->count_all_results('ProviderCategory', array());
        if ($b == $a) {
            echo json_encode(array("msg" => "your selected Category not deleted,retry!", "flag" => 1));
            return;
        } else {
            echo json_encode(array("msg" => "your selected Category deleted successfully", "flag" => 0));
            return;
        }
    }

    function DeleteSubCategory() {
        $this->load->library('mongo_db');
        $ids = $this->input->post('val');
        $idar = explode(",", $ids);
        $iarrm = array();
        for ($i = 0; $i < count($idar); $i++) {
            array_push($iarrm, new MongoId($idar[$i]));
        }
        $b = $this->mongo_db->count_all_results('ProviderSubCategory', array());
        $return[] = $this->mongo_db->delete('ProviderSubCategory', array('_id' => array('$in' => $iarrm)));
        $a = $this->mongo_db->count_all_results('ProviderSubCategory', array());
        if ($b == $a) {
            echo json_encode(array("msg" => "your selected sub Category not deleted,retry!", "flag" => 1));
            return;
        } else {
            echo json_encode(array("msg" => "your selected sub Category deleted successfully", "flag" => 0));
            return;
        }
    }

    function GetOneServices($sid) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderCategory', array('_id' => new MongoId($sid)));
        $slist = array();
        foreach ($cursor as $cur) {
            $slist[] = $cur;
        }
        return $slist;
    }

    function GetOneSubServices($sid) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderSubCategory', array('_id' => new MongoId($sid)));
        $slist = array();
        foreach ($cursor as $cur) {
            $slist[] = $cur;
        }
        return $slist;
    }

    public function GetAllServices() {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderCategory', array());
        $entities = array();
        foreach ($cursor as $data) {
            $data['id'] = (string) $data['_id'];
            $entities[] = $data;
        }
        return $entities;
    }

    function GetAllServicesByCity($cityid) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderSubCategory', array('city_id' => $cityid));
        $entities = array();
        foreach ($cursor as $data) {
            $data['id'] = (string) $data['_id'];
            $entities[] = $data;
        }
        return $entities;
    }

    public function GetAllCategories() {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('Category', array(), array('count' => 1));
        $entities = array();
        $i = 0;
        foreach ($cursor as $data) {
            $entities[] = $data;
            $i++;
        }
        return $entities;
    }

    function get_company() {
        $query = $this->db->query("select * from company_info where Status = 3 order by companyname")->result();
        return $query;
    }

    function insert_payment($mas_id = '') {
        $this->load->library('mongo_db');
        $currunEarnigs = $this->input->post('dueamount');
        $amoutpaid = $this->input->post('paid_amount');
        $payment_mode = $this->input->post('payment_mode');
        $curuntdate = $this->input->post('ctime');
        $closingamt = $currunEarnigs - $amoutpaid;

        $settelApptAmt = $amoutpaid;
        $Allbookings = $this->db->query("select * from bookings where pro_id = " . $mas_id . " and payment_status != 1 order by bid asc")->result_array();

        $countPayrollCycle = $this->db->query('select count(*) as countPayrollCycle from payroll where mas_id = ' . $mas_id)->row()->countPayrollCycle;
        if ($countPayrollCycle == 0)
            $currunEarnigs = 0;

        if ($payment_mode == 2) {
            //Stripe Paid Start
            $stripe = new StripeModuleNew();
            $userType = $this->mongo_db->get_one('location', array('user' => (int) $mas_id));
            if (!$userType['StripeAccId']) {
                return array("error" => "Bank Details Not Added", 'test' => $userType);
            }
            $InStripetransfer = $stripe->apiStripe('CreateTransfer', array('amount' => ((float) $amoutpaid * 100), 'description' => "Payroll Trigger.", 'currency' => 'USD', 'CONNECTED_STRIPE_ACCOUNT_ID' => $userType['StripeAccId']));
            if ($InStripetransfer['error']) {
                $this->session->set_userdata(array('pay_error' => $InStripetransfer['error']['message']));
                return;
            }
            $transferid = $InStripetransfer['id'];
            //Stripe Paid END            
        } else {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $dummycash = '';
            for ($i = 0; $i < 20; $i++)
                $dummycash .= $characters[rand(0, strlen($characters) - 1)];
            $transferid = "cash_tr_" . $dummycash;
        }

        $query = "insert into payroll(mas_id) VALUES ('" . $mas_id . "')";
        $this->db->query($query);

        if (!$this->db->insert_id() > 0) {
            return array("error" => "Error2", 'test' => $query);
        }
        $cycle_id = $this->db->insert_id();
        $booking_cycle = 0; //booking_cycle
        $booking_earning = 0; //booking_earning
        $cash_due_app = 0;
        $gateway_fees = 0.25; //gateway_fees
        $net_paid = $amoutpaid - $gateway_fees;  //net_paid
        foreach ($Allbookings as $bookings) {
            if ($bookings['payment_status'] == 0) {
                $booking_cycle++;
                if ($bookings['payment_type'] == 2)
                    $booking_earning = $booking_earning + $bookings['pro_earning'];
                if ($bookings['payment_type'] == 1)
                    $cash_due_app = $cash_due_app + $bookings['app_earning'];
            }
            if ($bookings['payment_status'] == 2) {
                $payAmt = $this->db->query('select sum(pay_amount) as paid from payroll where mas_id = ' . $mas_id)->row()->paid;
                $aptSetAmount = $this->db->query('select sum(pro_earning) as paid from bookings where payment_status IN(1,2) and pro_id = ' . $mas_id)->row()->paid;
                $bookings['pro_earning'] = $aptSetAmount - $payAmt;
            }
            if ($bookings['payment_status'] == 1) {
                $payment_status = 1;
            } else if ($settelApptAmt == 0) {
                $payment_status = 3;
            } else if ($bookings['pro_earning'] <= $settelApptAmt) {
                $payment_status = 1;
                $settelApptAmt = $settelApptAmt - $bookings['pro_earning'];
            } else if ($bookings['pro_earning'] > $settelApptAmt) {
                $payment_status = 2;
                $settelApptAmt = 0;
            }
            $setFlagQuery = "update bookings set payment_status = " . $payment_status . "  where bid = '" . $bookings['bid'] . "' and pro_id = '" . $mas_id . "' and status = 7";
            $qres = $this->db->query($setFlagQuery);
            if (!$qres) {
                return array("error" => "Error1", 'test' => $setFlagQuery);
            }
            if ($payment_status == 1 || $payment_status == 2) {
                $query = "insert into pay_cycle(bid,payid,status) VALUES ('" . $bookings['bid'] . "','" . $cycle_id . "','" . $payment_status . "')";
                $this->db->query($query);
                if (!$this->db->insert_id() > 0) {
                    return array("error" => "Error3", 'test' => $query);
                }
            }
        }

        $first_b = (int) 0;
        $past_cycle_qry = 'SELECT * FROM payroll WHERE mas_id ="' . $mas_id . '" and payroll_id != "' . $cycle_id . '" order by payroll_id desc limit 1';
        $past_cycle = $this->db->query($past_cycle_qry)->row_array();
        if (count($past_cycle) > 0) {
            $first_b = (int) $past_cycle['last_bid'];
//            $first_b = (int) $Allbookings[count($Allbookings) - 1]['bid'];
        }

        $lastbooking = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id))->sort(array('_id' => -1))->limit(1);
        foreach ($lastbooking as $key => $value) {
            $last_b = $value['_id'];
            break;
        }

        $tot_bookings = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id, '_id' => array('$gt' => $first_b)))->count();
        $tot_accepted = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id, 'dispatched.res' => "2", 'status' => 7, '_id' => array('$gt' => $first_b)))->count();
        $tot_cancel = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id, 'dispatched.res' => "2", 'status' => 10, '_id' => array('$gt' => $first_b)))->count();
        $tot_reject = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id, 'dispatched.res' => "3", '_id' => array('$gt' => $first_b)))->count();
        $tot_ignore = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id, 'dispatched.res' => array('$exists' => false), '_id' => array('$gt' => $first_b)))->count();
        $acpt_rate = round(($tot_accepted / $tot_bookings) * 100, 2);
        $rev_rate = round($this->db->query('SELECT avg(star_rating) as rev_rate FROM doctor_ratings WHERE doc_id ="' . $mas_id . '" and appointment_id > "' . $first_b . '" ')->row()->rev_rate, 2);

        $inserPay = "update payroll set opening_balance = '" . $currunEarnigs . "'"
                . ",pay_date = '" . $curuntdate . "'"
                . ",pay_amount = '" . $amoutpaid . "'"
                . ",closing_balance = '" . $closingamt . "'"
                . ",due_amount = '" . $closingamt . "'"
                . ",trasaction_id = '" . $transferid . "'"
                . ",booking_cycle = '" . $booking_cycle . "'"
                . ",booking_earning = '" . $booking_earning . "'"
                . ",cash_due_app = '" . $cash_due_app . "'"
                . ",gateway_fees = '" . $gateway_fees . "'"
                . ",net_paid = '" . $net_paid . "'"
                . ",tot_bookings = '" . $tot_bookings . "'"
                . ",tot_accepted = '" . $tot_accepted . "'"
                . ",tot_reject = '" . $tot_reject . "'"
                . ",tot_ignore = '" . $tot_ignore . "'"
                . ",acpt_rate = '" . $acpt_rate . "'"
                . ",tot_cancel = '" . $tot_cancel . "'"
                . ",first_bid = '" . $first_b . "'"
                . ",last_bid = '" . $last_b . "'"
                . ",payment_type = '" . $payment_mode . "'"
                . ",cr_dr = 1"
                . ",rev_rate = '" . $rev_rate . "'  where payroll_id = '" . $cycle_id . "' ";


        $qres = $this->db->query($inserPay);
        if (!$qres) {
            return array("error" => "Error1", 'test' => $inserPay);
        } else {
            return array("msg" => "sucess", 'test' => $inserPay);
        }
    }

    function collect_driver_amount($mas_id = '') {
        $this->load->library('mongo_db');
        $currunEarnigs = $this->input->post('dueamount');
        $amoutpaid = (int) $this->input->post('collect_amount');
        if (!extension_loaded("gmp")) {
            echo "Install gmp Extesion For PHP";
            exit();
        }
        $amoutpaid = gmp_strval(gmp_neg($amoutpaid));
        $payment_mode = $this->input->post('payment_mode');
        $curuntdate = $this->input->post('ctime');
        $closingamt = $currunEarnigs - $amoutpaid;

        $settelApptAmt = $amoutpaid;
//        echo $settelApptAmt;
//        exit();
        $Allbookings = $this->db->query("select * from bookings where pro_id = " . $mas_id . " and payment_status != 1 order by bid asc")->result_array();

        $countPayrollCycle = $this->db->query('select count(*) as countPayrollCycle from payroll where mas_id = ' . $mas_id)->row()->countPayrollCycle;
        if ($countPayrollCycle == 0)
            $currunEarnigs = 0;


        if ($payment_mode == 2) {
            //Stripe Paid Start
            $stripe = new StripeModuleNew();
            $userType = $this->mongo_db->get_one('location', array('user' => (int) $mas_id));
            if (!$userType['StripeAccId']) {
                return array("error" => "Bank Details Not Added", 'test' => $userType);
            }
            $InStripetransfer = $stripe->apiStripe('CreateTransfer', array('amount' => ((float) $amoutpaid * 100), 'description' => "Payroll Trigger.", 'currency' => 'USD', 'CONNECTED_STRIPE_ACCOUNT_ID' => $userType['StripeAccId']));
            if ($InStripetransfer['error']) {
                $this->session->set_userdata(array('pay_error' => $InStripetransfer['error']['message']));
                return;
            }
            $transferid = $InStripetransfer['id'];
            //Stripe Paid END            
        } else {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $dummycash = '';
            for ($i = 0; $i < 20; $i++)
                $dummycash .= $characters[rand(0, strlen($characters) - 1)];
            $transferid = "cash_tr_" . $dummycash;
        }

        $query = "insert into payroll(mas_id) VALUES ('" . $mas_id . "')";
        $this->db->query($query);

        if (!$this->db->insert_id() > 0) {
            return array("error" => "Error2", 'test' => $query);
        }
        $cycle_id = $this->db->insert_id();

        $booking_cycle = 0; //booking_cycle
        $booking_earning = 0; //booking_earning
        $cash_due_app = 0;
        $gateway_fees = 0.25; //gateway_fees
        $net_paid = $amoutpaid - $gateway_fees;  //net_paid
        foreach ($Allbookings as $bookings) {
            if ($bookings['payment_status'] == 0) {
                $booking_cycle++;
                if ($bookings['payment_type'] == 2)
                    $booking_earning = $booking_earning + $bookings['pro_earning'];
                if ($bookings['payment_type'] == 1)
                    $cash_due_app = $cash_due_app + $bookings['app_earning'];
            }
            if ($bookings['payment_status'] == 2) {
                $payAmt = $this->db->query('select sum(pay_amount) as paid from payroll where mas_id = ' . $mas_id)->row()->paid;
                $aptSetAmount = $this->db->query('select sum(pro_earning) as paid from bookings where payment_status IN(1,2) and pro_id = ' . $mas_id)->row()->paid;
                $bookings['pro_earning'] = $aptSetAmount - $payAmt;
            }
            if ($bookings['payment_status'] == 1) {
                $payment_status = 1;
            } else if ($settelApptAmt == 0) {
                $payment_status = 3;
            } else if ($bookings['pro_earning'] <= $settelApptAmt) {
                $payment_status = 1;
                $settelApptAmt = $settelApptAmt - $bookings['pro_earning'];
            } else if ($bookings['pro_earning'] > $settelApptAmt) {
                $payment_status = 2;
                $settelApptAmt = 0;
            }
            $setFlagQuery = "update bookings set payment_status = " . $payment_status . "  where bid = '" . $bookings['bid'] . "' and pro_id = '" . $mas_id . "' and status = 7";
            $qres = $this->db->query($setFlagQuery);
            if (!$qres) {
                return array("error" => "Error1", 'test' => $setFlagQuery);
            }
            if ($payment_status == 1 || $payment_status == 2) {
                $query = "insert into pay_cycle(bid,payid,status) VALUES ('" . $bookings['bid'] . "','" . $cycle_id . "','" . $payment_status . "')";
                $this->db->query($query);
                if (!$this->db->insert_id() > 0) {
                    return array("error" => "Error3", 'test' => $query);
                }
            }
        }

        $first_b = (int) 0;
        $past_cycle_qry = 'SELECT * FROM payroll WHERE mas_id ="' . $mas_id . '" and payroll_id != "' . $cycle_id . '" order by payroll_id desc limit 1';
        $past_cycle = $this->db->query($past_cycle_qry)->row_array();
        if (count($past_cycle) > 0) {
            $first_b = (int) $past_cycle['last_bid'];
//            $first_b = (int) $Allbookings[count($Allbookings) - 1]['bid'];
        }

        $lastbooking = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id))->sort(array('_id' => -1))->limit(1);
        foreach ($lastbooking as $key => $value) {
            $last_b = $value['_id'];
            break;
        }

        $tot_bookings = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id, '_id' => array('$gt' => $first_b)))->count();
        $tot_accepted = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id, 'dispatched.res' => "2", 'status' => 7, '_id' => array('$gt' => $first_b)))->count();
        $tot_cancel = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id, 'dispatched.res' => "2", 'status' => 10, '_id' => array('$gt' => $first_b)))->count();
        $tot_reject = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id, 'dispatched.res' => "3", '_id' => array('$gt' => $first_b)))->count();
        $tot_ignore = $this->mongo_db->get_where('bookings', array('dispatched.pid' => $mas_id, 'dispatched.res' => array('$exists' => false), '_id' => array('$gt' => $first_b)))->count();
        $acpt_rate = round(($tot_accepted / $tot_bookings) * 100, 2);
        $rev_rate = round($this->db->query('SELECT avg(star_rating) as rev_rate FROM doctor_ratings WHERE doc_id ="' . $mas_id . '" and appointment_id > "' . $first_b . '" ')->row()->rev_rate, 2);

        $inserPay = "update payroll set opening_balance = '" . $currunEarnigs . "'"
                . ",pay_date = '" . $curuntdate . "'"
                . ",pay_amount = '" . $amoutpaid . "'"
                . ",closing_balance = '" . $closingamt . "'"
                . ",due_amount = '" . $closingamt . "'"
                . ",trasaction_id = '" . $transferid . "'"
                . ",booking_cycle = '" . $booking_cycle . "'"
                . ",booking_earning = '" . $booking_earning . "'"
                . ",cash_due_app = '" . $cash_due_app . "'"
                . ",gateway_fees = '" . $gateway_fees . "'"
                . ",net_paid = '" . $net_paid . "'"
                . ",tot_bookings = '" . $tot_bookings . "'"
                . ",tot_accepted = '" . $tot_accepted . "'"
                . ",tot_reject = '" . $tot_reject . "'"
                . ",tot_ignore = '" . $tot_ignore . "'"
                . ",acpt_rate = '" . $acpt_rate . "'"
                . ",tot_cancel = '" . $tot_cancel . "'"
                . ",first_bid = '" . $first_b . "'"
                . ",last_bid = '" . $last_b . "'"
                . ",payment_type = '" . $payment_mode . "'"
                . ",cr_dr = 2"
                . ",rev_rate = '" . $rev_rate . "'  where payroll_id = '" . $cycle_id . "' ";


        $qres = $this->db->query($inserPay);
        if (!$qres) {
            return array("error" => "Error1", 'test' => $inserPay);
        } else {
            return array("msg" => "sucess", 'test' => $inserPay);
        }
    }

    function addcity() {
        $countryid = $this->input->post('countryid');

        $data3 = $this->input->post('data3');
        $data = $this->input->post('data');
        $existcity = '';
        $getcityname = $this->db->query("select * from city where  City_Name = '" . $data3 . "' and Country_Id='" . $countryid . "'");

        if ($getcityname->num_rows() > 0) {
            echo json_encode(array('msg' => "city already exists", 'flag' => 1));
            return;
        } else {
            $this->db->query("insert into city(Country_Id,City_Name,Currency) values('$countryid','$data3','$data')");
            if ($this->db->affected_rows() > 0) {
                echo json_encode(array('msg' => "city added successfully", 'flag' => 0));
                return;
            }
        }
    }

    function activate_company() {
        $val = $this->input->post('val');
        foreach ($val as $result) {
            $this->db->query("update company_info set status=3  where company_id='" . $result . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected company/companies activated succesfully", 'flag' => 1));
            return;
        }
    }

    function activate_vehicle() {
        $val = $this->input->post('val');
        foreach ($val as $result) {
            $this->db->query("update workplace set status=2  where workplace_id='" . $result . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected vehicle/vehicles activated succesfully", 'flag' => 1));
            return;
        }
    }

    function acceptdrivers() {

        $val = $this->input->post('val');
        $this->load->library('mongo_db');
        $data = $this->db->query('select * from  doctor where doc_id = "' . $val . '" and status = 3');

        foreach ($val as $result) {
            $res = $this->mongo_db->update('location', array('accepted' => 1), array('user' => (int) $result));
            $this->db->query("update doctor set status = 3  where doc_id='" . $result . "' ");
        }

        $msg = 'Your profile has been Acceptd on ' . Appname . ', contact ' . Appname . ' customer care';
        $alert = "Accepted";
        $action = 21;
        $this->sendPushNotification($val, $action, $msg, $alert, 1);


        echo json_encode(array('flag' => 0, 'msg' => 'Status Updated'));

        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected driver/drivers accepted succesfully", 'flag' => 1));
            return;
        }

        return $data;
    }

    function testpush() {
        $msg = 'Your profile has been Acc';
        $alert = "Accepted";
        $action = 22;
        $val = array();
        $val[] = 142;
        $this->sendPushNotification($val, $action, $msg, $alert, 1);
    }

//    function AcceptCategory() {
//
//        $this->load->library('mongo_db');
//        $pid = $this->input->post('pid');
//        $catcb = $this->input->post('catcb');
//        
//        
//        $res = $this->mongo_db->update('location', array('accepted' => 1), array('user' => (int) $pid));
//        $this->db->query("update doctor set status = 3 where doc_id='" . $pid . "' ");
//        $catArray = array();
//        foreach ($catcb as $cat) {
//            $update = array('catlist.$.status' => 1);
//            $con = array('catlist.cid' => $cat, 'user' => (int) $pid);
//            $res = $this->mongo_db->update('location', $update, $con);
//            $catName = $this->mongo_db->get_one('Category', array('_id' => new MongoId($cat)));
//            array_push($catArray, $catName['cat_name']);
//        }
//
//        $msg = 'Your profile has been Accepted on ' . Appname . ', contact ' . Appname . ' customer care';
//        $alert = "Accepted";
//        $action = 22;
//        $val = array();
//        $val[] = $pid;
//        $this->sendPushNotification($val, $action, $msg, $alert, 1);
////        print_r("a");
////        print_r($this->db->affected_rows());
////        if ($this->db->affected_rows() > 0) {
////            print_r("dsfa");die;
//        $dlist = $this->db->query('select first_name,email,password from  doctor where doc_id = "' . $pid . '" and status = 3')->result();
//        $sendMail = new sendAMail(APP_SERVER_HOST);
//        $mailRes = $sendMail->masterActivated($dlist[0]->email, $dlist[0]->first_name, $catArray);
//
//        echo json_encode(array('msg' => "your selected Provider accepted succesfully", 'flag' => 1));
//        return;
////        }
//    }
    function AcceptCategory() {

        $this->load->library('mongo_db');
        $pid = $this->input->post('pid');
        $catcb = $this->input->post('catcb');

        $opr_id = $this->input->post('opr_id');
        $isOperator = $this->input->post('isOperator');

        //$opid = $this->mongo_db->get_one('operators', array("_id" => new MongoId($opr_id)));


        if ($isOperator == 1 && $opr_id != 0) {
            $res = $this->mongo_db->update('location', array('accepted' => 1, 'isOperator' => $isOperator), array('user' => (int) $pid));
//                $this->mongo_db->update('location', array('email' => $opid['operator_email']), array('user' => (int) $pid));
            $odata = array('proid' => $pid);
            $op = $this->mongo_db->get_one('operators', array("_id" => new MongoId($opr_id)));
            $arr = array();
            foreach ($op['prolist'] as $val) {
                $arr[] = $val['proid'];
            }


            if (in_array($pid, $arr)) {
                
            } else {
                $rej = $this->mongo_db->get_one('operators', array("prolist.proid" => $pid));
                $rej2 = array('proid' => $pid);

                $this->mongo_db->updatewithpull('operators', array("prolist" => $rej2), array("_id" => new MongoId($rej['_id'])));
                $this->mongo_db->updatewithpush('operators', array("prolist" => $odata), array("_id" => new MongoId($opr_id)));
            }
        } else {

            $res = $this->mongo_db->update('location', array('accepted' => 1, 'isOperator' => "0"), array('user' => (int) $pid));
        }


        $this->db->query("update doctor set status = 3 where doc_id='" . $pid . "' ");
        $catArray = array();
        foreach ($catcb as $cat) {
            $update = array('catlist.$.status' => 1);
            $con = array('catlist.cid' => $cat, 'user' => (int) $pid);
            $res = $this->mongo_db->update('location', $update, $con);
            $catName = $this->mongo_db->get_one('Category', array('_id' => new MongoId($cat)));
            array_push($catArray, $catName['cat_name']);
        }

        $msg = 'Your profile has been Accepted on ' . Appname . ', contact ' . Appname . ' customer care';
        $alert = "Accepted";
        $action = 22;
        $val = array();
        $val[] = $pid;
        $this->sendPushNotification($val, $action, $msg, $alert, 1);
//        print_r("a");
//        print_r($this->db->affected_rows());
//        if ($this->db->affected_rows() > 0) {
//            print_r("dsfa");die;
        $dlist = $this->db->query('select first_name,email,password from  doctor where doc_id = "' . $pid . '" and status = 3')->result();
        $sendMail = new sendAMail(APP_SERVER_HOST);
        $mailRes = $sendMail->masterActivated($dlist[0]->email, $dlist[0]->first_name, $catArray);

        echo json_encode(array('msg' => "your selected Provider accepted succesfully", 'flag' => 1));
        return;
//        }
    }

    function GetAllCatForProvider($proid) {
        $this->load->library('mongo_db');
        $dlist = $this->db->query('select status,city_id from  doctor where doc_id = "' . $proid . '"')->result();
        $City_Name = $this->db->query('select City_Name from city where City_Id = "' . $dlist[0]->city_id . '"')->row();

        $pdata = $this->mongo_db->get_one('location', array('user' => (int) $proid));
        $cdata = $this->mongo_db->get_where('Category', array());


        $operators = $this->mongo_db->get_where('operators', array());
        $isOperator = $this->mongo_db->get_where('operators', array('prolist.proid' => (string) $proid))->count();
        $oparr = array();
        foreach ($operators as $opr) {
            $oparr[] = array('id' => (string) $opr['_id'], 'operator_name' => $opr['operator_name']);
        }

        $catarr = array();
        foreach ($cdata as $cat) {
            $catarr[] = array('id' => (string) $cat['_id'], 'cat_name' => $cat['cat_name']);
        }
        $accCat = array();
        $rejCat = array();
        $newCat = array();
        foreach ($pdata['catlist'] as $ucat) {
            if ($dlist[0]->status == 1) {
                foreach ($catarr as $ccat) {
                    if ($ccat['id'] == $ucat['cid']) {
                        $newCat[] = array('id' => $ccat['id'], 'cat_name' => $ccat['cat_name']);
                    }
                }
            } elseif ($dlist[0]->status == 3) {
                foreach ($catarr as $ccat) {
                    if ($ccat['id'] == $ucat['cid'] && $ucat['status'] == 1) {
                        $accCat[] = array('id' => $ccat['id'], 'cat_name' => $ccat['cat_name']);
                    } else if ($ccat['id'] == $ucat['cid'] && $ucat['status'] == 0) {
                        $rejCat[] = array('id' => $ccat['id'], 'cat_name' => $ccat['cat_name']);
                    }
                }
            } elseif ($dlist[0]->status == 4) {
                foreach ($catarr as $ccat) {
                    if ($ccat['id'] == $ucat['cid'] && $ucat['status'] == 1) {
                        $accCat[] = array('id' => $ccat['id'], 'cat_name' => $ccat['cat_name']);
                    } else if ($ccat['id'] == $ucat['cid'] && $ucat['status'] == 0) {
                        $rejCat[] = array('id' => $ccat['id'], 'cat_name' => $ccat['cat_name']);
                    }
                }
            } else {
                foreach ($catarr as $ccat) {
                    if ($ccat['id'] == $ucat['cid']) {
                        $newCat[] = array('id' => $ccat['id'], 'cat_name' => $ccat['cat_name']);
                    } elseif ($ccat['id'] == $ucat['cid'] && $ucat['status'] == 1) {
                        $accCat[] = array('id' => $ccat['id'], 'cat_name' => $ccat['cat_name']);
                    } else if ($ccat['id'] == $ucat['cid'] && $ucat['status'] == 0) {
                        $rejCat[] = array('id' => $ccat['id'], 'cat_name' => $ccat['cat_name']);
                    }
                }
            }
        }

        echo json_encode(array('isOperator' => $isOperator, 'oparr' => $oparr, 'newCat' => $newCat, 'accCat' => $accCat, 'rejCat' => $rejCat, 'cityName' => $City_Name->City_Name));
    }

    function rejectdrivers() {
        $val = $this->input->post('val');

        $this->load->library('mongo_db');
        foreach ($val as $user) {
            $this->mongo_db->update('location', array('accepted' => 0), array('user' => (int) $user));
            $pdata = $this->mongo_db->get_one('location', array('user' => (int) $user));
            foreach ($pdata['catlist'] as $ucat) {
                $update = array('catlist.$.status' => 0, 'isOperator' => "0");
                $con = array('catlist.cid' => $ucat['cid'], 'user' => (int) $user);

                $res = $this->mongo_db->update('location', $update, $con);
                $this->mongo_db->updatewithpull('operators', array('prolist' => array('proid' => $user)), array('prolist.proid' => $user));
            }

            $this->db->query("update doctor set status = '4' where doc_id IN (" . implode(',', $val) . ")");

            if ($this->db->affected_rows() > 0) {
                $this->mongo_db->update('location', array('status' => 4, 'carId' => 0, 'type' => 0), array('user' => (int) $user));

                $msg = 'Your profile has been suspended on ' . Appname . ', contact ' . Appname . ' customer care';
                $alert = "Rejected";
                $action = 21;
                $tempArr = array();
                $tempArr[] = $user;
                $this->sendPushNotification($tempArr, $action, $msg, $alert, 1);

                $this->db->query("update user_sessions set loggedIn = 2 where oid = '" . $user . "' and loggedIn = 1 and user_type = 1");
                $dlist = $this->db->query('select first_name,email from  doctor where doc_id = "' . $user . '"')->result();
                $sendMail = new sendAMail(APP_SERVER_HOST);
                $status = $sendMail->masterSuspended($dlist[0]->email, $dlist[0]->first_name);
            }
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected driver/drivers rejected Successfully", 'flag' => 1, 'res' => $res));
            return;
        }
    }

    function sendAndroidPush($tokenArr, $andrContent, $apiKey) {
        $fields = array(
            'registration_ids' => $tokenArr,
            'data' => $andrContent,
        );

        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ANDROID_PUSH_URL);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        curl_close($ch);
        $res_dec = json_decode($result);

        if ($res_dec->success >= 1)
            return array('errorNo' => 44, 'result' => $result);
        else
            return array('errorNo' => 46, 'result' => $result);
    }

    function editdriverpassword() {
        $newpass = $this->input->post('newpass');
        $val = $this->input->post('val');
        $pass = $this->db->query("select first_name, email,password from doctor where doc_id='" . $val . "' ")->result();
        if ($pass[0]->password == md5($newpass)) {
            echo json_encode(array('msg' => "this password already exists. Enter new password", 'flag' => 1));
            return;
        } else {
            $this->db->query("update doctor set password = md5('" . $newpass . "') where doc_id = '" . $val . "' ");
            if ($this->db->affected_rows() > 0) {
                $details1 = array('first_name' => $pass[0]->first_name, 'email' => $pass[0]->email);
                $sendMail = new sendAMail(APP_SERVER_HOST);
                $data = $sendMail->passwordChanged($details1, $newpass);
                echo json_encode(array('msg' => "your new password updated successfully", 'flag' => 0));
                return;
            }
        }
    }

    function editdispatcherpassword() {
        $newpass = $this->input->post('newpass');

        $val = $this->input->post('val');
        $pass = $this->db->query("select first_name, email,password from doctor where doc_id='" . $val . "' ")->result();

        $doc = $this->mongo_db->get_one('dispatchers', array('_id' => new MongoId($val)));
//        foreach($doc as $val)
//        {
//            if($val['password']==md5($newpass))
//            {
//                echo json_encode(array('msg' => "this password already exists. Enter new password", 'flag' => 1));
//                return;
//            }
//            else
//            {
//                $this->mongo_db->update('dispatchers', array('password' => md5($newpass)), array('_id' => new MongoId($val)));
//                echo json_encode(array('msg' => "your new password updated successfully", 'flag' => 0));
//                return;
//            }
//            
//        }
        $this->mongo_db->update('dispatchers', array('password' => md5($newpass)), array('_id' => new MongoId($val)));
        echo json_encode(array('msg' => "your new password updated successfully", 'flag' => 0));
        return;
    }

    function getdispatcherdetail() {
        $val = $this->input->post('val');
        $doc = $this->mongo_db->get_one('dispatchers', array('_id' => new MongoId($val)));
        $countrydata = $this->db->query("select Country_Name from country where Country_Id='" . $doc['country'] . "' ")->result();
        $countrycode = $doc['country'];
        $countryname = $countrydata[0]->Country_Name;
        $ccode = $doc['country_code'];
        $phone = $doc['mobile'];
        $city = $doc['city'];
        $dispatcher_name = $doc['dispatcher_name'];
        $dispatcher_email = $doc['dispatcher_email'];
        // $phonecode = substr($countrynum, 0, 3);
        // $phonenumber = substr($countrynum, 3);
        //$data = array('ccode' => $countrycode, 'cname' => $countryname, 'city' => $city, 'dpname' => $dispatcher_name, 'demail' => $dispatcher_email, 'phonecode' => $phonecode, 'phonenum' => $phonenumber);
        $data = array('ccode' => $countrycode, 'cname' => $countryname, 'city' => $city, 'dpname' => $dispatcher_name, 'demail' => $dispatcher_email, 'phoncode' => $ccode, 'phonenum' => $phone);
        echo json_encode($data);
    }

    function deleteDriverBank() {

        $masid = $this->input->post('uid');
        $bankId = $this->input->post('id');
        $oldbankId = $this->input->post('deleteaccount');
        $stripe = new StripeModuleNew();
        $this->load->library('mongo_db');

        $userType = $this->mongo_db->get_one('location', array('user' => (int) $masid));

        if ($bankId == "") {
            $res = $stripe->apiStripe('DeleteABankAccount', array(
                'accountId' => $userType['StripeAccId'],
                'externale_accId' => $oldbankId
            ));

            if ($res['deleted']) {

                $this->mongo_db->updatewithpull('location', array('accountDetail' => array("tocken" => $oldbankId)), array("user" => (int) $masid));
                $array = array('flag' => 0, 'message' => "Card Deleted Successfully.", 'res' => $res, 'userData' => $this->getDriverBankData($masid));
            } else {
                $errorObj = $res['error']['e'];
                $array = array('flag' => 1, 'message' => $errorObj->jsonBody['error']['message'], 'res' => $res);
            }

            echo json_encode($array);
        }

        $StripeRes = $stripe->apiStripe('CreateAnBankAccout', array('accountId' => $userType['StripeAccId'], 'externale_accId' => $bankId));
        if ($StripeRes['error']) {
            return array('flag' => 1, 'msg' => "Error While Creating Account", 'res' => $StripeRes);
        } else {
            $this->mongo_db->updatewithpush('location', array('accountDetail' => array('status' => 'active', 'tocken' => $StripeRes['id'])), array('user' => (int) $masid));
        }

        $res = $stripe->apiStripe('DeleteABankAccount', array(
            'accountId' => $userType['StripeAccId'],
            'externale_accId' => $oldbankId
        ));

        if ($res['deleted']) {
            $this->mongo_db->updatewithpull('location', array('accountDetail' => array("tocken" => $oldbankId)), array("user" => (int) $masid));
            $array = array('flag' => 0, 'message' => "Card Deleted Successfully.", 'res' => $res, 'userData' => $this->getDriverBankData($masid));
        } else
            $array = array('flag' => 1, 'message' => "There were some problem with stripe.", 'res' => $res);
        echo json_encode($array);
    }

    function getDriverBankData($status) {
        $stripe = new StripeModuleNew();
        $this->load->library('mongo_db');
        $userType = $this->mongo_db->get_one('location', array('user' => (int) $status));

        $bank_arr = array();
        $i = 0;
        foreach ($userType['accountDetail'] as $user) {
            $getResp = array('accountId' => $userType['StripeAccId'], 'BankAccountTocken' => $user['tocken']);
            $rep = $stripe->apiStripe('RetriveABankAccount', $getResp);
            $bank_arr[$i]['accountId'] = $userType['StripeAccId'];
            $bank_arr[$i]['bank_id'] = $rep['id'];
            $bank_arr[$i]['account_holder_name'] = $rep['account_holder_name'];
            $bank_arr[$i]['bank_name'] = $rep['bank_name'];
            $bank_arr[$i]['routing_number'] = $rep['routing_number'];
            $bank_arr[$i]['country'] = $rep['country'];
            $bank_arr[$i]['currency'] = $rep['currency'];
            $bank_arr[$i]['last4'] = $rep['last4'];
            $i++;
        }
        return $bank_arr;
    }

    // add bank account Initally 
    function AddBankAccountInitial() {

        $dateformante = explode('/', $this->input->post('dob'));
        $data['day'] = $dateformante[1];
        $data['month'] = $dateformante[0];
        $data['year'] = $dateformante[2];

        $accountholdername = explode(" ", $this->input->post('account_holder_name'));

        $data['first_name'] = $accountholdername[0];
        $data['last_name'] = $accountholdername[1];

        $data['personal_id_number'] = $this->input->post('presnalid');

        $data['state'] = $this->input->post('state');
        $data['postal_code'] = $this->input->post('postalcode');
        $data['city'] = $this->input->post('cityname');
        $data['line1'] = $this->input->post('Address');

        $mas_id = $this->input->post('mas_id');
        $banktoken = $this->input->post('banktoken');
        $IdProoF = $this->input->post('IdProoF');

        $stripe = new StripeModuleNew();
        $this->load->library('mongo_db');
        $master = $this->mongo_db->get_one('location', array('user' => (int) $mas_id));
        ///
//        return array('master' => $master, 'banktoken' => $banktoken, 'data' => $data);
        if ($banktoken != '') {
//echo "1";            
            $stripeAcc = new StripeModuleNew();
//echo "2";
            if (is_null($master['StripeAccId']) || $master['StripeAccId'] == "") {
                $StripeRes = $stripeAcc->apiStripe('CreateAccount', array());
//return array('StripeRes' => $StripeRes);
                $accid = $StripeRes['id'];
                $this->mongo_db->update('location', array('StripeAccId' => $accid), array('user' => (int) $mas_id));
            } else {
                $accid = $master['StripeAccId'];
            }

            $data['acc_id'] = $accid;


//                  echo $accid;
//            echo 'here';
//            exit();
            $res = $stripe->apiStripe('updateAccountInfo', $data);
            if ($res['error']) {
//                  $arr = json_decode($res['error']['e']['httpBody'], true);
                $errorObj = $res['error']['e'];
                $array = array('flag' => 1, 'msg' => $errorObj->jsonBody['error']['message'], 'status' => "while updateing account Info", 'fullres' => $res);
                return $array;
            }



            $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/' . mainfolder . '/pics/';
            $verification = $stripe->apiStripe('IdentifyiVerification', array('acc_id' => $accid, 'IdProoF' => $documentfolder . $IdProoF));
            if ($verification['flag'] == 1) {
                $array = array('flag' => 1, 'msg' => $verification['msg'], 'status' => "while updateing Bank Idproof", 'fullres' => $verification);
                return $array;
            }


            $StripeResAcc = $stripe->apiStripe('CreateAnBankAccout', array('accountId' => $accid, 'externale_accId' => $banktoken));
            $this->mongo_db->updatewithpush('location', array('accountDetail' => array('status' => 'active', 'tocken' => $StripeResAcc['id'])), array('user' => (int) $mas_id));

//return array('test'=>1);
            return array('flag' => 0, 'msg' => "Congrats You Have added account!", 'userData' => $this->getDriverBankData($mas_id));
        } else {
            return array('flag' => 1, 'msg' => 'bankId is missing ');
        }
    }

    function editsuperpassword() {
        $newpass = $this->input->post('newpass');
        $currentpassword = $this->input->post('currentpassword');

        $this->load->library('mongo_db');
        $role = 'Super Admin';
        $result = $this->mongo_db->get_one('admin_users', array('role' => $role));
        $pass = $result['pass'];



        if (md5($currentpassword) != $pass) {
            echo json_encode(array('msg' => "you have entered the incorrect current password", 'flag' => 2));
            return;
        } else {

            if ($pass == md5($newpass)) {
                echo json_encode(array('msg' => "this password already exists. Enter new password", 'flag' => 1));
                return;
            } else {
                $this->db->query("update superadmin set password = md5('" . $newpass . "') where id = 1 ");
                $update = $this->mongo_db->update('admin_users', array('pass' => md5($newpass)), array('role' => $role));

                if ($update) {
                    echo json_encode(array('msg' => "your new password updated successfully", 'flag' => 0));
                    return;
                }
            }
        }
    }

    public function GetProductDetails($param = '') {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_one('MasterProducts', array('_id' => new MongoId($param)));
        return $cursor;
    }

    public function GetAllProducts($BizId = '') {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('MasterProducts', array(), array('count' => 1));

        $entities = array();
        $i = 0;

        foreach ($cursor as $data) {
            $entities[] = $data;
            $i++;
        }
        return $entities;
    }

    function get_product_count($bizid = '') {

        $this->load->library('mongo_db');
        $array = $this->mongo_db->get_where('MasterProducts', array());
        $count = 0;
        foreach ($array as $total) {
            $count++;
        }
        $count++;
        return $count;
    }

//    function EditCategory()
//    {
//          $this->load->library('mongo_db');
//        $CategoryId = $this->input->post("CategoryId");        
//        $cid = $this->input->post("cid");
//        $cityList = $this->db->query("select * from city_available where City_Id = '". $cid  ."'")->result();
//        $loc['latitude'] = (double)$cityList[0]->City_Lat;
//        $loc['longitude'] = (double)$cityList[0]->City_Long;
//        if ($CategoryId != '') {
//            $cid = (string)$this->input->post("cid");
//            $scname =(string)$this->input->post("scname");
//            $scdesc = (string)$this->input->post("scdesc");
//            $min_fees = (string)$this->input->post("min_fees");
//            $base_fees = (string)$this->input->post("base_fees");
//            $pr_min = (string)$this->input->post("pr_min");
//            $sp = (string)$this->input->post("sp");
//              $sdata = array('city_id' => $cid, 'cat_name' => $scname,
//                'cat_desc' => $scdesc , 'min_fees' => $min_fees , 'base_fees' => $base_fees, 
//                'price_min' => $pr_min , 'start_price'=> $sp);
//            $this->mongo_db->update('Category', $sdata, array("_id" => new MongoId($CategoryId)));
//        }
//    }

    function AddNewCategory() {

        $this->load->library('mongo_db');
        $unsel_img_name = "";
        $sel_img_name = "";
        $sel_img = $_FILES["sel_img"]["name"];
        $unsel_img = $_FILES["unsel_img"]["name"];
        $banner_img = $_FILES['banner_img']['name'];
        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/pics/';

        if ($banner_img != "") {
            $ext = substr($banner_img, strrpos($banner_img, '.') + 1);
            $banner_img_name = (rand(1000, 9999) * time()) . '.' . $ext;
            try {
                if (move_uploaded_file($_FILES['banner_img']['tmp_name'], $documentfolder . $banner_img_name)) {
                    echo "22222";
                    $this->uploadimage_diffrent_redulation($documentfolder . $banner_img_name, $banner_img_name, $_SERVER['DOCUMENT_ROOT'] . '/', $ext);
                    echo "1111";
                }
            } catch (Exception $ex) {
                print_r($ex);
                return false;
            }
        }
        if ($sel_img != "") {
            $ext1 = substr($sel_img, strrpos($sel_img, '.') + 1);
            $sel_img_name = (rand(1000, 9999) * time()) . '.' . $ext1;
            try {
                if (move_uploaded_file($_FILES['sel_img']['tmp_name'], $documentfolder . $sel_img_name)) {
                    $this->uploadimage_diffrent_redulation($documentfolder . $sel_img_name, $sel_img_name, $_SERVER['DOCUMENT_ROOT'] . '/', $ext1);
                }
            } catch (Exception $ex1) {
                print_r($ex1);
                return false;
            }
        }
        if ($unsel_img != "") {
            $ext2 = substr($unsel_img, strrpos($unsel_img, '.') + 1);
            $unsel_img_name = (rand(1000, 9999) * time()) . '.' . $ext2;
            try {
                if (move_uploaded_file($_FILES['unsel_img']['tmp_name'], $documentfolder . $unsel_img_name)) {
                    $this->uploadimage_diffrent_redulation($documentfolder . $unsel_img_name, $unsel_img_name, $_SERVER['DOCUMENT_ROOT'] . '/', $ext2);
                }
            } catch (Exception $ex2) {
                print_r($ex2);
                return false;
            }
        }

        $fdata = $this->input->post("fdata");

        $CategoryId = $this->input->post("CategoryId");
        $city_id = $this->input->post("city_id");
        $min_fees = $this->input->post("min_fees");
        $base_fees = $this->input->post("base_fees");
        $cat_desc = $this->input->post("cat_desc");
        $cat_name = $this->input->post("cat_name");
        $price_min = $this->input->post("price_min");
        $fee_type = $this->input->post("fee_type");
        $fixed_price = $this->input->post("fixed_price");

        $can_condition = $this->input->post('can_condition');
        $pay_commision = $this->input->post("pay_commision");
        $price_set_by = $this->input->post("price_set_by");
        $can_fees = $this->input->post("can_fees");
        $visit_fees = $this->input->post("visit_fees");
        $price_mile = $this->input->post("price_mile");


        $cityList = $this->db->query("select * from city_available where City_Id = '" . $city_id . "'")->result();
        $loc['longitude'] = (double) $cityList[0]->City_Long;
        $loc['latitude'] = (double) $cityList[0]->City_Lat;


        if ($fee_type == "Fixed") {
            $base_fees = "";
            $min_fees = "";
            $price_min = "";
            $price_mile = "";
            $visit_fees = "";
        }
        if ($fee_type == "Hourly") {
            $base_fees = "";
            $min_fees = "";
            $fixed_price = "";
            $price_mile = "";
        }
        if ($fee_type == "Mileage") {
            $fixed_price = "";
            $visit_fees = "";
        }


        $sdata['city_id'] = $city_id;
        $sdata['cat_name'] = $cat_name;
        $sdata['cat_desc'] = $cat_desc;
        $sdata['min_fees'] = $min_fees;
        $sdata['base_fees'] = $base_fees;
        $sdata['price_min'] = $price_min;
        $sdata['fee_type'] = $fee_type;
        $sdata['fixed_price'] = $fixed_price;
        $sdata['pay_commision'] = $pay_commision;
        $sdata['price_set_by'] = $price_set_by;
        $sdata['can_fees'] = $can_fees;
        $sdata['visit_fees'] = $visit_fees;
        $sdata['price_mile'] = $price_mile;
        $sdata['can_condition'] = $can_condition;

        if ($sel_img_name != '') {
            $sdata['sel_img'] = $sel_img_name;
        }
        if ($unsel_img_name != '') {
            $sdata['unsel_img'] = $unsel_img_name;
        }
        if ($banner_img_name != '') {
            $sdata['banner_img'] = $banner_img_name;
        }

        $sdata['location'] = $loc;
        if ($CategoryId != '') {
            $this->mongo_db->update('Category', $sdata, array("_id" => new MongoId($CategoryId)));
        } else {
            $this->mongo_db->insert('Category', $sdata);
        }
    }

    function AddService() {
        $this->load->library('mongo_db');
        $SubCategoryId = $this->input->post("SubCategoryId");
        $cid = $this->input->post("cid");
        $catid = $this->input->post("catid");
        $group_id = $this->input->post("group_id");
        $scname = $this->input->post("s_name");
        $scdesc = $this->input->post("s_desc");
        $fp = $this->input->post("fp");
        $sdata = array('city_id' => $cid, 'cat_id' => $catid, 's_name' => $scname,
            's_desc' => $scdesc, 'group_id' => $group_id,
            'fixed_price' => $fp);
        if ($SubCategoryId != '') {
            $this->mongo_db->update('ProviderSubCategory', $sdata, array("_id" => new MongoId($SubCategoryId)));
        } else {
            $this->mongo_db->insert('ProviderSubCategory', $sdata);
        }
    }

    function AddnewProduct() {

        $this->load->library('mongo_db');
        $ProductId = $this->input->post("ProductId");
        if ($ProductId != '') {
            $this->mongo_db->update('MasterProducts', $this->input->post("FData"), array("_id" => new MongoId($ProductId)));
        } else {
            $this->mongo_db->insert('MasterProducts', $this->input->post("FData"));
        }
    }

    function editvehicle($status) {

        $data['vehicle'] = $this->db->query("select w.*,wt.city_id,v.id,v.vehiclemodel from  workplace w ,workplace_types wt,vehiclemodel v where workplace_id='" . $status . "' and w.type_id = wt.type_id and v.id = w.Vehicle_Model ")->result();

        $cityId = $data['vehicle'][0]->city_id;

        if ($cityId == '')
            return array('flag' => 1);

        $data['company'] = $this->db->query("select companyname,company_id from company_info where city = '" . $cityId . "'")->result();
        $data['cityList'] = $this->db->query("select City_Name,City_Id from city_available")->result();
        $data['workplaceTypes'] = $this->db->query("select * from workplace_types where city_id = '" . $cityId . "'")->result();
        $data['vehicleTypes'] = $this->db->query("select * from vehicleType")->result();
        $data['vehicleDoc'] = $this->db->query("select * from vechiledoc where vechileid = '" . $status . "'")->result();
        return $data;
    }

    function deactivate_company() {
        $val = $this->input->post('val');
        foreach ($val as $result) {
            $this->db->query("update company_info set status=5  where company_id='" . $result . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected company/companies deactivated succesfully", 'flag' => 1));
            return;
        }
    }

    function datatable_commission() {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        if ($this->input->post('sSearch') != '') {
            $cond = array('name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
        } else {
            $cond = array("accepted" => 1);
        }
        $num = 0;
        $data = array();
        $MAsterData = $this->mongo_db->get_where('location', $cond);
        foreach ($MAsterData as $row) {
            $num++;
            $comm = 10;
            if ($row['Commission'] == "") {
                $this->mongo_db->update('location', array("Commission" => $comm), array("_id" => new MongoId($row['_id'])));
            } else {
                $this->mongo_db->update('location', array("Commission" => $row['Commission']), array("_id" => new MongoId($row['_id'])));
            }
            $data[] = array($num, $row['user'], $row['name'], $row['email'], ($row['Commission'] == "") ? "$comm" : $row['Commission'] . ' %', "<input type='checkbox' class='checkbox' data='" . $row['Commission'] . "' value='" . $row['_id'] . "'>");
        }
        echo $this->datatables->commission_Data($data);
    }

    function datatable_sgroup() {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $citylist = $this->get_cities();
//        if ($this->input->post('sSearch') != '') {
//            $cond = array('group_name' => new MongoRegex("/^{$this->input->post('sSearch')}/i"));
//        } else {
        $cond = array();
//        }
        $num = 1;
        $data = array();
        $MAsterData = $this->mongo_db->get_where('ServiceGroup', $cond);
        foreach ($MAsterData as $row) {
            $cityName = "";
            foreach ($citylist as $city) {
                if ($city->City_Id == $row['city_id']) {
                    $cityName = $city->City_Name;
                    break;
                }
            }
//            echo $row['cat_id'];
//            echo "sss";
            if ($row['cat_id'] != 0 && $row['cat_id'] != '') {
                $catname = $this->mongo_db->get_one('ProviderCategory', array('_id' => new MongoId($row['cat_id'])), array('cat_name' => 1));
            }
            $edata = $row['_id'] . "," . $row['city_id'] . "," . $row['cat_id'] . "," . $catname['cat_name'] . ',' . $row['group_name'];
            $data[] = array($num++, $cityName, $catname['cat_name'], $row['group_name'], "<input type='checkbox' class='checkbox' data='" . $edata . "' value='" . $row['_id'] . "'>");
        }
//print_r($data);
//exit();

        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = ucwords($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(ucwords($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);

//        echo $this->datatables->commission_Data($data1);
    }

    function datatable_pgateway() {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $cond = array();
        $num = 1;
        $data = array();
        $PaymentData = $this->mongo_db->get_where('pgateway', $cond);
        foreach ($PaymentData as $row) {
            $data[] = array($num++, $row['name'], $row['percent'], $row['fixed'], "<input type='checkbox' class='checkbox' data='" . $row['_id'] . "' value='" . $row['_id'] . "'>");
        }
        if ($this->input->post('sSearch') != '') {
            $FilterArr = array();
            foreach ($data as $row) {
                $needle = ucwords($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(ucwords($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }
        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function datatable_pro_rating($proid = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select("dr.appointment_id as rahul,dr.patient_id,dr.star_rating from doctor_ratings dr", FALSE)
                ->where('dr.doc_id', $proid);
        $this->db->order_by("rahul", "desc");

        echo $this->datatables->generate();
    }

    function datatable_cservices() {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $citylist = $this->get_cities();
//        if ($this->input->post('sSearch') != '') {
//            $cond = array('name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
//        } else {
//            $cond = array();
//        }
        $num = 1;
        $data = array();
        foreach ($citylist as $city) {
            $count = $this->mongo_db->count_all_results(('ProviderSubCategory'), array('city_id' => $city->City_Id));
            $link = "<a href='" . base_url() . "index.php/superadmin/show_services/" . $city->City_Id . "'>" . $count . "</a>";
            $data[] = array($num++, $city->City_Name, $link);
        }
        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = ucwords($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(ucwords($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function datatable_show_services($cityid) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderSubCategory', array('city_id' => $cityid));
        $data = array();
        $num = 1;
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            if ($cur['group_id'] && $cur['group_id'] != '') {
                $gcur = $this->mongo_db->get_one('ServiceGroup', array('_id' => new MongoId($cur['group_id'])), array('group_name' => 1));
                $gname = $gcur['group_name'];
                $data[] = array($num++, $gname, $cur['s_name'], $cur['s_desc'], $cur['fixed_price'], "<input type='checkbox' class='checkbox' data='" . $cur['id'] . "' value='" . $cur['id'] . "'>");
            }
        }

        echo $this->datatables->commission_Data($data);
    }

    function group_sort_ajax($cityid, $groupid) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderSubCategory', array('city_id' => $cityid, 'group_id' => $groupid));
        $data = array();
        $num = 1;
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $gcur = $this->mongo_db->get_one('ServiceGroup', array('_id' => new MongoId($cur['group_id'])), array('group_name' => 1));
            $gname = $gcur['group_name'];
            $data[] = array($num++, $gname, $cur['s_name'], $cur['s_desc'], $cur['fixed_price'], "<input type='checkbox' class='checkbox' data='" . $cur['id'] . "' value='" . $cur['id'] . "'>");
        }
        echo $this->datatables->commission_Data($data);
    }

    function datatable_show_cat($cityid) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderCategory', array('city_id' => (string) $cityid));
        $data = array();
        $num = 1;
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $bimg = '<img src="' . base_url("../../pics/user.jpg") . '" style=" border-radius: 50%;" height="34" width="34">';
            $simg = '<img src="' . base_url("../../pics/user.jpg") . '" style=" border-radius: 50%;" height="34" width="34">';
            $uimg = '<img src="' . base_url("../../pics/user.jpg") . '" style=" border-radius: 50%;" height="34" width="34">';
            if ($cur['banner_img'] != "") {
                $bimg = '<img src="' . base_url("../../pics/" . $cur["banner_img"]) . '" style=" border-radius: 50%;" height="34" width="34">';
            }
            if ($cur['sel_img'] != "") {
                $simg = '<img src="' . base_url("../../pics/" . $cur["sel_img"]) . '" style=" border-radius: 50%;" height="34" width="34">';
            }
            if ($cur['unsel_img'] != "") {
                $uimg = '<img src="' . base_url("../../pics/" . $cur["unsel_img"]) . '" style=" border-radius: 50%;" height="34" width="34">';
            }
            $min_fees = "N/A";
            $base_fees = "N/A";
            $price_min = "N/A";
            $price_mile = "N/A";
            $visit_fees = "N/A";
            $fixed_price = "N/A";
            if ($cur['min_fees'] != "")
                $min_fees = $cur['min_fees'];
            if ($cur['base_fees'] != "")
                $base_fees = $cur['base_fees'];
            if ($cur['price_min'] != "")
                $price_min = $cur['price_min'];
            if ($cur['price_mile'] != "")
                $price_mile = $cur['price_mile'];
            if ($cur['visit_fees'] != "")
                $visit_fees = $cur['visit_fees'];
            if ($cur['fixed_price'] != "")
                $fixed_price = $cur['fixed_price'];

            $data[] = array($num++, $cur['cat_name'], $cur['cat_desc'], $bimg, $simg, $uimg, $cur['price_set_by'], $cur['pay_commision'],
                $cur['fee_type'], $cur['can_fees'], $min_fees, $base_fees, $price_min, $price_mile, $visit_fees, $fixed_price,
                "<input type='checkbox' class='checkbox' data='" . $cur['id'] . "' value='" . $cur['id'] . "'>");
        }

        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = ucwords($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(ucwords($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function insertcommission($status = '') {
        $providername = $this->input->post('providername');
        $commission = $this->input->post('commission');
        $this->load->library('mongo_db');
        $this->mongo_db->update('location', array("Commission" => $commission), array("_id" => new MongoId($providername)));
    }

    function AddServiceGroup() {
        $city_id = $this->input->post('city_id');
        $cat_id = $this->input->post('cat_id');
        $group_name = $this->input->post('group_name');
        $cmand = $this->input->post('cmand');
        $cmult = $this->input->post('cmult');
        $this->mongo_db->insert('ServiceGroup', array('group_name' => $group_name, 'city_id' => $city_id,
            'cat_id' => $cat_id, 'cmand' => $cmand, 'cmult' => $cmult));
    }

    function AddPaymentGateway() {
        $name = $this->input->post('name');
        $percent = $this->input->post('percent');
        $fixed = $this->input->post('fixed');
        $this->mongo_db->insert('pgateway', array('name' => $name, 'percent' => $percent, 'fixed' => $fixed));
    }

    function EditServiceGroup() {
        $egid = $this->input->post('egid');
        $city_id = $this->input->post('city_id');
        $cat_id = $this->input->post('cat_id');
        $group_name = $this->input->post('group_name');
        $cmand = $this->input->post('cmand');
        $cmult = $this->input->post('cmult');
        $this->mongo_db->update('ServiceGroup', array('group_name' => $group_name, 'city_id' => $city_id,
            'cat_id' => $cat_id, 'cmand' => $cmand, 'cmult' => $cmult), array('_id' => new MongoId($egid)));
    }

    function EditPaymentGateWay() {
        $payid = $this->input->post('pay_id');
        $name = $this->input->post('name');
        $percent = $this->input->post('percent');
        $fixed = $this->input->post('fixed');
        $this->mongo_db->update('pgateway', array('name' => $name, 'percent' => $percent, 'fixed' => $fixed), array('_id' => new MongoId($payid)));
    }

    function EditGetServiceGroup() {
        $egid = $this->input->post('egid');

        $result = $this->mongo_db->get_one('ServiceGroup', array('_id' => new MongoId($egid)));
        echo json_encode($result);
    }

    function EditGetPaymentGateWay() {
        $payid = $this->input->post('payid');

        $result = $this->mongo_db->get_one('pgateway', array('_id' => new MongoId($payid)));
        $result['_id'] = (string) $result['_id'];
        echo json_encode($result);
    }

    function DeleteServiceGroup() {
        $this->load->library('mongo_db');
        $val1 = $this->input->post('val');
        foreach ($val1 as $row1) {
            $this->mongo_db->delete('ServiceGroup', array("_id" => new MongoId($row1)));
        }
    }

    function DeletePaymentGateway() {
        $this->load->library('mongo_db');
        $val1 = $this->input->post('val');
        foreach ($val1 as $row1) {
            $this->mongo_db->delete('pgateway', array("_id" => new MongoId($row1)));
        }
    }

    function editcommission($status = '') {
        $id = $this->input->post('id');
        $commission = $this->input->post('commission');
        $this->load->library('mongo_db');
        $this->mongo_db->update('location', array("Commission" => $commission), array("_id" => new MongoId($id)));
    }

    function deletecommission() {
        $this->load->library('mongo_db');
        $val1 = $this->input->post('val');
        foreach ($val1 as $row1) {
            $this->mongo_db->updateWithUnset('location', array("_id" => new MongoId($row1)));
        }
        return;
    }

    function GetRechargedata_ajax() {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->datatables->select("m.last_name,m.doc_id,m.first_name,ROUND(((select sum(RechargeAmount) from DriverRecharge where m.doc_id = mas_id) - (select sum(app_owner_pl) from appointment where status = 7  and doc_id = m.doc_id)),2),(select RechargeDate from DriverRecharge where m.doc_id = mas_id order by id desc limit 1)", false)
                ->edit_column('m.last_name', 'counter/$1', 'm.doc_id')
                ->add_column('OPERATION', '<a href="' . base_url("index.php/superadmin/ProRechargeStatement/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">STATEMENT</button></a>
<a href="' . base_url("index.php/superadmin/Recharge/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">RECHARGE</button>', 'm.doc_id')
                ->from('doctor m');

        echo $this->datatables->generate();
    }

    function makeonline() {
        $masterid = $this->input->post('masterid');
        $this->load->library('mongo_db');
        foreach ($masterid as $row) {
            $res = $this->mongo_db->update('location', array('status' => (int) 3), array('user' => (int) $row));
        }
        echo json_encode(array('flag' => 0, 'msg' => 'Status Updated'));
    }

    function makeOffline() {
        $masterid = $this->input->post('masterid');
        $this->load->library('mongo_db');
        foreach ($masterid as $row) {
            $res = $this->mongo_db->update('location', array('status' => (int) 4), array('user' => (int) $row));
        }
        echo json_encode(array('flag' => 0, 'msg' => 'Status Updated'));
    }

    function makeLogout() {
        $masterid = $this->input->post('masterid');
        $this->load->library('mongo_db');
        foreach ($masterid as $row) {
            $this->db->query("update user_sessions set loggedIn = '2' where oid = '" . $row . "' and user_type = '1'");
            $res = $this->mongo_db->update('location', array('status' => 4, 'login' => 2, 'online' => 0), array('user' => (int) $row));
        }
        $msg = 'Your profile has been suspended on ' . Appname . ', contact ' . Appname . ' customer care';
        $alert = "LoggedOut";
        $action = 23;
        $this->sendPushNotification($masterid, $action, $msg, $alert, 1);
        echo json_encode(array('flag' => 0, 'msg' => 'Status Updated'));
    }

    function sendPushNotification($proidArr, $action, $msg = "", $alert = "", $userType) {

        $res = array();
//        $getTokensQry = $this->db->query("select u.* from user_sessions u where u.oid IN (" . implode(',', $proidArr) . ") and u.loggedIn = 1 and u.user_type = '" . $userType . "' ")->result();
        $sesQuery = "select u.* from user_sessions u where u.oid IN (" . implode(',', $proidArr) . ") and u.loggedIn = 1 and u.user_type = " . $userType;

        $getTokensQry = $this->db->query($sesQuery)->result();
        foreach ($getTokensQry as $token) {
            if ($token->type == '2') {

                $res [] = $this->sendAndroidPush(array($token->push_token), array('st' => $action, 'payload' => $msg), PROVIDER_ANDROID_PUSH_KEY);
            } else {
                $amazon = new AwsPush();
                $pushReturn2 = $amazon->publishJson(array(
                    'MessageStructure' => 'json',
                    'TargetArn' => $token->push_token,
                    'Message' => json_encode(array(
                        'APNS' => json_encode(array(
                            'aps' => array('alert' => $msg, 'action' => $action)
                        ))
                    )),
                ));

                if ($pushReturn2[0]['MessageId'] == '')
                    $res[] = array('errorNo' => 44);
                else
                    $res[] = array('errorNo' => 46);
            }
        }

        return $res;
    }

    function GetProDetils($id) {

        $mas = $this->db->query("select * from doctor where doc_id = '" . $id . "'")->row();
        return $mas;
    }

    function ProRechargeDetails($id) {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->datatables->select("id,RechargeAmount,DATE_FORMAT(RechargeDate, '%b %d %Y %h:%i %p') as rdat,mas_id", false)
                ->add_column('OPERATION', '<button class="btn btn-success btn-cons-onclick" style="min-width: 83px !important;" id="$1">EDIT</button>
    <a href="' . base_url("index.php/superadmin/RechargeOperation/2/$1/$2") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">DELETE</button>', 'id,mas_id')
                ->unset_column('mas_id')
                ->from('DriverRecharge')
                ->where('mas_id', $id);

        echo $this->datatables->generate();
    }

    function RechargeOperation($for, $id, $masid) {

        $message = "something went wrong try after some time.";

        if ($for == 0) {
            $amount = $id; // here id is nothing but amount
            $query = "insert into DriverRecharge(mas_id,RechargeDate,RechargeAmount)values('" . $masid . "',now(),'" . $amount . "')";
            $flag = $this->db->query($query);
            if ($flag)
                $message = 'Added Amount to wallet.';
        }
        else if ($for == 1) {
            // $id is nothing but amount
            $query = "update DriverRecharge set RechargeAmount = '" . $id . "' where  id ='" . $masid . "'";
            $flag = $this->db->query($query);
            if ($flag)
                $message = 'Updated Amount.';
        }
        else if ($for == 2) {
            // here id is nothing but amount
            $query = "delete from DriverRecharge where id ='" . $id . "'";
            $flag = $this->db->query($query);
            if ($flag)
                $message = 'Deleted Amount.';
            return 44;
        }

        echo json_encode(array('error' => $message));
    }

    function ProRechargeStatement($id) {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->datatables->select("ap.OpeningBal,ap.appointment_id,ROUND(ap.app_owner_pl,2),ap.OpeningBal as opbal", false)
                ->from('appointment ap', false)
                ->where('ap.doc_id', $id);

        echo $this->datatables->generate();
    }

    function suspend_company() {
        $val = $this->input->post('val');
        foreach ($val as $result) {
            $this->db->query("update company_info set status=6  where company_id='" . $result . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected company/companies suspended succesfully", 'flag' => 1));
            return;
        }
    }

    function get_vehicle_data() {
        $query = $this->db->query("select w.*,cty.City_Name from workplace_types w, city_available cty where w.city_id = cty.City_Id")->result();

        return $query;
    }

    function logoutdriver() {
        $driverid = $this->input->post('driverid');
        $this->load->library('mongo_db');
        $this->db->query("update user_sessions  set loggedIn = 2 where user_type = '1' and oid = '" . $driverid . "' and loggedIn = 1");

        $this->mongo_db->update('location', array('status' => 4), array('user' => (int) $driverid));
    }

    function insert_vehicletype() {
        $vehicletype = $this->input->post('vehicletype');
        $seating = $this->input->post('seating');
        $minimumfare = $this->input->post('minimumfare');
        $basefare = $this->input->post('basefare');
        $priceperminute = $this->input->post('priceperminute');
        $priceperkm = $this->input->post('priceperkm');
        $discription = $this->input->post('discription');
        $city = $this->input->post('city');
        $cancilationfee = $this->input->post('cancilationfee');

        $resulrt = $this->db->query("insert into workplace_types(type_name,max_size,basefare,min_fare,price_per_min,
        price_per_km,type_desc,city_id,cancilation_fee) values('" . $vehicletype . "',

        '" . $seating . "',

        '" . $basefare . "',
        '" . $minimumfare . "',
        '" . $priceperminute . "',
        '" . $priceperkm . "',
        '" . $discription . "',
        '" . $city . "','" . $cancilationfee . "')");

        $type_id = $this->db->insert_id();
        $this->load->database();
        $cityData = $this->db->query("select * from city_available where city_id =  '" . $city . "'")->row_array();
        $this->load->library('mongo_db');

        $insertArr = array('type' => (int) $type_id, 'type_name' => $vehicletype, 'max_size' => (int) $seating, 'basefare' => (float) $basefare, 'min_fare' => (float) $minimumfare, 'price_per_min' => (float) $priceperminute, 'price_per_km' => (float) $priceperkm, 'type_desc' => $discription, 'city_id' => (int) $city, "location" => array("longitude" => (double) $cityData['City_Long'], "latitude" => (double) $cityData['City_Lat']));
        $this->mongo_db->insert('vehicleTypes', $insertArr);
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your vehicle type added succesfully", 'flag' => 1));
            return;
        }
    }

    function edit_vehicletype($param) {
        $result = $this->db->query("select * from workplace_types where type_id='" . $param . "'")->result();
        return $result;
    }

    function update_vehicletype($param) {
        $vehicletype = $this->input->post('vehicletype');
        $seating = $this->input->post('seating');
        $minimumfare = $this->input->post('minimumfare');
        $basefare = $this->input->post('basefare');
        $priceperminute = $this->input->post('priceperminute');
        $priceperkm = $this->input->post('priceperkm');
        $discription = $this->input->post('discription');
        $city = $this->input->post('city');
        $cancilationfee = $this->input->post('cancilationfee');

        $fdata = array('type_name' => $vehicletype,
            'max_size' => (int) $seating,
            'basefare' => (float) $basefare, 'min_fare' => (float) $minimumfare,
            'price_per_min' => (float) $priceperminute,
            'price_per_km' => (float) $priceperkm, 'type_desc' => $discription,
            'city_id' => (int) $city,
        );
        $result = $this->db->query("update workplace_types set type_name='" . $vehicletype . "',
        max_size='" . $seating . "',
        basefare='" . $basefare . "',
        min_fare='" . $minimumfare . "',
        price_per_min='" . $priceperminute . "',
        price_per_km='" . $priceperkm . "',
        type_desc='" . $discription . "',
        city_id='" . $city . "',cancilation_fee = '" . $cancilationfee . "' where type_id='" . $param . "' ");

        $this->load->library('mongo_db');

        $this->mongo_db->update("vehicleTypes", $fdata, array("type" => (int) $param));




        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your vehicle type updated succesfully", 'flag' => 1));
            return;
        } else {
            echo json_encode(array('msg' => "your vehicle type not updated try again!", 'flag' => 0));
            return;
        }
    }

    function get_vehiclemake() {
        return $this->db->query("select * from vehicleType")->result();
    }

    function get_vehiclemodal() {
        return $this->db->query("select vm.*,vt.vehicletype from vehiclemodel vm,vehicleType vt where vm.vehicletypeid= vt.id")->result();
    }

    function vehiclemodal() {
        return $this->db->query("select *  from vehiclemodel order by vehiclemodel")->result();
    }

    function insert_typename() {
        $typename = $this->input->post('typename');

        $result = $this->db->query("insert into vehicleType(vehicletype) values('" . $typename . "')");
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your  type name added succesfully", 'flag' => 1));
            return;
        }
    }

    function deletetype() {
        $vehicleid = $this->input->post('vehicletypeid');

        $result = $this->db->query("delete from workplace_types where type_id ='" . $vehicleid . "'");
    }

    function deletecompany() {
        $companyid = $this->input->post('companyid');

        //        $result = $this->db->query("delete from company_info where company_id ='" . $companyid . "' ");

        $affectedRows = 0;

        $deleteVehicleTypes = $this->db->query("delete from company_info where company_id = " . $companyid)->row_array();
        $affectedRows += $this->db->affected_rows();

        if ($affectedRows <= 0) {

            echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'Failed to delete'));
            return false;
        }

        $selectType = $this->db->query("select type_id from workplace where company_id = '" . $companyid . "'")->result();


        foreach ($selectType as $type) {
            $type_ids[] = (int) $type['type_id'];
        }

        $deleteAllVehicles = $this->db->query("delete from workplace_types where type_id  in (" . implode(',', $type_ids) . ")");
        $affectedRows += $this->db->affected_rows();

        $deleteAllVehicles = $this->db->query("delete from workplace where type_id  in (" . implode(',', $type_ids) . ")");
        $affectedRows += $this->db->affected_rows();


        $this->load->library('mongo_db');

        $return[] = $this->mongo_db->delete('vehicleTypes', array('type' => array('$in' => $type_ids)));

        $getAllDriversCursor = $this->mongo_db->get('vehicleTypes', array('type' => array('$in' => $type_ids)));

        $mas_id = array();

        foreach ($getAllDriversCursor as $driver) {
            $mas_id[] = (int) $driver['user'];
        }

        $return[] = $this->mongo_db->delete('location', array('user' => array('$in' => $mas_id)));

        $updateMysqlDriverQry = $this->db->query("delete from master where mas_id in (" . implode(',', $mas_id) . ")");
        $affectedRows += $this->db->affected_rows();

        $updateMysqlApptQry = $this->db->query("delete from appointment where mas_id in (" . implode(',', $mas_id) . ")");
        $affectedRows += $this->db->affected_rows();

        $updateMysqlReviewQry = $this->db->query("delete from passenger_rating where mas_id in (" . implode(',', $mas_id) . ")");
        $affectedRows += $this->db->affected_rows();

        $updateMysqlReviewQry = $this->db->query("delete from master_ratings where mas_id in (" . implode(',', $mas_id) . ")");
        $affectedRows += $this->db->affected_rows();

        $updateMysqlReviewQry = $this->db->query("delete from user_sessions where user_type = 1 and oid in (" . implode(',', $mas_id) . ")");
        $affectedRows += $this->db->affected_rows();

        echo json_encode(array('flag' => 0, 'affectedRows' => $deleteAllVehicles . $deleteVehicleTypes . $updateMysqlDriverQry));
    }

    function deletecountry() {
        $countryid = $this->input->post('countryid');

        $result = $this->db->query("delete from country where Country_Id ='" . $countryid . "'");
    }

    function deletepagecity() {
        $cityid = $this->input->post('cityid');
        $result = $this->db->query("select company_id from company_info where city = '" . $cityid . "'")->result();
        $companies = array();
        foreach ($result as $company) {
            $companies[] = $company->company_id;
        }
        $result1 = $this->db->query("select type_id from workplace_types where city_id = '" . $cityid . "'")->result();
        $vehicleTypes = array();
        foreach ($result1 as $company) {
            $vehicleTypes[] = $company->type_id;
        }
        $this->db->query("delete from city_available where City_Id = '" . $cityid . "'");
        $this->db->query("delete from company_info where company_id in (" . implode(',', $companies) . ")");
        $this->db->query("delete from dispatcher where city ='" . $cityid . "'");
        $this->db->query("delete from workplace where type_id in (" . implode(',', $vehicleTypes) . ")");
        $this->db->query("delete from workplace_types where type_id in (" . implode(',', $vehicleTypes) . ")");
        $this->db->query("delete from coupons where city_id ='" . $cityid . "'");
        $this->db->query("delete from master where company_id  in (" . implode(',', $companies) . ")");
    }

    function deletedriver() {
        $masterid = $this->input->post('masterid');

        $this->load->library('mongo_db');
        $affectedRows = 0;
        foreach ($masterid as $row) {
            $getMasterDet = $this->db->query("select * from doctor where doc_id = '" . $row . "'")->row_array();
            if (!is_array($getMasterDet)) {

                echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'Driver not available'));
                return false;
            }
            $userType = $this->mongo_db->get_one('location', array('user' => (int) $row));

            $emailD = "ProfileImages/" . $userType['email'] . ".png";
            $bucket = BUCKET_NAME;
            $s3 = new S3(AMAZON_AWS_ACCESS_KEY, AMAZON_AWS_AUTH_SECRET);
            if ($s3->deleteObject($bucket, $emailD)) {
                $msg = true;
            } else
                $msg = false;
            $emailD = "ProfileImages/" . $userType['email'] . '.jpg';
            $bucket = BUCKET_NAME;
            $s3 = new S3(AMAZON_AWS_ACCESS_KEY, AMAZON_AWS_AUTH_SECRET);
            if ($s3->deleteObject($bucket, $emailD)) {
                $msg = true;
            } else
                $msg = false;



            $location = $this->mongo_db->delete('location', array('user' => (int) $row));
            $bookings = $this->mongo_db->delete('bookings', array('provider_id' => $row));

            $updateMysqlDriverQry = $this->db->query("delete from doctor where doc_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlApptQry = $this->db->query("delete from bookings where pro_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();


            $updateMysqlReviewQry = $this->db->query("delete from doctor_ratings where doc_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from user_sessions where user_type = 1 and oid = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

            $this->mongo_db->updatewithpull('operators', array('prolist' => array('proid' => $row)), array('prolist.proid' => $row));
        }


        echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'msg' => 'Process completed.'));
    }

    function deletePatient() {
        $masterid = $this->input->post('masterid');

        $affectedRows = 0;
        foreach ($masterid as $row) {
            $getMasterDet = $this->db->query("select * from patient where patient_id = '" . $row . "'")->row_array();
            if (!is_array($getMasterDet)) {
                echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'patient not available'));
                return false;
            }
            $userType = $this->mongo_db->get_one('customer', array('cid' => (int) $row));

            $emailD = "ProfileImages/" . $userType['email'] . ".png";
            $bucket = BUCKET_NAME;
            $s3 = new S3(AMAZON_AWS_ACCESS_KEY, AMAZON_AWS_AUTH_SECRET);
            if ($s3->deleteObject($bucket, $emailD)) {
                $msg = true;
            } else
                $msg = false;
            $emailD = "ProfileImages/" . $userType['email'] . '.jpg';

            if ($s3->deleteObject($bucket, $emailD)) {
                $msg = true;
            } else
                $msg = false;

            $this->mongo_db->delete('customer', array('cid' => (int) $row));
            $updateMysqlDriverQry = $this->db->query("delete from patient where patient_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();
            print_r($affectedRows);
            $updateMysqlApptQry = $this->db->query("delete from bookings where cust_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

//            $updateMysqlReviewQry = $this->db->query("delete from patient_rating where patient_id = '" . $row . "'");
//            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from doctor_ratings where patient_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from user_sessions where user_type = 2 and oid = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();
        }
        echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'msg' => 'Process completed.'));
    }

    function deletemodal() {
        $modalid = $this->input->post('modalid');

        $result = $this->db->query("delete from vehiclemodel where id ='" . $modalid . "'");
    }

    function insert_modal() {
        $typeid = $this->input->post('typeid');

        $modal = $this->input->post('modal');

        $res = $this->db->query("insert into vehiclemodel(vehiclemodel,vehicletypeid) values('" . $modal . "','" . $typeid . "')");
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your  modal name added succesfully", 'flag' => 1));
            return;
        }
    }

    function deletevehicletype() {
        $val = $this->input->post('val');
        $this->load->library('mongo_db');
        foreach ($val as $row) {
            $affectedRows = 0;


            $deleteAllVehicles = $this->db->query("delete from workplace_types where type_id  = '" . $row . "'");

            $affectedRows += $this->db->affected_rows();

            if ($affectedRows <= 0) {

                echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'Failed to delete'));
                return false;
            }

            $deleteAllVehicles = $this->db->query("delete from workplace where type_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();




            $return[] = $this->mongo_db->delete('vehicleTypes', array('type' => (int) $row));

            $getAllDriversCursor = $this->mongo_db->get('location', array('type' => (int) $row));

            $mas_id = array();

            foreach ($getAllDriversCursor as $driver) {
                $mas_id[] = (int) $driver['user'];
            }

            $return[] = $this->mongo_db->delete('location', array('user' => array('$in' => $mas_id)));

            $updateMysqlDriverQry = $this->db->query("delete from master where mas_id in (" . implode(',', $mas_id) . ")");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlApptQry = $this->db->query("delete from appointment where mas_id in (" . implode(',', $mas_id) . ")");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from passenger_rating where mas_id in (" . implode(',', $mas_id) . ")");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from master_ratings where mas_id in (" . implode(',', $mas_id) . ")");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from user_sessions where user_type = 1 and oid in (" . implode(',', $mas_id) . ")");
            $affectedRows += $this->db->affected_rows();
        }

        echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'msg' => 'Process completed.'));
    }

    function deletevehiclemodal() {
        $val = $this->input->post('val');
        foreach ($val as $row) {
            $this->db->query("delete  from vehiclemodel where id = '" . $row . "' ");
        }
    }

    function deletevehicletypemodel() {
        $val = $this->input->post('val');
        foreach ($val as $row) {
            $this->db->query("delete from vehicleType where id = '" . $row . "' ");
        }
    }

    function editlonglat() {
        $val = $this->input->post('val');
        $lat = $this->input->post('lat');
        $lon = $this->input->post('lon');
        $this->db->query("update city_available set City_Lat = '" . $lat . "',City_Long = '" . $lon . "' where City_Id ='" . $val . "' ");
        $loc['longitude'] = (double) $lon;
        $loc['latitude'] = (double) $lat;
        $sdata['location'] = $loc;
        $this->mongo_db->update('Category', $sdata, array("city_id" => $val));
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => 'your latlong added successfully', 'flag' => 0));
            return;
        } else {
            echo json_encode(array('msg' => 'your latlong update failed', 'flag' => 1));
            return;
        }
    }

    function insert_city_available() {
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');
        $country = $this->input->post('country');
        $city = $this->input->post('city');

        $query = $this->db->query("select * from city_available where City_Id ='" . $city . "' ");

        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => "city  already exists", 'flag' => 0));
            return;
        } else {

            $selectCity = "select City_Name from city where City_Id = '" . $city . "'";

            $Result = $this->db->query($selectCity)->result_array();

            $this->db->query("insert into city_available(City_Id,Country_Id,City_Name,City_Lat,City_Long) values('" . $city . "','" . $country . "','" . $Result[0]['City_Name'] . "','" . $lat . "','" . $lng . "')");

            if ($this->db->affected_rows() > 0) {
                echo json_encode(array('msg' => "city added successfully", 'flag' => 1));
                return;
            }
        }
    }

    function city() {
        return $this->db->query("select City_Name,City_Id from city_available ORDER BY City_Name ASC ")->result();
    }

    function getAllServiceFromCity() {
        
    }

    function city_sorted() {
        return $this->db->query("select City_Name,City_Id from city_available ORDER BY City_Name ASC ")->result();
    }

    function get_driver() {
        return $this->db->query("select * from master ORDER BY last_name")->result();
    }

    function insert_company() {
        $companyname = $this->input->post('companyname');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $mobile = $this->input->post('mobilenumber');
        $city = $this->input->post('cityname');
        $state = $this->input->post('state');
        $postcode = $this->input->post('pincode');
        $vatnumber = $this->input->post('vatnumber');

        $status = 1;
        $companylogo = $_FILES["companylogo"]["name"];
        $extra = substr($companylogo, strrpos($companylogo, '.') + 1); //explode(".", $insurname);
        $logo = (rand(1000, 9999) * time()) . '.' . $extra;



        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/' . mainfolder . '/pics/';



        try {
            move_uploaded_file($_FILES['companylogo']['tmp_name'], $documentfolder . $logo);
        } catch (Exception $ex) {
            print_r($ex);
            return false;
        }

        $getcityname = $this->db->query("select * from company_info where  email = '" . $email . "'");
        if ($getcityname->num_rows() > 0) {


            echo json_encode(array('err' => 0));
        } else {

            $result['data'] = $this->db->query("insert into company_info(companyname,addressline1,city,state,postcode,vat_number,firstname,
        lastname,email,mobile,userame,password,status,logo) values(
        '" . $companyname . "',
        '" . $address . "',
        '" . $city . "',
        '" . $state . "',
        '" . $postcode . "',
        '" . $vatnumber . "',
        '" . $firstname . "',
        '" . $lastname . "',
        '" . $email . "',
        '" . $mobile . "',

        '" . $username . "',
        '" . $password . "', '" . $status . "','" . $logo . "')");
            echo json_encode(array('err' => 1));
        }
    }

    function update_company($param) {

        $companyname = $this->input->post('companyname');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $mobile = $this->input->post('mobilenumber');
        $city = $this->input->post('cityname');
        $state = $this->input->post('state');
        $postcode = $this->input->post('pincode');
        $vatnumber = $this->input->post('vatnumber');

        $result['data'] = $this->db->query("update company_info set companyname='" . $companyname . "',
        addressline1='" . $address . "' ,
        city='" . $city . "',
        state='" . $state . "',
        vat_number='" . $vatnumber . "',
        postcode='" . $postcode . "',
        userame='" . $username . "',
        firstname='" . $firstname . "',
        lastname='" . $lastname . "',
        email='" . $email . "',
        mobile='" . $mobile . "',
        password='" . $password . "' where company_id='" . $param . "'");


        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your company  updated successfully", 'flag' => 1));
            return;
        }
    }

    function get_passengerinfo($status) {
        $varToShowData = $this->db->query("select * from patient where status='" . $status . "'order by patient_id DESC")->result();

        return $varToShowData;
    }

    function inactivepassengers() {
        $val = $this->input->post('val');
        $res = array();
        foreach ($val as $result) {
            $this->db->query("update patient set status=1 where patient_id='" . $result . "'");

            $msg = 'Your profile has been suspended on ' . Appname . ', please contact  ' . Appname . ' customer care for further queries';
            $alert = "Rejected";
            $action = 23;
            $tempRes[] = $result;
            $res[] = $this->sendPushNotification($tempRes, $action, $msg, $alert, 2);

            $this->db->query("update user_sessions set loggedIN=2 where user_type = 2 and oid='" . $result . "'");
        }
        echo json_encode(array('pushRes' => $res));
    }

    function get_compaigns_data($status = '') {
        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        if ($status == '1' || $status == '')
            $cond = array('coupon_type' => 1, 'coupon_code' => 'REFERRAL', 'user_type' => 2, 'status' => 0);
        else if ($status == '2')
            $cond = array('coupon_type' => 2, 'user_type' => 2, 'status' => 0);
        else if ($status == '3')
            $cond = array('coupon_type' => 3, 'user_type' => 1); //, 'expiry_date' => array('$gte' => time()));

        $find = $selecttb->find($cond)->sort(array('start_date' => 1));

        $allDocs = array();

        foreach ($find as $doc) {

            $allDocs[] = $doc;
        }
        if ($status == '2') {

            foreach ($allDocs as $key => $row) {
                $volume[$key] = $row['start_date'];
            }
            array_multisort($volume, SORT_DESC, $allDocs);
            return array_reverse($allDocs);
        }
        return $allDocs;
    }

    function get_compaigns_data_ajax($for = '') {
        $st = $this->input->post('value');
        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;
        $selecttb = $db->selectCollection('coupons');
        if ($for == '1')
            $cond = array('coupon_type' => 1, 'coupon_code' => 'REFERRAL', 'user_type' => 2, 'status' => (int) $st);
        else if ($for == '2')
            if ($st == 0)
                $cond = array('coupon_type' => 2, 'user_type' => 2, 'status' => 0);
            else if ($st = 10)
                $cond = array('coupon_type' => 2, 'user_type' => 2, 'status' => 1);


        $find = $selecttb->find($cond);

        $allDocs = array();

        foreach ($find as $doc) {
            $doc['id'] = (string) $doc['_id'];

            $doc['start_date'] = date('Y-m-d', $doc['start_date']);
            $doc['expiry_date'] = date('Y-m-d', $doc['expiry_date']);

            $allDocs[] = $doc;
        }
        return json_encode(array('data' => $allDocs));
    }

    function deactivecompaigns() {
        $val = $this->input->post('val');


        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        foreach ($val as $row) {

            $selecttb->update(array('_id' => new MongoId($row)), array('$set' => array('status' => 1)));
        }
    }

    function activepassengers() {
        $val = $this->input->post('val');

        foreach ($val as $result) {
            $this->db->query("update patient set status=3 where patient_id='" . $result . "'");
        }
        echo json_encode(array('pushRes' => ""));
    }

    function insertdispatches() {
        $name = $this->input->post('name');
        $city = $this->input->post('city');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $status = 1;
        $res = $this->db->query("insert into dispatcher(dis_name,dis_email,dis_pass,city) values('" . $name . "','" . $email . "','" . $password . "','" . $city . "')");

        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => '0'));
            return;
        } else {
            echo json_encode(array('msg' => '1'));
            return;
        }
    }

    function inactivedispatchers() {
        $status = $this->input->post('val');
        foreach ($status as $row) {
            $result = $this->db->query("update dispatcher set status=2 where dis_id='" . $row . "'");
        }
    }

    function activedispatchers() {
        $status = $this->input->post('val');
        foreach ($status as $row) {
            $result = $this->db->query("update dispatcher set status=1 where dis_id='" . $row . "'");
        }
    }

    function editdispatchers() {
        $city = $this->input->post('cityval');
        $val = $this->input->post('val');

        $this->db->query("update dispatcher set city='" . $city . "' where dis_id='" . $val . "'");

        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => 'your city edited successfully', 'flag' => 0));
            return;
        } else {
            echo json_encode(array('msg' => 'your city update failed', 'flag' => 1));
            return;
        }
    }

    function editdispatcherdata() {
        $val = $this->input->post('val');

        $coun = $this->input->post('coun');

        $city = $this->input->post('city');

        $dename = $this->input->post('dename');

        $demail = $this->input->post('demail');

        $uc_code = $this->input->post('uc_code');

        $mobile2 = $this->input->post('mobile2');

        $data = array('country' => $coun, 'city' => $city, 'dispatcher_name' => $dename, 'dispatcher_email' => $demail, 'country_code' => '+' . $uc_code, 'mobile' => $mobile2);
        $this->mongo_db->update('dispatchers', $data, array('_id' => new MongoId($val)));
        echo json_encode(array('msg' => 'Dispatcher has been updated successfully', 'flag' => 0));
        return;
    }

    function editpass() {
        $newpass = $this->input->post('newpass');
        $val = $this->input->post('val');
        $this->db->query("update dispatcher set dis_pass='" . $newpass . "' where dis_id = '" . $val . "' ");
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "this password already exists. Enter new password", 'flag' => 1));
            return;
        }
    }

    function get_disputesdata($status) {
        $result = $this->db->query(" select mas.first_name as mas_fname,mas.last_name as mas_lname,mas.mas_id,slv.slave_id,slv.first_name as slv_name,slv.last_name as slv_lname,rep.report_msg,rep.report_id,rep.report_dt,rep.appointment_id from master mas,slave slv, reports rep where rep.mas_id = mas.mas_id   and rep.slave_id = slv.slave_id and rep.report_status = '" . $status . "' order by rep.report_id DESC")->result();

        return $result;
    }

    function resolvedisputes() {
        $value = $this->input->post('val');
        $mesage = $this->input->post('message');

        $res = $this->mongo_db->update('bookings', array('dispute_status' => '2', 'dispute_mngt_msg' => $mesage), array('bid' => (string) $value));

        if ($res) {

            $bdata = $this->mongo_db->db->selectCollection('bookings')->findOne(array('bid' => (string) $value));

            $sendMail = new sendAMail(APP_SERVER_HOST);
            $mailRes = $sendMail->DisputeResolved($bdata);
            echo json_encode(array('msg' => "your selected dispute resolved succesfully", 'flag' => 1, 'mailRes' => $mailRes));
            return;
        } else {
            echo json_encode(array('msg' => "Error Occured in resolving dispute", 'flag' => 1));
            return;
        }
    }

    function driver() {
        $res = $this->db->query("select * from master order by first_name")->result();
        return $res;
    }

    function passenger() {
        $res = $this->db->query("select * from slave")->result();
        return $res;
    }

    function insertcampaigns() {

        //$coupon_type == '1'
        $city = $this->input->post('city');
        $coupon_type = $this->input->post('coupon_type');
        $discount = $this->input->post('discount');
        $discounttype = $this->input->post('discountradio');
        $referaldiscount = $this->input->post('referaldiscount');
        $refferaldiscounttype = $this->input->post('refferalradio');
        $message = $this->input->post('message');
        $title = $this->input->post('title');

        //$coupon_type == '2'
        $codes = $this->input->post('codes');
        $citys = $this->input->post('citys');
        $discounts = $this->input->post('discounts');
        $messages = $this->input->post('messages');
        $discounttypes = $this->input->post('discounttypes');


        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        if ($coupon_type == '1') {
            $cond = array('coupon_type' => 1, 'coupon_code' => 'REFERRAL', 'city_id' => (int) $city, 'status' => 0);
            $find = $selecttb->findOne($cond);

            if (is_array($find)) {
                return json_encode(array('msg' => "Referral campaign already exists in this city ", 'flag' => 1));
            }
        }

        if ($coupon_type == '2') {
            $city = $citys;
            $cond = array('coupon_type' => 2, 'coupon_code' => $codes, 'city_id' => (int) $city, 'status' => 0, 'expiry_date' => array('$gt' => time()));
            $find = $selecttb->findOne($cond);

            if (is_array($find)) {
                return json_encode(array('msg' => "Same coupon already exists in this city", 'flag' => 1));
            }
        }

        $cityDet = $this->db->query("select ca.*,c.Currency from city_available ca,city c  where c.City_Id = ca.City_Id and ca.City_Id = '" . $city . "'")->result();

        if ($coupon_type == '1') {

            $insert = array(
                "coupon_code" => "REFERRAL",
                "coupon_type" => 1,
                "discount_type" => (int) $discounttype,
                "discount" => (float) $discount,
                "referral_discount_type" => (int) $refferaldiscounttype,
                "referral_discount" => (float) $referaldiscount,
                "message" => $message,
                "status" => 0,
                "title" => $title,
                "city_id" => (int) $city,
                "currency" => $cityDet[0]->Currency,
                "city_name" => $cityDet[0]->City_Name,
                "location" => array(
                    "longitude" => (double) $cityDet[0]->City_Long,
                    "latitude" => (double) $cityDet[0]->City_Lat
                ),
                "user_type" => 2
            );

            $selecttb->insert($insert);
        } else if ($coupon_type == '2') {
            $insert = array(
                "coupon_code" => $codes,
                "coupon_type" => 2,
                "start_date" => (int) strtotime($this->input->post('sdate')),
                "expiry_date" => (int) strtotime($this->input->post('edate')),
                "discount_type" => (int) $discounttypes,
                "discount" => (float) $discounts,
                "message" => $messages,
                "status" => 0,
                "title" => $title,
                "city_id" => (int) $city,
                "currency" => $cityDet[0]->Currency,
                "city_name" => $cityDet[0]->City_Name,
                "location" => array(
                    "longitude" => (double) $cityDet[0]->City_Long,
                    "latitude" => (double) $cityDet[0]->City_Lat
                ),
                "user_type" => 2
            );
            $selecttb->insert($insert);
        }
        //         else{
        return json_encode(array('msg' => "Great! Your referrals has been added sucessfully for this city", 'flag' => 0, 'data' => $insert));
        //            }
    }

    function updatecompaigns() {

        // for coupon types 1
        $coupon_type = $this->input->post('coupon_type');
        $discount = $this->input->post('discount');
        $discounttype = $this->input->post('discountradio');
        $referaldiscount = $this->input->post('referaldiscount');
        $refferaldiscounttype = $this->input->post('refferalradio');
        $message = $this->input->post('message');
        $title = $this->input->post('title');
        $cuponid = $this->input->post('val');

        // for coupon types 2
        $cuponids = $this->input->post('val2');
        $discounts = $this->input->post('discounts');
        $messages = $this->input->post('messages');
        $codes = $this->input->post('codes');
        $discounttypes = $this->input->post('discounttypes');

        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        if ($coupon_type == '1') {

            $selecttb->update(array('_id' => new MongoId($cuponid)), array(
                '$set' => array(
                    "discount_type" => (int) $discounttype,
                    "discount" => (float) $discount,
                    "referral_discount_type" => (int) $refferaldiscounttype,
                    "referral_discount" => (float) $referaldiscount,
                    "message" => $message,
                    "title" => $title,
                    "status" => 0
            )));
        } else if ($coupon_type == '2') {
            $selecttb->update(array('_id' => new MongoId($cuponids)), array(
                '$set' => array(
                    "coupon_code" => $codes,
                    "start_date" => (int) strtotime($this->input->post('sdate')),
                    "expiry_date" => (int) strtotime($this->input->post('edate')),
                    "discount_type" => (int) $discounttypes,
                    "discount" => (float) $discounts,
                    "message" => $messages,
                    "status" => 0,
                    "title" => $title,
                    "user_type" => 2
            )));
        }
    }

    function get_promo_details($id, $page) {
        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;
        $selecttb = $db->selectCollection('coupons');
        $find = $selecttb->find(array('_id' => new MongoId($id)));
        $bookings = array();
        foreach ($find as $cur) {
            foreach ($cur['bookings'] as $dis) {
                $bookings[] = $dis['booking_id'];
            }
        }
        if (count($bookings) > 0) {
            $query = 'select a.appointment_dt,p.patient_id,p.email,a.bid from bookings a,patient p where a.cust_id = p.patient_id and a.bid IN(' . implode(',', $bookings) . ') and a.status = 7';
            return $this->db->query($query)->result_array();
        } else {
            return false;
        }
    }

    function get_referral_details($id, $page) {
        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;
        $selecttb = $db->selectCollection('coupons');
        $find = $selecttb->find(array('_id' => new MongoId($id)));
        $all = array();
        foreach ($find as $cur)
            $all[] = $cur;
        return $all;
    }

    function refered($code, $refCode, $page) {
        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;
        $selecttb = $db->selectCollection('coupons');
        $find = $selecttb->find(array('_id' => new MongoId($code), 'signups.coupon_code' => $refCode), array('signups.$' => 1));
        $all = array();
        foreach ($find as $cur) {
            $all[] = $cur;
        }
        return $all;
    }

    function editcompaigns() {
        $value = $this->input->post('val');

        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        $resu = $selecttb->findOne(array('_id' => new MongoId($value)));

        //        print_r($resu);exit();
        $resu['start_date'] = date('Y-m-d', $resu['start_date']);
        $resu['expiry_date'] = date('Y-m-d', $resu['expiry_date']);


        echo json_encode($resu);
    }

    function insertpass() {
        $password = $this->input->post('newpass');
        $val = $this->input->post('val');
//        print_r($val);die;
        $res = $this->db->query("update patient set password = md5('" . $password . "')  where patient_id='" . $val . "'");
        //        return $res;
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "Password updated successfully", 'flag' => 0));
            return;
        }
    }

    function get_company_data($param) {
        $result = $this->db->query("select * from company_info where company_id='" . $param . "' ")->result();
        return $result;
    }

    function company_data() {
        $result = $this->db->query("select * from company_info")->result();
        return $result;
    }

    function get_dispatchers_data($status) {

        $res = $this->db->query("select * from dispatcher where status='" . $status . "'")->result();
        return $res;
    }

    function delete_dispatcher() {
        $var = $this->input->post('val');

        foreach ($var as $row) {
            $this->db->query("delete  from dispatcher where dis_id ='" . $row . "'");
        }
    }

    function get_country() {
        return $this->db->query("select * from country order by Country_Name")->result();
    }

    function datatable_cities() {

        $this->load->library('Datatables');
        $this->load->library('table');
        $_POST["sColumns"] = "UCASE(co.Country_Name),UCASE(ci.City_Name),UCASE(b.Currency),ci.City_Lat,ci.City_Long";
        $this->datatables->select('ci.City_Id,UCASE(co.Country_Name),UCASE(ci.City_Name),UCASE(b.Currency),ci.City_Lat,ci.City_Long')
                ->unset_column('ci.City_Id')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'ci.City_Id')
                ->from('city_available ci,country co,city b')
                ->where('ci.country_id = co.country_id AND ci.City_Id = b.City_Id'); //order by slave_id DESC ",false);
//        $this->db->order_by("ci.City_Id", "desc");

        echo $this->datatables->generate();
    }

    function datatable_dispatchers() {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $cond = array();
        if ($this->input->post('sSearch') != '') {
            $cond = array('dispatcher_name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
        }
        $disData = $this->mongo_db->get_where('dispatchers', $cond);



        $num = 0;
        $data = array();
        foreach ($disData as $row) {
            $num++;

            $data[] = array($num, $row['dispatcher_name'], $row['dispatcher_email'], $num,
                $row['city'], $row['country_code'] . $row['mobile'], "<input type='checkbox' class='checkbox' data='" . $row['_id'] . "' value='" . $row['_id'] . "'>");
        }
        echo $this->datatables->commission_Data($data);
    }

    function datatable_promotion($id) {

        $this->load->library('mongo_db');

        $find = array('_id' => new MongoId($id));
        $disData = $this->mongo_db->get_where('coupons', $find);
        $bookings = array();
        foreach ($disData as $cur) {

            foreach ($cur['usage'] as $dis) {
                $bookings[] = $dis['booking_id'];
            }
        }



        $data = array();
        $this->load->library('Datatables');
        $this->load->library('table');
        $city = $this->mongo_db->get_where('bookings', array('bid' => array('$in' => $bookings)));
        foreach ($city as $result) {
            $data[] = array($result['comp']['sub_total_cust'], $result['coupon_discount'], $result['invoiceData']['billed_amount'], $result['appt_created'], $result['bid'], $result['customer']['id'], $result['customer']['fname'], $result['customer']['email']);
        }

        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = strtoupper($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(strtoupper($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function GetSubCategoryByMainCat($mcat) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderSubCategory', array('cat_id' => $mcat));
        $slist = array();
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $slist[] = $cur;
        }
        return $slist;
    }

    function get_All_SubCategory() {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderSubCategory', array());
        $slist = array();
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $slist[] = $cur;
        }
        return $slist;
    }

    function datatable_referrals() {
        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select('c.referred_user_id,c.user_id,s.created_dt,s.slave_id,s.first_name')
                ->unset_column('')
                ->add_column('')
                ->from('')
                ->where('');
        $this->db->order_by("", "desc");
        echo $this->datatables->generate();
    }

    function datatable_promodetails() {
        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select('c.')
                ->unset_column('')
                ->add_column('')
                ->from('')
                ->where('');
        $this->db->order_by("", "desc");
        echo $this->datatables->generate();
    }

    function datatable_companys($status = '') {

        $city = $this->session->userdata('city_id');
        if ($city != '0')
            $citylist = 'status ="' . $status . '"  and co.city = "' . $city . '"';
        else
            $citylist = 'status ="' . $status . '"';

        $this->load->library('Datatables');
        $this->load->library('table');


        $this->datatables->select('co.company_id,co.companyname,co.addressline1,(select City_Name from city  where City_Id = co.city) as cities,co.state,co.postcode,co.firstname,co.lastname,co.email,co.mobile', false)
                //                ->unset_column('co.status')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value= "$1"/>', 'co.company_id')
                ->from('company_info co')
                ->where($citylist);
        $this->db->order_by("co.company_id", "desc");
        echo $this->datatables->generate();
    }

    function datatable_vehicletype($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');
        $city = $this->session->userdata('city_id');
        $cityCond = "";
        if ($city != '0')
            $cityCond = ' and w.city_id = "' . $city . '"';

        $this->datatables->select(' w.type_id,w.type_name,w.max_size,w.basefare,w.min_fare,w.price_per_min,w.price_per_km,w.type_desc,cty.City_Name,w.cancilation_fee')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value= "$1"/>', 'w.type_id')
                ->from('workplace_types w, city_available cty')
                ->where('w.city_id = cty.City_Id' . $cityCond); //order by slave_id DESC ",false);
        $this->db->order_by("w.type_id", "desc");
        echo $this->datatables->generate();
    }

    function documentgetdata() {
        $val = $this->input->post("val");
        $return = array();
        foreach ($val as $row) {
            $data = $this->db->query("select * from doctor where doc_id = '" . $row . "'")->result();
        }
        foreach ($data as $doc) {
            if ($doc->medical_license_pic != NULL)
                $return[] = array('doctype' => $doc->license_type, 'url' => $doc->medical_license_pic, 'expirydate' => $doc->board_certification_expiry_dt);
        }
        return $return;
    }

    function datatable_vehicles($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $city = $this->session->userdata('city_id');
        $company = $this->session->userdata('company_id');
        if (($city != '0') && ($company == '0'))
            $citylist = ' and wt.city_id = "' . $city . '"';
        else if (($city != '0') && ($company != '0'))
            $citylist = ' and w.company = "' . $company . '"';
        if ($status == '12')
            $status = '1,2';

        $this->datatables->select('w.workplace_id,w.uniq_identity,'
                        . '(select vehicletype from vehicleType where id = w.Title),'
                        . '(select vehiclemodel from vehiclemodel where id = w.Vehicle_Model),'
                        . '(select type_name from workplace_types  where type_id = w.type_id),'
                        . '(select companyname from company_info  where company_id = w.company),'
                        . 'w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Insurance_No,w.Vehicle_Color,'
                        . '(select City_Name from city where City_Id = wt.city_id)')
                ->unset_column('w.workplace_id')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value= "$1"/>', 'w.workplace_id')
                ->from('workplace w,vehicleType vt,vehiclemodel vm,workplace_types wt,company_info ci ')
                ->where('vt.id = w.title and w.company = ci.company_id and vm.id=w.Vehicle_Model and wt.type_id = w.type_id  and w.status IN (' . $status . ')' . $citylist); //order by slave_id DESC ",false);
        $this->db->order_by("w.workplace_id", "desc");


        echo $this->datatables->generate();
    }

    function loadAvailableCity() {
        $countryid = $this->input->post('country');
        $Result = $this->db->query("select c.* from city c where c.Country_Id = '" . $countryid . "' and c.City_Id not in (select City_Id from city_available where Country_Id = '" . $countryid . "')")->result();
        $q = "select c.* from city c where c.Country_Id = '" . $countryid . "' and c.City_Id not in (select City_Id from city_available where Country_Id = '" . $countryid . "')";
        return $Result;
    }

    function datatable_disputes($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');

        if ($this->input->post('sSearch') != '') {
            $cond = array('provider.fname' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
        } else {
            $cond = array('disputed' => 1, 'dispute_status' => (string) $status);
        }
        $num = 0;
        $data = array();
//        print_r($cond);
        $disputeData = $this->mongo_db->get_where('bookings', $cond);
        foreach ($disputeData as $row) {
            $num++;
            if ($row['dispute_status'] == '2') {
                $data[] = array($num, $row['customer']['id'], $row['customer']['fname'], $row['provider']['id'],
                    $row['provider']['fname'], $row['dispute_mngt_msg'], $row['dispute_dt'], $row['bid'],
                    "<input type='checkbox' class='checkbox' data='" . $row['bid'] . "' value='" . $row['bid'] . "'>");
            } else {
                $data[] = array($num, $row['customer']['id'], $row['customer']['fname'], $row['provider']['id'],
                    $row['provider']['fname'], $row['dispute_msg'], $row['dispute_dt'], $row['bid'],
                    "<input type='checkbox' class='checkbox' data='" . $row['bid'] . "' value='" . $row['bid'] . "'>");
            }
        }
        echo $this->datatables->commission_Data($data);
    }

    function validateCompanyEmail() {

        $query = $this->db->query("select company_id from company_info where email='" . $this->input->post('email') . "'");
        if ($query->num_rows() > 0) {

            echo json_encode(array('msg' => '1'));
            return;
        } else {
            echo json_encode(array('msg' => '0'));
        }
    }

    function datatable_vehiclemodels($status) {


        $this->load->library('Datatables');
        $this->load->library('table');

        if ($status == 1) {

            $this->datatables->select("id,vehicletype")
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'id')
                    ->from("vehicleType");
        } else if ($status == 2) {


            $this->datatables->select("vm.id,vm.*,vt.vehicletype")
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'vm.id')
                    ->from("vehiclemodel vm,vehicleType vt")
                    ->where("vm.vehicletypeid = vt.id");
        }
        $this->db->order_by("id", "desc");
        echo $this->datatables->generate();
    }

    function datatable_drivers($for = '', $status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');
        $company = $this->session->userdata('company_id');
        $_POST["sColumns"] = "rahul,proname,monum,mas.email,mas.created_dt,mas.app_version,rating,fees_type,PROFILE PIC,DEVICE TYPE,LATITUDE,OPTION,SELECT";

        if ($for == 'my') {
            $meta = array('lastDoctor' => 'my/' . $status);
            $whererc = "mas.status IN ('" . $status . "') ";
            $whererc = "mas.status IN ('" . $status . "')";
            if ($status == 1) {
                $this->datatables->select("mas.doc_id as rahul,concat(mas.first_name,' ',mas.last_name) as proname,concat(mas.country_code,' ',mas.mobile) as monum, mas.doc_id as em,mas.created_dt,mas.app_version,IFNULL((select round(avg(star_rating),1)  from doctor_ratings where mas.doc_id = doc_id), 0) as rating, mas.doc_id as fees_type,mas.doc_id as typename, mas.profile_pic as pp,"
                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'user.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
                        ->unset_column('type_img')
                        ->unset_column('pp')
                        ->unset_column('fees_type')
                        ->unset_column('typename')
                        ->edit_column('em', "secureEmailPro/$1", 'em')
                        ->edit_column('proname', '<a href=" ' . base_url() . '../admin/index.php/masteradmin/AuthenticateUser/$1" target="_blank">$2</a>', 'rahul,proname')
                        ->edit_column('mas.created_dt', "get_formatedDate/$1", 'mas.created_dt')
                        ->edit_column('rating', '<a href=" ' . base_url() . '../superadmin/index.php/superadmin/pro_rating/$1">$2</a>', 'rahul,rating')
                        ->add_column('FEES TYPE', "get_fee_group/$1", 'fees_type')
                        ->add_column('PROFILE PIC', '<img src="$1" width="50px" height="50px" class="imageborder" onerror="this.src = \'' . base_url('/../pics/user.jpg') . '\'">', 'pp')
                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../pics/pp/$1" width="30px">', 'type_img')
                        ->add_column('LATITUDE', "get_lat/$1", 'rahul')
                        ->add_column('OPTION', '<div class="btn-group">
                                <a href="' . base_url() . 'index.php?/superadmin/editdriver/$1"><button id="$1" type="button" style="color: #ffffff !important;background-color: #10cfbd;" class="btn btn-success  pull-left"><i class="fa fa-pencil"></i>
                                  </button></a>
                                  
                              </div>', 'rahul')
                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
//                        ->add_column('DETAILS', '<a href=" ' . base_url() . '../admin/index.php/masteradmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
                        ->edit_column('rahul', '<a href=" ' . base_url() . '../superadmin/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
                        ->from("doctor mas")
                        ->where("mas.status IN ('" . $status . "')");
            } else if ($status == 3 || $status == 4) {
                $this->datatables->select("mas.doc_id as rahul,concat(mas.first_name,' ',mas.last_name) as proname,concat(mas.country_code,' ',mas.mobile) as monum, mas.doc_id as em,mas.created_dt,mas.app_version, mas.doc_id as fees_type,mas.doc_id as typename, mas.profile_pic as pp,IFNULL((select round(avg(star_rating),1)  from doctor_ratings where mas.doc_id = doc_id), 0) as rating,"
                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'user.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
                        ->unset_column('type_img')
                        ->unset_column('pp')
                        ->unset_column('fees_type')
                        ->unset_column('typename')
                        ->edit_column('em', "secureEmailPro/$1", 'em')
//                        ->edit_column('rahul', '<a href=" ' . base_url() . '../superadmin/index.php/superadmin/pro_details/$1" target="_blank">$1</a>', 'rahul')
                        ->edit_column('proname', '<a href=" ' . base_url() . '../admin/index.php/masteradmin/AuthenticateUser/$1"  target="_blank">$2</a>', 'rahul,proname')
                        ->edit_column('mas.created_dt', "get_formatedDate/$1", 'mas.created_dt')
                        ->edit_column('rating', '<a href=" ' . base_url() . '../superadmin/index.php/superadmin/pro_rating/$1" >$2</a>', 'rahul,rating')
                        ->add_column('FEES TYPE', "get_fee_group/$1", 'fees_type')
                        ->add_column('PROFILE PIC', '<img src="$1" width="50px" height="50px" class="imageborder" onerror="this.src = \'' . base_url('/../pics/user.jpg') . '\'">', 'pp')
                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../pics/pp/$1" width="30px">', 'type_img')
                        ->add_column('LATITUDE', "get_lat/$1", 'rahul')
                        ->add_column('OPTION', '<div class="btn-group">
                                <a href="' . base_url() . 'index.php?/superadmin/editdriver/$1"><button id="$1" type="button" style="color: #ffffff !important;background-color: #10cfbd;" class="btn btn-success  pull-left"><i class="fa fa-pencil"></i>
                                  </button></a>
                                 
                              </div>', 'rahul')
                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
                        ->edit_column('rahul', '<a href=" ' . base_url() . '../superadmin/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
//                        ->add_column('DETAILS', '<a href=" ' . base_url() . '../admin/index.php/masteradmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
                        ->from("doctor mas")
                        ->where($whererc);
            } else if ($status == 6) {
                $darray = array();
                $this->load->library('mongo_db');
                $db = $this->mongo_db->db;
                $selecttb = $db->location;
                $drivers = $selecttb->find(array('login' => 2));
                foreach ($drivers as $mas_id) {
                    $darray[] = $mas_id['user'];
                }
//                $result = $this->db->query("select distinct u.oid from user_sessions u ,doctor d where u.loggedIn = 2  and u.user_type = 1 and doc_id=u.oid and d.status != 1")->result();
//                foreach ($result as $res) {
//                    $darray[] = $res->oid;
//                }


                $mas_ids = implode(',', array_filter(array_unique($darray)));
                if ($mas_ids == '')
                    $mas_ids = 0;
                $whererc = "mas.doc_id = IN (" . $mas_ids . ")";
//                $whererc = "mas.doc_id = u.oid and  (u.loggedIn = 2 or  u.loggedIn = 3)  and u.user_type = 1 and mas.doc_id NOT IN (" . $mas_ids . ")";
                $this->datatables->select("mas.doc_id as rahul,concat(mas.first_name,' ',mas.last_name) as proname,concat(mas.country_code,' ',mas.mobile),mas.doc_id as em,mas.created_dt,mas.app_version, mas.doc_id as fees_type,mas.doc_id as typename, mas.profile_pic as pp,IFNULL((select round(avg(star_rating),1)  from doctor_ratings where mas.doc_id = doc_id), 0) as rating,"
                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'user.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
                        ->unset_column('type_img')
                        ->unset_column('pp')
                        ->unset_column('fees_type')
                        ->unset_column('typename')
                        ->edit_column('em', "secureEmailPro/$1", 'em')
//                        ->edit_column('rahul', '<a href=" ' . base_url() . '../superadmin/index.php/superadmin/pro_details/$1" target="_blank">$1</a>', 'rahul')
                        ->edit_column('proname', '<a href=" ' . base_url() . '../admin/index.php/masteradmin/AuthenticateUser/$1"  target="_blank">$2</a>', 'rahul,proname')
                        ->edit_column('mas.created_dt', "get_formatedDate/$1", 'mas.created_dt')
                        ->edit_column('rating', '<a href=" ' . base_url() . '../superadmin/index.php/superadmin/pro_rating/$1" >$2</a>', 'rahul,rating')
                        ->add_column('FEES TYPE', "get_fee_group/$1", 'fees_type')
                        ->add_column('PROFILE PIC', '<img src="$1" width="50px" height="50px" class="imageborder" onerror="this.src = \'' . base_url('/../pics/user.jpg') . '\'">', 'pp')
                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../pics/pp/$1" width="30px">', 'type_img')
                        ->add_column('LATITUDE', "get_lat/$1", 'rahul')
                        ->add_column('OPTION', '<div class="btn-group">
                                <a href="' . base_url() . 'index.php?/superadmin/editdriver/$1"><button id="$1" type="button" style="color: #ffffff !important;background-color: #10cfbd;" class="btn btn-success  pull-left"><i class="fa fa-pencil"></i>
                                  </button></a>
                                 
                              </div>', 'rahul')
                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
                        ->edit_column('rahul', '<a href=" ' . base_url() . '../superadmin/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
//                        ->add_column('DETAILS', '<a href=" ' . base_url() . '../admin/index.php/masteradmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
                        ->from("doctor mas")
                        ->where("mas.doc_id IN (" . $mas_ids . ")");
            }
        } else if ($for == 'mo') {
            $meta = array('lastDoctor' => 'mo/' . $status);
            $this->load->library('mongo_db');
            $db = $this->mongo_db->db;
            $selecttb = $db->location;
            $darray = $latlong = array();
            if ($status == 3) { //online or free
//                $drivers = $selecttb->find(array('status' => (int) $status));
                $drivers = $selecttb->find(array('status' => (int) $status, 'login' => 1, 'booked' => 0));
                foreach ($drivers as $mas_id) {
                    $darray[] = $mas_id['user'];
                    $latlong[$mas_id['user']] = array($mas_id['location']['latitude'], $mas_id['location']['longitude']);
                }
            } elseif ($status == 567) {//booked
                $drivers = $selecttb->find(array('login' => 1, 'booked' => 1)); //array('$in' => array(5, 6, 7))));
                foreach ($drivers as $mas_id) {
                    $darray[] = $mas_id['user'];

                    $latlong[$mas_id['user']] = array($mas_id['location']['latitude'], $mas_id['location']['longitude']);
                }
            } elseif ($status == 30) {//OFFLINE
                $drivers = $selecttb->find(array('login' => 1, 'status' => array('$in' => array(4)), 'booked' => 0));
                foreach ($drivers as $mas_id) {
                    $darray[] = $mas_id['user'];
                    $latlong[$mas_id['user']] = array($mas_id['location']['latitude'], $mas_id['location']['longitude']);
                }
            }
            $mas_ids = implode(',', array_filter(array_unique($darray)));
            if ($mas_ids == '')
                $mas_ids = 0;
            $this->datatables->select("mas.doc_id as rahul,concat(mas.first_name,' ',mas.last_name) as proname ,concat(mas.country_code,' ',mas.mobile),mas.doc_id as em,mas.created_dt,mas.app_version, mas.doc_id as fees_type,mas.doc_id as typename, mas.profile_pic as pp,IFNULL((select round(avg(star_rating),1)  from doctor_ratings where mas.doc_id = doc_id), 0) as rating,"
                            . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'user.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
                    ->unset_column('type_img')
                    ->unset_column('pp')
                    ->unset_column('fees_type')
                    ->unset_column('typename')
                    ->edit_column('em', "secureEmailPro/$1", 'em')
                    ->add_column('FEES TYPE', "get_fee_group/$1", 'fees_type')
                    ->add_column('PROFILE PIC', '<img src="$1" width="50px" class="imageborder" onerror="this.src = \'' . base_url('/../pics/user.jpg') . '\'">', 'pp')
                    ->edit_column('proname', '<a href=" ' . base_url() . '../admin/index.php/masteradmin/AuthenticateUser/$1" target="_blank">$2</a>', 'rahul,proname')
                    ->edit_column('rating', '<a href=" ' . base_url() . '../superadmin/index.php/superadmin/pro_rating/$1"  >$2</a>', 'rahul,rating')
                    ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
                    ->add_column('LATITUDE', "get_lat/$1", 'rahul')
                    ->add_column('OPTION', '<div class="btn-group">
                                <a href="' . base_url() . 'index.php?/superadmin/editdriver/$1"><button id="$1" type="button" style="color: #ffffff !important;background-color: #10cfbd;" class="btn btn-success  pull-left"><i class="fa fa-pencil"></i>
                                  </button></a>
                                  
                              </div>', 'rahul')
                    ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
//                    ->add_column('DETAILS', '<a href="' . base_url() . 'index.php/superadmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
                    ->unset_column('DEVICE TYPE')
                    ->edit_column('rahul', '<a href=" ' . base_url() . '../superadmin/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
                    ->from("doctor mas")
                    ->where("mas.doc_id IN (" . $mas_ids . ")");
        }

        $this->session->set_userdata($meta);
        //$this->db->order_by("rahul", "desc");
        echo $this->datatables->generate();
    }

    function datatable_cust_details($custid) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->datatables->select("sid,device,manf, dev_model,  (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) as type_img, dev_os, app_version, create_date  from (SELECT * FROM user_sessions where oid = " . $custid . " and user_type = 2 ORDER BY sid DESC) as t", false)
                ->unset_column('sid')
                ->edit_column('type_img', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img');
//                ->from("user_sessions")
//                ->where("oid = " . $custid . " and user_type = 2");
//        $this->db->order_by("sid", "desc");
        $this->db->group_by('device');
        echo $this->datatables->generate();
    }

    function datatable_pro_details($custid) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->datatables->select("sid,device,manf, dev_model, (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) as type_img, dev_os, app_version, create_date from (SELECT * FROM user_sessions where oid = " . $custid . " and user_type = 1 ORDER BY sid DESC) as t ", false)
                ->unset_column('sid')
                ->edit_column('type_img', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img');
//                ->from("user_sessions")
//                ->where("oid = " . $custid . " and user_type = 1");
//        $this->db->order_by("sid", "desc");
        $this->db->group_by('device');
        echo $this->datatables->generate();
    }

    function pro_details_get($proid) {
        $data = $this->db->query("select * from doctor where doc_id ='" . $proid . "'")->row();
        return $data;
    }

    function cust_details_get($custid) {
        $data = $this->db->query("select * from patient where patient_id ='" . $custid . "'")->row();
        return $data;
    }

    function uniq_val_chk() {

        $query = $this->db->query('select * from workplace where uniq_identity = "' . $this->input->post('uniq_id') . '"');
        if ($query->num_rows() > 0) {

            echo json_encode(array('msg' => "This vehicleId Is Already Allocated", 'flag' => '1'));
        } else {
            echo json_encode(array('msg' => "", 'flag' => '0'));
        }
        return;
    }

    function get_options($id) {

        if ($id != '')
            return '<img src="' . base_url() . '../../pics/' . $id . '" width="50px">';
        else
            return '<img src="' . base_url() . '../../superadmin/img/user.jpg" width="50px">';
    }

    function get_devicetype($id) {
        //return $id;

        if ($id)
            return '<img src="' . base_url() . '../../superadmin/assets/' . $id . '" width="50px" class="imageborder" >';
        else
            return '<img src="' . base_url() . '../../superadmin/img/user.jpg" width="50px" class="imageborder">';
    }

    function datatable_bookings($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select("a.appointment_id,m.doc_id,m.first_name,s.first_name,a.address_line1,a.drop_addr1,a.appointment_dt,a.distance_in_mts")->from("appointment a,doctor m,patient s")->where("a.patient_id = s.patient_id and a.doc_id = m.doc_id"); //order by slave_id DESC ",false);

        echo $this->datatables->generate();
    }

//    function datatable_dispatcher($status = '') {
//        $this->load->library('Datatables');
//        $this->load->library('table');
//        $city = $this->session->userdata('city_id');
//        $cityCond = "";
//        if ($city != 0) {
//            $cityCond = ' and city = "' . $city . '"';
//        } else {
//            
//        }
//        $this->datatables->select('dis_id,(select City_Name from city where City_Id = city),dis_email,dis_name')
//                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'dis_id')
//                ->from('dispatcher')
//                ->where('status = "' . $status . '"' . $cityCond); //order by slave_id DESC ",false);
//        echo $this->datatables->generate();
//    }

    function datatable_dispatcher($status = '') {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        if ($this->input->post('sSearch') != '') {
            $cond = array(
                '$or' => array(
                    array('cid' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i"))),
                    array('pid' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i"))),
                    array('bid' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")))
                )
            );
        } else {
            $cond = array('status' => array('$ne' => 7));
        }
        $num = 0;
        $data = array();
        $cdata = $this->mongo_db->get_where('ProviderCategory', array());
        $adata = $this->mongo_db->get_where('apptstatus', $cond)->sort(array('bid' => -1));
        $custdata = $this->mongo_db->get_where('customer', array());
        $location = $this->mongo_db->get_where('location', array());

        foreach ($adata as $row) {
            $catname = "";
            $custname = "";
            $proname = "";
            foreach ($cdata as $cat) {
                $cid = (string) $cat['_id'];
                if ($row['cat_id'] == $cid) {
                    $catname = $cat['cat_name'];
                }
            }
            foreach ($custdata as $cust) {
                if ($row['cid'] == $cust['cid']) {
                    $custname = $cust['fname'] . " " . $cust['lname'];
                }
            }

            if (isset($row['SentToPro'])) {
                $prolist = $row['SentToPro'];
                $dtype = "IOS";
                $btype = "LATER";
                if ($row['dtype'] == '1')
                    $dtype = "ANDROID";
                if ($row['btype'] == '1')
                    $btype = "NOW";

                foreach ($prolist as $pro) {

                    foreach ($location as $loc) {
                        if ($pro['pid'] == $loc['user']) {
                            $proname = $loc['name'];
                        }
                    }
                    $status = "NO";
                    if ($pro['status'] == '1')
                        $status = "Booking Got";
                    $data[] = array($row['bid'], $row['cid'], $custname,
                        $pro['pid'], $proname, $catname, $pro['dt'], $dtype, $btype, $status);
                }
            }
        }
        echo $this->datatables->commission_Data($data);
    }

    function datatable_document($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $company = $this->session->userdata('company_id');

        if ($status == '1') {

            $this->datatables->select("d.doc_ids,c.first_name,c.last_name,d.expirydate,d.url")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<button type="button" name="view"  width="50px">'
                            . '<a target="_blank" href="' . base_url() . '../../pics/$1">view</a><a target="_blank" href="' . base_url() . '../../pics/$1"></button><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
                    ->from("master c,docdetail d")
                    ->where("c.mas_id = d.driverid and d.doctype=1" . ($company != 0 ? ' and c.company_id = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '2') {

            $this->datatables->select("d.doc_ids,c.first_name,c.last_name,d.expirydate,d.url")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">view</button></a><a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
                    ->from("master c,docdetail d")->where("c.mas_id = d.driverid and d.doctype=2" . ($company != 0 ? ' and c.company_id = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '3') {


            $this->datatables->select("d.docid,d.vechileid,(select companyname from company_info where company_id = w.company) as companyname,d.expirydate,d.url")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<button type="button" name="view"  width="50px"><a target="_blank" href="' . base_url() . '../../pics/$1">view</button></a><a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
//                     ->select("(select companyname from company_info where company_id = w.company) as companyname",false)
                    ->from("workplace w,vechiledoc d,vehicleType v")
                    ->where("w.title = v.id and w.workplace_id = d.vechileid and d.doctype = 2" . ($company != 0 ? ' and w.company = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '4') {

            $this->datatables->select("d.docid,d.vechileid,(select companyname from company_info where company_id = w.company) as companyname,d.url,d.expirydate")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">view</button></a><a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
//                     ->select("(select companyname from company_info where company_id = w.company) as companyname",false)
                    ->from("workplace w,vechiledoc d,vehicleType v")
                    ->where("w.title = v.id and w.workplace_id = d.vechileid and d.doctype = 3" . ($company != 0 ? ' and w.company = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '5') {

            $this->datatables->select("d.docid,d.vechileid,(select companyname from company_info where company_id = w.company) as companyname,d.url,d.expirydate")
                    ->select("(select companyname from company_info where company_id = w.company) as companyname", false)
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">view</button></a><a target="_blank" target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
                    ->from("workplace w,vechiledoc d,vehicleType v")
                    ->where("w.title = v.id and w.workplace_id = d.vechileid and d.doctype = 1" . ($company != 0 ? ' and w.company = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        }

        echo $this->datatables->generate();
    }

    function datatable_driverreview($status = '') {
        $this->load->library('Datatables');
        $this->load->library('table');
        $_POST["sColumns"] = "r.appointment_id,a.appointment_dt,d.first_name,r.patient_id,r.review,rat";
        $this->datatables->select('r.appointment_id,a.appointment_dt, d.first_name,r.patient_id,r.review,round(r.star_rating,2) as rat', false)
                ->edit_column('a.appointment_dt', "get_formatedDate/$1", 'a.appointment_dt')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'r.appointment_id')
                ->from("doctor_ratings r, doctor d, patient p,bookings a", false)
                ->where("r.patient_id = p.patient_id  AND r.doc_id = d.doc_id  AND r.status ='" . $status . "'AND r.review<>'' AND a.bid = r.appointment_id");
        //$this->db->order_by("r.appointment_id", "desc");
        echo $this->datatables->generate();
    }

    function datatable_proreview($status = '') {
        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select("r.appointment_id,r.patient_id as cid, p.first_name as cname, r.review,round(r.star_rating,2) ,r.review_dt", FALSE)
//                 ->edit_column('cname', "get_customerName/$1", 'cname')
//                ->edit_column('r.review_dt', "get_formatedDate/$1", 'r.review_dt')
                ->from("doctor_ratings r, patient p", false)
                ->where("r.doc_id ='" . $status . "' and r.patient_id = p.patient_id");
//                        ->where("r.doc_id ='" . $status . "'AND r.review<>''");
        $this->db->order_by("r.appointment_id", "desc");
        echo $this->datatables->generate();
    }

    function editdispatchers_city() {
        $val = $this->input->post('val');

        $var = $this->db->query("select city from dispatcher where dis_id='" . $val . "'")->result();
        return $var;
    }

    function datatable_passengerrating() {

        $this->load->library('Datatables');
        $this->load->library('table');
        $status = 1;
        $this->datatables->select('p.patient_id, p.first_name ,p.email,IFNULL((select round(avg(rating),1)  from patient_rating where p.patient_id = patient_id), 0) as rating', false)
                ->from('patient p'); //->where('r.status =" ' . $status . '"'); //order by slave_id DESC ",false);
        $this->db->order_by("p.patient_id", "desc");
        echo $this->datatables->generate();
    }

    function datatable_compaigns($status) {

        $this->load->library('Datatables');
        $this->load->library('table');
        if ($status == 1) {
            $this->datatables->select("cp.id,cp.discount,cp.referral_discount,cp.message,c.city_name")
                    ->unset_column('cp.id')
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', ' cp.id')
                    ->from(" coupons cp, city c")
                    ->where('cp.city_id = c.city_id and cp.coupon_type = " ' . $status . ' " and cp.status = "0" and user_type = 2'); //order by slave_id DESC ",false);
        } elseif ($status == 2) {

            $this->datatables->select("cp.id,cp.coupon_code,cp.start_date,cp.expiry_date, cp.discount,cp.message,c.city_name")
                    ->unset_column('cp.id')
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', ' cp.id')
                    ->from(" coupons cp, city c")
                    ->where('cp.city_id = c.city_id and cp.coupon_type = " ' . $status . ' " and cp.status = "0" and user_type = 2'); //order by slave_id DESC ",false);
        } else if ($status == 3) {
            $this->datatables->select("cp.id,cp.coupon_code,cp.start_date,cp.expiry_date, cp.discount,cp.message,c.city_name")
                    ->unset_column('cp.id')
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', ' cp.id')
                    ->from(" coupons cp, city c")
                    ->where('cp.city_id = c.city_id and cp.coupon_type = " ' . $status . ' " and cp.status = "0" and user_type = 2'); //order by slave_id DESC ",false);
        }
        echo $this->datatables->generate();
    }

    function editNewVehicleData() {

        $vehicle_id = $this->input->post('vehicle_id');
        $title = $this->input->post('title');
        $vehiclemodel = $this->input->post('vehiclemodel');
        $vechileregno = $this->input->post('vechileregno');
        $licenceplaetno = $this->input->post('licenceplaetno');
        $vechilecolor = $this->input->post('vechilecolor');
        $type_id = $this->input->post('getvechiletype');
        $expirationrc = $this->input->post('expirationrc');
        $expirationinsurance = $this->input->post('expirationinsurance');
        $expirationpermit = $this->input->post('expirationpermit');
        $companyid = $this->input->post('company_id'); //$this->session->userdata('LoginId');

        $insuranceno = $this->input->post('Vehicle_Insurance_No'); //$_REQUEST['Vehicle_Insurance_No'];
//        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/roadyo_live/pics/';


        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/' . mainfolder . '/pics/';


        if ($_FILES["certificate"]["name"] != '' && $_FILES["certificate"]["size"] > 0) {
            $name = $_FILES["certificate"]["name"];
            $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice
            $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;
            move_uploaded_file($_FILES['certificate']['tmp_name'], $documentfolder . $cert_name);
            $this->db->query("update vechiledoc set expirydate = '" . $expirationrc . "', url = '" . $cert_name . "' where doctype = 1 and vechileid = '" . $vehicle_id . "'");
        } else {
            $this->db->query("update vechiledoc set expirydate = '" . $expirationrc . "' where doctype = 1 and vechileid = '" . $vehicle_id . "'");
        }
        if ($_FILES["insurcertificate"]["name"] != '' && $_FILES["insurcertificate"]["size"] > 0) {
            $name = $_FILES["insurcertificate"]["name"];
            $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice
            $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;
            move_uploaded_file($_FILES['insurcertificate']['tmp_name'], $documentfolder . $cert_name);
            $this->db->query("update vechiledoc set expirydate = '" . $expirationinsurance . "', url = '" . $cert_name . "' where doctype = 2 and vechileid = '" . $vehicle_id . "'");
        } else {
            $this->db->query("update vechiledoc set expirydate = '" . $expirationinsurance . "' where doctype = 2 and vechileid = '" . $vehicle_id . "'");
        }
        if ($_FILES["carriagecertificate"]["name"] != '' && $_FILES["carriagecertificate"]["size"] > 0) {
            $name = $_FILES["carriagecertificate"]["name"];
            $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice
            $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;
            move_uploaded_file($_FILES['carriagecertificate']['tmp_name'], $documentfolder . $cert_name);
            $this->db->query("update vechiledoc set expirydate = '" . $expirationpermit . "', url = '" . $cert_name . "' where doctype = 3 and vechileid = '" . $vehicle_id . "'");
        } else {
            $this->db->query("update vechiledoc set expirydate = '" . $expirationpermit . "' where doctype = 3 and vechileid = '" . $vehicle_id . "'");
        }

        if ($_FILES["imagefile"]["name"] != '' && $_FILES["imagefile"]["size"] > 0) {
            $name = $_FILES["imagefile"]["name"];
            $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice
            $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;
            move_uploaded_file($_FILES['imagefile']['tmp_name'], $documentfolder . $cert_name);
            $updateImageString = ", Vehicle_Image = '" . $cert_name . "'";
        }


//        try {
//
//            move_uploaded_file($_FILES['insurcertificate']['tmp_name'], $documentfolder . $insurance_name);
//            move_uploaded_file($_FILES['carriagecertificate']['tmp_name'], $documentfolder . $carriage_name);
//            move_uploaded_file($_FILES['imagefile']['tmp_name'], $documentfolder . $image_name);
//        } catch (Exception $ex) {
//            print_r($ex);
//            return false;
//        }

        $this->db->query("update workplace set type_id = '" . $type_id . "',Title = '" . $title . "',Vehicle_Model = '" . $vehiclemodel . "',Vehicle_Reg_No = '" . $vechileregno . "', License_Plate_No = '" . $licenceplaetno . "',Vehicle_Color = '" . $vechilecolor . "',company = '" . $companyid . "',Vehicle_Insurance_No = '" . $insuranceno . "'" . $updateImageString . " where workplace_id = '" . $vehicle_id . "'");

        if ($this->db->affected_rows > 0) {
            return true;
        } else {
            return false;
        }


        return;
    }

    function delete_vehicletype() {
        $val = $this->input->post('val');
        $this->load->library('mongo_db');
        foreach ($val as $row) {

            $this->db->query("delete from workplace_types where type_id='" . $row . "' ");

            $this->mongo_db->delete('vehicleTypes', array('type' => (int) $row));
        }
        if ($this->db->affected_rows() > 0) {

            echo json_encode(array('msg' => "vehicle type deleted successfully", 'flag' => 1));
            return;
        }
    }

    function delete_company() {

        $val = $this->input->post('val');
        foreach ($val as $row) {

            $this->db->query("delete from company_info where company_id='" . $row . "' ");

            $this->db->query("delete from master where company_id ='" . $row . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "company/companies deleted successfully", 'flag' => 1));
            return;
        }
    }

    function get_documentdata($status) {
        if ($status == 1) {
            $result = $this->db->query("select c.first_name,c.last_name,d.url,d.doc_ids,d.expirydate from master c,docdetail d where c.mas_id=d.driverid and d.doctype=1")->result();
            return $result;
        } else if ($status == 2) {
            $result = $this->db->query("select c.first_name,c.last_name,d.url,d.doc_ids,d.expirydate from master c,docdetail d where c.mas_id=d.driverid and d.doctype=2")->result();
            return $result;
        } else if ($status == 3) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        } else if ($status == 4) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=3")->result();
            return $result;
        } else if ($status == 5) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        } else if ($status == 5) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        } else if ($status == 6) {
            $result = $this->db->query("SELECT d.url,d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        }
    }

    function setsessiondata($tablename, $LoginId, $res, $email, $password, $off) {

        $offset = -($off);

        $sessiondata = array(
            'emailid' => $email,
            'password' => $password,
            'LoginId' => $res->$LoginId,
            'profile_pic' => $res->logo,
            'first_name' => $res->companyname,
            'table' => $tablename,
            'city_id' => '0', 'company_id' => '0',
            'validate' => true,
            'admin' => 'super',
            'offset' => $offset
        );

        return $sessiondata;
    }

    function Drivers($status = '') {

        $quaery = $this->db->query("SELECT mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt,(select type from user_sessions where oid = mas.mas_id order by oid DESC limit 0,1) as dev_type FROM master mas where  mas.status IN (" . $status . ") and mas.company_id IN (" . $this->session->userdata('LoginId') . ") order by mas.mas_id DESC")->result();
        return $quaery;
    }

    function datatable($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select("*")->from('slave')->where('status', 3); //order by slave_id DESC ",false);

        echo $this->datatables->generate();
    }

    function validateEmail() {

        $query = $this->db->query("select doc_id from doctor where email='" . $this->input->post('email') . "'");
        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => '1'));
            return;
        } else {
            echo json_encode(array('msg' => '0'));
        }
    }

    function validatedispatchEmail() {

        $query = $this->db->query("select dis_id from dispatcher where dis_email='" . $this->input->post('email') . "'");
        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => '1'));
            return;
        } else {
            echo json_encode(array('msg' => '0'));
            return;
        }
    }

    function get_workplace() {
        $res = $this->db->query("select * from workplace_types")->result();
        return $res;
    }

    function get_cities() {
        $query = $this->db->query('select * from city_available')->result();
        return $query;
    }

    function loadcity() {
        $countryid = $this->input->post('country');
        $Result = $this->db->query("select * from city where Country_Id=" . $countryid . "")->result();
        return $Result;
    }

    function loadcompany() {
        $cityid = $this->input->post('city');
        $Result = $this->db->query("select * from doctor where city_id=" . $cityid . " and status = 3 ")->result_array();
        return $Result;
    }

    function get_city() {
        return $this->db->query("select ci.*,co.Country_Name from city_available ci,country co where ci.country_id = co.country_id ORDER BY ci.City_Name ASC")->result();
    }

    function get_companyinfo($status) {
        return $this->db->query("select * from company_info where status = '" . $status . "' ")->result();
    }

    function editdriver($status = '') {
        return $this->db->query("select * from doctor where doc_id ='" . $status . "' LIMIT 1 ")->result();
    }

    function getDriverDetail($status = '') {
        $this->load->library('mongo_db');
        $data = $this->mongo_db->get_one('location', array('user' => (int) $status));
        return $data;
        ;
    }

//    function editdriverBank($status = '') {
//        $data['masterdata'] = $this->db->query("select * from doctor where doc_id ='" . $status . "' ")->result();
//        $data['masterdoc'] = $this->db->query("select * from docdetail where driverid ='" . $status . "' ")->result();
//
//        $stripe = new StripeModuleNew();
//        $this->load->library('mongo_db');
//        $userType = $this->mongo_db->get_one('location', array('user' => (int) $status));
//
//        $data['bankAccountStatus'] = 0;
//        $GetUser = $stripe->apiStripe('RetriveStripConnectAccount', array('accountId' => $userType['StripeAccId']));
//        if ($GetUser['transfers_enabled'] != true || $userType['StripeAccId'] == "") {
//            $data['bankAccountStatus'] = 1;
//            $data['bankAccountFieldSNeeded'] = $GetUser['verification']['fields_needed'];
//        }
//
//        if ($userType['StripeAccId'] != "" && ($userType['accountDetail'] == "" || empty($userType['accountDetail'])) && ( count($GetUser['external_accounts']['data']) > 0)) {
//            $tokenToInsert = array();
//            foreach ($GetUser['external_accounts']['data'] as $tocken) {
//                $tokenToInsert[] = array('status' => 'active', 'tocken' => $tocken['id']);
//            }
//            if ($userType['accountDetail'] == "")
//                $this->mongo_db->update('location', array('accountDetail' => $tokenToInsert), array('user' => (int) $status));
//            else if (empty($userType['accountDetail']))
//                $this->mongo_db->updatewithpush('location', array('accountDetail' => $tokenToInsert), array('user' => (int) $status));
//            $userType = $this->mongo_db->get_one('location', array('user' => (int) $status));
//        }
//
//
//        $bank_arr = array();
//        $i = 0;
//        foreach ($userType['accountDetail'] as $user) {
//            $getResp = array('accountId' => $userType['StripeAccId'], 'BankAccountTocken' => $user['tocken']);
//            $rep = $stripe->apiStripe('RetriveABankAccount', $getResp);
//            $bank_arr[$i]['accountId'] = $userType['StripeAccId'];
//            $bank_arr[$i]['bank_id'] = $rep['id'];
//            $bank_arr[$i]['account_holder_name'] = $rep['account_holder_name'];
//            $bank_arr[$i]['bank_name'] = $rep['bank_name'];
//            $bank_arr[$i]['routing_number'] = $rep['routing_number'];
//            $bank_arr[$i]['country'] = $rep['country'];
//            $bank_arr[$i]['currency'] = $rep['currency'];
//            $bank_arr[$i]['last4'] = $rep['last4'];
//            $i++;
//        }
//        $data['StripeAccId'] = $userType['StripeAccId'];
//        $data['bankData'] = $bank_arr;
//
//        return $data;
//    }

    function GetBookingStatusMessage($status) {
        $message = "Status Unavailable.";
        if ($status == 1) {
            $message = 'Provider Requested.';
        } else if ($status == 2) {
            $message = 'Provider Accepted.';
        } else if ($status == 3) {
            $message = 'Provider Rejected.';
        } else if ($status == 4) {
            $message = 'Customer has cancelled with out fee.';
        } else if ($status == 5) {
            $message = 'Provider is on the way.';
        } else if ($status == 21) {
            $message = 'Provider Arrived.';
        } else if ($status == 6) {
            $message = 'Job Started.';
        } else if ($status == 22) {
            $message = 'Invoice Raised.';
        } else if ($status == 7) {
            $message = 'Job Completed.';
        } else if ($status == 9) {
            $message = 'Customer has cancelled with fee.';
        } else if ($status == 10) {
            $message = 'Provider Cancelled';
        }
        return $message;
    }

    function getbooking_data($status = '') {

        $this->load->library('mongo_db');
        $this->load->library('Datatables');
//        $data = $this->mongo_db->get('bookings')->sort(array('_id' => -1));

        if ($status == '')
            $data = $this->mongo_db->get('bookings')->sort(array('_id' => -1));
        else
            $data = $this->mongo_db->get_where('bookings', array('status' => (int) $status))->sort(array('_id' => -1));
        if ($this->input->post('sSearch') != '') {
            $cond = array('status' => (int) $status,
                '$or' => array(
                    array('bid' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i"))),
                    array('customer.id' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i"))),
                    array('customer.fname' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i"))),
                    array('address1' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i"))),
                    array('cat_name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i"))),
                    array('customer.mobile' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")))
                )
            );
            $data = $this->mongo_db->get_where('bookings', $cond)->sort(array('_id' => -1));
        }
        $return = array();
        foreach ($data as $res) {
            if ($res['status'] == 7) {
                if ($res['disputed'] == 1) {
                    $res['status'] = 101;
                } else if ($res['job_status'] == 100) {
                    $res['status'] = 100;
                }
            }
            $return[] = $res;
        }

        echo $this->datatables->getdataFromMongoNew($return, array("_id", "appt_date",
            array("customer", "id"), array("customer", "fname"),
            array("customer", "mobile"), "address1",
            "booking_type", "status", array("provider", "fname"),
            array("provider", "mobile"), 'cat_name', "ProCount"));
    }

    public function getprofessiondata() {
        $data = $this->db->query('select type_id,type_name from doctor_type')->result();
        return $data;
    }

    public function getDatafromdate_for_all_bookings($stdate = '', $enddate = '', $status = '') {
        $this->load->library('mongo_db');
        $this->load->library('Datatables');
        $stdate = $stdate . ' 00:00:00';
        $enddate = $enddate . ' 24:00:00';
        $data = array();


        $condition = array('appt_server_ts' => array('$gte' => strtotime($stdate), '$lte' => strtotime($enddate)),
            'status' => (int) $status);
        $condition_2 = array('appt_server_ts' => array('$gte' => strtotime($stdate), '$lte' => strtotime($enddate)));
        if ($status == '')
            $data = $this->mongo_db->get_where('bookings', $condition_2)->sort(array('_id' => -1));
        else
            $data = $this->mongo_db->get_where('bookings', $condition)->sort(array('_id' => -1));
        $return = array();
        foreach ($data as $res) {
            $return[] = $res;
        }
        echo $this->datatables->getdataFromMongoNew($return, array("_id", "appt_date",
            array("customer", "id"), array("customer", "fname"),
            array("customer", "mobile"), "address1",
            "booking_type", "status", array("provider", "fname"),
            array("provider", "mobile"), 'cat_name', "ProCount"));
//        $this->load->library('Datatables');
//        $this->load->library('table');
//        if ($status == '11')
//            $query = 'a.cust_id = s.patient_id and a.pro_id = m.doc_id  and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '"';
//        else
//            $query = 'a.cust_id = s.patient_id and a.pro_id = m.doc_id and a.status = "' . $status . '" and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '"';
//        $this->datatables->select("a.bid_id,m.doc_id,(select type_name from doctor_type where type_id = m.type_id),(case a.booking_type when 1 then 'Now' when 2 then 'Later' else 'Unavailable.' END) as b_type,m.first_name,m.mobile,s.first_name as name,s.phone,a.address_line1,a.appointment_dt,            
//(
//case a.status when 1 then 'Request'
//when 2   then
//'Provider accepted.'
//when 3  then
//'Provider rejected.'
//when 4  then
//'Customer has cancelled.'
//when 5   then
//'Provider has cancelled.'
//when 6   then
//'Provider is on the way.'
//when 7  then
//'JOB started.'
//when 8   then
//'Provider arrived.'
//when 9   then
//'JOB completed.'
//when 10 then
//'JOB timed out.'
//else
//'Status Unavailable.'
//END) as status_result,accpt_time", false)
//                ->edit_column('a.appointment_dt', "get_formatedDate/$1", 'a.appointment_dt')
//                ->from('bookings a,doctor m,patient s')
//                ->where($query);
//
//        $this->db->order_by('a.appointment_id', 'DESC');
//        echo $this->datatables->generate();
    }

    function payroll() {

        $this->load->library('Datatables');
        $this->load->library('table');
        $_POST["sColumns"] = "masid,name,p.closing_balance,BookingCycle,EarningCycle,ClosingBal,SHOW";

        if ($this->input->post('sSearch') != '') {
//            $this->datatables->select('doc.doc_id as masid,(concat(doc.first_name,\'  \',doc.last_name))as name,'
//                            . " IFNULL(round((select p.closing_balance from payroll p,bookings b where  b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0) ,"
//                            . " IFNULL((select count(*) from bookings b where b.pro_id = doc.doc_id and payment_Status = 0),0)  as BookingCycle ,"
//                            . " IFNULL(round((select round(sum(b.pro_earning),2) from bookings b  where b.pro_id = doc.doc_id and payment_status = 0),2),0) as EarningCycle ,"
//                            . " IFNULL(round((select round(sum(b.pro_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 2),2),0) - IFNULL(round((select round(sum(b.app_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 1),2),0) + IFNULL(round((select p.closing_balance from bookings b,payroll p  where b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0) as ClosingBal", false)
//                    ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/pay_provider/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
//                    ->from('doctor doc', false)
//                    ->where('doc.status = 3');
            $this->datatables->select('doc.doc_id as masid,(concat(doc.first_name,\'  \',doc.last_name)) as name,'
                            . " concat('" . CURRENCY . "',' ', IFNULL(round((select p.closing_balance from payroll p,bookings b where  b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0)) ,"
                            . " IFNULL((select count(*) from bookings b where b.pro_id = doc.doc_id and payment_Status = 0),0)  as BookingCycle ,"
                            . " concat('" . CURRENCY . "',' ',  IFNULL(round((select round(sum(b.pro_earning),2) from bookings b  where b.pro_id = doc.doc_id and payment_status = 0),2),0)) as EarningCycle ,"
                            . " concat('" . CURRENCY . "',' ', IFNULL(round((select round(sum(b.pro_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 2),2),0) - IFNULL(round((select round(sum(b.app_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 1),2),0) + IFNULL(round((select p.closing_balance from bookings b,payroll p  where b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0)) as ClosingBal", false)
                    ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/pay_provider/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
                    ->from('doctor doc', false)
                    ->where('doc.status = 3');

            // - IFNULL(round((select round(sum(bo.app_earning),2) from bookings bo where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 1),2),0)
        } else {
            $this->datatables->select('doc.doc_id as masid,(concat(doc.first_name,\'  \',doc.last_name)) as name,'
                            . " concat('" . CURRENCY . "',' ', IFNULL(round((select p.closing_balance from payroll p,bookings b where  b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0)) ,"
                            . " IFNULL((select count(*) from bookings b where b.pro_id = doc.doc_id and payment_Status = 0),0)  as BookingCycle ,"
                            . " concat('" . CURRENCY . "',' ',  IFNULL(round((select round(sum(b.pro_earning),2) from bookings b  where b.pro_id = doc.doc_id and payment_status = 0),2),0)) as EarningCycle ,"
                            . " concat('" . CURRENCY . "',' ', IFNULL(round((select round(sum(b.pro_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 2),2),0) - IFNULL(round((select round(sum(b.app_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 1),2),0) + IFNULL(round((select p.closing_balance from bookings b,payroll p  where b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0)) as ClosingBal", false)
                    ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/pay_provider/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
                    ->from('doctor doc', false)
                    ->where('doc.status = 3');
        }
        // $this->db->order_by('doc.doc_id', 'DESC');
        echo $this->datatables->generate();
    }

    function datatable_payroll() {

        $explodeDateTime = explode(' ', date("Y-m-d H:s:i"));
        $explodeDate = explode('-', $explodeDateTime[0]);
        $weekData = $this->week_start_end_by_date(date("Y-m-d H:s:i"));
        $this->load->library('Datatables');
        $this->load->library('table');
        $wereclousetocome = ';';

        $this->datatables->select('doc.doc_id as masid,(concat(doc.first_name,\'  \',doc.last_name)),'
                        . "(case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 7),2)  IS NULL then '--'  else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 7),2) END) as TODAY_EARNINGS ,"
                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 7 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2)  IS NULL then '--' else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 7 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2) END) as WEEK_EARNINGS ,"
                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 7 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  IS NULL then '--' else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 7 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  END) as MONTH_EARNINGS,"
                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 7),2)  IS NULL then '--'  else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 7),2) END) as LIFE_TIME_EARNINGS,"
                        . "(case  when TRUNCATE((select sum(pay_amount) from payroll where doc.doc_id = doc_id),2)  IS NULL then '--' else TRUNCATE((select sum(pay_amount) from payroll where doc.doc_id = mas_id),2) END) as PAID,"
                        . "(case  when  TRUNCATE(((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 7 and payment_status in (1,3))-(select sum(pay_amount) from payroll where doc.doc_id = mas_id)),2) IS NULL then '--' else TRUNCATE(((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 7)-(select sum(pay_amount) from payroll where doc.doc_id = mas_id)),2)END) as DUE", false)
                ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/patientDetails/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">DETAILS</button></a>
                <a href="' . base_url("index.php/superadmin/Driver_pay/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
                ->from('doctor doc', false)
                ->where('doc.status = 3');

        $this->db->order_by('doc.doc_id', 'DESC');
        echo $this->datatables->generate();
    }

    function getmap_values() {

        $m = new MongoClient();
        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $bookingid = $this->input->post('mapval');


        $data = $this->mongo_db->get_one('booking_route', array('bid' => (int) $bookingid));

        echo json_encode($data["route"]);
    }

    function payroll_data_form_date($stdate = '', $enddate = '', $company_id = '') {

        $explodeDateTime = explode(' ', date("Y-m-d H:s:i"));
        $explodeDate = explode('-', $explodeDateTime[0]);
        $weekData = $this->week_start_end_by_date(date("Y-m-d H:s:i"));

        $this->load->library('Datatables');
        $this->load->library('table');

        $query = 'doc.doc_id = a.pro_id and doc.status = 3 and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '"';
//
//        $this->datatables->select('doc.doc_id as masid,doc.first_name,'
//                        . "(case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9),2)  IS NULL then '--'  else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9),2) END) as TODAY_EARNINGS ,"
//                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2)  IS NULL then '--' else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2) END) as WEEK_EARNINGS ,"
//                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  IS NULL then '--' else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  END) as MONTH_EARNINGS,"
//                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9),2)  IS NULL then '--'  else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9),2) END) as LIFE_TIME_EARNINGS,"
//                        . "(case  when TRUNCATE((select sum(pay_amount) from payroll where mas_id = doc.doc_id),2)  IS NULL then '--' else TRUNCATE((select sum(pay_amount) from payroll where mas_id = doc.doc_id),2) END) as PAID,"
//                        . "(case  when  TRUNCATE(((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9 and payment_status in (1,3))-(select sum(pay_amount) from payroll where mas_id = doc.doc_id)),2) IS NULL then '--' else TRUNCATE(((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9)-(select sum(pay_amount) from payroll where mas_id = doc.doc_id)),2)END) as DUE", false)
//                ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/patientDetails/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">DETAILS</button></a>
//    <a href="' . base_url("index.php/superadmin/Driver_pay/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
//                ->from(' doctor doc,appointment a ', false)
//                ->where($query);

        $this->datatables->select('doc.doc_id as masid,(concat(doc.first_name,\'  \',doc.last_name)),'
                        . " concat('" . CURRENCY . "',' ', IFNULL(round((select p.closing_balance from payroll p,bookings b where  b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0)) ,"
                        . " IFNULL((select count(*) from bookings b where b.pro_id = doc.doc_id and payment_Status = 0),0)  as BookingCycle ,"
                        . " concat('" . CURRENCY . "',' ',  IFNULL(round((select round(sum(b.pro_earning),2) from bookings b  where b.pro_id = doc.doc_id and payment_status = 0),2),0)) as EarningCycle ,"
                        . " concat('" . CURRENCY . "',' ', IFNULL(round((select round(sum(b.pro_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 2),2),0) - IFNULL(round((select round(sum(b.app_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 1),2),0) + IFNULL(round((select p.closing_balance from bookings b,payroll p  where b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0)) as ClosingBal", false)
                ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/pay_provider/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
                ->from('doctor doc,bookings a', false)
                ->where($query);
//                    ->where('doc.status = 3');
        //                $this->db->order_by('ap.appointment_id' ,'DESC');


        echo $this->datatables->generate();
    }

    function tripDetails($param) {

        $this->load->library('mongo_db');
        $data['res'] = $this->mongo_db->get_one('bookings', array('bid' => $param));
        $driver_id = $data['res']['provider_id'];
        $data['master_rating_data'] = $this->db->query("select * from doctor_ratings where doc_id = '" . $driver_id . "' and appointment_id = '" . $param . "'")->row();
//      
//        $data['appt_data'] = $this->db->query("select * from appointment where mongoId = '" . $this->db->escape_str($param) . "' and status = '9'")->row();
//
//        $driver_id = $data['appt_data']->mas_id;
//        $appointment_id = $data['appt_data']->appointment_id;
//        $data['driver_data'] = $this->db->query("select * from master where mas_id = '" . $this->db->escape_str($driver_id) . "'")->row();
//
//        $slave_id = $data['appt_data']->slave_id;
//        $data['customer_data'] = $this->db->query("select * from slave where slave_id = '" . $this->db->escape_str($slave_id) . "'")->row();
//
//        $type_id = $data['appt_data']->type_id;
//        $data['car_data'] = $this->db->query("select * from workplace_types where type_id = '" . $this->db->escape_str($type_id) . "'")->row();
////
////        $data['master_rating_data'] = $this->db->query("select * from master_ratings where mas_id = '" . $driver_id . "' and appointment_id = '" . $appointment_id . "'")->row();
//        $tarrm = $this->mongo_db->get_where('booking_route', array('bid' => (int) $param));
//        $tarr = array();
//        //        $tarr = array();
////        foreach ($tarrm as $value) {
////            foreach ($value['route'] as $val) {
////                $tarr[$val['subid']]=  json_decode($val['ent_shipment_latlogs']);
////            }
////        }
//        $data['trip_route'] = $data['res'];
//print_r($data);die;
        return $data;
    }

    function addNewDriverData() {
        $datai['first_name'] = $this->input->post('firstname');
        $datai['last_name'] = $this->input->post('lastname');
        $pass = $this->input->post('password');
        $datai['password'] = md5($pass);
        $datai['status'] = 1;
        $email = $this->input->post('email');
        $datai['email'] = $this->input->post('email');
        $datai['type_id'] = $this->input->post('pro_type_arr');
        $datai['fees_group'] = $this->input->post('fees_group');
        $datai['mobile'] = $this->input->post('mobile');
        $datai['zipcode'] = $this->input->post('postal_code');
        $datai['country_code'] = '+' . $this->input->post('c_code');
        $datai['city_id'] = $this->input->post('cityid');

        $datai['address_line1'] = $this->input->post('proaddress');
        $datai['countryName'] = $this->input->post('country');
        $datai['cityName'] = $this->input->post('administrative_area_level_1');
        $datai['tax_num'] = $this->input->post('tax_num');
        $radius = $this->input->post('radius');
        $exp_date = $this->input->post('expirationrc_l');
        $datai['board_certification_expiry_dt'] = $exp_date;
        if ($exp_date != '') {
            $datai['board_certification_expiry_dt'] = date("Y-m-d", strtotime($exp_date));
        }
        $datai['medical_license_num'] = $this->input->post('licence_no');
        $datai['about'] = $this->input->post('aboutdata');
        $datai['expertise'] = $this->input->post('experties');
        $datai['languages'] = $this->input->post('lanKnow');



        date_default_timezone_set("Asia/Kolkata");
        $datai['created_dt'] = date("Y-m-d H:i:s");
        $datai['app_version'] = "N/A";
        $pro_pic = $_FILES["photos"]["name"];
        $size = $_FILES['photos']['size'];
        $tmp = $_FILES['photos']['tmp_name'];
        $pro_pic_ext = substr($pro_pic, strrpos($pro_pic, '.') + 1);
        $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");
        $msg = false;
        $bucket = BUCKET_NAME;
        $s3 = new S3(AMAZON_AWS_ACCESS_KEY, AMAZON_AWS_AUTH_SECRET);
        $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);

        if (strlen($pro_pic) > 0) {
            if (in_array($pro_pic_ext, $valid_formats)) {
                if ($size < (1024 * 1024)) {
                    $actual_image_name = "ProfileImages/" . $email . "." . $pro_pic_ext;
                    if ($s3->putObjectFile($tmp, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
                        $msg = true;
                        $s3file = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                    } else
                        $msg = false;
                } else
                    $msg = false;
            } else
                $msg = false;
        } else
            $msg = false;

//        if ($msg == FALSE)
//            return false;

        $datai['profile_pic'] = $s3file;

        $expirationrc = $this->input->post('expirationrc');
//        $expirationPassbook = $this->input->post('expirationPassbook');
//        $datai['company_id'] = $this->session->userdata('LoginId');

        $name = $_FILES["certificate"]["name"];
        $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice //1  doctype
        $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;




        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/pics/';

        try {

            if (move_uploaded_file($_FILES['certificate']['tmp_name'], $documentfolder . $cert_name)) {
                
            }
        } catch (Exception $ex) {
            print_r($ex);
            return false;
        }

        if ($datai['fees_group'] == '') {
            $datai['fees_group'] = "N/A";
        }

        $datai['medical_license_pic'] = $cert_name;

        $this->db->insert('doctor', $datai);
        $newdriverid = $this->db->insert_id();

        $docdetail = array('url' => $cert_name, 'expirydate' => date("Y-m-d", strtotime($expirationrc)), 'doctype' => 1, 'driverid' => $newdriverid);
        $this->db->insert('docdetail', $docdetail);
//        $docdetail = array('url' => $carriage_name, 'doctype' => 2, 'driverid' => $newdriverid);
//        $this->db->insert('docdetail', $docdetail);
        //insert pro with cat in mongodb
//        $this->load->library('mongo_db');
//        $tarr = explode(",", $this->input->post('pro_type_arr'));
//        foreach ($tarr as $type) {
//            $this->mongo_db->updatewithpush('ProviderCategory', array('prolist' => array('pid' => $newdriverid, 'status' => 0)), array('_id' => new MongoId($type)));
//        }

        if ($datai['fees_group'] == 1) {
            $tarr = explode(",", $this->input->post('mtypearr'));
        } else if ($datai['fees_group'] == 2) {
            $tarr = explode(",", $this->input->post('htypearr'));
        } else if ($datai['fees_group'] == 3) {
            $tarr = explode(",", $this->input->post('ftypearr'));
        }

        $sel_cat = array();
        foreach ($tarr as $type) {
            $db = $this->mongo_db->db;
            $selecttb = $db->selectCollection('Category');
            $resu = $selecttb->findOne(array('_id' => new MongoId($type)));
            $sel_cat[] = array('cid' => $type, 'status' => 0, 'amount' => $resu['price_min']);
        }

        // insert into user_session
        $us['oid'] = $newdriverid;
        $us['user_type'] = 1;
        $us['type'] = 3;
        $us['loggedIn'] = 2;
        $us['create_date'] = date("Y-m-d H:i:s");
        $this->db->insert('user_sessions', $us);

        $curr_date = time();
        $curr_gmt_dates = gmdate('Y-m-d H:i:s', $curr_date);
        $curr_gmt_date = new MongoDate(strtotime($curr_gmt_dates));
        $mongoArr = array("radius" => (int) $radius, "type" => 0, "user" => (int) $newdriverid,
            "name" => $datai['first_name'], "lname" => $datai['last_name'],
            "fee_group" => $datai['fees_group'],
            'catlist' => $sel_cat, 'mobile' => $datai['mobile'],
            'booked' => 0,
            "location" => array(
                "longitude" => 0,
                "latitude" => 0
            ), "image" => $s3file, 'accepted' => 0, 'Commission' => 10, 'later_status' => 1, 'city_id' => $datai['city_id'],
            "rating" => 0, 'status' => 1, 'email' => strtolower($datai['email']), 'dt' => $curr_gmt_date
        );

        $this->mongo_db->insert('location', $mongoArr);

        //        $mail = new sendAMail(APP_SERVER_HOST);        
        //        $mailArr = $mail->sendDocWelcomeMail($datai['email'], $datai['first_name']);
        //        $mailres = $mail->sendMailToSuperAdmin($datai);

        return true;
    }

    function editdriverdata() {
        $profile_pic = '';
        $this->load->library('mongo_db');
        $driverid = (int) $this->input->post('driver_id');
        $first_name = $this->input->post('firstname');
        $last_name = $this->input->post('lastname');
        //$password = $this->input->post('password');
        $city_id = $this->input->post('city_id');
        $email = $this->input->post('email');
        //$mobile = $this->input->post('mobile');
        $zipcode = $this->input->post('postal_code');
        $country_code = '+' . $this->input->post('c_code');
        $created_dt = date('Y-m-d H:i:s', time());
        $type_id = 1;
        $pro_type_arr = $this->input->post('pro_type_arr');
        $fees_group = $this->input->post('fees_group');

        $address_line1 = $this->input->post('proaddress');
        $countryName = $this->input->post('country');
        $cityName = $this->input->post('administrative_area_level_1');
        $tax_num = $this->input->post('tax_num');
        $exp_date = $this->input->post('expirationrc_l');
        $board_certification_expiry_dt = $exp_date;
        if ($exp_date != '') {
            $board_certification_expiry_dt = date("Y-m-d", strtotime($exp_date));
        }
        $medical_license_num = $this->input->post('licence_no');
        $about = $this->input->post('aboutdata');
        $expertise = $this->input->post('experties');
        $languages = $this->input->post('lanKnow');
        if (isset($_FILES["photos"])) {
            $pro_pic = $_FILES["photos"]["name"];
            $size = $_FILES['photos']['size'];
            $tmp = $_FILES['photos']['tmp_name'];
            $pro_pic_ext = substr($pro_pic, strrpos($pro_pic, '.') + 1);
            $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");
            $msg = false;
            $bucket = BUCKET_NAME;
            $s3 = new S3(AMAZON_AWS_ACCESS_KEY, AMAZON_AWS_AUTH_SECRET);
            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);

            if (strlen($pro_pic) > 0) {

                if (in_array($pro_pic_ext, $valid_formats)) {
                    if ($size < (1024 * 1024)) {
                        $actual_image_name = "ProfileImages/" . $email . "." . $pro_pic_ext;
                        if ($s3->putObjectFile($tmp, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
                            $msg = true;
                            $s3file = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                        } else
                            $msg = false;
                    } else
                        $msg = false;
                } else
                    $msg = false;
            } else
                $msg = true;

//            if ($msg == FALSE) {
//                die('hjhjgj');
//                return false;
//            }
            if (isset($s3file)) {
                $profile_pic = $s3file;
            }
        }
//      document edit for provider start code
        $certificate_upload = '';
        $expirationrc = $this->input->post('expirationrc');
        if (isset($_FILES["certificate"])) {
            $name1 = $_FILES["certificate"]["name"];
            $ext1 = substr($name1, strrpos($name1, '.') + 1); //explode(".", $name); # extra () to prevent notice //1  doctype
            $cert_name1 = (rand(1000, 9999) * time()) . '.' . $ext1;

            $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/pics/';

            try {

                if (move_uploaded_file($_FILES['certificate']['tmp_name'], $documentfolder . $cert_name1)) {
                    $certificate_upload = $cert_name1;
                }
            } catch (Exception $ext1) {
                print_r($ext1);
                return false;
            }
        }
//      document edit for provider end code

        if ($profile_pic != '') {
            if ($password == "******") {
                $driverdetails = array('first_name' => $first_name, 'last_name' => $last_name, 'profile_pic' => $profile_pic, 'city_id' => $city_id,
                    'created_dt' => $created_dt, 'type_id' => $type_id, 'zipcode' => $zipcode, 'email' => $email,
                    'country_code' => $country_code, 'address_line1' => $address_line1, 'countryName' => $countryName, 'cityName' => $cityName, 'tax_num' => $tax_num,
                    'board_certification_expiry_dt' => $board_certification_expiry_dt, 'medical_license_num' => $medical_license_num, 'about' => $about, 'expertise' => $expertise, 'languages' => $languages);
            } else {
                $driverdetails = array('first_name' => $first_name, 'last_name' => $last_name, 'profile_pic' => $profile_pic, 'city_id' => $city_id,
                    'created_dt' => $created_dt, 'type_id' => $type_id, 'zipcode' => $zipcode, 'email' => $email, 'country_code' => $country_code, 'address_line1' => $address_line1, 'countryName' => $countryName, 'cityName' => $cityName, 'tax_num' => $tax_num,
                    'board_certification_expiry_dt' => $board_certification_expiry_dt, 'medical_license_num' => $medical_license_num, 'about' => $about, 'expertise' => $expertise, 'languages' => $languages);
            }
        } else {
            if ($password == "******") {
                $driverdetails = array('first_name' => $first_name, 'last_name' => $last_name, 'city_id' => $city_id,
                    'created_dt' => $created_dt, 'type_id' => $type_id, 'zipcode' => $zipcode, 'email' => $email, 'country_code' => $country_code, 'address_line1' => $address_line1, 'countryName' => $countryName, 'cityName' => $cityName, 'tax_num' => $tax_num,
                    'board_certification_expiry_dt' => $board_certification_expiry_dt, 'medical_license_num' => $medical_license_num, 'about' => $about, 'expertise' => $expertise, 'languages' => $languages);
            } else {
                $driverdetails = array('first_name' => $first_name, 'last_name' => $last_name, 'city_id' => $city_id,
                    'created_dt' => $created_dt, 'type_id' => $type_id, 'zipcode' => $zipcode, 'email' => $email, 'country_code' => $country_code, 'address_line1' => $address_line1, 'countryName' => $countryName, 'cityName' => $cityName, 'tax_num' => $tax_num,
                    'board_certification_expiry_dt' => $board_certification_expiry_dt, 'medical_license_num' => $medical_license_num, 'about' => $about, 'expertise' => $expertise, 'languages' => $languages);
            }
        }
        if ($certificate_upload != '') {
            $driverdetails['medical_license_pic'] = $certificate_upload;
        }

        $this->db->where('doc_id', $driverid);
        $this->db->update('doctor', $driverdetails);

        $tarr = array();
        if ($fees_group == 1) {
            $tarr = explode(",", $this->input->post('mtypearr'));
        } else if ($fees_group == 2) {
            $tarr = explode(",", $this->input->post('htypearr'));
        } else if ($fees_group == 3) {
            $tarr = explode(",", $this->input->post('ftypearr'));
        }

        $sel_cat = array();
        foreach ($tarr as $type) {

            $flag = 0;
            $db = $this->mongo_db->db;
            $selecttb = $db->selectCollection('location');
            $resu = $selecttb->findOne(array('user' => $driverid));
            foreach ($resu['catlist'] as $catlist) {
                if ($catlist['cid'] == $type) {
                    $flag = 1;
                    $sel_cat[] = array('cid' => $type, 'status' => $catlist['status'], 'amount' => $catlist['amount']);
                }
            }
            if ($flag == 0) {
                $selecttbd = $db->selectCollection('Category');
                $resue = $selecttbd->findOne(array('_id' => new MongoId($type)));
                $sel_cat[] = array('cid' => $type, 'status' => 1, 'amount' => $resue['price_min']);
            }
        }

        $radius = $this->input->post('radius');
        $curr_date = time();
        $curr_gmt_dates = gmdate('Y-m-d H:i:s', $curr_date);
        $curr_gmt_date = new MongoDate(strtotime($curr_gmt_dates));
        $catstr = $this->input->post('pro_type_arr');
        $mongoArr = array();
        if ($profile_pic != '') {
            if ($fees_group == '') {

                $mongoArr = array("name" => $first_name, "lname" => $last_name, "image" => $profile_pic,
                    'city_id' => $city_id, 'radius' => (int) $radius,
                    'email' => strtolower($email), 'dt' => $curr_gmt_date
                );
            } else {

                $mongoArr = array("name" => $first_name, "lname" => $last_name, "image" => $profile_pic,
                    "fee_group" => $fees_group,
                    'catlist' => $sel_cat, 'radius' => (int) $radius,
                    'city_id' => $city_id,
                    'email' => strtolower($email), 'dt' => $curr_gmt_date
                );
            }
        } else {
            if ($fees_group == '') {
                $mongoArr = array("name" => $first_name, "lname" => $last_name,
                    'city_id' => $city_id, 'radius' => (int) $radius,
                    'email' => strtolower($email), 'dt' => $curr_gmt_date
                );
            } else {
                $mongoArr = array("name" => $first_name, "lname" => $last_name,
                    "fee_group" => $fees_group,
                    'catlist' => $sel_cat, 'radius' => (int) $radius,
                    'city_id' => $city_id,
                    'email' => strtolower($email), 'dt' => $curr_gmt_date
                );
            }
        }





//        if ($insurname != '')
//            $mongoArr = array("name" => $first_name, "lname" => $last_name, "image" => $profile_pic,);
//        else
//            $mongoArr = array("name" => $first_name, "lname" => $last_name);

        $this->mongo_db->update('location', $mongoArr, array('user' => $driverid));

        return true;
    }

    function documentgetdatavehicles() {
        $val = $this->input->post("val");

        $vehicleImage = array();

        $return = $data = array();
        foreach ($val as $row) {
            $data = $this->db->query("select * from vechiledoc where vechileid = '" . $row . "'")->result();
            //            return $data;
        }
        foreach ($data as $vehicle) {


            $return[] = array('doctype' => $vehicle->doctype, 'url' => $vehicle->url, 'expirydate' => $vehicle->expirydate);
        }
        $vehicleImage = $this->db->query("select Vehicle_Image from workplace where workplace_id = '" . $val[0] . "'")->row_array();
        $return[] = array('doctype' => '99', 'urls' => $vehicleImage['Vehicle_Image'], 'expirydate' => "");

        return $return;
    }

    function uploadimage_diffrent_redulation($file_to_open, $imagename, $servername, $ext) {

        list($width, $height) = getimagesize($file_to_open);

        $ratio = $height / $width;
        /* mdpi 36*36 */
        $mdpi_nw = 36;
        $mdpi_nh = $ratio * 36;

        $mtmp = imagecreatetruecolor($mdpi_nw, $mdpi_nh);
        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($mtmp, $new_image, 0, 0, 0, 0, $mdpi_nw, $mdpi_nh, $width, $height);

        $mdpi_file = $servername . 'pics/mdpi/' . $imagename;

        imagejpeg($mtmp, $mdpi_file, 100);

        /* HDPI Image creation 55*55 */
        $hdpi_nw = 55;
        $hdpi_nh = $ratio * 55;

        $tmp = imagecreatetruecolor($hdpi_nw, $hdpi_nh);

        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($tmp, $new_image, 0, 0, 0, 0, $hdpi_nw, $hdpi_nh, $width, $height);

        $hdpi_file = $servername . 'pics/hdpi/' . $imagename;

        imagejpeg($tmp, $hdpi_file, 100);

        /* XHDPI 84*84 */
        $xhdpi_nw = 84;
        $xhdpi_nh = $ratio * 84;

        $xtmp = imagecreatetruecolor($xhdpi_nw, $xhdpi_nh);

        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($xtmp, $new_image, 0, 0, 0, 0, $xhdpi_nw, $xhdpi_nh, $width, $height);

        $xhdpi_file = $servername . 'pics/xhdpi/' . $imagename;

        imagejpeg($xtmp, $xhdpi_file, 100);

        /* xXHDPI 125*125 */
        $xxhdpi_nw = 125;
        $xxhdpi_nh = $ratio * 125;

        $xxtmp = imagecreatetruecolor($xxhdpi_nw, $xxhdpi_nh);

        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($xxtmp, $new_image, 0, 0, 0, 0, $xxhdpi_nw, $xxhdpi_nh, $width, $height);

        $xxhdpi_file = $servername . 'pics/xxhdpi/' . $imagename;

        imagejpeg($xxtmp, $xxhdpi_file, 100);
    }

    function AddNewVehicleData() {

        //        'expirydate' => date("Y-m-d", strtotime($expirationrc)),

        $title = $this->input->post('title');
        $vehiclemodel = $this->input->post('vehiclemodel');
        $vechileregno = $this->input->post('vechileregno');
        $licenceplaetno = $this->input->post('licenceplaetno');
        $vechilecolor = $this->input->post('vechilecolor');
        $type_id = $this->input->post('getvechiletype');
        $expirationrc = $this->input->post('expirationrc');

        $expirationinsurance = $this->input->post('expirationinsurance');
        $expirationpermit = $this->input->post('expirationpermit');
        $companyname = $this->input->post('company_select'); //$this->session->userdata('LoginId');
        $vehicleid = $this->input->post('vehicleid'); //$this->session->userdata('LoginId');

        $insuranceno = $_REQUEST['Vehicle_Insurance_No'];


        $name = $_FILES["certificate"]["name"];
        $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice
        $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;

        $insurname = $_FILES["insurcertificate"]["name"];
        $ext1 = substr($insurname, strrpos($insurname, '.') + 1); //explode(".", $insurname);
        $insurance_name = (rand(1000, 9999) * time()) . '.' . $ext1;

        $carriagecert = $_FILES["carriagecertificate"]["name"];
        $ext2 = substr($carriagecert, strrpos($carriagecert, '.') + 1); //explode(".", $carriagecert);
        $carriage_name = (rand(1000, 9999) * time()) . '.' . $ext2;

        $vehicleimage = $_FILES["imagefile"]["name"];
        $text3 = substr($vehicleimage, strrpos($vehicleimage, '.') + 1);
        $image_name = (rand(1000, 999) * time()) . '.' . $text3;



        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/' . mainfolder . '/pics/';

        try {
            move_uploaded_file($_FILES['certificate']['tmp_name'], $documentfolder . $cert_name);
            move_uploaded_file($_FILES['insurcertificate']['tmp_name'], $documentfolder . $insurance_name);
            move_uploaded_file($_FILES['carriagecertificate']['tmp_name'], $documentfolder . $carriage_name);
            move_uploaded_file($_FILES['imagefile']['tmp_name'], $documentfolder . $image_name);
        } catch (Exception $ex) {
            print_r($ex);
            return false;
        }

        $selectPrefixQry = $this->db->query("select (select LEFT(companyname,2) from company_info where company_id = '" . $companyname . "') as company_prefix,(select LEFT(type_name,2) from workplace_types where type_id = '" . $type_id . "') as type_prefix from dual")->result();

        $vehiclePrefix = strtoupper($selectPrefixQry->company_prefix) . strtoupper($selectPrefixQry->type_prefix);

        $get_last_inserted_id = $this->insertQuery($vehiclePrefix, $type_id, $title, $vehiclemodel, $vechileregno, $licenceplaetno, $vechilecolor, $companyname, $insuranceno, $image_name, $vehicleid);

        $insert_doc = $this->db->query("INSERT INTO `vechiledoc`(`url`, `expirydate`, `doctype`,`vechileid`) VALUES ('" . $insurance_name . "','" . (date("Y-m-d", strtotime($expirationinsurance))) . "','2','" . $get_last_inserted_id . "'),
        ('" . $cert_name . "','" . (date("Y-m-d", strtotime($expirationrc))) . "','1','" . $get_last_inserted_id . "'),
        ('" . $carriage_name . "','" . (date("Y-m-d", strtotime($expirationpermit))) . "','3','" . $get_last_inserted_id . "')");



        return;
    }

    function insertQuery($vehiclePrefix, $type_id, $title, $vehiclemodel, $vechileregno, $licenceplaetno, $vechilecolor, $companyname, $insuranceno, $image_name, $vehicleid) {

        if ($vehicleid != '') {
            $uniq_id = $vehicleid;
        } else {
            $rand = rand(100000, 999999);
            $uniq_id = $vehiclePrefix . $rand; //str_pad($rand, 6, '0', STR_PAD_LEFT);
        }

        $this->db->query("INSERT INTO workplace(uniq_identity,type_id,Title,Vehicle_Model,Vehicle_Reg_No, License_Plate_No,Vehicle_Color,company,Status,Vehicle_Insurance_No,Vehicle_Image) VALUES ('" . $uniq_id . "','" . $type_id . "','" . $title . "','" . $vehiclemodel . "','" . $vechileregno . "','" . $licenceplaetno . "','" . $vechilecolor . "','" . $companyname . "','5','" . $insuranceno . "','" . $image_name . "')");

        if ($this->db->_error_number() == 1586) {
            if ($vehicleid != '') {
                return false;
            }
            return $this->insertQuery($uniq_id, $type_id, $title, $vehiclemodel, $vechileregno, $licenceplaetno, $vechilecolor, $companyname, $insuranceno, $vehicleid);
        } else {
            return $this->db->insert_id();
        }
    }

    function getBookingDetails() {
        $cond = array('bid' => $this->input->post('bid'));
        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;
        $bookingsColl = $db->selectCollection('bookings');
        $booking = $bookingsColl->findOne($cond);
//        $booking = $this->mongo_db->get_where('bookings', array('bid' => $bid));
        return $booking;
    }

    function get_appointmentDetials() {

        $bid = $this->input->post('app_id');
        $this->load->library('mongo_db');
        $result = $this->mongo_db->get_one('bookings', array('bid' => $bid));
        if ($result['payment_type'] == 1)
            $payment_type = "CASH";
        else
            $payment_type = "CARD";
        $returnJson['provider_id'] = $result['provider_id'];
        $returnJson['appointment_id'] = $result['bid'];
        $returnJson['address_line1'] = $result['address1'];
        $returnJson['appointment_dt'] = $result['appt_date'];
        $returnJson['typename'] = $result['cat_name'];
        $returnJson['paymentstatus'] = $payment_type;
        $returnJson['status_result'] = $this->GetBookingStatusMessage($result['status']);
        $returnJson['sname'] = $result['customer']['fname'];
        $returnJson['phone'] = $result['customer']['mobile'];
        $returnJson['email'] = $result['customer']['email'];
        $returnJson['visit_fees'] = $result['visit_fees'];
        $returnJson['price_min'] = $result['price_min'];
        $returnJson['mas_id'] = $result['provider_id'];
        $returnJson['appt_lat'] = $result['appt_lat'];
        $returnJson['appt_long'] = $result['appt_long'];

        $returnJson = json_decode(json_encode($returnJson), true);
        $db = $this->mongo_db->db->selectCollection('location');
//        echo "<pre>";
//        print_r($returnJson);
        $getBookingIds = $db->findOne(array('user' => (int) $returnJson['provider_id']));


        $services_total = 0;
        $services = "";
        if (isset($result['services']) && is_array($result['services'])) {
            foreach ($result['services'] as $services) {
                $services_total += $services['sprice'];
                $services .= " , " . $services['sname'];
            }
        }

        $approxamt = $returnJson['visit_fees'] + $returnJson['price_min'] + $services_total;

        if ($services == "")
            $services = "No Services Selected";

        $booking_type = "NOW";
        if ($result['booking_type'] == "2")
            $booking_type = "LATER";

        $returnJson['services'] = $services;
        $returnJson['booking_type'] = $booking_type;
        $returnJson['coupon_discount'] = $result['coupon_discount'];
        $returnJson['apprxAmt'] = CURRENCY . " " . round($approxamt);
        $returnJson['chn'] = $getBookingIds['socket'];
        $returnJson['first_name'] = $result['provider']['fname'];
        $returnJson['mobile'] = $result['provider']['mobile'];


        echo json_encode($returnJson); //json_encode($returnJson);
    }

    function get_appointment_details() {

        $bid = $this->input->post('app_id');
        $this->load->library('mongo_db');
        $result = $this->mongo_db->get_one('bookings', array('bid' => $bid));

        $returnJson = array(
            'appointment_id' => $result['bid'],
            'mas_id' => $result['provider_id'],
            'first_name' => $result['provider']['fname'],
            'mobile' => $result['provider']['mobile'],
            'pessanger_fname' => $result['customer']['fname'],
            'phone' => $result['customer']['mobile'],
            'appointment_dt' => $result['appt_date'],
            'address_line1' => $result['address'],
            'drop_addr1' => $result['Drop_address'],
        );


        echo json_encode($returnJson); //json_encode($returnJson);
    }

    function complete_getTransectionData() {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        if ($this->input->post('sSearch') != '') {
            $cond = array('status' => 7);
//            $cond = array('name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
        } else {
            $cond = array('status' => 7);
        }
        $data = array();

        $bookingData = $this->mongo_db->get_where('bookings', $cond)->sort(array('_id' => -1));
        foreach ($bookingData as $row) {
            if ($row['booking_type'] == 1)
                $booking_type = "NOW";
            else if ($row['booking_type'] == 2)
                $booking_type = "Later";
            else if ($row['booking_type'] == 3)
                $booking_type = "NOW";

            if (!isset($row['invoiceData']))
                continue;
            $pg_Commission = $row['invoiceData']['pro_pg_comm'] + $row['invoiceData']['app_pg_comm'];

            if ($row['payment_type'] == 1)
                $payment_type = "CASH";
            else if ($row['payment_type'] == 2)
                $payment_type = "CARD";
            $status = $this->GetBookingStatusMessage($row['status']);

            $data[] = array($row['bid'], $row['provider_id'], $row['provider']['fname'],
                $row['customer']['id'], $row['customer']['fname'], $this->dateTimes($row['appt_last_modified']), $row['cat_name'],
                $booking_type
                , $status,
                $payment_type, '<a id="download"  href="' . base_url() . '/index.php/superadmin/tripDetails/' . $row['bid'] . '">SHOW</a>');
        }


        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = strtoupper($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(strtoupper($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function DriverAcceptence_ajax() {


        $this->load->library('Datatables');
        $this->load->library('table');
        $city = $this->session->userdata('city_id');

        $_POST["sColumns"] = "m.doc_id,m.first_name,monum,Total Bookings,Total Accepted,Total Rejected,Total Cancel,Didn't respond,AcceptenceRate";
        if ($city != '0') {
            $query = "ci.city = ca.City_Id and ca.City_Id = '" . $city . "'";
            $this->datatables->select("m.doc_id,m.first_name,m.mobile as monum", false)
                    ->add_column('Total Bookings', 'Totalbookings/$1', 'm.doc_id')
                    ->add_column('Total Accepted', 'Totalaccepted/$1', 'm.doc_id')
                    ->add_column('Total Rejected', 'Totalrejected/$1', 'm.doc_id')
                    ->add_column('Total Cancel', 'Totalcancel/$1', 'm.doc_id')
                    ->add_column("Didn't respond", 'DidntRespond/$1', 'm.doc_id')
                    ->add_column("AcceptenceRate", 'AcceptenceRate/$1', 'm.doc_id')
                    ->from('doctor m,city_available ca', false)
                    ->where($query);
        } else
            $this->datatables->select("m.doc_id,m.first_name,m.mobile as monum", false)
                    ->add_column('Total Bookings', 'Totalbookings/$1', 'm.doc_id')
                    ->add_column('Total Accepted', 'Totalaccepted/$1', 'm.doc_id')
                    ->add_column('Total Rejected', 'Totalrejected/$1', 'm.doc_id')
                    ->add_column('Total Cancel', 'Totalcancel/$1', 'm.doc_id')
                    ->add_column("Didn't respond", 'DidntRespond/$1', 'm.doc_id')
                    ->add_column("AcceptenceRate", 'AcceptenceRate/$1', 'm.doc_id')
                    ->from('doctor m')
                    ->where('status != 1');

        // $this->db->order_by("m.doc_id", "desc");

        echo $this->datatables->generate();
    }

    //Get the all the Dispatched jobs by filter by company
    function filter_AllOnGoing_jobs() {

        $city = $this->session->userdata('city_id');
        $company = $this->session->userdata('company_id');
        $query_new = " appointment a,slave p,master m where a.mas_id = m.mas_id and a.slave_id=p.slave_id  and a.status IN (6,7,8) order by a.appointment_id desc LIMIT 200";
        if (($city != '0' && $company != '0'))
            $query_new = '  appointment a,slave p,master m,company_info ci,city_available ca where ca.City_Id = ci.city  and ci.company_id = m.company_id and  m.company_id = "' . $company . '" and a.mas_id = m.mas_id and a.slave_id=p.slave_id  and a.status IN (6,7,8) order by a.appointment_id desc LIMIT 200 ';
        else if ($city != '0')
            $query_new = '  appointment a,slave p,master m,company_info ci,city_available ca where ca.City_Id = "' . $city . '" AND ci.city = "' . $city . '" and ci.company_id = m.company_id and a.mas_id = m.mas_id and a.slave_id=p.slave_id  and a.status IN (6,7,8) order by a.appointment_id desc LIMIT 200 ';

        $query = $this->db->query("select m.first_name,m.last_name,a.mas_id,m.mobile as dphone,a.appointment_id,a.complete_dt,a.appointment_dt,a.B_type,a.address_line1,a.address_line2,a.apprxAmt,a.drop_addr1,a.drop_addr2,a.mas_id,a.slave_id,p.first_name as pessanger_fname,p.last_name as pessanger_lname,p.phone,a.status from " . $query_new)->result();
        echo json_encode(array('aaData' => $query, 'query' => $query_new));
    }

    function CompleteBooking() {

        $actionType = $this->input->post('actionType');
        $bid = $this->input->post('app_id');
        $proid = $this->input->post('proid');
        $amount = $this->input->post('amount');
        $appdate = $this->input->post('apdate');
        $pemail = $this->input->post('email');
        $resp = $this->input->post('response');
        $currdatetime = date('Y-m-d');


        $url = $this->serviceUrl . 'updateApptStatusFromAdmin';
        $fields = array(
            'ent_bid' => $bid,
            'ent_pro_id' => $proid,
            'ent_amount' => $amount,
            'ent_appnt_dt' => $appdate,
            'ent_response' => 7,
            'ent_pat_email' => $pemail,
            'ent_date_time' => $currdatetime
        );


        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        print_r($result);
        $cycleData = json_decode($result, true);
        print_r($cycleData);
        return;


//        $bid = $this->input->post('app_id');
//        $data = $this->input->post('data');
//        $CompletedDate = $this->input->post('duration');
//        $amount = trim(str_replace('£', "", $this->input->post('amount')));
//
//        $this->load->library('mongo_db');
//        $apptDet = $this->mongo_db->get_one('bookings', array('bid' => $bid));
//
//        $query = "select s.stripe_id,s.email from patient s where patient_id = '" . $apptDet['customer']['id'] . "'";
//        $res = $this->db->query($query)->row_array();
//
//        $return = array('flag' => 0, 'msg' => 'Updated Successfully.');
//
//        $getAppointmnet = $this->db->query("select * from bookings where bid = '" . $bid . "'");
//        if ($getAppointmnet->num_rows() > 0) {
//            echo json_encode(array('flag' => 1, "msg" => "Process already completed."));
//            return;
//        }
//
//        if ($apptDet['payment_type'] == 1) {
//            $transfer['id'] = "cash_000_" . $bid;
//            $this->db->query("update bookings set status = 7,billed_amount ='" . $amount . "' where bid = '" . $bid . "' ");
//        } else {
//            if ($res['stripe_id'] == '') {
//                $return = array("flag" => 1, "error" => "Card Is not define.");
//            }
//            $stripe = new StripeModule();
//
//            $chargeCustomerArr = array('stripe_id' => $res['stripe_id'], 'amount' => (int) ((float) $amount * 100), 'currency' => CURRENCY, 'description' => 'From ' . $res['email']);
//
//            $transfer = $stripe->apiStripe('chargeCard', $chargeCustomerArr);
//
//            if ($transfer['error']) {
//
//                $return = array('flag' => 1, "msg" => $transfer['error']['message'], "stripeerror" => $transfer['error']['message']);
//            }
//            $this->db->query("update bookings set status = 7,billed_amount ='" . $amount . "' where bid = '" . $bid . "' ");
//        }
//
//        $profilt = ($amount * (app_commision / 100));
//        $masearning = ($amount - $profilt);
//
//        $insertAppointmentQry = "insert into bookings(bid,pro_id,cust_id,appointment_dt,status,billed_amount,app_billing,
//            pro_billing,pro_sub_total,pro_pg_comm,pro_earning,app_sub_total,app_pg_comm,app_earning,payment_status,trans_id) 
//            values('" . $bid . "','" . $apptDet['mas_id'] . "','" . $apptDet['customer']['id'] . "','" . $apptDet['appt_date'] . "',
//                '7','" . $amount . "','" . $amount . "','" . $amount . "','0','0','" . $masearning . "','" . $profilt . "','0','" . $CompletedDate . "','0','" . $transfer['id'] . "')";
//        $this->db->query($insertAppointmentQry);
//        if ($this->db->insert_id() < 0) {
//            echo json_encode(array('flag' => 1, "msg" => "error while processing  request."));
//            return;
//        }
//
//
//        $InvoiceData = array(
//            "txn_id" => $transfer['id'],
//            "invoice_id" => '',
//            "billed_amount" => (float) ($amount),
//            "app_billing" => $amount,
//            "pro_billing" => (float) ($amount),
//            'pro_sub_total' => '0',
//            'pro_pg_comm' => '0',
//            "pro_earning" => (float) ($masearning),
//            "app_sub_total" => $profilt,
//            "app_pg_comm" => '0',
//            "app_earning" => '0'
//        );
//
//
//        $mongoArr = array('status' => 3, "booked" => 0);
//        $this->mongo_db->update('bookings', array('status' => 7, "job_status" => 100, 'invoiceData' => $InvoiceData), array('bid' => $bid));
////        $updateAppointment = $this->mongo_db->updatewithpush('bookings', array('invoiceData' => $InvoiceData), array("_id" => (int) $bid));
//        $this->mongo_db->update('location', $mongoArr, array('user' => (int) $apptDet['provider_id']));
//        echo json_encode($return);
    }

    function cancelBooking() {
        $actionType = $this->input->post('actionType');
        $bid = $this->input->post('app_id');
        $proid = $this->input->post('proid');
        // $amount = $this->input->post('amount');
        $appdate = $this->input->post('apdate');
        // $pemail = $this->input->post('email');
        // $resp = $this->input->post('response');
        $currdatetime = date('Y-m-d');


        $url = $this->serviceUrl . 'abortAppointmentFromSuperadmin';

        $fields = array(
            'ent_bid' => $bid,
            'ent_pro_id' => $proid,
            'ent_appnt_dt' => $appdate,
            'ent_date_time' => $currdatetime
        );



        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        print_r($result);
        $cycleData = json_decode($result, true);
//            print_r($cycleData);
        echo $cycleData;
//        $bid = $this->input->post('app_id');
//        $data = $this->input->post('data');
//
//
//
//
//        $this->db->query("update bookings set status = 7 where bid = '" . $bid . "' ");
//
//
//        $mongoArr = array('status' => 3, "booked" => 0);
//        $this->mongo_db->update('bookings', array('status' => 10, "job_status" => 101), array('bid' => $bid));
////        $updateAppointment = $this->mongo_db->updatewithpush('bookings', array('invoiceData' => $InvoiceData), array("_id" => (int) $bid));
//        $this->mongo_db->update('location', $mongoArr, array('user' => (int) $apptDet['provider_id']));
        echo json_encode($return);
    }

    function get_ongoing_bookings() {
        $this->load->library('mongo_db');
        $this->mongo_db->wheres = array('status' => array('$in' => array(2, 5, 21, 6, 22)));
        $appointments = $this->mongo_db->get('bookings')->sort(array('_id' => -1));
        $returnJson[] = (object) array();
        foreach ($appointments as $result) {
            $returnJson[] = (object) array(
                        'appointment_id' => $result['bid'],
                        'mas_id' => $result['provider_id'],
                        'first_name' => $result['provider']['fname'],
                        'dphone' => $result['provider']['mobile'],
                        'status' => $this->GetBookingStatusMessage($result['status']),
                        'pessanger_fname' => $result['customer']['fname'],
                        'phone' => $result['customer']['mobile'],
                        'appointment_dt' => $result['appt_date'],
                        'address_line1' => $result['address1']
            );
        }
        return $returnJson;
    }

    function datatable_onGoing_jobs() {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $_POST["sColumns"] = "NAME,STATUS,NO ON LOCATIONS,SELECT";
        $this->mongo_db->wheres = array('status' => array('$in' => array(2, 5, 21, 6, 22)));
        $appointments = $this->mongo_db->get('bookings')->sort(array('_id' => -1));
        $returnJson = array();
        foreach ($appointments as $result) {
            $returnJson[] = array(
                '<a id = "d_no" onclick="getdetail(' . $result['bid'] . ')" class="idonclick" data = "' . $result['bid'] . '" style="cursor: pointer"><P>' . $result['bid'] . '</p></a>',
                $result['provider_id'],
                $result['provider']['fname'], $result['provider']['mobile'],
                $result['customer']['fname'], $result['customer']['mobile'],
                $result['appt_date'], $result['address1'],
                $this->GetBookingStatusMessage($result['status'])
            );
        }
        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($returnJson as $row) {
                $needle = strtoupper($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(strtoupper($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($returnJson);
//        print_r($returnJson);
//        echo $this->datatables->commission_Data($returnJson);
    }

    function get_provider_jobs($proid) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $this->mongo_db->wheres = array('provider_id' => $proid, 'status' => array('$in' => array(5, 21, 6, 22)));
        $appointments = $this->mongo_db->get('bookings')->sort(array('_id' => -1));
        $returnJson = array();
        foreach ($appointments as $result) {
            unset($result['dispatched']);
            unset($result['dispatchedRes']);
            unset($result['services']);
            $returnJson[] = $result;
        }
        return $returnJson;
    }

    function getTransectionData() {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        if ($this->input->post('sSearch') != '') {
            $cond = array('status' => array('$in' => array(7, 4, 9)));
//            $cond = array('name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
        } else {
            $cond = array('status' => array('$in' => array(7, 4, 9)));
        }
        $data = array();
        $bookingData = $this->mongo_db->get_where('bookings', $cond)->sort(array('_id' => -1));

        foreach ($bookingData as $row) {

            if ($row['booking_type'] == 1)
                $booking_type = "Now";
            else if ($row['booking_type'] == 2)
                $booking_type = "Later";
            else if ($row['booking_type'] == 3)
                $booking_type = "Now";

            if (!isset($row['invoiceData']))
                continue;
            $pg_Commission = $row['invoiceData']['total_pg_comm'];
            if ($pg_Commission == '') {
                $pg_Commission = 0;
            }

            if ($row['payment_type'] == 1)
                $payment_type = "CASH";
            else if ($row['payment_type'] == 2)
                $payment_type = "CARD";
            $status = $this->GetBookingStatusMessage($row['status']);
            if ($row['status'] == 7) {
                if ($row['dispute_status'] == "1") {
                    $status = "Booking Disputed";
                } elseif ($row['dispute_status'] == "2") {
                    $status = ' Dispute Resolved';
                } elseif ($row['job_status'] == 100) {
                    $status = 'Job Completed by Admin';
                }
            }
            if ($row['coupon_type'] == 1) {
                $discont = $row['coupon_discount'] . ' %';
            } else {
                $discont = CURRENCY . ' ' . $row['coupon_discount'];
            }

            $data[] = array('<a id="download" target="_blank" href="' . base_url() . '/index.php/superadmin/tripDetails/' . $row['bid'] . '">' . $row['bid'] . '</a>', $row['provider_id'], $row['provider']['fname'],
                $row['customer']['id'], $row['customer']['fname'], $row['appt_date'], $row['cat_name'],
                $booking_type, $row['fee_type'],
                '<a id="dis"  onclick="coupon_discount(' . $row['bid'] . ')">' . $discont . '</a>',
                '<a id="billed_amt" " onclick="billed_amount(' . $row['bid'] . ')">' . CURRENCY . ' ' . $row['invoiceData']['billed_amount'] . '</a>',
                '<a id="billed_amt" " onclick="pro_earning(' . $row['bid'] . ')">' . CURRENCY . ' ' . $row['invoiceData']['pro_earning'] . '</a>',
                '<a id="billed_amt" " onclick="app_earning(' . $row['bid'] . ')">' . CURRENCY . ' ' . $row['invoiceData']['app_earning'] . '</a>',
                '<a id="billed_amt" " onclick="pg_Commission(' . $row['bid'] . ')">' . CURRENCY . ' ' . $pg_Commission . '</a>',
                $row['invoiceData']['txn_id'], $status,
                $payment_type, '<a id="download" target="_blank" href="' . base_url() . '../getPDF.php?apntId=' . $row['bid'] . '">Download</a>');
        }

        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = strtoupper($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(strtoupper($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function get_all_data($stdate, $enddate) {


        if ($this->input->post('sSearch') != '') {
            $cond = array('name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
        } else {
            $cond = array('status' => 7);
        }

        $data = array();
        $bookingData = $this->mongo_db->get_where('bookings', $cond)->sort(array('_id' => -1));
        foreach ($bookingData as $row) {
            if ($row['booking_type'] == 1)
                $booking_type = "With Dispatcher";
            else if ($row['booking_type'] == 2)
                $booking_type = "Later";
            else if ($row['booking_type'] == 3)
                $booking_type = "WithOut Dispatcher";
            $pg_Commission = $row['invoiceData']['pro_pg_comm'] + $row['invoiceData']['app_pg_comm'];
            if ($row['payment_type'] == 1)
                $payment_type = "CASH";
            else if ($row['payment_type'] == 2)
                $payment_type = "CARD";
            $status = $this->GetBookingStatusMessage($row['status']);


            $data[] = array('JOB ID' => $row['bid'], 'PROVIDER ID' => $row['provider_id'],
                'PROVIDER NAME' => $row['provider']['fname'],
                'CUSTOMER ID' => $row['customer']['id'], 'CUSTOMER NAME' => $row['customer']['fname'],
                'DATE' => $row['appt_date'],
                'CATEGORY NAME' => $row['cat_name'],
                'BOOKING TYPE' => $booking_type,
                'FEE TYPE' => $row['fee_type'],
                'PAYMENT GATEWAY COMMISSION' => $pg_Commission,
                'STATUS' => $status,
                'PAYMENT TYPE' => $payment_type);
        }
        return $data;
    }

    function getBilledAmount($bid) {

        $this->load->library('mongo_db');
        $cond = array('bid' => $bid);

        $bookingData = $this->mongo_db->get_one('bookings', $cond);

        return $bookingData;
    }

    function transection_data_form_date($stdate = '', $enddate = '') {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $stdate = $stdate . '00:00';
        $enddate = $enddate . '24:00';
        $cond = array('appt_server_ts' => array('$gte' => strtotime($stdate), '$lte' => strtotime($enddate)),
            'status' => 7);

//         
        $data = array();
        $bookingData = $this->mongo_db->get_where('bookings', $cond)->sort(array('_id' => -1));
        foreach ($bookingData as $row) {
            if ($row['booking_type'] == 1)
                $booking_type = "Now";
            else if ($row['booking_type'] == 2)
                $booking_type = "Later";
            else if ($row['booking_type'] == 3)
                $booking_type = "Now";

            if (!isset($row['invoiceData']))
                continue;
            $pg_Commission = $row['invoiceData']['pro_pg_comm'] + $row['invoiceData']['app_pg_comm'];

            if ($row['payment_type'] == 1)
                $payment_type = "CASH";
            else if ($row['payment_type'] == 2)
                $payment_type = "CARD";
            $status = $this->GetBookingStatusMessage($row['status']);
            if ($row['status'] == 7) {
                if ($row['disputed'] == 1) {
                    $status = "Booking Disputed";
                } elseif ($row['job_status'] == 100) {
                    $status = 'Job Completed by Admin';
                }
            }


            $data[] = array($row['bid'], $row['provider_id'], $row['provider']['fname'],
                $row['customer']['id'], $row['customer']['fname'], $row['appt_date'], $row['cat_name'],
                $booking_type, $row['fee_type'],
                '<a id="dis"  onclick="coupon_discount(' . $row['bid'] . ')">' . CURRENCY . ' ' . $row['coupon_discount'] . '</a>',
                '<a id="billed_amt" " onclick="billed_amount(' . $row['bid'] . ')">' . CURRENCY . ' ' . $row['invoiceData']['billed_amount'] . '</a>',
                '<a id="billed_amt" " onclick="pro_earning(' . $row['bid'] . ')">' . CURRENCY . ' ' . $row['invoiceData']['pro_earning'] . '</a>',
                '<a id="billed_amt" " onclick="app_earning(' . $row['bid'] . ')">' . CURRENCY . ' ' . $row['invoiceData']['app_earning'] . '</a>',
                '<a id="billed_amt" " onclick="pg_Commission(' . $row['bid'] . ')">' . CURRENCY . ' ' . $pg_Commission . '</a>',
                $row['invoiceData']['txn_id'], $status,
                $payment_type, '<a id="download" target="_blank" href="' . base_url() . '../getPDF.php?apntId=' . $row['bid'] . '">Download</a>');
        }





        echo $this->datatables->commission_Data($data);
    }

    function getDataSelected($selectdval = '') {
        $this->load->library('Datatables');
        $this->load->library('table');
        if ($selectdval != '0') {
            $query = 'ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 7 and payment_status in(1,3) and ap.payment_type = "' . $selectdval . '"';
        } else if ($selectdval == '0' && $this->session->userdata('company_id') == '0') {
            $query = 'd.company_id = c.company_id and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 and payment_status in(1,3)';
        } else if ($selectdval != '0' && $this->session->userdata('company_id') == '0') {
            $query = 'd.company_id = c.company_id and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 and payment_status in(1,3) and ap.payment_type = "' . $selectdval . '" ';
        } else {
            $query = 'ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 and payment_status in(1,3) and d.company_id="' . $this->session->userdata('company_id') . '"';
        }


        $this->datatables->select("ap.appointment_id,ap.mas_id,ap.appointment_dt,round((ap.discount + ap.amount),2),ap.discount,ap.amount,ap.app_commission, ap.pg_commission,ap.mas_earning, ap.txn_id,(
        case ap.status when 1 then 'Request'
        when 2   then
        'Driver accepted.'
        when 3  then
        'Driver rejected.'
        when 4  then
        'Customer has cancelled.'
        when 5   then
        'Driver has cancelled.'
        when 6   then
        'Driver is on the way.'
        when 7  then
        'Appointment started.'
        when 8   then
        'Driver arrived.'
        when 9   then
        'Appointment completed.'
        when 10 then
        'Appointment timed out.'
        else
        'Status Unavailable.'
        END) as status_result, (case ap.payment_type  when 1 then 'card' when 2 then 'cash' END)", false)
                ->edit_column('ap.appointment_dt', "get_formatedDate/$1", 'ap.appointment_dt')
                ->add_column('Download', '<a href="' . base_url() . '../../getPDF.php?apntId=$1" target="_blank"> <button class="btn btn-primary btn-cons">Download </button></a>', 'ap.appointment_id')
                ->from('appointment ap,master d,slave p,company_info c')
                ->where($query);
        $this->db->order_by('ap.appointment_id', 'DESC');
        echo $this->datatables->generate();
    }

    function passenger_rating() {
        $status = 1;
        $query = $this->db->query(" SELECT p.patient_id, p.first_name ,p.email,r.rating FROM patient_rating r, patient p WHERE r.patient_id = p.patient_id  AND r.status = 1")->result();


        return $query;
    }

    function driver_review($status) {


        $query = $this->db->query(" SELECT r.review, r.status,r.star_rating, r.review_dt,r.appointment_id, r.doc_id, d.first_name AS mastername, p.patient_id,a.appointment_dt  FROM doctor_ratings r, doctor d, patient p,bookings a WHERE r.patient_id = p.patient_id  AND r.doc_id = d.doc_id  AND r.status ='" . $status . "' AND r.review <>'' AND a.bid = r.appointment_id ")->result();
        return $query;
    }

    function patientDetails($pay_id = '', $pro_id) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $_POST["sColumns"] = "bid,payment_method,b.appointment_dt,tot_ser_earn,other_earn,pro_earning,cash_collect,cash_due";
        if ($pay_id == 0) {
            $this->datatables->select('b.bid as bid,(case b.payment_type when 1 then "CASH" when 2 then "CARD" else "N/A" end) as payment_method, b.appointment_dt,b.bid as tot_ser_earn,b.bid as other_earn,CONCAT("' . CURRENCY . '"," ", b.pro_earning),b.bid as cash_collect,b.app_earning as cash_due,(case b.payment_status when 1 then "Paid" when 2 then "Partially Paid" else "Not Paid" end) as payment_status', false)
                    ->edit_column('tot_ser_earn', "get_toatl_service_earning/$1", 'tot_ser_earn')
                    ->edit_column('other_earn', "get_other_earning/$1", 'other_earn')
                    ->edit_column('cash_collect', "get_cash_collect/$1", 'cash_collect')
                    ->edit_column('cash_due', "$1", 'cash_due')
                    ->where('b.payment_status = 0 and b.pro_id = "' . $pro_id . '"')
                    ->from('bookings b', false);
        } else {
//            $this->datatables->select('b.bid as bid,(case b.payment_type when 1 then "CASH" when 2 then "CARD" else "N/A" end) as payment_method, b.appointment_dt,b.bid as tot_ser_earn,b.bid as other_earn,b.pro_earning,b.bid as cash_collect,b.app_earning as cash_due,(case p.status when 1 then "Paid" when 2 then "Partially Paid" else "Not Paid" end) as payment_status', false)
//                    ->edit_column('tot_ser_earn', "get_toatl_service_earning/$1", 'tot_ser_earn')
//                    ->edit_column('other_earn', "get_other_earning/$1", 'other_earn')
//                    ->edit_column('cash_collect', "get_cash_collect/$1", 'cash_collect')
//                    ->edit_column('cash_due', "$1", 'cash_due')
//                    ->where('p.bid = b.bid and p.payid = "' . $pay_id . '"')
//                    ->from('bookings b, pay_cycle p', false);

            $this->datatables->select('b.bid as bid,(case b.payment_type when 1 then "CASH" when 2 then "CARD" else "N/A" end) as payment_method, b.appointment_dt,b.bid as tot_ser_earn,b.bid as other_earn,b.pro_earning,b.bid as cash_collect,b.app_earning as cash_due', false)
                    ->edit_column('tot_ser_earn', "get_toatl_service_earning/$1", 'tot_ser_earn')
                    ->edit_column('other_earn', "get_other_earning/$1", 'other_earn')
                    ->edit_column('cash_collect', "get_cash_collect/$1", 'cash_collect')
                    ->edit_column('cash_due', "$1", 'cash_due')
//                    ->where('b.bid >= p.first_bid and b.bid <= p.last_bid and b.pro_id = "' . $pro_id . '"')
                    ->where('b.bid >= p.first_bid and b.bid <= p.last_bid and b.pro_id = "' . $pro_id . '" and p.mas_id = "' . $pro_id . '" ')
                    ->from('bookings b, payroll p', false);
//(case p.status when 1 then "Paid" when 2 then "Partially Paid" else "Not Paid" end) as payment_status
//            SELECT * FROM `bookings` WHERE bid > 0 and bid <= 693 and pro_id = 128
        }
        echo $this->datatables->generate();
    }

    function patientDetails_form_Date($stdate = '', $enddate = '', $doc_id = '') {

        $this->load->library('Datatables');
        $this->load->library('table');
        $query = 'ap.status = 9 and ap.payment_status in(1,3) and p.patient_id = ap.patient_id and ap.doc_id="' . $doc_id . '" and DATE(ap.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '" and ap.doc_id=doc.doc_id';
        $this->datatables->select('ap.appointment_id,p.first_name,ap.amount, ap.app_commision, ap.pg_commision,ap.doc_amount', false)
                ->from(' doctor doc,appointment ap,patient p', false)
                ->where($query);
        echo $this->datatables->generate();
    }

    function inactivedriver_review() {
        $val = $this->input->post('val');

        foreach ($val as $row) {
            $values = explode(",", $row);
            $query = $this->db->query("update doctor_ratings set status = 2 where appointment_id= '" . $row . "'");
        }
    }

    function activedriver_review() {
        $val = $this->input->post('val');

        foreach ($val as $row) {
            $values = explode(",", $row);
            $query = $this->db->query("update doctor_ratings set status=1 where  appointment_id= '" . $row . "'");
        }
    }

    function editGetdriver_review() {
        $did = $this->input->post("did");
        $queryM = $this->db->query("select * from doctor_ratings where appointment_id ='" . $did . "'")->result();
        echo json_encode($queryM);
    }

    function editdriver_review() {

        $did = $this->input->post("did");

        $review = $this->input->post("review");
        $rating = $this->input->post("rating");


        $query = $this->db->query("update doctor_ratings set review='" . $review . "',star_rating='" . $rating . "' where  appointment_id= '" . $did . "'");
        echo json_encode(array('msg' => "Review updated successfully", 'flag' => 0));
        return;
    }

    function get_Drivers_from_mongo($status) {

        $m = new MongoClient();
        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->location;
        $darray = array();
        if ($status == 3) { //online or free
            $drivers = $selecttb->find(array('status' => (int) $status));

            foreach ($drivers as $mas_id) {
                $darray[] = $mas_id['user'];
            }
        } elseif ($status == 567) {//booked
            $drivers = $selecttb->find(array('status' => array('$in' => array(5, 6, 7))));
            foreach ($drivers as $mas_id) {
                $darray[] = $mas_id['user'];
            }
        } elseif ($status == 30) {//OFFLINE
            $drivers = $selecttb->find(array('status' => (int) 4));
            foreach ($drivers as $mas_id) {
                $darray[] = $mas_id['user'];
            }
        }

        $mas_ids = implode(', ', $darray);

        $quaery = $this->db->query("SELECT mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt,(select type from user_sessions where oid = mas.mas_id order by oid DESC limit 0,1) as dev_type FROM master mas where  mas.mas_id IN (" . $mas_ids . ")  order by mas.mas_id DESC")->result();
        return $quaery;
    }

    function getDtiverDetail() {

        $did = $this->input->post("did");
        $queryM = $this->db->query("select * from doctor where doc_id ='" . $did . "'")->result();
        $queryapp = $this->db->query("select appointment_id,appointment_dt,address_line1 from appointment  where doc_id='" . $did . "' and status  in(1,2,6,7,8)")->result();
        foreach ($queryM as $master) {
            $name = $master->first_name . $master->last_name;
            $mobile = $master->mobile;
            $email = $master->email;
            $profile = $master->profile_pic;
        }

        if ($profile) {
            $img = base_url() . "../../pics/" . $profile;
        } else {
            $img = base_url() . "../../pics/aa_default_profile_pic.gif";
        }
        $html = '<div id="quickview" class="quickview-wrapper open" data-pages="quickview" style="max-height: 487px;margin-top: 39px;">
            <ul class="nav nav-tabs" style="padding: 0 14px;">
                <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                    <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="' . $img . '" data-src="' . $img . '" src="' . $img . '" class="col-top">
                        </span>
                    </span>
                    <p class="p-l-20 col-xs-height col-middle col-xs-12">
                        <span class="text-master" style="color: #ffffff !important;">' . $name . '</span>
                        <span class="block text-master hint-text fs-12" style="color: #ffffff !important;">+91' . $mobile . '</span>
                    </p>
                </a>
            </ul>
            <p class="close_quick"> <a class="btn-link quickview-toggle"><i class="pg-close" style="color: #ffffff ! important;" ></i></a></p>
            <div class="tab-content" style="top: 21px !important;">
                <div class="list-view-group-container" >
                    <ul>
                        <li class="chat-user-list clearfix">
                            <div class="form-control">
                                <label class="col-sm-5 control-label">Email</label><label class="col-sm-7 control-label">' . $email . '</label>
                            </div>
                        </li>
                    </ul>
                    <div class="list-view-group-container" style="overflow-y: scroll;max-height: 314px;">
                        <div class="list-view-group-header text-uppercase" style="background-color: #f0f0f0;padding: 10px;">
                            APPOINTMENTS</div>';
        foreach ($queryapp as $result) {
            $html .= '<div style="overflow: auto;background: #fff;">
                            <ul style="margin-top: 15px;">
                                <li class="chat-user-list clearfix">
                                    <div class="item share share-self col1" data-social="item" style="border: 2px solid #e5e8e9;">
                                        <div class="pull-right" style="margin: 5px 5px 0px 11px;width: 157px;">
                                            ' . date("M d Y g:i A", strtotime($result->appointment_dt)) . '
                                        </div>
                                        <div class="item-header clearfix" style="margin: 5px 8px 11px 12px;">

                                            ' . $result->appointment_id . '
                                        </div>
                                        <div class="item-description" style="">
                                            <ul>
                                                <li class="chat-user-list clearfix">
                                                    <div class=""  style="border: 1px solid rgba(0, 0, 0, 0.07);">
                                                        <p style="padding: 8px;">' . $result->address_line1 . '</p>
                                                    </div>
                                                </li>
                                                <li class="chat-user-list clearfix">
                                                    <div class="" style="border: 1px solid rgba(0, 0, 0, 0.07);">
                                                        <p style="padding: 8px;">' . $result->drop_addr1 . '</p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>';
        }
        $html .= '</div></div></div></div>';
        echo json_encode(array('html' => $html));
    }

    function Provider_Pay($proid = '') {
        $query1 = "select round(sum(pro_earning),2) as BookingEarning  from bookings where  pro_id = " . $proid . " and payment_status = 0";
        $query2 = "select IFNULL(round((select closing_balance from payroll where mas_id = " . $proid . " order by payroll_id desc limit 1),2),0) as ClosingBal";

        $res1 = $this->db->query($query1)->row();
        $res2 = $this->db->query($query2)->row();
        return round($res1->BookingEarning + $res2->ClosingBal, 2);
    }

    function pay_provider($proid = '') {
        $query1 = "select round(sum(pro_earning),2) as BookingEarning  from bookings where  pro_id = " . $proid . " and payment_status = 0 and payment_type = 2";
        $query2 = "select IFNULL(round((select closing_balance from payroll where mas_id = " . $proid . " order by payroll_id desc limit 1),2),0) as ClosingBal";
        $query3 = "select round(sum(app_earning),2) as appEarning  from bookings where  pro_id = " . $proid . " and payment_status = 0 and payment_type = 1";

        $res1 = $this->db->query($query1)->row();
        $res2 = $this->db->query($query2)->row();
        $res3 = $this->db->query($query3)->row();
        $duebalance = round(($res1->BookingEarning + $res2->ClosingBal) - $res3->appEarning, 2);
        $query = "select first_name from doctor where doc_id = '" . $proid . "'";
        $provider_name = $this->db->query($query)->result()[0]->first_name;
        $totalamountpaid = $this->db->query("SELECT sum(pay_amount) as totalamt from payroll WHERE  mas_id = '" . $proid . "'")->result()[0]->totalamt;
        $payrolldata = $this->db->query("SELECT * from payroll WHERE  mas_id = '" . $proid . "'")->result();

        $url = $this->serviceUrl . 'GetFinancialDataSuperadmin';
        $fields = array(
            'ent_pro_id' => $proid
        );
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $cycleData = json_decode($result, true);

        return array('provider_name' => $provider_name,
            'totalamountpaid' => $totalamountpaid,
            'duebalance' => $duebalance,
            'cycleData' => $cycleData);
    }

    function cycleDetails($pay_id, $pro_id) {
        $query = "select first_name from doctor where doc_id = '" . $pro_id . "'";
        $provider_name = $this->db->query($query)->result()[0]->first_name;

        if ($pay_id == 0) {
            $payQur = "select * from payroll where mas_id = '" . $pro_id . "' order by payroll_id";
            $payData = $this->db->query($payQur)->result();
            $last_ap_id = 0;
            if (count($payData) > 0) {
                $last_ap_id = (int) $payData[count($payData) - 1]->last_bid;
            } else {
                $last_ap_id = 0;
            }

            $rateQur = "SELECT avg(star_rating) as rev_rate FROM doctor_ratings WHERE doc_id = " . $pro_id . " and appointment_id > '" . $last_ap_id . "' ";
            $rateRes = $this->db->query($rateQur)->result()[0];
            $rev_rate = round($rateRes->rev_rate, 2);

            $totalBoookings = $this->mongo_db->db->selectCollection('bookings')->find(array('dispatched.pid' => $pro_id, '_id' => array('$gt' => $last_ap_id)))->count();
            $totalAccepted = $this->mongo_db->db->selectCollection('bookings')->find(array('dispatched.pid' => $pro_id, 'dispatched.res' => "2", 'status' => 7, '_id' => array('$gt' => $last_ap_id)))->count();
            $acpt_rate = (string) round(($totalAccepted / $totalBoookings) * 100, 2);

            if (count($payData) == 0) {
                $selectAppt = "select created_dt as start_date from doctor where doc_id = " . $pro_id;
            } else {
                $selectAppt = "select pay_date as start_date from payroll where mas_id = '" . $pro_id . "' order by payroll_id desc limit 1";
            }
            $start_date = $this->db->query($selectAppt)->result()[0]->start_date;
        } else {
            $cycleData = $this->db->query("SELECT * from payroll WHERE  payroll_id = '" . $pay_id . "'")->result();
            $rev_rate = $cycleData[0]->rev_rate;
            $acpt_rate = $cycleData[0]->acpt_rate;
            $count = 1;
            foreach ($cycleData as $pay) {
                if ($count == 1) {
                    $dateQur = "select created_dt from doctor where doc_id = " . $pro_id;
                    $start_date = $this->db->query($dateQur)->result()[0]->created_dt;
                } else {
                    $start_date = $pay->pay_date;
                }
            }
            $count++;
        }

        return array(
            'provider_name' => $provider_name,
            'rev_rate' => $rev_rate,
            'start_date' => $start_date,
            'acpt_rate' => $acpt_rate,
        );
    }

    function revDetails($pay_id, $pro_id) {
        $query = "select first_name from doctor where doc_id = '" . $pro_id . "'";
        $provider_name = $this->db->query($query)->result()[0]->first_name;

        if ($pay_id == 0) {
            $payQur = "select * from payroll where mas_id = '" . $pro_id . "' order by payroll_id";
            $payData = $this->db->query($payQur)->result();
            $last_ap_id = 0;
            if (count($payData) > 0) {
                $last_ap_id = (int) $payData[count($payData) - 1]->last_bid;
            } else {
                $last_ap_id = 0;
            }

            $rateQur = "SELECT avg(star_rating) as rev_rate FROM doctor_ratings WHERE doc_id = " . $pro_id . " and appointment_id > '" . $last_ap_id . "' ";
            $rateRes = $this->db->query($rateQur)->result()[0];
            $rev_rate = round($rateRes->rev_rate, 2);

            $allrevQury = "SELECT * FROM doctor_ratings WHERE doc_id = " . $pro_id . " and appointment_id > '" . $last_ap_id . "' ";
            $allrevRes = $this->db->query($allrevQury)->result();

            if (count($payData) == 0) {
                $selectAppt = "select created_dt as start_date from doctor where doc_id = " . $pro_id;
            } else {
                $selectAppt = "select pay_date as start_date from payroll where mas_id = '" . $pro_id . "' order by payroll_id desc limit 1";
            }
            $start_date = $this->db->query($selectAppt)->result()[0]->start_date;
        } else {
            $cycleData = $this->db->query("SELECT * from payroll WHERE  payroll_id = '" . $pay_id . "'")->result();
            $rev_rate = $cycleData[0]->rev_rate;

            $allrevQury = "SELECT * FROM doctor_ratings WHERE doc_id = " . $pro_id . " and appointment_id > '" . $cycleData[0]->first_bid . "' and appointment_id < '" . $cycleData[0]->last_bid . "' ";
            $allrevRes = $this->db->query($allrevQury)->result();

            $count = 1;
            foreach ($cycleData as $pay) {
                if ($count == 1) {
                    $dateQur = "select created_dt from doctor where doc_id = " . $pro_id;
                    $start_date = $this->db->query($dateQur)->result()[0]->created_dt;
                } else {
                    $start_date = $pay->pay_date;
                }
            }
            $count++;
        }

        return array(
            'provider_name' => $provider_name,
            'rev_rate' => $rev_rate,
            'start_date' => $start_date,
            'allrevRes' => $allrevRes
        );
    }

    function acptDetails($pay_id, $pro_id) {
        $query = "select first_name from doctor where doc_id = '" . $pro_id . "'";
        $provider_name = $this->db->query($query)->result()[0]->first_name;

        if ($pay_id == 0) {
            $payQur = "select * from payroll where mas_id = '" . $pro_id . "' order by payroll_id";
            $payData = $this->db->query($payQur)->result();
            $last_ap_id = 0;
            if (count($payData) > 0) {
                $last_ap_id = (int) $payData[count($payData) - 1]->last_bid;
            } else {
                $last_ap_id = 0;
            }

            $totalBoookings = $this->mongo_db->db->selectCollection('bookings')->find(array('dispatched.pid' => $pro_id, '_id' => array('$gt' => $last_ap_id)))->count();
            $totalAccepted = $this->mongo_db->db->selectCollection('bookings')->find(array('dispatched.pid' => $pro_id, 'dispatched.res' => "2", 'status' => 7, '_id' => array('$gt' => $last_ap_id)))->count();
            $tot_cancel = $this->mongo_db->db->selectCollection('bookings')->find(array('dispatched.pid' => $pro_id, 'dispatched.res' => "2", 'status' => 10, '_id' => array('$gt' => $last_ap_id)))->count();
            $totalRejected = $this->mongo_db->db->selectCollection('bookings')->find(array('dispatched.pid' => $pro_id, 'dispatched.res' => "3", '_id' => array('$gt' => $last_ap_id)))->count();
            $totalIgnored = $this->mongo_db->db->selectCollection('bookings')->find(array('dispatched.res' => array('$exists' => false), 'dispatched.pid' => $pro_id, '_id' => array('$gt' => $last_ap_id)))->count();

            $acpt_rate = (string) round(($totalAccepted / $totalBoookings) * 100, 2);

            if (count($payData) == 0) {
                $selectAppt = "select created_dt as start_date from doctor where doc_id = " . $pro_id;
            } else {
                $selectAppt = "select pay_date as start_date from payroll where mas_id = '" . $pro_id . "' order by payroll_id desc limit 1";
            }
            $start_date = $this->db->query($selectAppt)->result()[0]->start_date;
        } else {
            $cycleData = $this->db->query("SELECT * from payroll WHERE  payroll_id = '" . $pay_id . "'")->result();

            $totalBoookings = $cycleData[0]->tot_bookings;
            $totalAccepted = $cycleData[0]->tot_accepted;
            $tot_cancel = $cycleData[0]->tot_cancel;
            $totalRejected = $cycleData[0]->tot_reject;
            $totalIgnored = $cycleData[0]->tot_ignore;

            $acpt_rate = $cycleData[0]->acpt_rate;
            $count = 1;
            foreach ($cycleData as $pay) {
                if ($count == 1) {
                    $dateQur = "select created_dt from doctor where doc_id = " . $pro_id;
                    $start_date = $this->db->query($dateQur)->result()[0]->created_dt;
                } else {
                    $start_date = $pay->pay_date;
                }
            }
            $count++;
        }

        return array(
            'provider_name' => $provider_name,
            'totalBoookings' => $totalBoookings,
            'totalAccepted' => $totalAccepted,
            'tot_cancel' => $tot_cancel,
            'totalRejected' => $totalRejected,
            'totalIgnored' => $totalIgnored,
            'start_date' => $start_date,
            'acpt_rate' => $acpt_rate,
        );
    }

    function Driver_pay($masid = '') {
        $query = "select first_name from doctor where doc_id = '" . $masid . "' ";
        return $this->db->query($query)->result();
    }

    function get_payrolldata($id = '') {
        $quaery = $this->db->query("SELECT * from payroll WHERE  mas_id = '" . $id . "'")->result();
        return $quaery;
    }

    function Totalamountpaid($id = '') {
        $quaery = $this->db->query("SELECT sum(pay_amount) as totalamt from payroll WHERE  mas_id = '" . $id . "'")->result();
        return $quaery;
    }

    function getDatafromdate($stdate, $enddate) {
        $query = $this->db->query("select ap.appointment_dt,ap.appointment_id,ap.inv_id,ap.txn_id as tr_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname,c.company_id from appointment ap,master d,slave p,company_info c where c.company_id = d.company_id and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($stdate)) . "' AND '" . date('Y-m-d', strtotime($enddate)) . "' order by ap.appointment_id DESC LIMIT 200")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function getuserinfo() {
        $query = $this->db->query("select * from company_info  ")->row();
        return $query;
    }

    function Vehicles($status = '') {
        $quaery = $this->db->query("select w.workplace_id,w.uniq_identity,w.Title,w.Vehicle_Model,w.type_id,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Insurance_No,w.Vehicle_Color,vt.vehicletype,vm.vehiclemodel,wt.type_id,wt.type_name,ci.companyname FROM workplace w,vehicleType vt,vehiclemodel vm,workplace_types wt,company_info ci where vt.id=w.title and w.company = ci.company_id and vm.id=w.Vehicle_Model and wt.type_id =w.type_id  and w.status ='" . $status . "' order by w.workplace_id desc")->result();
        return $quaery;
    }

    function week_start_end_by_date($date, $format = 'Y-m-d') {

        //Is $date timestamp or date?
        if (is_numeric($date) AND strlen($date) == 10) {
            $time = $date;
        } else {
            $time = strtotime($date);
        }
        $week['week'] = date('W', $time);
        $week['year'] = date('o', $time);
        $week['year_week'] = date('oW', $time);
        $first_day_of_week_timestamp = strtotime($week['year'] . "W" . str_pad($week['week'], 2, "0", STR_PAD_LEFT));
        $week['first_day_of_week'] = date($format, $first_day_of_week_timestamp);
        $week['first_day_of_week_timestamp'] = $first_day_of_week_timestamp;
        $last_day_of_week_timestamp = strtotime($week['first_day_of_week'] . " +6 days");
        $week['last_day_of_week'] = date($format, $last_day_of_week_timestamp);
        $week['last_day_of_week_timestamp'] = $last_day_of_week_timestamp;

        return $week;
    }

    function updateDataProfile() {

        $formdataarray = $this->input->post('fdata');
        $this->db->update('company_info', $formdataarray, array('company_id' => $this->session->userdata("LoginId")));

        $this->session->set_userdata(array('profile_pic' => $formdataarray['logo'],
            'first_name' => $formdataarray['first_name'],
            'last_name' => $formdataarray['last_name']));
    }

    function updateMasterBank() {

        $stripe = new StripeModule(STRIPE_SECRET_KEY);
        $checkStripeId = $this->db->query("SELECT stripe_id from master where mas_id = " . $this->session->userdata("LoginId"))->row();
        $userData = $this->input->post('fdata');
        if ($checkStripeId->stripe_id == '') {
            $createRecipientArr = array('name' => $userData['name'], 'type' => 'individual', 'email' => $userData['email'], 'tax_id' => $userData['tax_id'], 'bank_account' => $userData['account_number'], 'routing_number' => $userData['routing_number'], 'description' => 'For ' . $userData['email']);
            $recipient = $stripe->apiStripe('createRecipient', $createRecipientArr);
        } else {
            $updateRecipientArr = array('name' => $userData['name'], 'email' => $userData['email'], 'tax_id' => $userData['tax_id'], 'bank_account' => $userData['account_number'], 'routing_number' => $userData['routing_number'], 'description' => 'For ' . $userData['email']);
            $recipient = $stripe->apiStripe('updateRecipient', $updateRecipientArr);
        }
        if (isset($recipient['error']))
            return array('flag' => 1, 'message' => $recipient['err']['error']['message'], 'data' => $userData); //, 'args' => $recipient);
        else if ($recipient['verified'] === FALSE)
            return array('flag' => 1, 'message' => "Need your full, legal name, you can check the details with the below link<br>https://support.stripe.com/questions/how-do-i-verify-transfer-recipients", 'link' => 'https://support.stripe.com/questions/how-do-i-verify-transfer-recipients', 'data' => $userData);
        else if ($recipient['verified'] === TRUE)
            return array('flag' => 0, 'message' => "Updated bank details successfully", 'data' => $userData);
    }

    function Getdashboarddata() {
        $this->load->library('mongo_db');
        $currTime = time();

        // today completed booking count
        $today = date('Y-m-d', $currTime);
//        $todayone = $this->db->query("SELECT a.appointment_id,m.doc_id FROM appointment a,doctor m  WHERE a.doc_id = m.doc_id   and a.appointment_dt like '" . date('Y-m-d') . "%' and a.status = 7 ");
        $todayone = $this->db->query("SELECT * FROM bookings  WHERE  appointment_dt like '" . date('Y-m-d') . "%' and status = 7 ");

        //this week completed booking
        $weekArr = $this->week_start_end_by_date($currTime);
        $week = $this->db->query("SELECT  * FROM bookings WHERE status = 7 and DATE(appointment_dt) >= '" . $weekArr['first_day_of_week'] . "'");


        // this month completed booking               
        $currMonth = date('n', $currTime);
        $currYear = date('Y', $currTime);
        $month = $this->db->query("SELECT * FROM bookings WHERE status = 7  and  MONTH(appointment_dt) = '" . $currMonth . "' and YEAR(appointment_dt) = '" . $currYear . "'");


        // lifetime completed booking
        $lifetime = $this->db->query("SELECT * FROM bookings WHERE status = 7");

        // total booking uptodate
        $totaluptodate = $this->db->query("SELECT bid FROM bookings");

        //today earnings
        $todayearning = $this->db->query("SELECT sum(billed_amount) as totamount FROM bookings WHERE appointment_dt  like '" . date('Y-m-d') . "%' and status = 7 ");

        //this week completed booking
        $weekearning = $this->db->query("SELECT sum(billed_amount) as totamount FROM bookings WHERE status = 7 and DATE(appointment_dt) >= '" . $weekArr['first_day_of_week'] . "'");

        // this month completed booking
        $monthearning = $this->db->query("SELECT sum(billed_amount) as totamount FROM bookings WHERE status = 7  and  MONTH(appointment_dt) = '" . $currMonth . "' ");

        // lifetime completed booking
        $lifetimeearning = $this->db->query("SELECT sum(billed_amount) as totamount FROM bookings WHERE status = 7 ");

        // total booking uptodate
        $totaluptodateearning = $this->db->query("SELECT  sum(billed_amount) as totalearning FROM bookings");


        $t = $todayearning->row();
        $w = $weekearning->row();
        $m = $monthearning->row();
        $l = $lifetimeearning->row();
        $te = $totaluptodateearning->row();


//        $data = array('today' => $todayone->num_rows(), 'week' => $week->num_rows(), 'month' => $month->num_rows(), 'lifetime' => $lifetime->num_rows(), 'total' => $totaluptodate->num_rows(),
//            'todayearning' => (float) (($t->totamount - ($t->totamount * (10 / 100)) - (float) (($t->totamount * (2.9 / 100)) + 0.3))), 'weekearning' => (float) (($w->totamount - ($w->totamount * (10 / 100)) - (float) (($w->totamount * (2.9 / 100)) + 0.3))), 'monthearning' => (float) (($m->totamount - ($m->totamount * (10 / 100)) - (float) (($m->totamount * (2.9 / 100)) + 0.3))), 'lifetimeearning' => (float) (($l->totamount - ($l->totamount * (10 / 100)) - (float) (($l->totamount * (2.9 / 100)) + 0.3))), 'totalearning' => $te->totalearning
//        );
        $data = array('today' => $todayone->num_rows(),
            'week' => $week->num_rows(),
            'month' => $month->num_rows(),
            'lifetime' => $lifetime->num_rows(),
            'total' => $totaluptodate->num_rows(),
            'todayearning' => (float) (($t->totamount - ($t->totamount * (10 / 100)) - (float) (($t->totamount * (2.9 / 100)) + 0.3))), 'weekearning' => (float) (($w->totamount - ($w->totamount * (10 / 100)) - (float) (($w->totamount * (2.9 / 100)) + 0.3))), 'monthearning' => (float) (($m->totamount - ($m->totamount * (10 / 100)) - (float) (($m->totamount * (2.9 / 100)) + 0.3))), 'lifetimeearning' => (float) (($l->totamount - ($l->totamount * (10 / 100)) - (float) (($l->totamount * (2.9 / 100)) + 0.3))), 'totalearning' => $te->totalearning
        );
        return $data;
    }

    function updateData($IdToChange = '', $databasename = '', $db_field_id_name = '') {
        $formdataarray = $this->input->post('fdata');
        $this->db->update($databasename, $formdataarray, array($db_field_id_name => $IdToChange));
    }

    function LoadAdminList() {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;
        $cursor = $collection->find(array('Role' => "SubAdmin"));
        //        $db->close();
        return $cursor;
    }

    function issessionset() {

        if ($this->session->userdata('emailid') && $this->session->userdata('password')) {

            return true;
        }
        return false;
    }

    public function dateTimes($date) {
        if ($date == '' || $date == '--')
            return "--";

        $ary = explode(" ", $date);

        $dat = explode("-", $ary[0]);

        $ts = explode(":", $ary[1]);
        $tss = (int) $ts[0];

        if ($tss > 12 && $$tss != 12) {
            $tss = $tss - 12;
            $am_pm = "PM";
        } else if ($tss == 12) {
            $am_pm = "PM";
        } else {
            $am_pm = "AM";
        }
        $date_formated = $dat[2] . "-" . $dat[1] . "-" . $dat[0] . " " . $tss . ":" . $ts[1] . " " . $am_pm;


        return $date_formated;
    }

    /*  STRIPE BANK */

    function addNewBankDetail() {
        $pro_pic = $_FILES["a_uploadautherFile"]["name"];
        $size = $_FILES['a_uploadautherFile']['size'];
        $tmp = $_FILES['a_uploadautherFile']['tmp_name'];
        $pro_pic_ext = substr($pro_pic, strrpos($pro_pic, '.') + 1);
        $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");
        $msg = false;
        $email = (rand(1000, 9999) * time());
        $bucket = BUCKET_NAME;
        $s3 = new S3(AMAZON_AWS_ACCESS_KEY, AMAZON_AWS_AUTH_SECRET);
        $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);

        if (strlen($pro_pic) > 0) {
            if (in_array($pro_pic_ext, $valid_formats)) {
                if ($size < (1024 * 1024)) {
                    $actual_image_name = "ProfileImages/" . $email . "." . $pro_pic_ext;
                    if ($s3->putObjectFile($tmp, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
                        $msg = true;
                        $s3file = 'https://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                    } else
                        $msg = false;
                } else
                    $msg = false;
            } else
                $msg = false;
        } else
            $msg = false;

        $proid = $this->input->post('a_pro_id');
        $fname = $this->input->post('a_fname');
        $lname = $this->input->post('a_lname');
        $city = $this->input->post('a_cityname');
        $address = $this->input->post('a_Address');
        $postal_code = $this->input->post('a_postalcode');
        $state = $this->input->post('a_state');
        $routing_number = $this->input->post('a_routing_number');
        $account_number = $this->input->post('a_account_number');
        $personal_id = $this->input->post('a_presnalid');
        $dob = $this->input->post('a_dob_day');
        $url = $this->serviceUrl . 'AddBankDetailsFromAdmin';
        $fields = array(
            'ent_pro_id' => $proid,
            'routing_number' => $routing_number,
            'account_number' => $account_number,
            'ent_last_name' => $lname,
            'account_holder_name' => $fname,
            'dob' => $dob,
            'personal_id' => $personal_id,
            'state' => $state,
            'postal_code' => $postal_code,
            'city' => $city,
            'address' => $address,
            'IdProof' => $s3file,
        );
//        print_r($fields);die;
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $cycleData = json_decode($result, true);
        return $cycleData;
//        print_r($cycleData);
    }

    function editdriverBank($status = '') {
        $data['masterdata'] = $this->db->query("select * from doctor where doc_id ='" . $status . "' ")->result();
        $data['masterdoc'] = $this->db->query("select * from docdetail where driverid ='" . $status . "' ")->result();

        $stripe = new StripeModuleNew();
        $this->load->library('mongo_db');
        $userType = $this->mongo_db->get_one('location', array('user' => (int) $status));

        $data['bankAccountStatus'] = 0;
        $GetUser = $stripe->apiStripe('RetriveStripConnectAccount', array('accountId' => $userType['StripeAccId']));
        if ($GetUser['transfers_enabled'] != true || $userType['StripeAccId'] == "") {
            $data['bankAccountStatus'] = 1;
            $data['bankAccountFieldSNeeded'] = $GetUser['verification']['fields_needed'];
        }

        if ($userType['StripeAccId'] != "" && ($userType['accountDetail'] == "" || empty($userType['accountDetail'])) && ( count($GetUser['external_accounts']['data']) > 0)) {
            $tokenToInsert = array();
            foreach ($GetUser['external_accounts']['data'] as $tocken) {
                $tokenToInsert[] = array('status' => 'active', 'tocken' => $tocken['id']);
            }
            if ($userType['accountDetail'] == "")
                $this->mongo_db->update('location', array('accountDetail' => $tokenToInsert), array('user' => (int) $status));
            else if (empty($userType['accountDetail']))
                $this->mongo_db->updatewithpush('location', array('accountDetail' => $tokenToInsert), array('user' => (int) $status));
            $userType = $this->mongo_db->get_one('location', array('user' => (int) $status));
        }

        $url = $this->serviceUrl . 'getMasterBankDataFromAdmin';
        $fields = array(
            'ent_pro_id' => $status
        );
//        print_r($fields);die;
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $cycleData = json_decode($result, true);
        $i = 0;
        $bank_arr = array();
//         print_r($cycleData);die;
        foreach ($cycleData['data'] as $rep) {
            $bank_arr[$i]['accountId'] = $rep['accountId'];
            $bank_arr[$i]['bank_id'] = $rep['bank_id'];
            $bank_arr[$i]['account_holder_name'] = $rep['account_holder_name'];
            $bank_arr[$i]['bank_name'] = $rep['bank_name'];
            $bank_arr[$i]['routing_number'] = $rep['routing_number'];
            $bank_arr[$i]['country'] = $rep['country'];
            $bank_arr[$i]['currency'] = $rep['currency'];
            $bank_arr[$i]['last4'] = $rep['last4'];
            $bank_arr[$i]['default_account'] = $rep['default_account'];
            $i++;
        }




        $data['StripeAccId'] = $userType['StripeAccId'];
        $data['bankData'] = $bank_arr;

        return $data;
    }

    function defaultBank() {

        $masid = $this->input->post('ent_pro_id');
        $bankId = $this->input->post('bank_token');
        $url = $this->serviceUrl . 'BankAccountDefaultFromAdmin';
        $fields = array(
            'ent_pro_id' => $masid,
            'bank_token' => $bankId
        );
//        print_r($fields);die;
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $cycleData = json_decode($result, true);
//        print_r($cycleData);die;
        return $cycleData;
    }

    function timeAcOffset($date) {
        return date('h:i a, dS M Y', (strtotime($date))); // + ($this->ci->session->userdata('offset') * 60)
    }

}

?>
