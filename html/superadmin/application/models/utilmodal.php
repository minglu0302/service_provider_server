<?php

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");

class utilmodal extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
//        $this->load->model('mastermodal');
        $this->load->database();
    }

    function get_lan_hlpText($param = '') {
        $this->load->library('mongo_db');
        if ($param == '')
            $res = $this->mongo_db->get('lang_hlp');
        else
            $res = $this->mongo_db->get_one('lang_hlp', array('lan_id' => (int) $param));
        return $res;
    }
    function checkcategorynamebycityid($cityid) {
        $cat_name = strtoupper($this->input->post('cat_name'));
        $this->load->library('mongo_db');
        $cursors = $this->mongo_db->get_where('lang_hlp', array('lan_name' => ($cityid)));
          
        foreach ($cursors as $cur) {
           $check = "true"; 
                return $check;
        }       
    }

    function lan_action() {
        $edit_id = $this->input->post('edit_id');
        $lan_name = $this->input->post('lan_name');
//        $msg = $this->input->post('lan_msg');
        $this->load->library('mongo_db');
        if ($edit_id == '') {
            $cursor = $this->mongo_db->get('lang_hlp');
            $data = array();
            $cur = $cursor->sort(array('lan_id' => -1))->limit(1);
            foreach ($cur as $res)
                $data = $res;
            if (!empty($data))
                $edit_id = $data['lan_id'] + 1;
            else
                $edit_id = 1;
            $this->mongo_db->insert('lang_hlp', array('lan_id' => $edit_id, "lan_name" => $lan_name));
            echo json_encode(array('msg' => '1', 'insert' => $edit_id));
            die;
        }else {
            $this->mongo_db->update('lang_hlp', array("lan_name" => $lan_name), array('lan_id' => (int) $edit_id));
            echo json_encode(array('msg' => '1', 'insert' => '0'));
            die;
        }
        echo json_encode(array('msg' => '0'));
    }

    function get_grp_hlpText() {
        $this->load->library('mongo_db');
        $res = $this->mongo_db->get('group_hlp');
        return $res;
    }

    function grp_action() {
        $edit_id = $this->input->post('edit_id');
        $lan_name = $this->input->post('grp_name');
        $this->load->library('mongo_db');
        if ($edit_id == '') {
            $cursor = $this->mongo_db->get('group_hlp');
            $data = array();
            $cur = $cursor->sort(array('grp_id' => -1))->limit(1);
            foreach ($cur as $res)
                $data = $res;
            if (!empty($data))
                $edit_id = $data['grp_id'] + 1;
            else
                $edit_id = 1;
            $this->mongo_db->insert('group_hlp', array("grp_name" => $lan_name, 'grp_id' => $edit_id));
            echo json_encode(array('msg' => '1', 'insert' => $edit_id));
            die;
        }else {
            $this->mongo_db->update('group_hlp', array("grp_name" => $lan_name), array('grp_id' => (int) $edit_id));
            echo json_encode(array('msg' => '1', 'insert' => '0'));
            die;
        }
        echo json_encode(array('msg' => '0'));
    }

    function get_SlaveDetails($sid = 0) {
        $data = $this->db->query('select * from slave where slave_id = "' . $sid . '"')->row();
        return $data;
    }

    function get_cat_hlpText($param = '', $param2 = '') {
        $this->load->library('mongo_db');
        if ($param == '')
            $res = $this->mongo_db->get('hlp_txt')->sort(array('cat_id' => 1));
        else {
            if ($param2 == '')
                $res = $this->mongo_db->get_one('hlp_txt', array('_id' => new MongoId($param)));
            else
                $res = $this->mongo_db->get_one('hlp_txt', array('cat_id' => (int) $param));
        }
        return $res;
    }

    function help_cat_action() {
        $edit_id = $this->input->post('edit_id');
        $cat_id = $this->input->post('cat_id');
        $this->load->library('mongo_db');

        $data = array();
        $data['name'] = $this->input->post('cat_name');
        $data['has_scat'] = $this->input->post('cat_subcat');
        if ($data['has_scat'] == false) {
            $data['desc'] = $this->input->post('desc');
            $data['cat_hform'] = $this->input->post('cat_hform');
            if ($data['cat_hform']) {
                $data['cat_hform'] = true;
                $data['zGroup'] = $this->input->post('zGroup');
                $lbl = $this->input->post('lbl');
                $dtype = $this->input->post('dtype');
                $mand = $this->input->post('Mand');
                $f_field = array();
                foreach ($lbl as $ind => $val) {
                    $str = array(
                        $val, $dtype[$ind], (($mand[$ind]) ? $mand[$ind] : 0));
                    array_push($f_field, $str);
                }
                $data['form_fields'] = $f_field;
            }
        } else {
            $data['has_scat'] = true;
            $data['sub_cat'] = array();
        }

        if ($edit_id == '') {
            if ($cat_id == '') {
                $cursor = $this->mongo_db->get('hlp_txt');
                $data1 = array();
                $cur = $cursor->sort(array('cat_id' => -1))->limit(1);
                foreach ($cur as $res)
                    $data1 = $res;
                if (!empty($data1))
                    $data['cat_id'] = $data1['cat_id'] + 1;
                else
                    $data['cat_id'] = 1;
                $this->mongo_db->insert('hlp_txt', $data);
            }else {
                unset($data['has_scat']);
                unset($data['sub_cat']);
                $data['scat_id'] = new MongoId();
                $this->mongo_db->updatewithpush('hlp_txt', array('sub_cat' => $data), array('cat_id' => (int) $cat_id));
//                print_r($data);die;
            }
        } else {
            unset($data['sub_cat']);
            $scat_id = $this->input->post('scat_id');
//            echo $scat_id;
//            print_r($data);die;
            if ($scat_id == '') {
                $this->mongo_db->update('hlp_txt', $data, array('_id' => new MongoId($edit_id)));
            } else {
                $this->mongo_db->updatewithpull('hlp_txt', array('sub_cat' => array('scat_id' => new MongoId($scat_id))), array('sub_cat.scat_id' => new MongoId($scat_id)));
                unset($data['sub_cat']);
                unset($data['has_scat']);
                $data['scat_id'] = new MongoId($scat_id);
                $this->mongo_db->updatewithpush('hlp_txt', array('sub_cat' => $data), array('_id' => new MongoId($edit_id)));
            }
//            $this->mongo_db->update('hlp_txt', array("grp_name" => $lan_name), array('grp_id' => $edit_id));
        }
    }

    function get_can_reasons() {
        $this->load->library('mongo_db');
        $res = $this->mongo_db->get('can_reason');
        return $res;
    }

    function cancell_act() {
        $edit_id = $this->input->post('edit_id');
        $reasons = $this->input->post('reasons');
        $res_for = $this->input->post('res_for');

        $this->load->library('mongo_db');
        if ($edit_id == '') {
            $cursor = $this->mongo_db->get('can_reason');
            $data = array();
            $cur = $cursor->sort(array('res_id' => -1))->limit(1);
            foreach ($cur as $res)
                $data = $res;
            if (!empty($data))
                $edit_id = $data['res_id'] + 1;
            else
                $edit_id = 1;
            $this->mongo_db->insert('can_reason', array('res_id' => $edit_id, "reasons" => $reasons, 'res_for' => $res_for));
            echo json_encode(array('msg' => '1', 'insert' => $edit_id, 'reason' => $reasons));
            die;
        }else {
            $this->mongo_db->update('can_reason', array("reasons" => $reasons, 'res_for' => $res_for), array('res_id' => (int) $edit_id));
            echo json_encode(array('msg' => '1', 'insert' => '0', 'reason' => $reasons));
            die;
        }
        echo json_encode(array('msg' => '0'));
    }

    function get_gaurantee($param = '', $param2 = '') {
        $this->load->library('mongo_db');
        if ($param2 == '')
            $res = $this->mongo_db->get_where('gaurantee', array('cycle_id' => $param));
        else
            $res = $this->mongo_db->get_one('gaurantee', array('_id' => new MongoId($param)));
        return $res;
    }

    function gaurantee_act($param = '') {
        $edit_id = $this->input->post('edit_id');
        $fdata = $this->input->post('fdata');

        $this->load->library('mongo_db');

        $arr = explode(' - ', $fdata['date']);

        $fdata['frm_date'] = strtotime($arr[0]);
        $fdata['to_date'] = strtotime($arr[1]);

        unset($fdata['date']);

        foreach ($fdata as $key => $val) {
            if ($val == '') {
                $fdata[$key] = 0;
            }
        }
        $fdata['drivers'] = (int) $fdata['drivers'];
        $fdata['currentCount'] = (int) $fdata['currentCount'];
        
        $frm_timearr = explode(":", $fdata['frm_time']);
        $frm_timearr[0] = ($frm_timearr[0] < 10)?"0".$frm_timearr[0]:$frm_timearr[0];
        $fdata['frm_time'] = implode(":", $frm_timearr);
        
        $to_timearr = explode(":", $fdata['to_time']);
        $to_timearr[0] = ($to_timearr[0] < 10)?"0".$to_timearr[0]:$to_timearr[0];
        $fdata['to_time'] = implode(":", $to_timearr);
        
        if ($edit_id == '0') {
            $this->mongo_db->insert('gaurantee', $fdata);
        } else {
            unset($fdata['currentCount']);
            unset($fdata['drivers']);
            unset($fdata['status']);
            $this->mongo_db->update('gaurantee', $fdata, array('_id' => new MongoId($edit_id)));
        }
        redirect(base_url() . "index.php/utilities/gaurantee/" . $param);
    }

    public function datatable_gaurantee_details($param = '', $param2 = '') {
        $this->load->library('Datatables');
        $this->load->library('table');

        $this->load->library('mongo_db');
        $res = $this->mongo_db->get_one('gaurantee', array('_id' => new MongoId($param)));

        $start_date = date('Y-m-d ', $res['frm_date']) . date('H:i:s', strtotime($res['frm_time']));
        $end_date = date('Y-m-d ', $res['to_date']) . date('H:i:s', strtotime($res['to_time']));

        $_POST['sColumns'] = "m.mas_id,cap_name,trips,d.acceptance_rate,act_time,avg_rate,trips,d.amount";

        $this->datatables->select('m.mas_id, CONCAT(m.first_name," ",m.last_name) as cap_name'
                        . ", d.numoftrips"
                        . ", d.acceptance_rate, round((d.active_time/60),2) as act_time, (d.average_rate*5/100) as avg_rate"
                        . ', d.masEarning'
                        . ', d.txnDate, d.amount, d.id', false)
//                ->edit_column('trips', "<a class='btn btn-success'><span>$1</span></a>", 'trips')
                ->from('DriverTxn d, master m')
                ->where('d.mas_id = m.mas_id and d.gauranteeId = "' . $param . '"');
//        $this->db->order_by("py.id", "asc");

        echo $this->datatables->generate();
    }
    
    public function datatable_driver_trips($param = '', $param2 = '') {
        $this->load->library('Datatables');
        $this->load->library('table');

        $this->load->library('mongo_db');
        $res = $this->mongo_db->get_one('gaurantee', array('_id' => new MongoId($param)));

        $start_date = date('Y-m-d ', $res['frm_date']) . date('H:i:s', strtotime($res['frm_time']));
        $end_date = date('Y-m-d ', $res['to_date']) . date('H:i:s', strtotime($res['to_time']));

        $_POST['sColumns'] = "m.mas_id,cap_name,trips,d.acceptance_rate,d.active_time,d.average_rate,trips,d.amount";

        $this->datatables->select('ap.mongoId, ap.appointment_dt'
                        . ", (case ap.payment_type  when 1 then 'card' when 2 then 'cash' END)"
                        . ", round(( case ap.amount when 0 then ap.BuildAmout else ap.amount + ap.discount END),2) as buildamount"
                        . ", ap.mas_earning "
                        . ", IFNULL((select mr.star_rating from master_ratings mr where mr.appointment_id = ap.MongoId limit 1), 0) as mas_rating", false)
                ->from('appointment ap')
                ->where('ap.mas_id in (select d.mas_id from DriverTxn d where d.id='.$param2.')'
                        . ' and ap.appointment_dt >= (select d.startTime from DriverTxn d where d.id='.$param2.' limit 1)'
                        . ' and ap.appointment_dt <= (select d.endTime from DriverTxn d where d.id='.$param2.' limit 1)');
//        $this->db->order_by("py.id", "asc");

        echo $this->datatables->generate();
    }

    function get_cat_support($param = '', $param2 = '') {
        $this->load->library('mongo_db');
        if ($param == '')
            $res = $this->mongo_db->get('support_txt')->sort(array('cat_id' => 1));
        else
        if ($param2 == '')
            $res = $this->mongo_db->get_one('support_txt', array('_id' => new MongoId($param)));
        else
            $res = $this->mongo_db->get_one('support_txt', array('cat_id' => (int) $param));
        return $res;
    }

    function support_action() {
        $edit_id = $this->input->post('edit_id');
        $cat_id = $this->input->post('cat_id');
        $this->load->library('mongo_db');

        $data = array();
        $data['name'] = $this->input->post('cat_name');
        $data['has_scat'] = $this->input->post('cat_subcat');
        if ($data['has_scat'] == false) {
            $data['desc'] = $this->input->post('desc');
//            $data['cat_hform'] = $this->input->post('cat_hform');
//            if ($data['cat_hform']) {
//                $data['cat_hform'] = true;
//                $data['zGroup'] = $this->input->post('zGroup');
//                $lbl = $this->input->post('lbl');
//                $dtype = $this->input->post('dtype');
//                $mand = $this->input->post('Mand');
//                $f_field = array();
//                foreach ($lbl as $ind => $val) {
//                    $str = array(
//                        $val, $dtype[$ind], (($mand[$ind]) ? $mand[$ind] : 0));
//                    array_push($f_field, $str);
//                }
//                $data['form_fields'] = $f_field;
//            }
        } else {
            $data['has_scat'] = true;
            $data['sub_cat'] = array();
        }

        if ($edit_id == '') {
            if ($cat_id == '') {
                $cursor = $this->mongo_db->get('support_txt');
                $data1 = array();
                $cur = $cursor->sort(array('cat_id' => -1))->limit(1);
                foreach ($cur as $res)
                    $data1 = $res;
                if (!empty($data1))
                    $data['cat_id'] = $data1['cat_id'] + 1;
                else
                    $data['cat_id'] = 1;
                $this->mongo_db->insert('support_txt', $data);
            }else {
                unset($data['has_scat']);
                unset($data['sub_cat']);
                $data['scat_id'] = new MongoId();
                $this->mongo_db->updatewithpush('support_txt', array('sub_cat' => $data), array('cat_id' => (int) $cat_id));
//                print_r($data);die;
            }
        } else {
            unset($data['sub_cat']);
            $scat_id = $this->input->post('scat_id');
//            echo $scat_id;
//            print_r($data);die;
            if ($scat_id == '') {
                $this->mongo_db->update('support_txt', $data, array('_id' => new MongoId($edit_id)));
            } else {
                $this->mongo_db->updatewithpull('support_txt', array('sub_cat' => array('scat_id' => new MongoId($scat_id))), array('sub_cat.scat_id' => new MongoId($scat_id)));
                unset($data['sub_cat']);
                unset($data['has_scat']);
                $data['scat_id'] = new MongoId($scat_id);
                $this->mongo_db->updatewithpush('support_txt', array('sub_cat' => $data), array('_id' => new MongoId($edit_id)));
            }
        }
    }

}

?>
