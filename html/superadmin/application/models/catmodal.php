<?php

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");

require_once '/var/www/html/Models/StripeModule.php';
require_once '/var/www/html/Models/config.php';
require_once '/var/www/html/Models/sendAMail.php';
//require_once '/var/www/html/test/Models/mandrill/src/Mandrill.php';
require_once('S3.php');
require 'aws.phar';
require_once 'AwsPush.php';

class Catmodal extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->database();
        $this->load->library('mongo_db');
    }

    function datatable_catogiries_services() {
        $data = array();
        $this->load->library('Datatables');
        $this->load->library('table');
        $city = $this->db->query("select a.City_Name, a.City_Id, b.Currency from city_available a, city b where a.City_Id = b.City_Id ORDER BY City_Name ASC ")->result();
        $count = 1;
        foreach ($city as $result) {
//                       
//            $con = new Mongo();
//            $db = $con->selectDB('iserve');
//            $col = $db->selectCollection('Category');
//            $no_of_city = $col->find(array('city_id' => $result->City_Id))->count();
            $no_of_city = $this->mongo_db->get_where('Category', array('city_id' => $result->City_Id))->count();
            if ($no_of_city > 0) {
                $link = "<a href='" . base_url() . "index.php/category/show_cat/" . $result->City_Id . "'><button class='btn btn-success btn-cons' style='width:20px;min-width: 41px;'>" . $no_of_city . "</button></a>";
            } else {
                $link = "<a href='#'><button class='btn btn-success btn-cons' style='width:20px;min-width: 41px;'>" . $no_of_city . "</button></a>";
            }
            $cityName = strtoupper($result->City_Name);
            $Currency = strtoupper($result->Currency);
            $data[] = array($count, $cityName, $Currency, $link);
            $count++;
        }

        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = strtoupper($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(strtoupper($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function GetServicesByCity($cityId) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderCategory', array('city_id' => (string) $cityId));
        $type = array();
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $type[] = $cur;
        }
//        return $type;
        $Mileage = $Hourly = $Fixed = array();
        for ($i = 0; $i < count($type); $i++) {
            $catid = (string) $type[$i]['_id'];
            if ($type[$i]['fee_type'] == "Mileage")
                $Mileage[] = array('cat_id' => $catid, 'cat_name' => $type[$i]['cat_name']);
            else if ($type[$i]['fee_type'] == "Hourly")
                $Hourly[] = array('cat_id' => $catid, 'cat_name' => $type[$i]['cat_name']);
            else if ($type[$i]['fee_type'] == "Fixed")
                $Fixed[] = array('cat_id' => $catid, 'cat_name' => $type[$i]['cat_name']);
        }
        return array('Mileage' => $Mileage, 'Hourly' => $Hourly, 'Fixed' => $Fixed);
    }

    function GetCategoryByCity($cityId) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderCategory', array('city_id' => (string) $cityId));
        $type = array();
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $type[] = $cur;
        }
        return $type;
    }

    function GetCurrncy($cityId) {
        $query = $this->db->query("select Currency  from city where City_Id= '" . $cityId . "'")->result();

        return $query;
    }

    function GetServicesByCategory($catId) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderSubCategory', array('cat_id' => (string) $catId));
        $slist = array();
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $slist[] = $cur;
        }
        return $slist;
    }

    function GetGroupByCategory($catId = "") {
        $this->load->library('mongo_db');
        if ($catId != "")
            $cursor = $this->mongo_db->get_where('ServiceGroup', array('cat_id' => $catId));
        else
            $cursor = $this->mongo_db->get_where('ServiceGroup', array());
        $glist = array();
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $glist[] = $cur;
        }
        return $glist;
    }

    function getcidbypro($pid) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('location', array('user' => (int) $pid));
        $catdata = array();
        foreach ($cursor as $cur) {
            $catdata[] = $cur;
        }

        return $catdata;
    }

    function getdocdetailes($pid) {

        return $this->db->query("select * from docdetail where driverid ='" . $pid . "' LIMIT 1 ")->result();
    }

    function DeleteServices() {
        $this->load->library('mongo_db');
        $ids = $this->input->post('val');
        $idar = explode(",", $ids);
        $iarrm = array();
        for ($i = 0; $i < count($idar); $i++) {
            array_push($iarrm, new MongoId($idar[$i]));
            $return[] = $this->mongo_db->updatewithpull('location', array('catlist' => array('cid' => $idar[$i])), array('catlist.cid' => $idar[$i]));
        }
        
        $b = $this->mongo_db->count_all_results('Category', array());
        $return[] = $this->mongo_db->delete('Category', array('_id' => array('$in' => $iarrm)));
        $a = $this->mongo_db->count_all_results('Category', array());
        if ($b == $a) {
            echo json_encode(array("msg" => "your selected Category not deleted,retry!", "flag" => 1));
            return;
        } else {
            echo json_encode(array("msg" => "your selected Category deleted successfully", "flag" => 0));
            return;
        }
    }

    function DeleteSubCategory() {
        $this->load->library('mongo_db');
        $ids = $this->input->post('val');
        $gid = $this->input->post('gid');
        $data = array();

        $result = $this->mongo_db->get_one('Category', array('groups.gid' => new MongoId($gid)));

        foreach ($result['groups'] as $group) {
            if ($group['gid'] == $gid) {
                foreach ($group['services'] as $service) {
                    if ((string) $service['sid'] != $ids) {
                        array_push($data, $service);
                    }
                }
            }
        }



        $this->mongo_db->update('Category', array("groups.$.services" => $data), array("groups.gid" => new MongoId($gid)));


//        $b = $this->mongo_db->count_all_results('ProviderSubCategory', array());
//        $return[] = $this->mongo_db->delete('ProviderSubCategory', array('_id' => array('$in' => $iarrm)));
//        $a = $this->mongo_db->count_all_results('ProviderSubCategory', array());
//        if ($b == $a) {
//            echo json_encode(array("msg" => "your selected sub Category not deleted,retry!", "flag" => 1));
//            return;
//        } else {
//            echo json_encode(array("msg" => "your selected sub Category deleted successfully", "flag" => 0));
//            return;
//        }
    }

    function GetOneServices($sid) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_one('Category', array('_id' => new MongoId($sid)));

//        $slist = array();
//        foreach ($cursor as $cur) {
//            $slist[] = $cur;
//        }
        return $cursor;
    }
    
    function checkcategorynamebycityid($cityid) {
        $cat_name = strtoupper($this->input->post('cat_name'));
        $this->load->library('mongo_db');
        $cursors = $this->mongo_db->get_where('Category', array('city_id' => ($cityid)));
          
        foreach ($cursors as $cur) {
             
            if($cat_name == strtoupper($cur['cat_name']))
            {
               $check = "true"; 
                return $check;
            }
           
        }
       
       
    }

    function GetOneSubServices($sid) {

        $data = array();
        $result = $this->mongo_db->get_one('Category', array('groups.services.sid' => new MongoId($sid)));

        foreach ($result['groups'] as $group) {
            foreach ($group['services'] as $service) {
                if ((string) $service['sid'] == $sid) {
                    $data['s_name'] = $service['s_name'];
                    $data['s_desc'] = $service['s_desc'];
                    $data['fixed_price'] = $service['fixed_price'];
                    $data['groupid'] = $group['gid'];
                }
            }
        }

        echo json_encode($data);
    }

    public function GetAllServices() {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderCategory', array());
        $entities = array();
        foreach ($cursor as $data) {
            $data['id'] = (string) $data['_id'];
            $entities[] = $data;
        }
        return $entities;
    }

    function GetAllServicesByCity($cityid) {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderSubCategory', array('city_id' => $cityid));
        $entities = array();
        foreach ($cursor as $data) {
            $data['id'] = (string) $data['_id'];
            $entities[] = $data;
        }
        return $entities;
    }

    public function GetAllCategories() {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderCategory', array(), array('count' => 1));
        $entities = array();
        $i = 0;
        foreach ($cursor as $data) {
            $entities[] = $data;
            $i++;
        }
        return $entities;
    }

    function get_company() {
        $query = $this->db->query("select * from company_info where Status = 3 order by companyname")->result();
        return $query;
    }

    function insert_payment($mas_id = '') {

        $currunEarnigs = $this->input->post('currunEarnigs');
        $amoutpaid = $this->input->post('paid_amount');
        $curuntdate = $this->input->post('ctime');
        $closingamt = $currunEarnigs - $amoutpaid;
        $lastAppointmentId = $this->input->post('last_unsettled_appointment_id');


        $getWhere = $this->db->get_where("doctor", array('doc_id' => $mas_id))->result_array();

        if ($getWhere[0]['stripe_id'] == '') {
            return array("error" => "Please update the account details for the driver to transfer");
        }
        $config = new config();

        $stripe = new StripeModule($config->getStripeSecretKey());

        $transfer = $stripe->apiStripe('createTransfer', array('amount' => ((int) $amoutpaid * 100), 'currency' => 'usd', 'recipient' => $getWhere[0]['stripe_id'], 'description' => 'From PRIVEMD', 'statement_description' => 'PriveMD PAYROLL'));

        if ($transfer['error']) {
            return array("error" => $transfer['error']['message']);
        }

        $query = "insert into payroll(mas_id,opening_balance,pay_date,pay_amount,closing_balance,due_amount,trasaction_id) VALUES (
        '" . $mas_id . "',
        '" . $currunEarnigs . "',
        '" . $curuntdate . "',
        '" . $amoutpaid . "',
        '" . $closingamt . "','" . $closingamt . "','" . $transfer->id . "')";
        $this->db->query($query);



        if ($this->db->insert_id() > 0) {

            $this->db->query("update appointment set settled_flag = 1 where appointment_id <= '" . $lastAppointmentId . "' and doc_id = '" . $mas_id . "' and settled_flag = 0 and status = 7 and payment_status IN (1,3)");
            if ($this->db->affected_rows() > 0) {
                return array("msg" => "Success");
            } else {
                return array("error" => "Error1");
            }
        } else {
            return array("error" => "Error2");
        }
    }

    function addcity() {
        $countryid = $this->input->post('countryid');

        $data3 = $this->input->post('data3');
        $data = $this->input->post('data');
        $existcity = '';
        $getcityname = $this->db->query("select * from city where  City_Name = '" . $data3 . "' and Country_Id='" . $countryid . "'");

        if ($getcityname->num_rows() > 0) {
            echo json_encode(array('msg' => "city already exists", 'flag' => 1));
            return;
        } else {
            $this->db->query("insert into city(Country_Id,City_Name,Currency) values('$countryid','$data3','$data')");
            if ($this->db->affected_rows() > 0) {
                echo json_encode(array('msg' => "city added successfully", 'flag' => 0));
                return;
            }
        }
    }

    function activate_company() {
        $val = $this->input->post('val');
        foreach ($val as $result) {
            $this->db->query("update company_info set status=3  where company_id='" . $result . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected company/companies activated succesfully", 'flag' => 1));
            return;
        }
    }

    function activate_vehicle() {
        $val = $this->input->post('val');
        foreach ($val as $result) {
            $this->db->query("update workplace set status=2  where workplace_id='" . $result . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected vehicle/vehicles activated succesfully", 'flag' => 1));
            return;
        }
    }

    function reject_vehicle() {
        $val = $this->input->post('val');

        foreach ($val as $result) {
            $this->db->query("update workplace set Status = 4 where workplace_id ='" . $result . "'");


            if ($this->db->affected_rows() > 0) {
                $getTokensQry = $this->db->query("select * from user_sessions where oid IN (select mas_id from master where workplace_id = '" . $result . "') and loggedIn = 1 and user_type = 1 and LENGTH(push_token) > 63")->result();
                $this->load->library('mongo_db');
                foreach ($getTokensQry as $token) {

                    if ($token->type == '2') {
                        $res [] = $this->sendAndroidPush(array($token->push_token), array('action' => 12, 'payload' => 'Your car has been suspended from admin, contact your company for more details'), 'AIzaSyBK7MVDQ-jm8GAd3BjmF0w2Z1_BjZ_qszA');
                    } else {
                        $amazon = new AwsPush();
                        $pushReturn2 = $amazon->publishJson(array(
                            'MessageStructure' => 'json',
                            'TargetArn' => $token->push_token,
                            'Message' => json_encode(array(
                                'APNS' => json_encode(array(
                                    'aps' => array('alert' => 'Rejected')
                                ))
                            )),
                        ));

                        if ($pushReturn2[0]['MessageId'] == '')
                            $ret[] = array('errorNo' => 44);
                        else
                            $ret[] = array('errorNo' => 46);
                    }

                    $query = "update appointment set status = '5',extra_notes = 'Admin rejected vehicle, so cancelled the booking',cancel_status = '8' where mas_id = '" . $token->oid . "' and status IN (6,7,8)";
                    $this->db->query($query);

                    $this->mongo_db->update('location', array("status" => 4, 'carId' => 0, 'type' => 0), array('user' => (int) $token->oid));

                    if ($token->workplace_id != 0) {
                        $this->db->query("update workplace set Status= 4   where workplace_id='" . $token->workplace_id . "'");
//                $this->mongo_db->update("location", array('carId' => 0), array('user' => (int) $token->oid));
                    }
                    $this->db->query("update user_sessions set loggedIn = 2 where oid = '" . $token->oid . "' and loggedIn = 1 and user_type = 1");
                }
            }
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected Vehicle rejected Successfully", 'flag' => 1, 'res' => $res));
            return;
        }
    }

    function acceptdrivers() {

        $val = $this->input->post('val');
        $this->load->library('mongo_db');
        $data = $this->db->query('select * from  doctor where doc_id = "' . $val . '" and status = 3');

        foreach ($val as $result) {
            $res = $this->mongo_db->update('location', array('accepted' => 1), array('user' => (int) $result));
            $this->db->query("update doctor set status = 3  where doc_id='" . $result . "' ");

            $getTokensQry = $this->db->query("select u.* from user_sessions u where u.oid IN (" . implode(',', $val) . ") and u.user_type = 1")->result();
//                foreach ($getTokensQry as $token) 
            $token = $getTokensQry[0];
            {
                if ($token->type == '2') {
                    $res [] = $this->sendAndroidPush(array($token->push_token), array('action' => 13, 'payload' => 'Your profile has been Accepted on ' . Appname . ', contact ' . Appname . ' customer care'), 'AIzaSyD38JT3At1ZrfzuSBXRvDKPpkPWz6YY3aA');
                } else {
                    $amazon = new AwsPush();
                    $pushReturn2 = $amazon->publishJson(array(
                        'MessageStructure' => 'json',
                        'TargetArn' => $token->push_token,
                        'Message' => json_encode(array(
                            'APNS' => json_encode(array(
                                'aps' => array('alert' => 'Rejected')
                            ))
                        )),
                    ));
                    if ($pushReturn2[0]['MessageId'] == '')
                        $ret[] = array('errorNo' => 44);
                    else
                        $ret[] = array('errorNo' => 46);
                }
            }
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected driver/drivers accepted succesfully", 'flag' => 1));
            return;
        }

        return $data;
    }

    function AcceptCategory() {
        $this->load->library('mongo_db');
        $pid = $this->input->post('pid');
        $catcb = $this->input->post('catcb');
        $res = $this->mongo_db->update('location', array('accepted' => 1), array('user' => (int) $pid));
        $this->db->query("update doctor set status = 3 where doc_id='" . $pid . "' ");
        print_r($res);die;
        foreach ($catcb as $cat) {
            $update = array('catlist.$.status' => 1);
            $con = array('catlist.cid' => $cat, array('user' => (int) $pid));
            $this->mongo_db->update('location', $update, $con);
        }
        if ($this->db->affected_rows() > 0) {

//            $msg = "Your profile has been Accepted on " . Appname . ", contact " . Appname . " customer care";
            $dlist = $this->db->query('select first_name,email,password from  doctor where doc_id = "' . $pid . '" and status = 3')->result();
//            $to = array($dlist[0]->email => $dlist[0]->first_name);
//            $subject = "Profile Accepted on" . Appname;
            $config = new config();
            $sendMail = new sendAMail($config->getHostUrl());
            $sendMail->masterActivated($dlist[0]->email, $dlist[0]->first_name, $dlist[0]->password);
//            $status = $sendMail->mailFun($to, $subject, $msg);

            echo json_encode(array('msg' => "your selected Provider accepted succesfully", 'flag' => 1));
            return;
        }
    }

//    function GetAllCatForProvider($proid) {
//        $this->load->library('mongo_db');
//        $cursor = $this->mongo_db->get_where('ProviderCategory', array('prolist.pid' => (int) $proid));
//        $arr = array();
//        foreach ($cursor as $data) {
//            $data['id'] = (string) $data['_id'];
//            $arr[] = $data;
//        }
//        echo json_encode($arr);
//    }

    function GetAllCatForProvider($proid) {
        $this->load->library('mongo_db');
        $pdata = $this->mongo_db->get_one('location', array('user' => (int) $proid));
        $cdata = $this->mongo_db->get_where('ProviderCategory', array());
        $catarr = array();
        foreach ($cdata as $cat) {
            $catarr[] = array('id' => (string) $cat['_id'], 'cat_name' => $cat['cat_name']);
        }
        $dataarr = array();
        foreach ($pdata['catlist'] as $ucat) {
            foreach ($catarr as $ccat) {
                if ($ccat['id'] == $ucat['cid']) {
                    $dataarr[] = array('id' => $ccat['id'], 'cat_name' => $ccat['cat_name']);
                }
            }
        }
        echo json_encode($dataarr);
    }

    function rejectdrivers() {
        $val = $this->input->post('val');

        $this->load->library('mongo_db');
        foreach ($val as $user) {
            $this->mongo_db->update('location', array('accepted' => 0), array('user' => (int) $user));
            $this->db->query("update doctor set status = '4' where doc_id IN (" . implode(',', $val) . ")");

            if ($this->db->affected_rows() > 0) {
                $this->mongo_db->update('location', array('status' => 4, 'carId' => 0, 'type' => 0), array('user' => (int) $user));

                $getTokensQry = $this->db->query("select u.* from user_sessions u where u.oid IN (" . implode(',', $val) . ") and u.loggedIn = 1 and u.user_type = 1")->result();
                foreach ($getTokensQry as $token) {
                    if ($token->type == '2') {
                        $res [] = $this->sendAndroidPush(array($token->push_token), array('action' => 12, 'payload' => 'Your profile has been suspended on ' . Appname . ', contact ' . Appname . ' customer care'), 'AIzaSyD38JT3At1ZrfzuSBXRvDKPpkPWz6YY3aA');
                    } else {
                        $amazon = new AwsPush();
                        $pushReturn2 = $amazon->publishJson(array(
                            'MessageStructure' => 'json',
                            'TargetArn' => $token->push_token,
                            'Message' => json_encode(array(
                                'APNS' => json_encode(array(
                                    'aps' => array('alert' => 'Rejected')
                                ))
                            )),
                        ));

                        if ($pushReturn2[0]['MessageId'] == '')
                            $ret[] = array('errorNo' => 44);
                        else
                            $ret[] = array('errorNo' => 46);
                    }
                }

                $this->db->query("update user_sessions set loggedIn = 2 where oid = '" . $user . "' and loggedIn = 1 and user_type = 1");
                $dlist = $this->db->query('select first_name,email from  doctor where doc_id = "' . $user . '"')->result();
//                $to = array($dlist[0]->email => $dlist[0]->first_name);
//                $subject = 'Profile Rejected on ' . Appname;
//                $msg = 'Your profile has been suspended on ' . Appname . ', contact ' . Appname . ' customer care';
                $config = new config();
                $sendMail = new sendAMail($config->getHostUrl());
                $status = $sendMail->masterSuspended($dlist[0]->email, $dlist[0]->first_name);
//                $status = $sendMail->mailFun($to, $subject, $msg);
            }
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected driver/drivers rejected Successfully", 'flag' => 1, 'res' => $res));
            return;
        }
    }

    function sendAndroidPush($tokenArr, $andrContent, $apiKey) {
        $fields = array(
            'registration_ids' => $tokenArr,
            'data' => $andrContent,
        );

        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://android.googleapis.com/gcm/send');

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        curl_close($ch);
        $res_dec = json_decode($result);

        if ($res_dec->success >= 1)
            return array('errorNo' => 44, 'result' => $result);
        else
            return array('errorNo' => 46, 'result' => $result);
    }

    function editdriverpassword() {
        $newpass = $this->input->post('newpass');
        $val = $this->input->post('val');
        $pass = $this->db->query("select first_name, email,password from doctor where doc_id='" . $val . "' ")->result();
        if ($pass[0]->password == md5($newpass)) {
            echo json_encode(array('msg' => "this password already exists. Enter new password", 'flag' => 1));
            return;
        } else {
            $this->db->query("update doctor set password = md5('" . $newpass . "') where doc_id = '" . $val . "' ");
            if ($this->db->affected_rows() > 0) {
                $config = new config();
                $details1 = array('first_name' => $pass[0]->first_name, 'email' => $pass[0]->email);
                $sendMail = new sendAMail($config->getHostUrl());
                $data = $sendMail->passwordChanged($details1, $newpass);
                echo json_encode(array('msg' => "your new password updated successfully", 'flag' => 0));
                return;
            }
        }
    }

    function editsuperpassword() {
        $newpass = $this->input->post('newpass');
        $currentpassword = $this->input->post('currentpassword');

        $pass = $this->db->query("select password from superadmin where id=1")->result();


        if (md5($currentpassword) != $pass['password']) {
            echo json_encode(array('msg' => "you have entered the incorrect current password", 'flag' => 2));
            return;
        } else {

            if ($pass['password'] == md5($newpass)) {
                echo json_encode(array('msg' => "this password already exists. Enter new password", 'flag' => 1));
                return;
            } else {
                $this->db->query("update superadmin set password = md5('" . $newpass . "') where id = 1 ");

                if ($this->db->affected_rows() > 0) {
                    echo json_encode(array('msg' => "your new password updated successfully", 'flag' => 0));
                    return;
                }
            }
        }
    }

    public function GetProductDetails($param = '') {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_one('MasterProducts', array('_id' => new MongoId($param)));
        return $cursor;
    }

    public function GetAllProducts($BizId = '') {
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('MasterProducts', array(), array('count' => 1));

        $entities = array();
        $i = 0;

        foreach ($cursor as $data) {
            $entities[] = $data;
            $i++;
        }
        return $entities;
    }

    function get_product_count($bizid = '') {

        $this->load->library('mongo_db');
        $array = $this->mongo_db->get_where('MasterProducts', array());
        $count = 0;
        foreach ($array as $total) {
            $count++;
        }
        $count++;
        return $count;
    }

//    function EditCategory()
//    {
//          $this->load->library('mongo_db');
//        $CategoryId = $this->input->post("CategoryId");        
//        $cid = $this->input->post("cid");
//        $cityList = $this->db->query("select * from city_available where City_Id = '". $cid  ."'")->result();
//        $loc['latitude'] = (double)$cityList[0]->City_Lat;
//        $loc['longitude'] = (double)$cityList[0]->City_Long;
//        if ($CategoryId != '') {
//            $cid = (string)$this->input->post("cid");
//            $scname =(string)$this->input->post("scname");
//            $scdesc = (string)$this->input->post("scdesc");
//            $min_fees = (string)$this->input->post("min_fees");
//            $base_fees = (string)$this->input->post("base_fees");
//            $pr_min = (string)$this->input->post("pr_min");
//            $sp = (string)$this->input->post("sp");
//              $sdata = array('city_id' => $cid, 'cat_name' => $scname,
//                'cat_desc' => $scdesc , 'min_fees' => $min_fees , 'base_fees' => $base_fees, 
//                'price_min' => $pr_min , 'start_price'=> $sp);
//            $this->mongo_db->update('ProviderCategory', $sdata, array("_id" => new MongoId($CategoryId)));
//        }
//    }
    function changeCatOrder() {

        $this->load->library('mongo_db');

        $Curruntcountval = $this->mongo_db->get_one('Category', array('_id' => new MongoId($this->input->post("curr_id"))));
        $Prevecountval = $this->mongo_db->get_one('Category', array('_id' => new MongoId($this->input->post("prev_id"))));

        $currcount = $Curruntcountval['num'];
        $prevcount = $Prevecountval['num'];
//        print_r($prevcount);
//        print_r($currcount);
//        exit();

        $this->mongo_db->update('Category', array('num' => $prevcount), array("_id" => new MongoId($this->input->post("curr_id"))));
        $this->mongo_db->update('Category', array('num' => $currcount), array("_id" => new MongoId($this->input->post("prev_id"))));
    }
    function AddNewCategory() {

        $this->load->library('mongo_db');
        $unsel_img_name = "";
        $sel_img_name = "";
        $sel_img = $_FILES["sel_img"]["name"];
        $unsel_img = $_FILES["unsel_img"]["name"];
        $banner_img = $_FILES['banner_img']['name'];
        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/pics/';
        $data = array();
        $cur = $this->mongo_db->get('Category')->sort(array('num' => -1))->limit(1);
        foreach ($cur as $res)
            $data = $res;
        
        if (!empty($data))
            $no_of_city = $data['num'] + 1;
        else
            $no_of_city = 1;
        if ($banner_img != "") {
            $ext = substr($banner_img, strrpos($banner_img, '.') + 1);
            $banner_img_name = (rand(1000, 9999) * time()) . '.' . $ext;
            try {
                if (move_uploaded_file($_FILES['banner_img']['tmp_name'], $documentfolder . $banner_img_name)) {
                    echo "22222";
                    $this->uploadimage_diffrent_redulation($documentfolder . $banner_img_name, $banner_img_name, $_SERVER['DOCUMENT_ROOT'] . '/', $ext);
                    echo "1111";
                }
            } catch (Exception $ex) {
                print_r($ex);
                return false;
            }
        }
        if ($sel_img != "") {
            $ext1 = substr($sel_img, strrpos($sel_img, '.') + 1);
            $sel_img_name = (rand(1000, 9999) * time()) . '.' . $ext1;
            try {
                if (move_uploaded_file($_FILES['sel_img']['tmp_name'], $documentfolder . $sel_img_name)) {
                    $this->uploadimage_diffrent_redulation($documentfolder . $sel_img_name, $sel_img_name, $_SERVER['DOCUMENT_ROOT'] . '/', $ext1);
                }
            } catch (Exception $ex1) {
                print_r($ex1);
                return false;
            }
        }
        if ($unsel_img != "") {
            $ext2 = substr($unsel_img, strrpos($unsel_img, '.') + 1);
            $unsel_img_name = (rand(1000, 9999) * time()) . '.' . $ext2;
            try {
                if (move_uploaded_file($_FILES['unsel_img']['tmp_name'], $documentfolder . $unsel_img_name)) {
                    $this->uploadimage_diffrent_redulation($documentfolder . $unsel_img_name, $unsel_img_name, $_SERVER['DOCUMENT_ROOT'] . '/', $ext2);
                }
            } catch (Exception $ex2) {
                print_r($ex2);
                return false;
            }
        }

        $fdata = $this->input->post("fdata");

        $CategoryId = $this->input->post("CategoryId");
        $city_id = $this->input->post("city_id");
        $min_fees = $this->input->post("min_fees");
        $base_fees = $this->input->post("base_fees");
        $cat_desc = $this->input->post("cat_desc");
        $cat_name = $this->input->post("cat_name");
        $price_min = $this->input->post("price_min");
        $fee_type = $this->input->post("fee_type");
        $fixed_price = $this->input->post("fixed_price");

        $can_condition = $this->input->post('can_condition');
        $pay_commision = $this->input->post("pay_commision");
        $price_set_by = $this->input->post("price_set_by");
        $can_fees = $this->input->post("can_fees");
        $visit_fees = $this->input->post("visit_fees");
        $price_mile = $this->input->post("price_mile");


        $cityList = $this->db->query("select * from city_available where City_Id = '" . $city_id . "'")->result();
        $loc['longitude'] = (double) $cityList[0]->City_Long;
        $loc['latitude'] = (double) $cityList[0]->City_Lat;


        if ($fee_type == "Fixed") {
            $base_fees = "";
            $min_fees = "";
            $price_min = "";
            $price_mile = "";
            $visit_fees = "";
        }
        if ($fee_type == "Hourly") {
            $base_fees = "";
            $min_fees = "";
            $fixed_price = "";
            $price_mile = "";
        }
        if ($fee_type == "Mileage") {
            $fixed_price = "";
            $visit_fees = "";
        }


        $sdata['city_id'] = $city_id;
        $sdata['cat_name'] = $cat_name;
        $sdata['cat_desc'] = $cat_desc;
        $sdata['min_fees'] = $min_fees;
        $sdata['base_fees'] = $base_fees;
        $sdata['price_min'] = $price_min;
        $sdata['fee_type'] = $fee_type;
        $sdata['fixed_price'] = $fixed_price;
        $sdata['pay_commision'] = $pay_commision;
        $sdata['price_set_by'] = $price_set_by;
        $sdata['can_fees'] = $can_fees;
        $sdata['visit_fees'] = $visit_fees;
        $sdata['price_mile'] = $price_mile;
        $sdata['can_condition'] = $can_condition;

        if ($sel_img_name != '') {
            $sdata['sel_img'] = $sel_img_name;
        }
        if ($unsel_img_name != '') {
            $sdata['unsel_img'] = $unsel_img_name;
        }
        if ($banner_img_name != '') {
            $sdata['banner_img'] = $banner_img_name;
        }

        $sdata['location'] = $loc;
        if ($CategoryId != '') {
            $this->mongo_db->update('Category', $sdata, array("_id" => new MongoId($CategoryId)));
            if ($price_set_by == "Admin") {
                    $this->mongo_db->update('location', array('catlist.$.amount' => $price_min), array("catlist.cid" => $CategoryId));               
            }
        } else {
             $sdata['num'] = $no_of_city;
            $res = $this->mongo_db->insert('Category', $sdata);
        }
    }

    function AddService() {

        $this->load->library('mongo_db');
        $gid = $this->input->post("gid");
        $scname = $this->input->post("s_name");
        $scdesc = $this->input->post("s_desc");
        $fp = $this->input->post("fp");


        $sdata = array('s_name' => $scname,
            's_desc' => $scdesc, 'sid' => new MongoId(),
            'fixed_price' => $fp);


        $this->mongo_db->updatewithpush('Category', array("groups.$.services" => $sdata), array("groups.gid" => new MongoId($gid)));
    }

    function EditServiceby() {

        $this->load->library('mongo_db');
        $gid = $this->input->post("gid");
        $grouplist = $this->input->post("grouplist");
        $scname = $this->input->post("s_name");
        $scdesc = $this->input->post("s_desc");
        $fp = $this->input->post("fp");
        $edits = $this->input->post("edits");
        $data = array();
        $service = array();
        $result = $this->mongo_db->get_one('Category', array('groups.gid' => new MongoId($edits)));

        foreach ($result['groups'] as $group) {
            if ((string) $group['gid'] == $edits) {
                foreach ($group['services'] as $service) {
                    if ((string) $service['sid'] != $gid) {
//                    $service = array('s_name' => $scname,
//                            's_desc' => $scdesc, 'sid' => new MongoId(),
//                            'fixed_price' => $fp);
                        array_push($data, $service);
                    }
                }
            }
        }
        $this->mongo_db->update('Category', array("groups.$.services" => $data), array("groups.gid" => new MongoId($edits)));

        $sdata = array('s_name' => $scname,
            's_desc' => $scdesc, 'sid' => new MongoId(),
            'fixed_price' => $fp);


        $this->mongo_db->updatewithpush('Category', array("groups.$.services" => $sdata), array("groups.gid" => new MongoId($grouplist)));

//       
//         
    }

    function AddnewProduct() {

        $this->load->library('mongo_db');
        $ProductId = $this->input->post("ProductId");
        if ($ProductId != '') {
            $this->mongo_db->update('MasterProducts', $this->input->post("FData"), array("_id" => new MongoId($ProductId)));
        } else {
            $this->mongo_db->insert('MasterProducts', $this->input->post("FData"));
        }
    }

    function editvehicle($status) {

        $data['vehicle'] = $this->db->query("select w.*,wt.city_id,v.id,v.vehiclemodel from  workplace w ,workplace_types wt,vehiclemodel v where workplace_id='" . $status . "' and w.type_id = wt.type_id and v.id = w.Vehicle_Model ")->result();

        $cityId = $data['vehicle'][0]->city_id;

        if ($cityId == '')
            return array('flag' => 1);

        $data['company'] = $this->db->query("select companyname,company_id from company_info where city = '" . $cityId . "'")->result();
        $data['cityList'] = $this->db->query("select City_Name,City_Id from city_available")->result();
        $data['workplaceTypes'] = $this->db->query("select * from workplace_types where city_id = '" . $cityId . "'")->result();
        $data['vehicleTypes'] = $this->db->query("select * from vehicleType")->result();
        $data['vehicleDoc'] = $this->db->query("select * from vechiledoc where vechileid = '" . $status . "'")->result();
        return $data;
    }

    function deactivate_company() {
        $val = $this->input->post('val');
        foreach ($val as $result) {
            $this->db->query("update company_info set status=5  where company_id='" . $result . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected company/companies deactivated succesfully", 'flag' => 1));
            return;
        }
    }

    function datatable_commission() {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        if ($this->input->post('sSearch') != '') {
            $cond = array('name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
        } else {
            $cond = array("accepted" => 1);
        }
        $num = 0;
        $data = array();
        $MAsterData = $this->mongo_db->get_where('location', $cond);
        foreach ($MAsterData as $row) {
            $num++;
            $comm = 10;
            if ($row['Commission'] == "") {
                $this->mongo_db->update('location', array("Commission" => $comm), array("_id" => new MongoId($row['_id'])));
            } else {
                $this->mongo_db->update('location', array("Commission" => $row['Commission']), array("_id" => new MongoId($row['_id'])));
            }
            $data[] = array($num, $row['user'], $row['name'], $row['email'], ($row['Commission'] == "") ? "$comm" : $row['Commission'], "<input type='checkbox' class='checkbox' data='" . $row['Commission'] . "' value='" . $row['_id'] . "'>");
        }
        echo $this->datatables->commission_Data($data);
    }

    function datatable_sgroup($parms) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $catId = $parms;

        $cursor = $this->mongo_db->get_one('Category', array('_id' => new MongoId($catId)));



        $num = 1;
        $data = array();

        if (isset($cursor['groups'])) {
            foreach ($cursor['groups'] as $groups) {
                if (isset($groups['services'])) {
                    $service_count = count($groups['services']);
                    $link = "<a href='" . base_url() . "index.php/category/show_services/" . $groups['gid'] . "'><button class='btn btn-success btn-cons' style='width:20px;min-width: 41px;'>" . $service_count . "</button></a>";
                    $data[] = array($num++, $groups['group_name'], $link, " <div class='checkbox check-primary'><input type='checkbox' id='checkbox2" . $groups['gid'] . "' class='checkbox' data='" . $groups['gid'] . "' value='" . $groups['gid'] . "'><label for='checkbox2" . $groups['gid'] . "'>Mark</label></div>");
                }
            }
        }


        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = strtoupper($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(strtoupper($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);

//        echo $this->datatables->commission_Data($data1);
    }

    function datatable_pgateway() {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $cond = array();
        $num = 1;
        $data = array();
        $PaymentData = $this->mongo_db->get_where('pgateway', $cond);
        foreach ($PaymentData as $row) {
            $data[] = array($num++, $row['name'], $row['percent'], $row['fixed'], "<input type='checkbox' class='checkbox' data='" . $row['_id'] . "' value='" . $row['_id'] . "'>");
        }
        if ($this->input->post('sSearch') != '') {
            $FilterArr = array();
            foreach ($data as $row) {
                $needle = strtoupper($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(strtoupper($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }
        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function datatable_pro_rating($proid = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select("dr.appointment_id as rahul,dr.patient_id,dr.star_rating from doctor_ratings dr", FALSE)
                ->where('dr.doc_id', $proid);
        $this->db->order_by("rahul", "desc");

        echo $this->datatables->generate();
    }

    function datatable_cservices() {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $citylist = $this->get_cities();
//        if ($this->input->post('sSearch') != '') {
//            $cond = array('name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
//        } else {
//            $cond = array();
//        }
        $num = 1;
        $data = array();
        foreach ($citylist as $city) {
            $count = $this->mongo_db->count_all_results(('ProviderSubCategory'), array('city_id' => $city->City_Id));
            $link = "<a href='" . base_url() . "index.php/superadmin/show_services/" . $city->City_Id . "'>" . $count . "</a>";
            $data[] = array($num++, $city->City_Name, $link);
        }
        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = strtoupper($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(strtoupper($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function datatable_show_services($parms) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $gid = $parms;

        $cursor = $this->mongo_db->get_one('Category', array('groups.gid' => new MongoId($gid)));



        $num = 1;
        $data = array();


        foreach ($cursor['groups'] as $groups) {

            if ((string) $groups['gid'] == $gid) {

                foreach ($groups['services'] as $service) {
                    $data[] = array($num++, $service['s_name'], $service['s_desc'], CURRENCY.' ' .$service['fixed_price'], " <div class='checkbox check-primary'><input type='checkbox' id='checkbox2" . $service['sid'] . "' class='checkbox' data='" . $service['sid'] . "' value='" . $service['sid'] . "'><label for='checkbox2" . $service['sid'] . "'>Mark</label></div>");
                }
            }
        }



        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = strtoupper($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(strtoupper($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function group_sort_ajax($cityid, $groupid) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        $cursor = $this->mongo_db->get_where('ProviderSubCategory', array('city_id' => $cityid, 'group_id' => $groupid));
        $data = array();
        $num = 1;
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $gcur = $this->mongo_db->get_one('ServiceGroup', array('_id' => new MongoId($cur['group_id'])), array('group_name' => 1));
            $gname = $gcur['group_name'];
            $data[] = array($num++, $gname, $cur['s_name'], $cur['s_desc'], $cur['fixed_price'], "<input type='checkbox' class='checkbox' data='" . $cur['id'] . "' value='" . $cur['id'] . "'>");
        }
        echo $this->datatables->commission_Data($data);
    }

    function datatable_show_cat($cityid) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        
        $cursor = $this->mongo_db->get_where('Category', array('city_id' => (string) $cityid))->sort(array('num'=>1));

        $data = array();
        $num = 1;
        foreach ($cursor as $cur) {
            
            $group_count = 0;
            if (isset($cur['groups']))
                $group_count = count($cur['groups']);
            $cur['id'] = (string) $cur['_id'];
            $bimg = '<img src="' . base_url("../pics/user.jpg") . '" style=" border-radius: 50%;" height="34" width="34">';
            $simg = '<img src="' . base_url("../pics/user.jpg") . '" style=" border-radius: 50%;" height="34" width="34">';
            $uimg = '<img src="' . base_url("../pics/user.jpg") . '" style=" border-radius: 50%;" height="34" width="34">';
            $imgs = array(
                'bimg' => '',
                'simg' => '',
                'usimg' => ''
            );
            if (isset($cur['banner_img']) && $cur['banner_img'] != "") {
                $bimg = '<img src="' . base_url("../pics/" . $cur["banner_img"]) . '" style=" border-radius: 50%;" height="34" width="34">';
                $imgs['bimg'] = $cur['banner_img'];
            }
            if (isset($cur['sel_img']) && $cur['sel_img'] != "") {
                $simg = '<img src="' . base_url("../pics/" . $cur["sel_img"]) . '" style=" border-radius: 50%;" height="34" width="34">';
                $imgs['simg'] = $cur['sel_img'];
            }
            if (isset($cur['unsel_img']) && $cur['unsel_img'] != "") {
                $uimg = '<img src="' . base_url("../pics/" . $cur["unsel_img"]) . '" style=" border-radius: 50%;" height="34" width="34">';
                $imgs['usimg'] = $cur['unsel_img'];
            }
            $uimg = '<a onclick="documents(this)" data-val=\'' . json_encode($imgs) . '\' data-id="' . $cur['id'] . '"><button class="btn btn-success btn-cons" style="width:40px;"> <i class="fa fa-eye"></i> Image</button></a>';




            $min_fees = "N/A";
            $base_fees = "N/A";
            $price_min = "N/A";
            $price_mile = "N/A";
            $visit_fees = "N/A";
            $fixed_price = "N/A";
            if ($cur['min_fees'] != "")
                $min_fees = $cur['min_fees'];
            if ($cur['base_fees'] != "")
                $base_fees = $cur['base_fees'];
            if ($cur['price_min'] != "")
                $price_min = $cur['price_min'];
            if ($cur['price_mile'] != "")
                $price_mile = $cur['price_mile'];
            if ($cur['visit_fees'] != "")
                $visit_fees = $cur['visit_fees'];
            if ($cur['fixed_price'] != "")
                $fixed_price = $cur['fixed_price'];

            $catFees = array(
                'price_set_by' => $cur['price_set_by'],
                'can_fees' => $cur['can_fees'],
                'min_fees' => $min_fees,
                'base_fees' => $base_fees,
                'price_min' => $price_min,
                'price_mile' => $price_mile,
                'visit_fees' => $visit_fees,
                'fixed_price' => $fixed_price,
            );
//              $cur['fee_type'], $cur['can_fees'], $min_fees, $base_fees, $price_min, $price_mile, $visit_fees, $fixed_price,
            $fee_types = '<a onclick="feeType(this)" data-val=\'' . json_encode($catFees) . '\' data-id="' . $cur['id'] . '"><button class="btn btn-success btn-cons" style="width:40px"><i class="fa fa-eye"></i> ' . $cur['fee_type'] . '</button></a>';
            $link = "<a href='" . base_url() . "index.php/category/sgroup/" . $cur['id'] . "'><button class='btn btn-success btn-cons' style='width:20px;min-width: 41px;'>" . $group_count . "</button></a>";

            $data[] = array($cur['num'], $cur['cat_name'], $cur['cat_desc'], $uimg, $fee_types, $link,
                '<a class="moveDown btn-padding" id=' . $cur['id'] . ' ><button id="' . $cur['id'] . '" onclick="moveDown(this)" type="button" style="color: #ffffff !important;background-color: #0090d9;border: 1px solid #0090d9;" class="btn btn-success"><i class="fa fa-arrow-down"></i> </button></a>
                    <a class="moveUp btn-padding" id=' . $cur['id'] . '><button id="' . $cur['id'] . '" onclick="moveUp(this)" type="button" style="color: #ffffff !important;background-color: #0090d9;border: 1px solid #0090d9;" class="btn btn-success"><i class="fa fa-arrow-up"></i></button></a>',
                
                "<div class='checkbox check-primary'><input type='checkbox' id='checkbox2" . $cur['id'] . "' class='checkbox' data='" . $cur['id'] . "' value='" . $cur['id'] . "'><label for='checkbox2" . $cur['id'] . "'>Mark</label></div>");
        }
 
        if ($this->input->post('sSearch') != '') {

            $FilterArr = array();
            foreach ($data as $row) {
                $needle = strtoupper($this->input->post('sSearch'));
                $ret = array_keys(array_filter($row, function($var) use ($needle) {
                            return strpos(strtoupper($var), $needle) !== false;
                        }));
                if (!empty($ret)) {
                    $FilterArr [] = $row;
                }
            }
            echo $this->datatables->commission_Data($FilterArr);
        }


        if ($this->input->post('sSearch') == '')
            echo $this->datatables->commission_Data($data);
    }

    function insertcommission($status = '') {
        $providername = $this->input->post('providername');
        $commission = $this->input->post('commission');
        $this->load->library('mongo_db');
        $this->mongo_db->update('location', array("Commission" => $commission), array("_id" => new MongoId($providername)));
    }

    function AddServiceGroup() {

        $cid = $this->input->post('cid');
        $group_name = $this->input->post('group_name');
        $cmand = $this->input->post('cmand');
        $cmult = $this->input->post('cmult');
        $service = array();
        $this->mongo_db->updatewithpush('Category', array("groups" => array('gid' => new MongoId(), 'group_name' => $group_name,
                'cmand' => $cmand, 'cmult' => $cmult, 'services' => $service)), array("_id" => new MongoId($cid)));
    }

    function AddPaymentGateway() {
        $name = $this->input->post('name');
        $percent = $this->input->post('percent');
        $fixed = $this->input->post('fixed');
        $this->mongo_db->insert('pgateway', array('name' => $name, 'percent' => $percent, 'fixed' => $fixed));
    }

    function EditServiceGroup() {

        $egid = $this->input->post('gid');
        $ecatlist = $this->input->post('ecatlist');

        $group_name = $this->input->post('group_name');
        $cmand = $this->input->post('cmand');
        $cmult = $this->input->post('cmult');
        $res = $this->mongo_db->get_one('Category', array("groups.gid" => new MongoId($egid)));
        $group = array();
        foreach ($res['groups'] as $grup) {
            if ((string) $grup['gid'] == $egid) {

                $group = array("groups" => array('gid' => new MongoId(), 'group_name' => $group_name,
                        'cmand' => $cmand, 'cmult' => $cmult,'services'=>$grup['services']));
            }
        }

        $this->mongo_db->updatewithpull('Category', array('groups' => array('gid' => new MongoId($egid))), array('groups.gid' => new MongoId($egid)));

        $this->mongo_db->updatewithpush('Category', $group, array("_id" => new MongoId($ecatlist)));

//        $this->mongo_db->update('Category', array('groups.$.group_name' => $group_name, 
//            'groups.$.cmand' => $cmand, 'groups.$.cmult' => $cmult), array("groups.gid" => new MongoId($egid)));
    }

    

    function EditGetServiceGroup() {
        $gid = $this->input->post('egid');
        $data = array();
        $result = $this->mongo_db->get_one('Category', array('groups.gid' => new MongoId($gid)));
        foreach ($result['groups'] as $group) {
            if ((string) $group['gid'] == $gid) {
                $data['group_name'] = $group['group_name'];
                $data['cmand'] = $group['cmand'];
                $data['cmult'] = $group['cmult'];
            }
        }
        echo json_encode($data);
    }

    function get_citiesname($parms) {
        $gid = $parms;

        $result = $this->mongo_db->get_one('Category', array('_id' => new MongoId($gid)));

        return $result;
    }

    function get_citiesnamebygroup($parms) {
        $gid = $parms;

        $result = $this->mongo_db->get_one('Category', array('groups.gid' => new MongoId($gid)));

        return $result;
    }

    function get_groupName($parms) {
        $gid = $parms;
        $data = array();
        $result = $this->mongo_db->get_one('Category', array('groups.gid' => new MongoId($gid)));
        foreach ($result['groups'] as $groups) {
            if ($groups['gid'] == $gid) {
                $data['groupsname'] = $groups['group_name'];
            }
        }
        $data['catid'] = $result['_id'];
        return $data;
    }

    function EditGetPaymentGateWay() {
        $payid = $this->input->post('payid');

        $result = $this->mongo_db->get_one('pgateway', array('_id' => new MongoId($payid)));
        $result['_id'] = (string) $result['_id'];
        echo json_encode($result);
    }

    function DeleteServiceGroup() {
        $this->load->library('mongo_db');
        $val1 = $this->input->post('val');

        foreach ($val1 as $row1) {
            $this->mongo_db->updatewithpull('Category', array('groups' => array('gid' => new MongoId($row1))), array('groups.gid' => new MongoId($row1)));
        }
    }

    function DeletePaymentGateway() {
        $this->load->library('mongo_db');
        $val1 = $this->input->post('val');
        foreach ($val1 as $row1) {
            $this->mongo_db->delete('pgateway', array("_id" => new MongoId($row1)));
        }
    }

    function editcommission($status = '') {
        $id = $this->input->post('id');
        $commission = $this->input->post('commission');
        $this->load->library('mongo_db');
        $this->mongo_db->update('location', array("Commission" => $commission), array("_id" => new MongoId($id)));
    }

    function deletecommission() {
        $this->load->library('mongo_db');
        $val1 = $this->input->post('val');
        foreach ($val1 as $row1) {
            $this->mongo_db->updateWithUnset('location', array("_id" => new MongoId($row1)));
        }
    }

    function GetRechargedata_ajax() {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->datatables->select("m.last_name,m.doc_id,m.first_name,ROUND(((select sum(RechargeAmount) from DriverRecharge where m.doc_id = mas_id) - (select sum(app_owner_pl) from appointment where status = 7  and doc_id = m.doc_id)),2),(select RechargeDate from DriverRecharge where m.doc_id = mas_id order by id desc limit 1)", false)
                ->edit_column('m.last_name', 'counter/$1', 'm.doc_id')
                ->add_column('OPERATION', '<a href="' . base_url("index.php/superadmin/ProRechargeStatement/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">STATEMENT</button></a>
<a href="' . base_url("index.php/superadmin/Recharge/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">RECHARGE</button>', 'm.doc_id')
                ->from('doctor m');

        echo $this->datatables->generate();
    }

    function makeonline() {
        $masterid = $this->input->post('masterid');
        $this->load->library('mongo_db');
        foreach ($masterid as $row) {
            $res = $this->mongo_db->update('location', array('status' => (int) 3), array('user' => (int) $row));
        }
        echo json_encode(array('flag' => 0, 'msg' => 'Status Updated'));
    }

    function makeOffline() {
        $masterid = $this->input->post('masterid');
        $this->load->library('mongo_db');
        foreach ($masterid as $row) {
            $res = $this->mongo_db->update('location', array('status' => (int) 4), array('user' => (int) $row));
        }
        echo json_encode(array('flag' => 0, 'msg' => 'Status Updated'));
    }

    function makeLogout() {
        $masterid = $this->input->post('masterid');
        $this->load->library('mongo_db');
        foreach ($masterid as $row) {
            $this->db->query("update user_sessions set loggedIn = '2' where oid = '" . $row . "' and user_type = '1'");
            $res = $this->mongo_db->update('location', array('status' => 4, 'login' => 2, 'online' => 0), array('user' => (int) $row));
        }
        echo json_encode(array('flag' => 0, 'msg' => 'Status Updated'));
    }

    function GetProDetils($id) {

        $mas = $this->db->query("select * from doctor where doc_id = '" . $id . "'")->row();
        return $mas;
    }

    function ProRechargeDetails($id) {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->datatables->select("id,RechargeAmount,DATE_FORMAT(RechargeDate, '%b %d %Y %h:%i %p') as rdat,mas_id", false)
                ->add_column('OPERATION', '<button class="btn btn-success btn-cons-onclick" style="min-width: 83px !important;" id="$1">EDIT</button>
    <a href="' . base_url("index.php/superadmin/RechargeOperation/2/$1/$2") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">DELETE</button>', 'id,mas_id')
                ->unset_column('mas_id')
                ->from('DriverRecharge')
                ->where('mas_id', $id);

        echo $this->datatables->generate();
    }

  
    function get_vehicle_data() {
        $query = $this->db->query("select w.*,cty.City_Name from workplace_types w, city_available cty where w.city_id = cty.City_Id")->result();

        return $query;
    }

    function logoutdriver() {
        $driverid = $this->input->post('driverid');
        $this->load->library('mongo_db');
        $this->db->query("update user_sessions  set loggedIn = 2 where user_type = '1' and oid = '" . $driverid . "' and loggedIn = 1");

        $this->mongo_db->update('location', array('status' => 4), array('user' => (int) $driverid));
    }

     

    function get_vehiclemake() {
        return $this->db->query("select * from vehicleType")->result();
    }

    function get_vehiclemodal() {
        return $this->db->query("select vm.*,vt.vehicletype from vehiclemodel vm,vehicleType vt where vm.vehicletypeid= vt.id")->result();
    }

    function vehiclemodal() {
        return $this->db->query("select *  from vehiclemodel order by vehiclemodel")->result();
    }

    function insert_typename() {
        $typename = $this->input->post('typename');
        $result = $this->db->query("insert into vehicleType(vehicletype) values('" . $typename . "')");
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your  type name added succesfully", 'flag' => 1));
            return;
        }
    }

    function deletetype() {
        $vehicleid = $this->input->post('vehicletypeid');

        $result = $this->db->query("delete from workplace_types where type_id ='" . $vehicleid . "'");
    }

    

    function deletecountry() {
        $countryid = $this->input->post('countryid');

        $result = $this->db->query("delete from country where Country_Id ='" . $countryid . "'");
    }

    function deletepagecity() {
        $cityid = $this->input->post('cityid');
        $result = $this->db->query("select company_id from company_info where city = '" . $cityid . "'")->result();
        $companies = array();
        foreach ($result as $company) {
            $companies[] = $company->company_id;
        }
        $result1 = $this->db->query("select type_id from workplace_types where city_id = '" . $cityid . "'")->result();
        $vehicleTypes = array();
        foreach ($result1 as $company) {
            $vehicleTypes[] = $company->type_id;
        }
        $this->db->query("delete from city_available where City_Id = '" . $cityid . "'");
        $this->db->query("delete from company_info where company_id in (" . implode(',', $companies) . ")");
        $this->db->query("delete from dispatcher where city ='" . $cityid . "'");
        $this->db->query("delete from workplace where type_id in (" . implode(',', $vehicleTypes) . ")");
        $this->db->query("delete from workplace_types where type_id in (" . implode(',', $vehicleTypes) . ")");
        $this->db->query("delete from coupons where city_id ='" . $cityid . "'");
        $this->db->query("delete from master where company_id  in (" . implode(',', $companies) . ")");
    }

    

    function deletemodal() {
        $modalid = $this->input->post('modalid');

        $result = $this->db->query("delete from vehiclemodel where id ='" . $modalid . "'");
    }

    function insert_modal() {
        $typeid = $this->input->post('typeid');

        $modal = $this->input->post('modal');

        $res = $this->db->query("insert into vehiclemodel(vehiclemodel,vehicletypeid) values('" . $modal . "','" . $typeid . "')");
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your  modal name added succesfully", 'flag' => 1));
            return;
        }
    }

    

    function editlonglat() {
        $val = $this->input->post('val');
        $lat = $this->input->post('lat');
        $lon = $this->input->post('lon');
        $this->db->query("update city_available set City_Lat = '" . $lat . "',City_Long = '" . $lon . "' where City_Id ='" . $val . "' ");
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => 'your latlong added successfully', 'flag' => 0));
            return;
        } else {
            echo json_encode(array('msg' => 'your latlong update failed', 'flag' => 1));
            return;
        }
    }

    function insert_city_available() {
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');
        $country = $this->input->post('country');
        $city = $this->input->post('city');

        $query = $this->db->query("select * from city_available where City_Id ='" . $city . "' ");

        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => "city  already exists", 'flag' => 0));
            return;
        } else {

            $selectCity = "select City_Name from city where City_Id = '" . $city . "'";

            $Result = $this->db->query($selectCity)->result_array();

            $this->db->query("insert into city_available(City_Id,Country_Id,City_Name,City_Lat,City_Long) values('" . $city . "','" . $country . "','" . $Result[0]['City_Name'] . "','" . $lat . "','" . $lng . "')");

            if ($this->db->affected_rows() > 0) {
                echo json_encode(array('msg' => "city added successfully", 'flag' => 1));
                return;
            }
        }
    }

    function city() {
        return $this->db->query("select a.City_Name, a.City_Id, b.Currency from city_available a, city b where a.City_Id = b.City_Id ORDER BY (a.City_Name) ASC ")->result();
    }

    function getAllServiceFromCity() {
        
    }

    function city_sorted() {
        return $this->db->query("select City_Name,City_Id from city_available ORDER BY City_Name ASC ")->result();
    }

    function get_driver() {
        return $this->db->query("select * from master ORDER BY last_name")->result();
    }

     

    function editpass() {
        $newpass = $this->input->post('newpass');
        $val = $this->input->post('val');
        $this->db->query("update dispatcher set dis_pass='" . $newpass . "' where dis_id = '" . $val . "' ");
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "this password already exists. Enter new password", 'flag' => 1));
            return;
        }
    }

    function get_disputesdata($status) {
        $result = $this->db->query(" select mas.first_name as mas_fname,mas.last_name as mas_lname,mas.mas_id,slv.slave_id,slv.first_name as slv_name,slv.last_name as slv_lname,rep.report_msg,rep.report_id,rep.report_dt,rep.appointment_id from master mas,slave slv, reports rep where rep.mas_id = mas.mas_id   and rep.slave_id = slv.slave_id and rep.report_status = '" . $status . "' order by rep.report_id DESC")->result();

        return $result;
    }

 

    function driver() {
        $res = $this->db->query("select * from master order by first_name")->result();
        return $res;
    }

    function passenger() {
        $res = $this->db->query("select * from slave")->result();
        return $res;
    }

    
    

    function insertpass() {
        $password = $this->input->post('newpass');
        $val = $this->input->post('val');

        $res = $this->db->query("update patient set password = md5('" . $password . "')  where patient_id='" . $val . "'");
        //        return $res;
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "Password updated successfully", 'flag' => 1));
            return;
        }
    }

    function get_company_data($param) {
        $result = $this->db->query("select * from company_info where company_id='" . $param . "' ")->result();
        return $result;
    }

    function company_data() {
        $result = $this->db->query("select * from company_info")->result();
        return $result;
    }

    function get_dispatchers_data($status) {

        $res = $this->db->query("select * from dispatcher where status='" . $status . "'")->result();
        return $res;
    }

    function delete_dispatcher() {
        $var = $this->input->post('val');

        foreach ($var as $row) {
            $this->db->query("delete  from dispatcher where dis_id ='" . $row . "'");
        }
    }

    function get_country() {
        return $this->db->query("select * from country order by Country_Name")->result();
    }

    function datatable_cities() {

        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select('ci.City_Id,co.Country_Name,ci.City_Name,ci.City_Lat,ci.City_Long')
                ->unset_column('ci.City_Id')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'ci.City_Id')
                ->from('city_available ci,country co')
                ->where('ci.country_id = co.country_id'); //order by slave_id DESC ",false);
        $this->db->order_by("ci.City_Id", "desc");

        echo $this->datatables->generate();
    }

 

    

     

     
     

    function documentgetdata() {
        $val = $this->input->post("val");
        $return = array();
        foreach ($val as $row) {
            $data = $this->db->query("select * from docdetail where driverid = '" . $row . "'")->result();
        }
        foreach ($data as $doc) {
            $return[] = array('doctype' => $doc->doctype, 'url' => $doc->url, 'expirydate' => $doc->expirydate);
        }
        return $return;
    }

    

    function loadAvailableCity() {
        $countryid = $this->input->post('country');
        $Result = $this->db->query("select c.* from city c where c.Country_Id = '" . $countryid . "' and c.City_Id not in (select City_Id from city_available where Country_Id = '" . $countryid . "')")->result();
        $q = "select c.* from city c where c.Country_Id = '" . $countryid . "' and c.City_Id not in (select City_Id from city_available where Country_Id = '" . $countryid . "')";
        return $Result;
    }

    

    function validateCompanyEmail() {

        $query = $this->db->query("select company_id from company_info where email='" . $this->input->post('email') . "'");
        if ($query->num_rows() > 0) {

            echo json_encode(array('msg' => '1'));
            return;
        } else {
            echo json_encode(array('msg' => '0'));
        }
    }

    function datatable_vehiclemodels($status) {


        $this->load->library('Datatables');
        $this->load->library('table');

        if ($status == 1) {

            $this->datatables->select("id,vehicletype")
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'id')
                    ->from("vehicleType");
        } else if ($status == 2) {


            $this->datatables->select("vm.id,vm.*,vt.vehicletype")
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'vm.id')
                    ->from("vehiclemodel vm,vehicleType vt")
                    ->where("vm.vehicletypeid = vt.id");
        }
        $this->db->order_by("id", "desc");
        echo $this->datatables->generate();
    }

     

    function uniq_val_chk() {

        $query = $this->db->query('select * from workplace where uniq_identity = "' . $this->input->post('uniq_id') . '"');
        if ($query->num_rows() > 0) {

            echo json_encode(array('msg' => "This vehicleId Is Already Allocated", 'flag' => '1'));
        } else {
            echo json_encode(array('msg' => "", 'flag' => '0'));
        }
        return;
    }

     
    function datatable_document($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $company = $this->session->userdata('company_id');

        if ($status == '1') {

            $this->datatables->select("d.doc_ids,c.first_name,c.last_name,d.expirydate,d.url")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<button type="button" name="view"  width="50px">'
                            . '<a target="_blank" href="' . base_url() . '../../pics/$1">view</a><a target="_blank" href="' . base_url() . '../../pics/$1"></button><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
                    ->from("master c,docdetail d")
                    ->where("c.mas_id = d.driverid and d.doctype=1" . ($company != 0 ? ' and c.company_id = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '2') {

            $this->datatables->select("d.doc_ids,c.first_name,c.last_name,d.expirydate,d.url")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">view</button></a><a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
                    ->from("master c,docdetail d")->where("c.mas_id = d.driverid and d.doctype=2" . ($company != 0 ? ' and c.company_id = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '3') {


            $this->datatables->select("d.docid,d.vechileid,(select companyname from company_info where company_id = w.company) as companyname,d.expirydate,d.url")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<button type="button" name="view"  width="50px"><a target="_blank" href="' . base_url() . '../../pics/$1">view</button></a><a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
//                     ->select("(select companyname from company_info where company_id = w.company) as companyname",false)
                    ->from("workplace w,vechiledoc d,vehicleType v")
                    ->where("w.title = v.id and w.workplace_id = d.vechileid and d.doctype = 2" . ($company != 0 ? ' and w.company = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '4') {

            $this->datatables->select("d.docid,d.vechileid,(select companyname from company_info where company_id = w.company) as companyname,d.url,d.expirydate")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">view</button></a><a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
//                     ->select("(select companyname from company_info where company_id = w.company) as companyname",false)
                    ->from("workplace w,vechiledoc d,vehicleType v")
                    ->where("w.title = v.id and w.workplace_id = d.vechileid and d.doctype = 3" . ($company != 0 ? ' and w.company = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '5') {

            $this->datatables->select("d.docid,d.vechileid,(select companyname from company_info where company_id = w.company) as companyname,d.url,d.expirydate")
                    ->select("(select companyname from company_info where company_id = w.company) as companyname", false)
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">view</button></a><a target="_blank" target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
                    ->from("workplace w,vechiledoc d,vehicleType v")
                    ->where("w.title = v.id and w.workplace_id = d.vechileid and d.doctype = 1" . ($company != 0 ? ' and w.company = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        }

        echo $this->datatables->generate();
    }
 

    function editdispatchers_city() {
        $val = $this->input->post('val');

        $var = $this->db->query("select city from dispatcher where dis_id='" . $val . "'")->result();
        return $var;
    }

   
   

    function get_documentdata($status) {
        if ($status == 1) {
            $result = $this->db->query("select c.first_name,c.last_name,d.url,d.doc_ids,d.expirydate from master c,docdetail d where c.mas_id=d.driverid and d.doctype=1")->result();
            return $result;
        } else if ($status == 2) {
            $result = $this->db->query("select c.first_name,c.last_name,d.url,d.doc_ids,d.expirydate from master c,docdetail d where c.mas_id=d.driverid and d.doctype=2")->result();
            return $result;
        } else if ($status == 3) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        } else if ($status == 4) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=3")->result();
            return $result;
        } else if ($status == 5) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        } else if ($status == 5) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        } else if ($status == 6) {
            $result = $this->db->query("SELECT d.url,d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        }
    }

    function setsessiondata($tablename, $LoginId, $res, $email, $password, $off) {

        $offset = -($off);

        $sessiondata = array(
            'emailid' => $email,
            'password' => $password,
            'LoginId' => $res->$LoginId,
            'profile_pic' => $res->logo,
            'first_name' => $res->companyname,
            'table' => $tablename,
            'city_id' => '0', 'company_id' => '0',
            'validate' => true,
            'admin' => 'super',
            'offset' => $offset
        );

        return $sessiondata;
    }

    function Drivers($status = '') {

        $quaery = $this->db->query("SELECT mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt,(select type from user_sessions where oid = mas.mas_id order by oid DESC limit 0,1) as dev_type FROM master mas where  mas.status IN (" . $status . ") and mas.company_id IN (" . $this->session->userdata('LoginId') . ") order by mas.mas_id DESC")->result();
        return $quaery;
    }

    function datatable($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select("*")->from('slave')->where('status', 3); //order by slave_id DESC ",false);

        echo $this->datatables->generate();
    }

    function validateEmail() {

        $query = $this->db->query("select doc_id from doctor where email='" . $this->input->post('email') . "'");
        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => '1'));
            return;
        } else {
            echo json_encode(array('msg' => '0'));
        }
    }

    function validatedispatchEmail() {

        $query = $this->db->query("select dis_id from dispatcher where dis_email='" . $this->input->post('email') . "'");
        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => '1'));
            return;
        } else {
            echo json_encode(array('msg' => '0'));
            return;
        }
    }

    function get_workplace() {
        $res = $this->db->query("select * from workplace_types")->result();
        return $res;
    }

    function get_cities() {
        $query = $this->db->query('select * from city_available')->result();
        return $query;
    }

    function loadcity() {
        $countryid = $this->input->post('country');
        $Result = $this->db->query("select * from city where Country_Id=" . $countryid . "")->result();
        return $Result;
    }

    function loadcompany() {
        $cityid = $this->input->post('city');
        $Result = $this->db->query("select * from doctor where city_id=" . $cityid . " and status = 3 ")->result_array();
        return $Result;
    }

    function get_city() {
        return $this->db->query("select ci.*,co.Country_Name from city_available ci,country co where ci.country_id = co.country_id ORDER BY ci.City_Name ASC")->result();
    }

    function get_companyinfo($status) {
        return $this->db->query("select * from company_info where status = '" . $status . "' ")->result();
    }

  
 
     
    public function getprofessiondata() {
        $data = $this->db->query('select type_id,type_name from doctor_type')->result();
        return $data;
    }

    

    function documentgetdatavehicles() {
        $val = $this->input->post("val");

        $vehicleImage = array();

        $return = $data = array();
        foreach ($val as $row) {
            $data = $this->db->query("select * from vechiledoc where vechileid = '" . $row . "'")->result();
            //            return $data;
        }
        foreach ($data as $vehicle) {


            $return[] = array('doctype' => $vehicle->doctype, 'url' => $vehicle->url, 'expirydate' => $vehicle->expirydate);
        }
        $vehicleImage = $this->db->query("select Vehicle_Image from workplace where workplace_id = '" . $val[0] . "'")->row_array();
        $return[] = array('doctype' => '99', 'urls' => $vehicleImage['Vehicle_Image'], 'expirydate' => "");

        return $return;
    }

    function uploadimage_diffrent_redulation($file_to_open, $imagename, $servername, $ext) {

        list($width, $height) = getimagesize($file_to_open);

        $ratio = $height / $width;
        /* mdpi 36*36 */
        $mdpi_nw = 36;
        $mdpi_nh = $ratio * 36;

        $mtmp = imagecreatetruecolor($mdpi_nw, $mdpi_nh);
        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($mtmp, $new_image, 0, 0, 0, 0, $mdpi_nw, $mdpi_nh, $width, $height);

        $mdpi_file = $servername . 'pics/mdpi/' . $imagename;

        imagejpeg($mtmp, $mdpi_file, 100);

        /* HDPI Image creation 55*55 */
        $hdpi_nw = 55;
        $hdpi_nh = $ratio * 55;

        $tmp = imagecreatetruecolor($hdpi_nw, $hdpi_nh);

        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($tmp, $new_image, 0, 0, 0, 0, $hdpi_nw, $hdpi_nh, $width, $height);

        $hdpi_file = $servername . 'pics/hdpi/' . $imagename;

        imagejpeg($tmp, $hdpi_file, 100);

        /* XHDPI 84*84 */
        $xhdpi_nw = 84;
        $xhdpi_nh = $ratio * 84;

        $xtmp = imagecreatetruecolor($xhdpi_nw, $xhdpi_nh);

        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($xtmp, $new_image, 0, 0, 0, 0, $xhdpi_nw, $xhdpi_nh, $width, $height);

        $xhdpi_file = $servername . 'pics/xhdpi/' . $imagename;

        imagejpeg($xtmp, $xhdpi_file, 100);

        /* xXHDPI 125*125 */
        $xxhdpi_nw = 125;
        $xxhdpi_nh = $ratio * 125;

        $xxtmp = imagecreatetruecolor($xxhdpi_nw, $xxhdpi_nh);

        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($xxtmp, $new_image, 0, 0, 0, 0, $xxhdpi_nw, $xxhdpi_nh, $width, $height);

        $xxhdpi_file = $servername . 'pics/xxhdpi/' . $imagename;

        imagejpeg($xxtmp, $xxhdpi_file, 100);
    }

    

  
    function updateData($IdToChange = '', $databasename = '', $db_field_id_name = '') {
        $formdataarray = $this->input->post('fdata');
        $this->db->update($databasename, $formdataarray, array($db_field_id_name => $IdToChange));
    }

    function LoadAdminList() {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;
        $cursor = $collection->find(array('Role' => "SubAdmin"));
        //        $db->close();
        return $cursor;
    }

    function issessionset() {

        if ($this->session->userdata('emailid') && $this->session->userdata('password')) {

            return true;
        }
        return false;
    }

}

?>
