<html>
    <head>
        <title><?= Appname ?> Help</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?php echo base_url(); ?>theme/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>theme/assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>theme/assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <div class="row" style='height:90vh;'>
                <div class="jumbotron" style="
             height: 100vh;
             width: 100vw;
             top: 0;
             position: fixed;
             background-color:rgba(182, 182, 182, 0.9);
             padding-top: 45vh;
             z-index:1000;
             /*display:none;*/
             ">
                    <?php
                        $s="glyphicon-remove";
                        if($flag == 0)
                            $s="glyphicon-ok";
                    ?>
                    <h3 class='text-center'>
                        <i class="glyphicon <?= $s?>" style='display:block;font-size:50px;margin-bottom:20px;'></i>
                        <?= $msg ?>
                    </h3>
                </div>
            </div>
        </div> 
    </body>
</html>