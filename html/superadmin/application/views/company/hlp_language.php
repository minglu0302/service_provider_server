<script src="<?= base_url() ?>theme/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
    $('.sm-table').find('.header-inner').html('<div class="brand inline" style="  width: auto;\
                     font-size: 27px;\
                     color: gray;\
                     margin-left: 100px;margin-right: 20px;margin-bottom: 12px; margin-top: 10px;">\
                    <strong><?= Appname ?> Super Admin Console</strong>\
                </div>');

    $(document).ready(function () {
        var lan_tbl = $('#lan_table');
        var settings = {
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };
        lan_tbl.DataTable(settings);
        $('#search-table').keyup(function () {
            lan_tbl.fnFilter($(this).val());
        });
        $('#add_new_lan').click(function () {
            $('#lan_title').html('Add new Language');
            $('#add_lan').html('Add');
            $('#lan_edit').val('');
            $('#lan_name').val('');
            $('#lan_msg').val('');
            $('#modal-lan').modal('show');
        });
    });
    var eid;
    function add_edit_lan() {
        $('#edit_txt').val('');
        if ($('#lan_name').val() == '' || $('#lan_name').val() == 'english' || $('#lan_name').val() == 'English') {
            $('#lan_name').closest('.form-group').addClass('has-error');
            return;
        }
        if($("#lan_name").val() != '' && $("#lan_name_old").val() != $("#lan_name").val()){
             $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url('index.php/utilities') ?>/checkLanName",
                        data: {cid: $("#lan_name").val()},
                        dataType: 'JSON',
                        success: function (result)
                        {
                            console.log(result);
                            if(result=="true")
                            {
                                $("#edit_txt").text('Language is already added for this city');
                               
                            }else{
                                $('#lan_name').closest('.form-group').removeClass('has-error');
//        $('#lan_msg').closest('.form-group').removeClass('has-error');
        $.ajax({
            url: "<?php echo base_url() ?>index.php/utilities/lan_action",
            type: "POST",
            data: $('#lan_form').serialize(),
            dataType: "JSON",
            success: function (result) {
                if (result.msg == '1') {
                    if (result.insert != 0) {
                        var lan_tbl = $('#lan_table').DataTable();
                        lan_tbl.row.add([
                            4,
                            $('#lan_name').val(),
//                            $('#lan_msg').val(),
                            "<a class='btn btn-default' onclick='editlan(this)' data-id='" + result.msg + "'>\
                                    <i class='fa fa-edit text-white'></i> Edit\
                                </a>'"
//                                <a class='btn btn-default' onclick='dellan(this)' data-id='" + result.msg + "'>\
//                                    <i class='fa fa-trash text-white'></i> Delete\
//                                </a>"
                        ]).draw();
                    } else {
                        $(eid).closest('tr').find("td:nth-child(2)").html($('#lan_name').val());
                        $(eid).closest('tr').find("td:nth-child(3)").html($('#lan_msg').val());
                        eid = '';
                    }
                    $('#modal-lan').modal('hide');
                } else {
                    alert('Problem occurred please try agin.');
                }
            },
            error: function () {
                alert('Problem occurred please try agin.');
            },
            timeout: 30000
        });
                            }
                                
                        
                    }
                });
        }
//        if ($('#lan_msg').val() == '') {
//            $('#lan_msg').closest('.form-group').addClass('has-error');
//            return;
//        }
        
    }
    function editlan(rowid) {
        eid = rowid;
        $('#lan_name_old').val("");
        $('#lan_title').html('Edit Language');
        $('#add_lan').html('Update');
        $('#lan_edit').val($(rowid).attr('data-id'));
        $('#lan_name').val($(rowid).closest('tr').find("td:nth-child(2)").html());
        $('#lan_name_old').val($(rowid).closest('tr').find("td:nth-child(2)").html());
        $('#lan_msg').val($(rowid).closest('tr').find("td:nth-child(3)").html());
        $('#modal-lan').modal('show');
    }
    function dellan(rowid) {
        $.ajax({
            url: "<?php echo base_url() ?>index.php/utilities/lan_action/del",
            type: "POST",
            data: {id: $(rowid).attr('data-id')},
            dataType: "JSON",
            success: function (result) {
                if (result.msg == '1') {
                    var lan_tbl = $('#lan_table').DataTable();
                    lan_tbl.row($(rowid).closest('tr'))
                            .remove()
                            .draw();
                } else {
                    alert('Problem in Deleting language please try agin.');
                }
            },
            error: function () {
                alert('Problem in Deleting language please try agin.');
            },
            timeout: 30000
        });
    }
</script>

<style>
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .rating>.rated {
        color: #10cfbd;
    }
    .social-user-profile {
        width: 83px;
    }
    .table > thead > tr > th{
        font-size: 14px;
    }
</style>

<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">Languages</a>
            </li>
            <!--            <li>
                            <a href="#" class="active">Job Details - <?php echo $data['appt_data']->appointment_id; ?></a>
                        </li>-->
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                    <li class="active">
                        <a href="#lan_grp" data-toggle="tab" role="tab" aria-expanded="true">Language</a>
                    </li>
                    <!--                    <li class="">
                                            <a href="#hlp_txt" data-toggle="tab" role="tab" aria-expanded="false">Help Text</a>
                                        </li>-->
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="lan_grp">
                        <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Language
                                    </div>
                                    <div class='pull-right'>
                                        <button class='btn btn-success' id='add_new_lan'>
                                            <i class="fa fa-plus text-white"></i> Add Language
                                        </button>
                                    </div>
                                    <div class="panel-body no-padding">
                                        <table class="table table-hover demo-table-search" id="lan_table">
                                            <thead>
                                                <tr>
                                                    <th>SL No.</th>
                                                    <th>Language</th>
                                                    <!--<th>Success Message</th>-->
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $l = 1;
                                                foreach ($language as $val) {
                                                    ?>
                                                    <tr>
                                                        <td class="v-align-middle semi-bold">
                                                            <?= $l++ ?>
                                                        </td>
                                                        <td><?= $val['lan_name'] ?></td>
                                                        <!--<td><?= $val['msg'] ?></td>-->
                                                        <td class="v-align-middle">
                                                            <a class='btn btn-default' onclick='editlan(this)' data-id='<?= $val['lan_id'] ?>'>
                                                                <i class="fa fa-edit"></i> Edit
                                                            </a>
                                                           
                                     
                                                            <a class='btn btn-default' onclick='dellan(this)' data-id='<?= $val['lan_id'] ?>'>
                                                                <i class="fa fa-trash text-white"></i> Delete
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 

<div class="modal fade slide-up disable-scroll" id="modal-lan" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content">
            <form id='lan_form' action=''>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style='margin: 10px;'>
                    <i class="pg-close"></i>
                </button>
                <div class="modal-header">
                    <h4 class="modal-title" id='lan_title'></h4>
                </div>
                <div class="modal-body m-t-50">
                    <div class="form-group-attached">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default required">
                                    <label>Language</label>
                                    <input type="text" required name='lan_name' id="lan_name" class="form-control">
                                    <input type="hidden" name='edit_id' id='lan_edit'>
                                    <input type="hidden" name='lan_name_old' id='lan_name_old'>
                                   
                                </div>
<!--                                <div class="form-group form-group-default required">
                                    <label>Success Message</label>
                                    <input type="text" required name='lan_msg' id="lan_msg" class="form-control">
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="edit_txt" style="text-align: center;"></div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10 pull-right">
                            <button type='button' class="btn btn-primary btn-block m-t-5" id='add_lan' onclick='add_edit_lan()'>Add</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>