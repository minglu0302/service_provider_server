<style>
    .div-table{
        display:table;         
        width:auto;         
        background-color:#eee;         
        border:1px solid  #666666;         
        border-spacing:5px;/*cellspacing:poor IE support for  this*/
    }
    .div-table-row{
        display:table-row;
        width:auto;
        clear:both;
    }
    .div-table-col{
        float:left;/*fix for  buggy browsers*/
        display:table-column;         
        width:200px;         
        background-color:#ccc;  
    }

    .select2-results {
        max-height: 192px;
    }
    td.details-control {
        background: url('<?php echo base_url() ?>theme/pages/img/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('<?php echo base_url() ?>theme/pages/img/details_close.png') no-repeat center center;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        var socket = io('http://iserve.ind.in:9999');
        socket.on('LiveBookingResponce', function (data) {
            $('#big_table').find('tr:first').find('th:first').trigger('click');
        });
    });
    function getdetail(bid) {
        $.ajax({
            type: 'post',
            url: '<?php echo base_url('index.php/superadmin/get_appointmentDetials') ?>',
            data: {app_id: bid},
            dataType: 'json',
            success: function (row1) {
                var now = new Date();
                var datestr = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() + ' ' + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
                var then = row1.appointment_dt;
                var dirr = moment.utc(moment(datestr, "YYYY-MM-DD HH:mm:ss").diff(moment(then, "YYYY-MM-DD HH:mm:ss"))).format("HH:mm:ss")
                $('.competeBooking').attr('id', row1.appointment_id);
                $('.competeBooking').attr('alt', row1.chn);
                $('.bookingid').html("Booking Id : " + row1.appointment_id);
                $('.pickupaddress').html(row1.address_line1);
                $('.BookingTime').html(dirr);
                $('.vehicleType').html(row1.typename);
                $('.approxAmount').html(row1.apprxAmt);
                $('#amounttocharge').val(row1.apprxAmt);
                $('.approxAmount').html(row1.apprxAmt);
                $('.paymentstatus').html(row1.paymentstatus);
                $('.bookingstatus').html(row1.status_result);
                $('.driverName').html(row1.first_name);
                $('.driverPhone').html(row1.mobile);
                $('.passengerName').html(row1.sname);
                $('.passengerPhone').html(row1.phone);
                $('#modal-container-186699441').modal('show');
            }
        });
    }
    $(document).ready(function () {
        $('.idonclick').click(function () {
            var bid = $(this).attr('data');
            alert(bid);
            $.ajax({
                type: 'post',
                url: '<?php echo base_url('index.php/superadmin/get_appointmentDetials') ?>',
                data: {app_id: bid},
                dataType: 'json',
                success: function (row1) {
                    var now = new Date();
                    var datestr = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() + ' ' + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
                    var then = row1.appointment_dt;
                    var dirr = moment.utc(moment(datestr, "YYYY-MM-DD HH:mm:ss").diff(moment(then, "YYYY-MM-DD HH:mm:ss"))).format("HH:mm:ss")
                    $('.competeBooking').attr('id', row1.appointment_id);
                    $('.competeBooking').attr('alt', row1.chn);
                    $('.bookingid').html("Booking Id : " + row1.appointment_id);
                    $('.pickupaddress').html(row1.address_line1);
                    $('.BookingTime').html(dirr);
                    $('.vehicleType').html(row1.typename);
                    $('.approxAmount').html(row1.apprxAmt);
                    $('#amounttocharge').val(row1.apprxAmt);
                    $('.approxAmount').html(row1.apprxAmt);
                    $('.paymentstatus').html(row1.paymentstatus);
                    $('.bookingstatus').html(row1.status_result);
                    $('.driverName').html(row1.first_name);
                    $('.driverPhone').html(row1.mobile);
                    $('.passengerName').html(row1.sname);
                    $('.passengerPhone').html(row1.phone);
                    $('#modal-container-186699441').modal('show');
                }
            });
        });


        $('#Docomplete').click(function () {
            var chn = $('.competeBooking').attr('alt');
            var bid = $('.competeBooking').attr('id');
            var data = $('.competeBooking').attr('data');
            var amount = $('#amounttobecharge').val();
            var d = new Date;
            var duration = [d.getFullYear(),
                d.getMonth() + 1,
                d.getDate()].join('-') + ' ' +
                    [d.getHours(),
                        d.getMinutes(),
                        d.getSeconds()].join(':');
            if (confirm('Are you sure.!')) {
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url('index.php/superadmin/CompleteBooking') ?>',
                    data: {app_id: bid, data: data, amount: amount, duration: duration},
                    dataType: 'json',
                    success: function (row1) {
                        if (row1.flag == 1)
                            alert(row1.msg);
                        else {
                            var t = $('#tableWithSearch').DataTable();
                            t.row($('.app_id_' + bid).closest("tr"))
                                    .remove()
                                    .draw();
                            $('.close_confirm').trigger('click');
                        }
                    }
                });
            }
        });
        $('.competeBooking').click(function () {
            if ($(this).attr('data') == '1') {

                var bid = $('.competeBooking').attr('id');
                var chn = $('.competeBooking').attr('alt');
                var data = $('.competeBooking').attr('data');
                var amount = $('#amounttobecharge').val();
                var d = new Date;
                var duration = [d.getFullYear(),
                    d.getMonth() + 1,
                    d.getDate()].join('-') + ' ' +
                        [d.getHours(),
                            d.getMinutes(),
                            d.getSeconds()].join(':');

                if (confirm('Are you sure.!')) {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url('index.php/superadmin/CompleteBooking') ?>',
                        data: {app_id: bid, data: data, amount: amount, duration: duration},
                        dataType: 'json',
                        success: function (row1) {
                            if (row1.flag == 1)
                                alert(row1.msg);
                            else {

                                var t = $('#tableWithSearch').DataTable();
                                t.row($('.app_id_' + bid).closest("tr"))
                                        .remove()
                                        .draw();
                                $('#modal-container-186699441').modal('hide');
                                $('.close_confirm').trigger('click');
                            }
                        }



                    });
                }

            } else {
                $('#modal-container-186699441').modal('hide');
                $('#ManualAmt').modal('show');
            }
        });



    });


    function oneFunction() {
        $.ajax({
            type: 'post',
            url: '<?php echo base_url('index.php/superadmin/filter_AllOnGoing_jobs') ?>',
            dataType: 'json',
            success: function (result) {
                var t = $('#tableWithSearch').DataTable();
                t.clear().draw();
                $.each(result.aaData, function (index, row1) {
                    var status = 'Status unavailable.';
                    t.row.add([
                        '<a class="idonclick" data="' + row1.appointment_id + '" style="cursor: pointer"> <p>' + row1.appointment_id + '</p></a>',
                        row1.mas_id,
                        row1.first_name,
                        row1.dphone,
                        row1.pessanger_fname,
                        row1.phone,
                        row1.appointment_dt,
                        row1.address_line1,
                        row1.drop_addr1 + row1.drop_addr2,
                        '<span class="app_id_' + row1.appointment_id + '">' + status + '</span>'
                    ]).order([[6, 'desc']])
                            .draw();
                });
            }
        });
    }


    function refreshTableOnCityChange() {
        oneFunction();
    }

    function refreshTableOnActualcitychagne() {
        oneFunction();
    }
</script>
<div id="message"></div>
<script src="http://iserve.ind.in:9999/socket.io/socket.io.js"></script>
<script src="http://momentjs.com/downloads/moment.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/superadmin/datatable_onGoing_jobs/',
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                console.log("data");
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });


        var detailRows = [];
        // On each draw, loop over the `detailRows` array and show any child rows
        table.on('draw', function () {
            $.each(detailRows, function (i, id) {
                $('#' + id + ' td.details-control').trigger('click');
            });
        });
    });


    function refreshTableOnCityChange() {

        var table = $('#big_table');
        var url = '<?php echo base_url(); ?>index.php/superadmin/bookings_data_ajax/' + $('#Sortby').val() + '/' + $('#companyid').val();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": url,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };
        table.dataTable(settings);
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

    }
</script>
<style>
    .exportOptions{
        display: none;
    }
</style>
<div class="page-content-wrapper"style="padding-top: 20px">
    <div class="content">
        <div class="content"style="padding-top: 3px">
            <div class="brand inline" style="  width: auto;
                 font-size: 20px;
                 color: gray;
                 margin-left: 30px;">               
                <strong style="color:#0090d9;">BOOKINGS</strong>
            </div>
            <div class="jumbotron" data-pages="parallax">
                <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                    <div class="tab-content">
                        <div class="container-fluid container-fixed-lg bg-white">
                            <div class="panel panel-transparent">
                                <div class="panel-heading">                                  
                                    <div class="row clearfix pull-right" >
                                        <div class="col-sm-12">
                                            <div class="searchbtn" >
                                                <div class="pull-right"><input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer">
                                        <div class="table-responsive">
                                            <?php
                                            $this->table->function = 'htmlspecialchars';
                                            echo $this->table->generate();
                                            ?>
                                        </div><div class="row"></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid container-fixed-lg">
        </div>
    </div>
</div>






<div class="modal in" id="modal-container-186699441" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="widget-15 panel  no-border no-margin widget-loader-circle">
                <div class="panel-heading">

                    <div class="panel-title bookingid">
                        Booking Id : 101
                    </div>
                    <div class="panel-controls">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="p-l-3">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="b-b b-t b-grey m-b-10">
                                    <!--pickpu address-->
                                    <div class="row m-t-10">
                                        <div class="col-md-5">
                                            <div class="panel-title">
                                                <i class="pg-map"></i>JOB Address
                                                <span class="caret"></span>
                                            </div>
                                            <p class="small hint-text pickupaddress">9th August 2014</p>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="pull-left">
                                                <p class="small hint-text no-margin">Customer</p>

                                                <span class="small hint-text passengerName">Ashish</span>
                                                <span class="small hint-text passengerPhone"> / 8892656768</span>

                                            </div>
                                            <div class="pull-right">
                                                <canvas height="64" width="64" class="clear-day"></canvas>
                                            </div>
                                        </div>
                                    </div>

                                    <!--dropp address--> 
                                    <div class="row m-t-1">
                                        <div class="col-md-5">
                                            <!--                                            <div class="panel-title">
                                                                                            <i class="pg-map text-danger"></i> Drop
                                                                                            <span class="caret"></span>
                                                                                        </div>
                                                                                        <p class="small hint-text Dropaddresss"> -------------- </p>-->
                                        </div>
                                        <div class="col-md-7" style="margin-bottom: -25px;">
                                            <p class="small hint-text no-margin">Provider</p>

                                            <span class="small hint-text driverName">Ashish</span>
                                            <span class="small hint-text driverPhone"> / 8892656768</span>

                                            <div class="pull-right">
                                                <canvas height="64" width="64" class="clear-day"></canvas>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <p class="bold">Booking Details</p>
                                <div class="widget-17-weather b-b b-grey">
                                    <div class="row">
                                        <div class="col-sm-6 p-r-10">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left ">Currunt Status</p>
                                                    <p class="pull-right bookingstatus"> On the way</p>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left">Payment Method</p>
                                                    <p class="pull-right paymentstatus">Cash</p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left bold">Approx Amount</p>
                                                    <p class="pull-right text-danger approxAmount"> <?php echo currency; ?> 500 </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 p-l-10">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left">Booking Time</p>
                                                    <p class="pull-right BookingTime">1 hour 30 m </p>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left">Category</p>
                                                    <p class="pull-right vehicleType">Silver</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-t-10 m-l-5">
                                    <div class="col-sm-6" style="float: right">
                                        <div class="row">
                                            <button class="btn btn-success btn-cons competeBooking" data="1">Complete (Don't Charge)</button>
                                        </div>
                                    </div>
                                    <input type="hidden" id="amounttocharge"> 
                                    <div class="col-sm-6 " style="float: left">
                                        <div class="row">
                                            <button class="btn btn-danger btn-cons  competeBooking" data="2">Complete (Charge) </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <img src="pages/img/progress/progress-circle-master.svg" style="display:none"></div>
        </div>
    </div>
</div>

<div id="ManualAmt" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <input type="text" id="amounttobecharge" placeholder="Enter Amount">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id='Docomplete'>Trigger</button>
                <button type="button" class="btn btn-default close_confirm" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script src="http://momentjs.com/downloads/moment.js"/>