<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
    .exportOptions{
        display: none;
    }
</style>

<script>
</script>
<div class="page-content-wrapper">
    <div class="content">
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <br/>
                <div class="inner" style="-webkit-transform: translateY(0px); opacity: 1;">
                    <ul class="breadcrumb">
                        <ul class="breadcrumb">
                            <li><a href="<?php echo base_url('index.php/superadmin/payroll') ?>" class="">Payroll</a>
                            </li>
                            <li><a href="<?php echo base_url('index.php/superadmin/pay_provider/'.$mas_id) ?>" class="active">Provider(<?php echo $provider_name ?>)</a>
                            </li>
                            <li><a href="<?php echo base_url('index.php/superadmin/patientDetails/'.$pay_id.'/'.$mas_id) ?>" class="active">Payment Cycle(<?php echo $start_date ?>)</a>
                            </li>
                            <li><a href="#" class="active">Average Rating</a>
                            </li>
                        </ul>
                    </ul>
                </div>

                <div class="panel panel-transparent ">
                    <div class="tab-content">
                        <div class="container-fluid container-fixed-lg bg-white">
                            <div class="panel panel-transparent">
                                <div class="panel-heading">
                                    <div class="row clearfix">
                                        <div class="pull-left">
                                            <div class="col-md-6">                                                
                                            </div>
                                        </div>
                                        <div class="pull-right">                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive"><table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">BOOKING ID</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">CUSTOMER ID</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">RATING</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">REVIEW</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($allrevRes as $result) {
                                                        ?>
                                                        <tr role="row"  class="gradeA odd">
                                                            <td class="v-align-middle sorting_1"> <p><?php echo $result->appointment_id; ?></p></td>
                                                            <td class="v-align-middle sorting_1"> <p><?php echo $result->patient_id ?></p></td>
                                                            <td class="v-align-middle sorting_1"> <p><?php echo $result->star_rating ?></a></b></p></td>
                                                            <td class="v-align-middle sorting_1"> <p><?php echo $result->review ?></p></td>                                                            
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table></div><div class="row">
                                        </div></div>
                                </div>
                            </div>                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>