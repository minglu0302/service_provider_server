<?php
date_default_timezone_set('UTC');
$rupee = "$";
error_reporting(0);
?>

<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    function get_other_earning(thisval)
    {
        var B_id = thisval;
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('index.php/superadmin') ?>/getBilledAmount",
            data: {bid: B_id},
            dataType: 'JSON',
            success: function (result)
            {
                var html = '';
                $('#ShowCategoryData').empty();
                html = '<div class="col-md-12 col-xlg-6">\n\
                                    <div class="widget-17-weather">\n\
                                        <div class="row">\n\
                                            <div class="col-sm-8 p-r-10">';
                html += '<div class="row"><p style="font-weight: bold; border-top: #d8d6d6 1px solid;padding-top: 10px;">PRO BILLING</p>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Misc Fee</p>\n\
                                                        <p class="pull-right bold"><?php echo CURRENCY ?> ' + result.comp.misc_fees + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Material Fee</p>\n\
                                                        <p class="pull-right bold"><?php echo CURRENCY ?> ' + result.comp.mat_fees + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Pro Discount</p>\n\
                                                        <p class="pull-right bold"><?php echo CURRENCY ?> ' + result.comp.pro_disc + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                         \n\ <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Sub Total</p>\n\
                                                        <p class="pull-right bold"><?php echo CURRENCY ?> ' + result.invoiceData.pro_billing + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                         \n\ <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Pro Notes</p>\n\
                                                        <p class="pull-right bold"> ' + result.comp.complete_note + '</p>\n\
                                                    </div>\n\
                                                </div>';
                $('.displayInvoiceData').html(html);
                $('#billed').modal('show');
            }
        });

    }
    $(document).ready(function () {
        $('.payroll').addClass('active');
        $('.payroll').attr('src', "<?php echo base_url(); ?>/theme/icon/payroll_on.png");
    });

    function refreshTableOnCityChange() {
        var table = $('#big_table');
        var url = '';
        url = '<?php echo base_url(); ?>index.php/superadmin/patientDetails_ajax/' +<?php echo $pay_id ?> + '/' + <?php echo $pro_id ?>;
        var settings = {
            "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": url,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

    }

</script>


<script type="text/javascript">
    $(document).ready(function () {


        $('#datepicker-component').on('changeDate', function () {
            $(this).datepicker('hide');
        });



//        $("#datepicker1").datepicker({ minDate: 0});
        var date = new Date();
        $('#datepicker-component').datepicker({
            startDate: date
        });
        var table = $('#big_table');
        var settings = {
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/superadmin/patientDetails_ajax/' +<?php echo $pay_id ?> + '/' + <?php echo $pro_id ?>,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "order": [[0, "desc"]],
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };
        table.dataTable(settings);
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });
    });
</script>
<style>
    .exportOptions{
        display: none;
    }
</style>
<div class="page-content-wrapper">
    <div class="content">
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <br/>
                <div class="inner">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url('index.php/superadmin/payroll') ?>" class="">Payroll</a>
                        </li>
                        <li><a href="<?php echo base_url('index.php/superadmin/pay_provider/'.$pro_id) ?>" class="">Provider(<?php echo $provider_name ?>)</a>
                        </li>
                        <li><a href="#" class="active">Payment Cycle(<?php echo $start_date ?>)</a>
                        </li>
                    </ul>
                </div>

                <!--start box-->
                <div class="row">
                    <div class="col-md-4">
                        <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="panel-heading  top-left top-right">
                                            <div class="panel-title text-black">
                                            </div>
                                            <div class="panel-controls">
                                                <ul>
                                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="p-l-20">
                                            <a href=" <?php echo base_url("index.php/superadmin/revDetails/" . $pay_id . "/" . $pro_id) ?> ">
                                                <h4 class="no-margin p-b-5 text-white">AVERAGE RATING</h4>                                           
                                                <div style="font-size: 42px;margin-left: 10%;">
                                                    <?php echo $rev_rate ?>
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-bottom">
                                        <div class="progress progress-small m-b-20">
                                            <div class="progress-bar progress-bar-white" data-percentage="0%" style="width: 0%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;">
                        </div>                        
                    </div>
                    <div class="col-md-4">
                        <div class="widget-9 panel no-border bg-success no-margin widget-loader-bar">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="panel-heading  top-left top-right">
                                            <div class="panel-title text-black">
                                            </div>
                                            <div class="panel-controls">
                                                <ul>
                                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="p-l-20">
                                            <a href=" <?php echo base_url("index.php/superadmin/acptDetails/" . $pay_id . "/" . $pro_id) ?> ">
                                                <h4 class="no-margin p-b-5 text-white">AVERAGE ACCEPTANCE RATE</h4>                          
                                                <div style="font-size: 42px;margin-left: 10%;">
                                                    <?php echo $acpt_rate ?> %
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-bottom">
                                        <div class="progress progress-small m-b-20">
                                            <div class="progress-bar progress-bar-white" data-percentage="95.454545454545%" style="width: 95.4545%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;"></div>
                    </div>                    
                </div>
                <!--end box-->
                <div class="container-fluid container-fixed-lg bg-white">
                    <div class="panel panel-transparent">
                        <div class="panel-heading">                            
                            <div class="row clearfix">
                                <div class="">
                                    <div class="pull-right"><input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer">
                                <div class="table-responsive">
                                    <?php echo $this->table->generate(); ?>
                                </div><div class="row"></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_OK; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade slide-up disable-scroll in" id="billed" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">       
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                        <div class="modal-body col-xs-height  displayInvoiceData ">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>