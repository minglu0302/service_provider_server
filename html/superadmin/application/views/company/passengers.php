<?php
date_default_timezone_set('UTC');
$rupee = "$";
//error_reporting(E_ALL);
?>

<style>
    /*.dataTables_wrapper .dataTables_info{
            padding: 0;
        }*/
    .panel-body {
        overflow-x: auto;
    }
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    .imageborder{
        border-radius: 50%;
    }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>

    $(document).ready(function () {

        $('#editbusiness').click(function () {
            var fname = $('#fName').val();
            var lname = $('#lName').val();
            var code = $('#c_code').val();
            var mobile = $('#mobile').val();
            var email = $('#emailId').val();
            var cid = $('#cid').val();




            $.ajax({
                url: "<?php echo base_url('index.php/superadmin') ?>/EditCustomer",
                type: 'POST',
                data: {
                    cid: cid,
                    fName: fname,
                    lName: lname,
                    c_code: code,
                    mobile: mobile,
                    email: email

                },
                dataType: 'JSON',
                success: function (response)
                {
                    $('.close').trigger('click');
//                        window.location = "<?php echo base_url(); ?>index.php/superadmin/sgroup";
                }
            });

        });



        var table = $('.tableWithSearch1');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "order": [[0, "desc"]]
        };
        table.dataTable(settings);

//         $('#search-table').keyup(function() {
//                table.fnFilter($(this).val());
//            });

        $("#chekdel").click(function () {
            $("#display-data").text("");
            var val = [];
            $('.checkbox:checked').each(function (i) {
                val[i] = $(this).val();
            });
            if (val.length > 0) {
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#deletedriver');
                if (size == "mini")
                {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#deletedriver').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorbox").text(<?php echo json_encode(POPUP_PASSENGERS_DELETE); ?>);
                $("#yesdelete").click(function () {
                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/deletePatient",
                        type: "POST",
                        data: {masterid: val},
                        success: function (result) {
                            $('.checkbox:checked').each(function (i) {
                                $(this).closest('tr').remove();
                            });
                            $(".close").trigger('click');
                        }
                    });
                });
            } else {
                //     alert("Please mark any one of options");
                $("#display-data").text(<?php echo json_encode(POPUP_PASSENGERS_ATLEAST); ?>);
            }
        });

        $('#btnStickUpSizeToggler').click(function () {
            $("#display-data").text("");
            var val = [];
            $('.checkbox:checked').each(function (i) {
                val[i] = $(this).val();
            });



            if (val.length == 0) {
                //     alert("please select any one to reset the password");
                $("#display-data").text(<?php echo json_encode(POPUP_PASSENGERS_ANYONEPASS); ?>);
            } else if (val.length > 1) {
                //        alert("please select only one to reset the password");
                $("#display-data").text(<?php echo json_encode(POPUP_PASSENGERS_ONLYONEPASS); ?>);
            } else
            {




                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#myModal');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#myModal').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }

            }
        });







        $("#insertpass").click(function () {
            $("#errors").text("");
            var newpass = $(".newpass").val();
            var confirmpass = $(".confirmpass").val();
            var reg = /^\S*(?=\S*[a-zA-Z])(?=\S*[0-9])\S*$/;    //^[-]?(?:[.]\d+|\d+(?:[.]\d*)?)$/;



            if (newpass == "" || newpass == null)
            {
//                alert("please enter the new password");
                $("#errorspass").text(<?php echo json_encode(POPUP_PASSENGERS_PASSNEW); ?>);
            } else if (!reg.test(newpass))
            {
//                alert("please enter the password with atleast one chareacter and one letter");
                $("#errorspass").text(<?php echo json_encode(POPUP_PASSENGERS_PASSVALID); ?>);
            } else if (confirmpass == "" || confirmpass == null)
            {
//                alert("please confirm the password");
                $("#errorspass").text(<?php echo json_encode(POPUP_PASSENGERS_PASSCONFIRM); ?>);
            } else if (confirmpass != newpass)
            {
                //  alert("please confirm the same password");
                $("#errorspass").text(<?php echo json_encode(POPUP_PASSENGERS_SAMEPASSCONFIRM); ?>);
            } else
            {

                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/insertpass",
                    type: 'POST',
                    data: {
                        newpass: newpass,
                        val: $('.checkbox:checked').val()
                    },
                    success: function (response)
                    {
                        if (response.flag != 1) {
                            $(".close").trigger('click');
                            var size = $('input[name=stickup_toggler]:checked').val()
                            var modalElem = $('#confirmmodelss');
                            if (size == "mini")
                            {
                                $('#modalStickUpSmall').modal('show')
                            } else
                            {
                                $('#confirmmodelss').modal('show')
                                if (size == "default") {
                                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                                } else if (size == "full") {
                                    modalElem.children('.modal-dialog').addClass('modal-lg');
                                }
                            }
                            $("#errorboxdatass").text(<?php echo json_encode(POPUP_DRIVERS_NEWPASSWORD); ?>);
                            $("#confirmedss").hide();
                            $(".newpass").val('');
                            $(".confirmpass").val('');
                        } else if (response.flag == 1)
                        {
                            alert("this password already exists. Enter new password");
                            $("#errorboxdatass").text(<?php echo json_encode(POPUP_DRIVERS_NEWPASSWORD_FAILED); ?>);
                        }
                        $("#confirmedss").hide();
//                        location.reload();

                    }

                });
            }

        });

        $('.error-box-class').keypress(function () {
            $('.error-box').text('');
        });


        $("#inactive").click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).
                    get();
            if (val.length > 0) {

                //      if (confirm("Are you sure to inactive " + val.length + " passengers"))
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodel');
                if (size == "mini")
                {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#confirmmodel').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdata").text(<?php echo json_encode(POPUP_PASSENGERS_DEACTIVAT); ?>);

                $("#confirmed").click(function () {

                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/inactivepassengers",
                        type: "POST",
                        data: {val: val},
                        dataType: 'json',
                        success: function (result)
                        {

                            $('.checkbox:checked').each(function (i) {
                                $(this).closest('tr').remove();
                            });
                            $(".close").trigger('click');
                        }
                    });


                });
            } else
            {
                //      alert("select atleast one passenger");
                $("#display-data").text(<?php echo json_encode(POPUP_PASSENGERS_ATLEAST); ?>);
            }

        });


        $("#active").click(function () {
            $("#confirmeds").css('display', 'block');
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).
                    get();
            if (val.length > 0) {
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodels');
                if (size == "mini")
                {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#confirmmodels').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdatas").text(<?php echo json_encode(POPUP_PASSENGERS_ACTIVAT); ?>);

                $("#confirmeds").click(function () {

                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/activepassengers",
                        type: "POST",
                        data: {val: val},
                        dataType: 'json',
                        success: function (result)
                        {

                            $('.checkbox:checked').each(function (i) {
                                $(this).closest('tr').remove();
                            });
                            $(".close").trigger('click');
                        }
                    });


                });
            } else
            {
                // alert("select atleast one passenger");
                $("#display-data").text(<?php echo json_encode(POPUP_PASSENGERS_ATLEAST); ?>);
            }

        });




    });


</script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#active').hide();


        var status = '<?php echo $status; ?>';

        if (status == 3) {
            $('#active').hide();
            $('#inactive').show();

            $('#btnStickUpSizeToggler').show();
            $("#display-data").text("");
        }

        $('.whenclicked li').click(function () {
            $('#active').hide();

            // alert($(this).attr('id'));
            if ($(this).attr('id') == 3) {
                $('#inactive').show();
                $('#active').hide();
                $('#btnStickUpSizeToggler').show();
                $("#display-data").text("");

            } else if ($(this).attr('id') == 1) {
                $('#inactive').hide();
                $('#active').show();
                $('#btnStickUpSizeToggler').show();
                $("#display-data").text("");
            }


        });



        var table = $('#big_table');


        $('#big_table_processing').show();
        $('#active').hide();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/dt_passenger/' + status,
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "aoColumns": [
                {"sWidth": "2%", "sClass": "text-center"},
                {"sWidth": "8%"},
                {"sWidth": "6%"},
                {"sWidth": "10%"},
                {"sWidth": "8%"},
                {"sWidth": "5%", "sClass": "text-center"},
                {"sWidth": "5%", "sClass": "text-center"},
                {"sWidth": "5%", "sClass": "text-center"},
                {"sWidth": "2%", "sClass": "text-center"},
            ],
            "order": [[0, "desc"]],
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };




        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });



        $('.changeMode').click(function () {
            $('#active').hide();
            var table = $('#big_table');
            $('#big_table_processing').show();

            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": $(this).attr('data'),
//                "bJQueryUI": true,
//                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                    $('#big_table_processing').hide();
                },
                "drawCallback": function () {
                    $('#big_table_processing').hide();
                },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                },
            };

            $('.tabs_active').removeClass('active');

            $(this).parent().addClass('active');



            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
                $('#big_table_processing').show();
                table.fnFilter($(this).val());
            });

        });


    });
</script>

<script>
    function isNumberKey(evt)
    {
        $("#mobify").text("");
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 45 || charCode > 57)) {
            //    alert("Only numbers are allowed");
            $("#mobify").text(<?php echo json_encode(LIST_COMPANY_MOBIFY); ?>);
            return false;
        }
        return true;
    }
    function editCustomer(data) {
        $('#editModal').modal('show');

        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/EditGetCustomer",
            type: 'POST',
            data: {
                cid: (data),
            },
            dataType: 'JSON',
            success: function (response)
            {


                $("#cid").val(response.patient_id);
                $("#fName").val(response.first_name);
                $("#lName").val(response.last_name);

                var s = response.country_code;
                while (s.charAt(0) === '+')
                    s = s.substr(1);
                $("#c_code").val(s);
                $("#mobile").val(response.phone);
                $("#emailId").val(response.email);
//                $("#bannerimg").attr("href", "<?php echo base_url() ?>../../pics/" + result.banner_img);
                $("#bannerimg").attr("href",response.profile_pic);


            }
        });
    }
</script>

<style>
    .exportOptions{
        display: none;
    }
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
</style>


<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">CUSTOMERS</a>
            </li>

        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white whenclicked li" role="tablist" data-init-reponsive-tabs="collapse">
                    <li id="3" class="tabs_active <?php echo ($status == 3 ? "active" : ""); ?>" style="cursor:pointer">
                        <a class="changeMode"  data="<?php echo base_url(); ?>index.php/superadmin/dt_passenger/3"><span><?php echo LIST_ACCEPTED; ?></span></a>
                    </li>
                    <li id="1" class="tabs_active <?php echo ($status == 1 ? "active" : ""); ?>" style="cursor:pointer">
                        <a  class="changeMode"  data="<?php echo base_url(); ?>index.php/superadmin/dt_passenger/1"><span><?php echo LIST_REJECTED; ?> </span></a>
                    </li>



                    <div class="pull-right m-t-10"><button class="btn btn-info btn-cons" id="btnStickUpSizeToggler" ><?php echo BUTTON_RESETPASSWORD; ?></button></div>

                    <div class="pull-right m-t-10"><button class="btn btn-danger btn-cons" id="chekdel"><?php echo BUTTON_DELETE; ?></button></div>

                    <div class="pull-right m-t-10"><button class="btn btn-warning btn-cons" id="inactive"><?php echo BUTTON_REJECT; ?></button></div>


                    <div class="pull-right m-t-10"><button class="btn btn-success btn-cons" id="active"><?php echo BUTTON_ACCEPT; ?></button></div>

                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"></div>
                                <div class="error-box" id="display-data" style="text-align:center"></div>
                                <div id="big_table_processing" class="dataTables_processing" style=""><img src="http://iserve.ind.in/pics/ajax-loader_dark.gif"></div>
                                <div class='pull-right cls110'>

                                    <div class="pull-right">
                                        <input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH; ?>"> </div>
                                </div>

                            </div>
                            <br>
                            <div class="panel-body no-padding">
                                <?php echo $this->table->generate(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







    <div class="modal fade stick-up" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                  
                    <div class="modal-header">

                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>

                        </div>
                        <h3> <?php echo LIST_RESETPASSWORD_HEAD; ?></h3>
                    </div>


                    <br>
                    <br>

                    <div class="modal-body">




                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_NEWPASSWORD; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="text"  id="newpass" name="latitude"  class="newpass form-control error-box-class" placeholder="eg:g3Ehadd">
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_CONFIRMPASWORD; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="text"  id="confirmpass" name="longitude" class="confirmpass form-control error-box-class" placeholder="H3dgsk">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4 error-box" id="errorspass" ></div>
                            <div class="col-sm-4" >
                                <button type="button" class="btn btn-primary pull-right" id="insertpass" ><?php echo BUTTON_SUBMIT; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
        </button>
    </div>

    <div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">
                    <div class="row">

                        <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                    </div>
                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmed" ><?php echo BUTTON_YES; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">
                    <div class="row">

                        <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                    </div>
                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_YES; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade stick-up" id="deletedriver" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                </div>
                <br>
                <div class="modal-body">
                    <div class="row">
                        <div class="error-box" id="errorbox" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>
                    </div>
                </div>
                <br>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div><div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="yesdelete" ><?php echo BUTTON_YES; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade stick-up" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-header">
                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                        </div>
                        <h3 id='modalHeading'>Edit Customer</h3>
                    </div>
                    <br/>
                    <div class="modal-body">
                        <form id="addentity" class="form-horizontal" role="form" action="<?php echo base_url(); ?>index.php/superadmin/EditCustomer" method="post" enctype="multipart/form-data" >                 
                            <input name="fdata[group_id]" type="hidden" id="egid"/>                                


                            <br>
                            <br>
                            <div class="form-group" class="formex">
                                <div class="frmSearch">
                                    <label for="group_name" class="col-sm-4 control-label">First Name<span style="color:red;font-size: 18px">*</span></label>
                                    <div class="col-sm-8">
                                        <input  type="text" id="fName" name="fName"  placeholder="Enter First Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                        <div id="suggesstion-box"></div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>                    
                            <div class="form-group" class="formex">
                                <div class="frmSearch">
                                    <label for="group_name" class="col-sm-4 control-label">Last Name<span style="color:red;font-size: 18px">*</span></label>
                                    <div class="col-sm-8">
                                        <input  type="text" id="lName" name="lName"  placeholder="Enter Last Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                        <div id="suggesstion-box"></div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>                    
                            <div class="form-group" class="formex">
                                <div class="frmSearch">
                                    <label for="group_name" class="col-sm-4 control-label">Mobile<span style="color:red;font-size: 18px">*</span></label>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="input-group">
                                                    <span class="input-group-addon">+</span>
                                                    <input type="text" id="c_code" name="c_code" required="required" class="form-control" onkeypress="return isNumberKey(event)">
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" id="mobile" name="mobile" required="required" class="form-control" onkeypress="return isNumberKey(event)" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>                    

                            <div class="form-group" class="formex">
                                <div class="frmSearch">
                                    <label for="group_name" class="col-sm-4 control-label">Email<span style="color:red;font-size: 18px">*</span></label>
                                    <div class="col-sm-8">
                                        <input  type="text" id="emailId" name="email"  placeholder="Enter Email Address" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                        <div id="suggesstion-box"></div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>         
                            <div class="form-group" class="formex">
                                <div class="frmSearch">
                                    <label for="address" class="col-sm-4">Upload Photo<span style="color:red;font-size: 18px">*</span></label>
                                    <div class="col-sm-8">
                                            <!--                <input type="text" class="form-control" name="entitydocname" id="entitydocname">-->
                                        <input type="file" class="form-control" style="height: 37px;" name="photos" id="file_upload">

                                       
                                        <a target="_blank" id="bannerimg" href="">view</a> 
                                    </div>
                                    <div class="col-sm-3 error-box" id="file_driver_photo"></div>
                                </div>
                            </div>
                            <br>
                            <br>     
                            <input type="hidden" id="cid" name="cid">
                            <div class="row">
                                <div class="col-sm-4" ></div>
                                <div class="col-sm-4 error-box" id="clearerror"></div>
                                <div class="col-sm-4" >
                                    <button type="submit" class="btn btn-primary pull-right" id="editbusinessx" >EDIT</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
        </button>
    </div>