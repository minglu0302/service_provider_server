<?php

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");

require_once '/var/www/html/Models/StripeModule.php';
require_once '/var/www/html/Models/config_1.php';
require_once '/var/www/html/Models/sendAMail.php';
require_once '/var/www/html/Models/mandrill/src/Mandrill.php';
require 'aws.phar';
require_once 'StripeModuleNew.php';
require_once 'AwsPush.php';
require_once('S3.php');
//require_once '/var/www/html/Models/Models/AwsPush.php';

class Superadminmodal extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
//        $this->load->model('mastermodal');
        $this->load->database();
    }

//    function validateSuperAdmin() {
//
//        $email = $this->input->post("email");
//        $password = $this->input->post("password");
//        $offset = $this->input->post("offset");
//
//        $queryforslave = $this->db->get_where('superadmin', array('username' => $email, 'password' => md5($password)));
//        $res = $queryforslave->row();
//
//        if ($queryforslave->num_rows > 0) {
//            $tablename = 'company_info';
//            $LoginId = 'company_id';
//            $sessiondata = $this->setsessiondata($tablename, $LoginId, $res, $email, $password, $offset);
//            $this->session->set_userdata($sessiondata);
//            $this->load->library('nativesession');
//            $_SESSION['admin'] = 'super';
//            return true;
//        }
//
//        return false;
//    }
function validateSuperAdmin() {

        $email = $this->input->post("email");
        $password = $this->input->post("password");

        $this->load->library('mongo_db');
        $result = $this->mongo_db->get_one('admin_users', array('email' => $email, 'pass' => md5($password)));
        $role = 'Super Admin';
        if ($result['role'] != $role) {
            $role = $this->mongo_db->get_one('admin_roles', array('_id' => new MongoId($result['role'])));
            $role = $role['role_name'];
        }
        $_SESSION['admin'] = 'super';
        if (count($result) > 0) {
            $tablename = 'company_info';
            $LoginId = 'company_id';
            $sessiondata = array(
                'emailid' => $email,
                'password' => $password,
                'LoginId' => $LoginId,
                'profile_pic' => $result['logo'],
                'first_name' => $result['name'],
                'access_rights' => $result['access'],
                'table' => $tablename,
                'city_id' => $result['city'],
                'city' => $result['city'],
                'role' => $role,
                'company_id' => '0',
                'validate' => true,
                'superadmin' => '1',
                'mainAdmin' => $result['superadmin']
            );

            $this->session->set_userdata($sessiondata);
            return true;
        }

        return false;
    }
    function ForgotPassword() {

        $useremail = $this->input->post('resetemail');
        $resetlink = '';
        $query = $this->db->query("SELECT * FROM superadmin where username='" . $useremail . "' ");
        foreach ($query->result() as $row) {
            $name = $row->username;
        }
        $this->db->where('username', $useremail);
        $this->db->update('superadmin', array('resetData' => $resetlink));

        header('Content-Type: application/json');

        if ($query->num_rows() > 0) {
            $rlink = md5(mt_rand());
            $resetlink = base_url() . "index.php/superadmin/VerifyResetLink/" . $rlink;
            $template = "<h3> Click below link to reset your password</h3><br><a href=" . $resetlink . ">Click Here</a>";
//            $to[] = array(
//                'email' => $useremail,
//                'name' => $name,
//                'type' => "to");
            $to = array($useremail => 'Admin');
            $subject = "Reset Password Link for SuperAdmin";

            $config = new config();
            $sendMail = new sendAMail($config->getHostUrl());
            $status = $sendMail->mailFun($to, $subject, $template);
//            print_r($status);

            $this->db->where('username', $useremail);
            $this->db->update('superadmin', array('resetData' => $resetlink));

            return 1;
        } else
            return 0;
    }

    //* naveena models *//




    function dt_passenger($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');



//        $this->datatables->select("s.patient_id as rahul,s.first_name,s.last_name,s.phone,s.email,s.created_dt,s.sign_dt,s.sign_url,s.profile_pic,"
//                        . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' END) from user_sessions where oid = rahul and user_type = 2 order by oid DESC limit 0,1) as dtype", FALSE)
//              

        $this->datatables->select("s.patient_id as rahul,s.first_name,s.last_name,s.phone,s.email,s.created_dt,s.sign_dt,s.sign_url,s.profile_pic", FALSE)
                ->edit_column('s.first_name', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/cust_details/$1">$2</a>', 'rahul,s.first_name')
//                ->unset_column('dtype')
//                ->unset_column('s.sign_url')
                ->unset_column('s.profile_pic')
                ->edit_column('s.created_dt', "get_formatedDate/$1", 's.created_dt')
                // ->add_column('SIGN' , '<img onerror="' . base_url() . '../../pics/error.png" data-src="$1" data-src-retina="$1" alt="$1" onclick="openimg(this)" src="$1" style="height:70px;width:70px;cursor:pointer;"  width="50px" class="imageborder">' , 's.sign_url')
//                ->add_column('SIGN' , '$1' , 's.sign_url')
//                ->add_column('SIGN', '<img onerror="<img src="" data-src="$1" data-src-retina="$1" alt="$1" onclick="openimg(this)" src="$1" style="height:70px;width:70px;cursor:pointer;"  width="50px" class="imageborder">', 's.sign_url')
                ->edit_column('s.sign_url', 'get_formatedImage/$1', 's.sign_url')
//                ->add_column('PROFILE PIC', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 's.profile_pic')
                ->add_column('PROFILE PIC', 'get_formatedProfileImage/$1', 's.profile_pic')
//                ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="50px" >', 'dtype')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value= "$1"/>', 'rahul')
                ->from('patient s')
                ->where('s.status', $status);
        $this->db->order_by("rahul", "desc");

        echo $this->datatables->generate();
    }

    function get_joblogsdata($value = '') {

//        
        $m = new MongoClient();
        $this->load->library('mongo_db');

        $db1 = $this->mongo_db->db;



        $logs = $db1->selectCollection('driver_log');

//        $masId = $_REQUEST['MasId'];

        $mas = $this->db->query("select email from master where mas_id = '" . $value . "'")->row();

//        $getMasEmail = "select email from master where mas_id=" . $masId;
//        $result1 = mysql_query($getMasEmail, $db1->conn);
//        $mas = mysql_fetch_assoc($result1);



        $getAllLogs = $logs->find(array('mas_email' => $mas->email))->sort(array("on_time" => 1));
        foreach ($getAllLogs as $l) {
            $minimumTimeStamp = $l['on_time'];
            break;
        }
        $currentDate = date('Y-m-d');
        $startDate = date('Y-m-d', $minimumTimeStamp);

//                $date1 = date_create($currentDate);
//                $date2 = date_create($startDate);
//                $diff = date_diff($currentDate, $startDate);
        $diff = abs(strtotime($currentDate) - strtotime($startDate));
        $days = floor($diff / (60 * 60 * 24));
//                echo $startDate . '-' . $currentDate . '-' . $days;

        $NextDay = $startDate;
        $totalData = 0;
        $dataByDay = array();
        for ($i = 0; $i <= $days; $i++) {


            $startTime = strtotime($NextDay . ' 00:00:01');
            $endTime = strtotime($NextDay . ' 23:23:59');
//                    echo $startTime . '-' . $endTime . '-' . $NextDay . '-----';
            $getAllTodayLogs = $logs->find(array('mas_email' => $mas->email,
                'on_time' => array('$gte' => $startTime),
                'off_time' => array('$lte' => $endTime)));
            $dataByDay[$i]['Date'] = $NextDay;
            $getData = array();
            $ii = 0;

            $lat1 = 0;
            $long1 = 0;
            $lat2 = 0;
            $long2 = 0;
            $dictance = 0;
            foreach ($getAllTodayLogs as $oneDay) {

                foreach ($oneDay['location'] as $latlngs) {
                    if ($lat1 == 0 && $long1 == 0) {
                        $lat1 = $latlngs['latitude'];
                        $long1 = $latlngs['longitude'];
                    } else {
                        $lat2 = $latlngs['latitude'];
                        $long2 = $latlngs['longitude'];
                        $dictance+=(double) $this->distance($lat1, $long1, $lat2, $long2, 'M');
                    }
                }
//                        $oneDay['Distance'] = $dictance;
//                        $getData[] = $oneDay;
                $ii++;
            }
            $dataByDay[$i]['Distance'] = $dictance;
            $dataByDay[$i]['total'] = $ii;
            $totalData = $i;
            $date1 = str_replace('-', '/', $NextDay);
            $NextDay = date('Y-m-d', strtotime($date1 . "+1 days"));
        }
//                print_r($dataByDay);


        $getLogs = $logs->find(array('mas_email' => $mas->email))->sort(array("on_time" => -1));
        $count = 1;

        $data = array();

        for ($Count = $totalData; $Count >= 0; $Count--) {
            if ($dataByDay[$Count]['total'] > 0) {


//                echo '<tr>';
                $sr = $Count + 1;

                $data[] = array('sr' => $sr, 'Date' => $dataByDay[$Count]['Date'], 'total' => $dataByDay[$Count]['total'], 'distance' => number_format($dataByDay[$Count]['Distance'], 2, '.', ','), 'view' => '<input type="button" value="Log" id="' . $value . '!' . $dataByDay[$Count]['Date'] . '" onclick="viewLog(this);">');


//                echo '<td>' . $sr . '</td>';
//                echo '<td>' . $dataByDay[$Count]['Date'] . '</td>';
//                echo '<td>' . $dataByDay[$Count]['total'] . '</td>';
//                echo '<td>' . number_format($dataByDay[$Count]['Distance'], 2, '.', ',') . '</td>';
//                echo '<td><input type="button" value="Log" id="' . $masId . '!' . $dataByDay[$Count]['Date'] . '" onclick="viewLog(this);"></td>';
//                echo '</tr>';
            }
        }

        return $data;
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    function get_sessiondetails($value = '') {

        $db1 = new ConDB();
        $logs = $db1->mongo->selectCollection('driver_log');
        $exp = explode('!', $_REQUEST['MasId']);
        $MasId = $exp[0];
        $Date = $exp[1];



        $startTime = strtotime($Date . ' 00:00:01');
        $endTime = strtotime($Date . ' 23:23:59');
        $getAllTodayLogs = $logs->find(array('mas_email' => $mas['email'],
            'on_time' => array('$gte' => $startTime),
            'off_time' => array('$lte' => $endTime)));

        $getLogs = $logs->find(array('mas_email' => $mas['email']))->sort(array("on_time" => -1));
        $count = 1;


        foreach ($getAllTodayLogs as $log) {
            $lat1 = 0;
            $long1 = 0;
            $lat2 = 0;
            $long2 = 0;
            $dictance = 0;
            echo '<tr>';
            echo '<td>' . $count . '</td>';
//                    echo '<td>' . $log['mas_email'] . '</td>';
            echo '<td>' . date('Y, M dS g:i a', $log['on_time']) . '</td>';
            if ($log['off_time'] == '') {
                echo '<td>Online Now</td>';
                echo '<td>-</td>';
            } else {
                echo '<td>' . date('Y, M dS g:i a', $log['off_time']) . '</td>';
                $diff = $log['off_time'] - $log['on_time'];
                $tm = explode('.', number_format(($diff / 60), 2, '.', ','));
                if ($tm[1] > 60) {
                    $tm[0] ++;
                    $tm[1] = $tm[1] - 60;
                }
                if (strlen($tm[1]) == 1) {
                    $tm[1] = $tm[1] . '0';
                }
                echo '<td>' . $tm[0] . ':' . $tm[1] . ' Mins' . '</td>';
            }

            //calculate distnce
            foreach ($log['location'] as $latlngs) {
                if ($lat1 == 0 && $long1 == 0) {
                    $lat1 = $latlngs['latitude'];
                    $long1 = $latlngs['longitude'];
                } else {
                    $lat2 = $latlngs['latitude'];
                    $long2 = $latlngs['longitude'];
//                            $gotDis = GetDrivingDistance($lat1, $lat2, $long1, $long2);
//                            $dictance+=(double) $gotDis['distance'];
                    $dictance+=(double) distance($lat1, $long1, $lat2, $long2, 'M');
                }
            }
            echo '<td>' . number_format($dictance, 2, '.', ',') . ' Miles</td>';
            echo '</tr>';
            $count++;
        }
    }

    function addcountry() {

        $var = $this->input->post('data2');
        $string = strtoupper($var);

        $query = $this->db->query("select * from country where Country_Name= '" . $string . "'");
        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => "country already exists", 'flag' => 0));
            return;
        } else {


            $data2 = $this->input->post('data2');
            $string = strtoupper($data2);

            $this->db->query("insert into country(Country_Name)  values('" . $string . "')");

            $countryId = $this->db->insert_id();

            if ($countryId > 0) {
                echo json_encode(array('msg' => "country added successfully", 'flag' => 0, 'id' => $countryId));
                return;
            } else {
                echo json_encode(array('msg' => "Unable to add country", 'flag' => 1));
                return;
            }
        }
    }

    function deletecity() {

        $query = $this->input->post('val');



        foreach ($query as $rowid) {

            $this->db->query("select * from city_available where City_Id ='" . $rowid . "'");



            if ($this->db->affected_rows() > 0) {



                $this->db->query("delete from city_available where City_Id ='" . $rowid . "'");
                echo json_encode(array("msg" => "your selected cities deleted successfully", "flag" => 0));
                return;
            } else {
                echo json_encode(array("msg" => "your selected cities not deleted,retry!", "flag" => 1));
                return;
            }
        }
    }

    function get_vehivletype() {
        $query = $this->db->query("select * from workplace_types order by type_name")->result();
        return $query;
    }

    function get_company() {
        $query = $this->db->query("select * from company_info where Status = 3 order by companyname")->result();
        return $query;
    }

    function insert_payment($mas_id = '') {
  $this->load->library('mongo_db');
        $currunEarnigs = $this->input->post('currunEarnigs');
        $amoutpaid = $this->input->post('paid_amount');
        $curuntdate = $this->input->post('ctime');
        $closingamt = $currunEarnigs - $amoutpaid;
        $lastAppointmentId = $this->input->post('last_unsettled_appointment_id');

//         $userType = $this->mongo_db->get_one('location', array('user' => (int) $mas_id));
//        $getWhere = $this->db->get_where("doctor", array('doc_id' => $mas_id))->result_array();
         
//        echo '1';
            $stripe = new StripeModuleNew();
            
            $userType = $this->mongo_db->get_one('location', array('user' => (int) $mas_id));
//            print_r($userType);
            if (!$userType['StripeAccId']) {
//                  echo '1';
                return array("error" => "Bank Details Not Added", 'test' => $userType);
            }
//            echo 's2' ;die;
            $InStripetransfer = $stripe->apiStripe('CreateTransfer', array('amount' => ((float) $amoutpaid * 100), 'description' => "Payroll Trigger.", 'currency' => 'USD', 'CONNECTED_STRIPE_ACCOUNT_ID' => $userType['StripeAccId']));
//            print_r($InStripetransfer);
//            echo '2s';die;
            if ($InStripetransfer['error']) {
                $this->session->set_userdata(array('pay_error' => $InStripetransfer['error']['message']));
//                return;
                return array("error" => "Please update the account details for the driver to transfer");
            }
            $transferid = $InStripetransfer['id'];

//        if ($getWhere[0]['stripe_id'] == '') {
//            return array("error" => "Please update the account details for the driver to transfer");
//        }
//        $config = new config();
//
//        $stripe = new StripeModule($config->getStripeSecretKey());
//
//        $transfer = $stripe->apiStripe('createTransfer', array('amount' => ((int) $amoutpaid * 100), 'currency' => 'usd', 'recipient' => $getWhere[0]['stripe_id'], 'description' => 'From PRIVEMD', 'statement_description' => 'PriveMD PAYROLL'));
//
//        if ($transfer['error']) {
//            return array("error" => $transfer['error']['message']);
//        }

        $query = "insert into payroll(mas_id,opening_balance,pay_date,pay_amount,closing_balance,due_amount,trasaction_id) VALUES (
        '" . $mas_id . "',
        '" . $currunEarnigs . "',
        '" . $curuntdate . "',
        '" . $amoutpaid . "',
        '" . $closingamt . "','" . $closingamt . "','" . $transferid . "')";
        $this->db->query($query);



        if ($this->db->insert_id() > 0) {

            $this->db->query("update appointment set settled_flag = 1 where appointment_id <= '" . $lastAppointmentId . "' and doc_id = '" . $mas_id . "' and settled_flag = 0 and status = 7 and payment_status IN (1,3)");
            if ($this->db->affected_rows() > 0) {
                return array("msg" => "Success");
            } else {
                return array("error" => "Error1");
            }
        } else {
            return array("error" => "Error2");
        }
    }

//    function insert_payment($mas_id = '') {
//
//
////        $total_earningsdata = $this->db->query('select sum(mas_earning) as total_mas_earning,(select appointment_id from appointment where mas_id ="' . $mas_id . '" and mas_earning != 0 and settled_flag = 0 order by appointment_id DESC limit 0,1) as last_appointment_id from appointment where mas_earning != 0 and settled_flag = 0 and mas_id ="' . $mas_id . '"')->row_array();
////
////        $total_earning_upto = round($total_earningsdata['total_mas_earning'], 2);
////        $lat_appointment_id = $total_earningsdata['last_appointment_id'];
//
//        $currunEarnigs = $this->input->post('currunEarnigs');
//        $lastAppointmentId = $this->input->post('last_unsettled_appointment_id');
//
//        $amoutpaid = $this->input->post('paid_amount');
//
//        $getWhere = $this->db->get_where("doctor", array('doc_id' => $mas_id))->result_array();
//
//        if ($getWhere[0]['stripe_id'] == '') {
//            return array("error" => "Please update the account details for the driver to transfer");
//        }
//        $config = new config();
//
//        $stripe = new StripeModule($config->getStripeSecretKey());
//
//        $transfer = $stripe->apiStripe('createTransfer', array('amount' => ((int) $amoutpaid * 100), 'currency' => 'usd', 'recipient' => $getWhere[0]['stripe_id'], 'description' => 'From PRIVEMD', 'statement_description' => 'PriveMD PAYROLL'));
//
//        if ($transfer['error']) {
//            return array("error" => $transfer['error']['message']);
//        }
//
//        $curuntdate = $this->input->post('ctime');
//        $closingamt = $currunEarnigs - $amoutpaid;
//
//        $getOpeningBal = $this->db->query("select closing_balance from payroll where mas_id = '" . $mas_id . "' order by payroll_id DESC limit 0,1")->row_array();
//
//        $query = "insert into payroll(mas_id,opening_balance,pay_date,pay_amount,closing_balance,due_amount,trasaction_id) VALUES (
//        '" . $mas_id . "',
//        '" . (double) $getOpeningBal['closing_balance'] . "',
//        '" . $curuntdate . "',
//        '" . $amoutpaid . "',
//        '" . $closingamt . "','" . $closingamt . "','" . $transfer->id . "')";
//        $this->db->query($query);
//
//        if ($this->db->insert_id() > 0) {
//
//            $this->db->query("update appointment set settled_flag = 1 where appointment_id <= '" . $lastAppointmentId . "' and doc_id = '" . $mas_id . "' and settled_flag = 0 and status = 9 and payment_status IN (1,3)");
//            if ($this->db->affected_rows() > 0) {
//                return array("msg" => "Success");
//            } else {
//                return array("error" => "Error1");
//            }
//        } else {
//            return array("error" => "Error2");
//        }
//
//
////        echo $query;
////        exit();
//    }

    function addcity() {
        $countryid = $this->input->post('countryid');

        $data3 = $this->input->post('data3');
        $data = $this->input->post('data');
        $existcity = '';
        $getcityname = $this->db->query("select * from city where  City_Name = '" . $data3 . "' and Country_Id='" . $countryid . "'");

        if ($getcityname->num_rows() > 0) {

//            $this->db->query("insert into city(Country_Id,City_Name,Currency) values('$countryid','$data3','$data')");
            echo json_encode(array('msg' => "city already exists", 'flag' => 1));
            return;
        } else {
            $this->db->query("insert into city(Country_Id,City_Name,Currency) values('$countryid','$data3','$data')");
            if ($this->db->affected_rows() > 0) {
                echo json_encode(array('msg' => "city added successfully", 'flag' => 0));
                return;
            }
//            else {
//                echo json_encode(array('msg' => "city already exists", 'flag' => 1));
//                return;
//            }
        }

        // }
    }

    function activate_company() {
        $val = $this->input->post('val');
        foreach ($val as $result) {
            $this->db->query("update company_info set status=3  where company_id='" . $result . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected company/companies activated succesfully", 'flag' => 1));
            return;
        }
    }

    function activate_vehicle() {
        $val = $this->input->post('val');
        foreach ($val as $result) {
            $this->db->query("update workplace set status=2  where workplace_id='" . $result . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected vehicle/vehicles activated succesfully", 'flag' => 1));
            return;
        }
    }

    function reject_vehicle() {
        $val = $this->input->post('val');
//        foreach ($val as $result) {
//            $this->db->query("update workplace set status=4  where workplace_id='" . $result . "'");
//        }
//        if ($this->db->affected_rows() > 0) {
//            echo json_encode(array('msg' => "your selected vehicle/vehicles rejected succesfully", 'flag' => 1));
//            return;
//        }

        foreach ($val as $result) {
            $this->db->query("update workplace set Status = 4 where workplace_id ='" . $result . "'");


            if ($this->db->affected_rows() > 0) {
                $getTokensQry = $this->db->query("select * from user_sessions where oid IN (select mas_id from master where workplace_id = '" . $result . "') and loggedIn = 1 and user_type = 1 and LENGTH(push_token) > 63")->result();
                $this->load->library('mongo_db');
                foreach ($getTokensQry as $token) {

                    if ($token->type == '2') {
                        $res [] = $this->sendAndroidPush(array($token->push_token), array('action' => 12, 'payload' => 'Your car has been suspended from admin, contact your company for more details'), 'AIzaSyBK7MVDQ-jm8GAd3BjmF0w2Z1_BjZ_qszA');
                    } else {
                        $amazon = new AwsPush();
                        $pushReturn2 = $amazon->publishJson(array(
                            'MessageStructure' => 'json',
                            'TargetArn' => $token->push_token,
                            'Message' => json_encode(array(
                                'APNS' => json_encode(array(
                                    'aps' => array('alert' => 'Rejected')
                                ))
                            )),
                        ));

                        if ($pushReturn2[0]['MessageId'] == '')
                            $ret[] = array('errorNo' => 44);
                        else
                            $ret[] = array('errorNo' => 46);
                    }

                    $query = "update appointment set status = '5',extra_notes = 'Admin rejected vehicle, so cancelled the booking',cancel_status = '8' where mas_id = '" . $token->oid . "' and status IN (6,7,8)";
                    $this->db->query($query);

                    $this->mongo_db->update('location', array("status" => 4, 'carId' => 0, 'type' => 0), array('user' => (int) $token->oid));

                    if ($token->workplace_id != 0) {
                        $this->db->query("update workplace set Status= 4   where workplace_id='" . $token->workplace_id . "'");
//                $this->mongo_db->update("location", array('carId' => 0), array('user' => (int) $token->oid));
                    }
                    $this->db->query("update user_sessions set loggedIn = 2 where oid = '" . $token->oid . "' and loggedIn = 1 and user_type = 1");
                }
            }
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected Vehicle rejected Successfully", 'flag' => 1, 'res' => $res));
            return;
        }

        // new modification
//        if ($_REQUEST['to_do'] == '4') {
//
//            $location = $db->mongo->selectCollection('location');
//            $newdata = array('$set' => array("status" => (int) $_REQUEST['to_do'], 'carId' => 0, 'type' => 0));
//
//            $nt = 12;
//            $passMsg = "Your car has been suspended from admin, contact your company for more details";
//
//            $getTokensQry = "select * from user_sessions where oid IN (select mas_id from master where workplace_id IN (" . implode(',', $_REQUEST['item_list']) . ")) and loggedIn = 1 and user_type = 1 and LENGTH(push_token) > 63";
//            $getTokensRes = mysql_query($getTokensQry, $db->conn);
////    echo $getTokensQry;
//
//            while ($token = mysql_fetch_assoc($getTokensRes)) {
//
//                $updateSession = "update user_sessions set loggedIn = 2 where sid = '" . $token['sid'] . "'";
//                mysql_query($updateSession, $db->conn);
//
//                $query = "update appointment set status = '5',extra_notes = 'Admin rejected vehicle, so cancelled the booking',cancel_status = '8' where mas_id = '" . $token['oid'] . "' and status IN (6,7,8)";
//                $res = mysql_query($query, $db->conn);
//
//                $location->update(array('user' => (int) $token['oid']), $newdata);
//
//                if ($token['type'] == '2') {
//                    $ret[] = sendAndroidPush(array($token['push_token']), array('action' => $nt, 'payload' => $passMsg), $db->driverAndroidPushApiKey);
//                } else {
//                    $ret[] = _sendApplePush(array($token['push_token']), array('nt' => $nt, 'alert' => $passMsg, 'sound' => 'default'), '1');
////                    $pushReturn2 = $pushWoosh->pushDriver($passMsg, array('nt' => $nt), array($token['push_token']));
////                    if ($pushReturn2['info']['http_code'] == 200)
////                        $ret[] = array('errorNo' => 44);
////                    else
////                        $ret[] = array('errorNo' => 46);
//                }
//            }
//        }
        // end of modification
    }

    function acceptdrivers() {
        $val = $this->input->post('val');
        $this->load->library('mongo_db');
        $data = $this->db->query('select * from  doctor where doc_id = "' . $val . '" and status = 3');

        foreach ($val as $result) {
            $this->mongo_db->update('location', array('accepted' => 1), array('user' => (int) $result));
            $this->db->query("update doctor set status = 3  where doc_id='" . $result . "' ");
            $getTokensQry = $this->db->query("select u.* from user_sessions u where u.oid IN (" . implode(',', $val) . ") and u.user_type = 1")->result();
            $token = $getTokensQry[0];
            if ($token->type == '2') {
                $res [] = $this->sendAndroidPush(array($token->push_token), array('action' => 13, 'payload' => 'Your profile has been Accepted on ' . Appname . ', contact ' . Appname . ' customer care'), 'AIzaSyD38JT3At1ZrfzuSBXRvDKPpkPWz6YY3aA');
            } else {
                $msg = "Your profile has been Accepted on PriveMd, contact PriveMd customer care";
                $aplPushContent = array('alert' => $msg, 'nt' => 13);
                $amazon = new AwsPush();
                $pushReturn2 = $amazon->publishJson(array(
                    'MessageStructure' => 'json',
                    'TargetArn' => $token->push_token,
                    'Message' => json_encode(array(
                        'APNS' => json_encode(array(
                            'aps' => $aplPushContent
                        ))
                    )),
                ));
                if ($pushReturn2[0]['MessageId'] == '')
                    $ret[] = array('errorNo' => 44);
                else
                    $ret[] = array('errorNo' => 46);
            }
            $dlist = $this->db->query('select first_name,email from  doctor where doc_id = "' . $result . '" and status = 3')->result();
            $to = array($dlist[0]->email => $dlist[0]->first_name);
            $subject = "Profile Accepted on" . Appname;
            $config = new config();
            $sendMail = new sendAMail($config->getHostUrl());
            $status = $sendMail->mailFun($to, $subject, $msg);
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected driver/drivers accepted succesfully", 'flag' => 1));
            return;
        }

        return $data;
    }

    function rejectdrivers() {
        $val = $this->input->post('val');

        $this->load->library('mongo_db');
        foreach ($val as $user) {
            $this->mongo_db->update('location', array('accepted' => 0), array('user' => (int) $user));
            $this->db->query("update doctor set status = '4' where doc_id IN (" . implode(',', $val) . ")");
            if ($this->db->affected_rows() > 0) {
                $this->mongo_db->update('location', array('status' => 4, 'carId' => 0, 'type' => 0), array('user' => (int) $user));
                $getTokensQry = $this->db->query("select u.* from user_sessions u where u.oid IN (" . implode(',', $val) . ") and u.loggedIn = 1 and u.user_type = 1")->result();
                foreach ($getTokensQry as $token) {
                    if ($token->type == '2') {
                        $res [] = $this->sendAndroidPush(array($token->push_token), array('action' => 12, 'payload' => 'Your profile has been suspended on ' . Appname . ', contact ' . Appname . ' customer care'), 'AIzaSyD38JT3At1ZrfzuSBXRvDKPpkPWz6YY3aA');
                    } else {
                        $amazon = new AwsPush();
                        $pushReturn2 = $amazon->publishJson(array(
                            'MessageStructure' => 'json',
                            'TargetArn' => $token->push_token,
                            'Message' => json_encode(array(
                                'APNS' => json_encode(array(
                                    'aps' => array('alert' => 'Rejected')
                                ))
                            )),
                        ));
                        if ($pushReturn2[0]['MessageId'] == '')
                            $ret[] = array('errorNo' => 44);
                        else
                            $ret[] = array('errorNo' => 46);
                    }
                }
                $this->db->query("update user_sessions set loggedIn = 2 where oid = '" . $user . "' and loggedIn = 1 and user_type = 1");
            }
            $dlist = $this->db->query('select first_name,email from  doctor where doc_id = "' . $user . '"')->result();
            $to = array($dlist[0]->email => $dlist[0]->first_name);
            $subject = 'Profile Rejected on ' . Appname;
            $msg = 'Your profile has been suspended on ' . Appname . ', contact ' . Appname . ' customer care';
            $config = new config();
            $sendMail = new sendAMail($config->getHostUrl());
            $status = $sendMail->mailFun($to, $subject, $msg);
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected driver/drivers rejected Successfully", 'flag' => 1, 'res' => $res));
            return;
        }
    }

    function sendAndroidPush($tokenArr, $andrContent, $apiKey) {
        $fields = array(
            'registration_ids' => $tokenArr,
            'data' => $andrContent,
        );

        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );
// Open connection
        $ch = curl_init();

// Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, 'http://android.googleapis.com/gcm/send');

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

// Execute post
        $result = curl_exec($ch);

        curl_close($ch);
//        echo 'Result from google:' . $result . '---';
        $res_dec = json_decode($result);

        if ($res_dec->success >= 1)
            return array('errorNo' => 44, 'result' => $result);
        else
            return array('errorNo' => 46, 'result' => $result);
    }

    function editdriverpassword() {
        $newpass = $this->input->post('newpass');
        $val = $this->input->post('val');
        $pass = $this->db->query("select first_name, email,password from doctor where doc_id='" . $val . "' ")->result();
        if ($pass[0]->password == md5($newpass)) {
            echo json_encode(array('msg' => "this password already exists. Enter new password", 'flag' => 1));
            return;
        } else {
            $this->db->query("update doctor set password = md5('" . $newpass . "') where doc_id = '" . $val . "' ");
            if ($this->db->affected_rows() > 0) {
                $config = new config();
                $details1 = array('first_name' => $pass[0]->first_name, 'email' => $pass[0]->email);
                $sendMail = new sendAMail($config->getHostUrl());
                $data = $sendMail->passwordChanged($details1, $newpass);
                echo json_encode(array('msg' => "your new password updated successfully", 'flag' => 0));
                return;
            }
        }
    }

    function editsuperpassword() {
        $newpass = $this->input->post('newpass');
        $currentpassword = $this->input->post('currentpassword');

        $pass = $this->db->query("select password from superadmin where id=1")->result();


        if (md5($currentpassword) != $pass['password']) {
            echo json_encode(array('msg' => "you have entered the incorrect current password", 'flag' => 2));
            return;
        } else {

            if ($pass['password'] == md5($newpass)) {
                echo json_encode(array('msg' => "this password already exists. Enter new password", 'flag' => 1));
                return;
            } else {
                $this->db->query("update superadmin set password = md5('" . $newpass . "') where id = 1 ");

                if ($this->db->affected_rows() > 0) {
                    echo json_encode(array('msg' => "your new password updated successfully", 'flag' => 0));
                    return;
                }
            }
        }
    }

    function editvehicle($status) {

        $data['vehicle'] = $this->db->query("select w.*,wt.city_id,v.id,v.vehiclemodel from  workplace w ,workplace_types wt,vehiclemodel v where workplace_id='" . $status . "' and w.type_id = wt.type_id and v.id = w.Vehicle_Model ")->result();

        $cityId = $data['vehicle'][0]->city_id;

        if ($cityId == '')
            return array('flag' => 1);

        $data['company'] = $this->db->query("select companyname,company_id from company_info where city = '" . $cityId . "'")->result();

        $data['cityList'] = $this->db->query("select City_Name,City_Id from city_available")->result();

        $data['workplaceTypes'] = $this->db->query("select * from workplace_types where city_id = '" . $cityId . "'")->result();

        $data['vehicleTypes'] = $this->db->query("select * from vehicleType")->result();




        $data['vehicleDoc'] = $this->db->query("select * from vechiledoc where vechileid = '" . $status . "'")->result();

        $this->load->library('mongo_db');


//        print_r($data);
//        exit();

        return $data;
    }

    function deactivate_company() {
        $val = $this->input->post('val');
        foreach ($val as $result) {
            $this->db->query("update company_info set status=5  where company_id='" . $result . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected company/companies deactivated succesfully", 'flag' => 1));
            return;
        }
    }

    function suspend_company() {
        $val = $this->input->post('val');
        foreach ($val as $result) {
            $this->db->query("update company_info set status=6  where company_id='" . $result . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected company/companies suspended succesfully", 'flag' => 1));
            return;
        }
    }

    function get_vehicle_data() {
        $query = $this->db->query("select w.*,cty.City_Name from workplace_types w, city_available cty where w.city_id = cty.City_Id")->result();

        return $query;
    }

    function logoutdriver() {
        $driverid = $this->input->post('driverid');
        $this->load->library('mongo_db');
        $this->db->query("update user_sessions  set loggedIn = 2 where user_type = '1' and oid = '" . $driverid . "' and loggedIn = 1");

        $this->mongo_db->update('location', array('status' => 4), array('user' => (int) $driverid));
    }

    function insert_vehicletype() {
        $vehicletype = $this->input->post('vehicletype');
        $seating = $this->input->post('seating');
        $minimumfare = $this->input->post('minimumfare');
        $basefare = $this->input->post('basefare');
        $priceperminute = $this->input->post('priceperminute');
        $priceperkm = $this->input->post('priceperkm');
        $discription = $this->input->post('discription');
        $city = $this->input->post('city');
        $cancilationfee = $this->input->post('cancilationfee');

        $resulrt = $this->db->query("insert into workplace_types(type_name,max_size,basefare,min_fare,price_per_min,
                     price_per_km,type_desc,city_id,cancilation_fee) values('" . $vehicletype . "',

                                                                    '" . $seating . "',

                                                                        '" . $basefare . "',
                                                                            '" . $minimumfare . "',
                                                                                '" . $priceperminute . "',
                                                                                    '" . $priceperkm . "',
                                                                                        '" . $discription . "',
                                                                                            '" . $city . "','" . $cancilationfee . "')");

        $type_id = $this->db->insert_id();




        $this->load->database();
        $cityData = $this->db->query("select * from city_available where city_id =  '" . $city . "'")->row_array();




        $this->load->library('mongo_db');

        $insertArr = array('type' => (int) $type_id, 'type_name' => $vehicletype, 'max_size' => (int) $seating, 'basefare' => (float) $basefare, 'min_fare' => (float) $minimumfare, 'price_per_min' => (float) $priceperminute, 'price_per_km' => (float) $priceperkm, 'type_desc' => $discription, 'city_id' => (int) $city, "location" => array("longitude" => (double) $cityData['City_Long'], "latitude" => (double) $cityData['City_Lat']));
        $this->mongo_db->insert('vehicleTypes', $insertArr);
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your vehicle type added succesfully", 'flag' => 1));
            return;
        }
    }

    function edit_vehicletype($param) {
        //   $city_id = $this->input->post('');

        $result = $this->db->query("select * from workplace_types where type_id='" . $param . "'")->result();
        //     $result = $this->db->query("select City_Id from city  where City_Name ='" . $param . "'")->result();
        //    $result = $this->db->query("update workplace_types set  where type_id='" . $param . "'")->result();
        return $result;
    }

    function update_vehicletype($param) {
        $vehicletype = $this->input->post('vehicletype');
        $seating = $this->input->post('seating');
        $minimumfare = $this->input->post('minimumfare');
        $basefare = $this->input->post('basefare');
        $priceperminute = $this->input->post('priceperminute');
        $priceperkm = $this->input->post('priceperkm');
        $discription = $this->input->post('discription');
        $city = $this->input->post('city');
        $cancilationfee = $this->input->post('cancilationfee');

        $fdata = array('type_name' => $vehicletype,
            'max_size' => (int) $seating,
            'basefare' => (float) $basefare, 'min_fare' => (float) $minimumfare,
            'price_per_min' => (float) $priceperminute,
            'price_per_km' => (float) $priceperkm, 'type_desc' => $discription,
            'city_id' => (int) $city,
        );
        //   $city_name = $this->db->query("select City_Name from city_available where City_Id = '" . $city . "'")->result();

        $result = $this->db->query("update workplace_types set type_name='" . $vehicletype . "',
                       max_size='" . $seating . "',
                       basefare='" . $basefare . "',
                       min_fare='" . $minimumfare . "',
                       price_per_min='" . $priceperminute . "',
                       price_per_km='" . $priceperkm . "',
                       type_desc='" . $discription . "',
                      city_id='" . $city . "',cancilation_fee = '" . $cancilationfee . "' where type_id='" . $param . "' ");

        $this->load->library('mongo_db');

        $this->mongo_db->update("vehicleTypes", $fdata, array("type" => (int) $param));




        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your vehicle type updated succesfully", 'flag' => 1));
            return;
        } else {
            echo json_encode(array('msg' => "your vehicle type not updated try again!", 'flag' => 0));
            return;
        }
    }

    function get_vehiclemake() {
        return $this->db->query("select * from vehicleType")->result();
    }

    function get_vehiclemodal() {
        return $this->db->query("select vm.*,vt.vehicletype from vehiclemodel vm,vehicleType vt where vm.vehicletypeid= vt.id")->result();
    }

    function vehiclemodal() {
        return $this->db->query("select *  from vehiclemodel order by vehiclemodel")->result();
    }

    function insert_typename() {
        $typename = $this->input->post('typename');

        $result = $this->db->query("insert into vehicleType(vehicletype) values('" . $typename . "')");
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your  type name added succesfully", 'flag' => 1));
            return;
        }
    }

    function deletetype() {
        $vehicleid = $this->input->post('vehicletypeid');

        $result = $this->db->query("delete from workplace_types where type_id ='" . $vehicleid . "'");
    }

    function deletecompany() {
        $companyid = $this->input->post('companyid');

//        $result = $this->db->query("delete from company_info where company_id ='" . $companyid . "' ");

        $affectedRows = 0;

        $deleteVehicleTypes = $this->db->query("delete from company_info where company_id = " . $companyid)->row_array();
        $affectedRows += $this->db->affected_rows();

        if ($affectedRows <= 0) {

            echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'Failed to delete'));
            return false;
        }

        $selectType = $this->db->query("select type_id from workplace where company_id = '" . $companyid . "'")->result();


        foreach ($selectType as $type) {
            $type_ids[] = (int) $type['type_id'];
        }

        $deleteAllVehicles = $this->db->query("delete from workplace_types where type_id  in (" . implode(',', $type_ids) . ")");
        $affectedRows += $this->db->affected_rows();

        $deleteAllVehicles = $this->db->query("delete from workplace where type_id  in (" . implode(',', $type_ids) . ")");
        $affectedRows += $this->db->affected_rows();


        $this->load->library('mongo_db');

        $return[] = $this->mongo_db->delete('vehicleTypes', array('type' => array('$in' => $type_ids)));

        $getAllDriversCursor = $this->mongo_db->get('vehicleTypes', array('type' => array('$in' => $type_ids)));

        $mas_id = array();

        foreach ($getAllDriversCursor as $driver) {
            $mas_id[] = (int) $driver['user'];
        }

        $return[] = $this->mongo_db->delete('location', array('user' => array('$in' => $mas_id)));

        $updateMysqlDriverQry = $this->db->query("delete from master where mas_id in (" . implode(',', $mas_id) . ")");
        $affectedRows += $this->db->affected_rows();

        $updateMysqlApptQry = $this->db->query("delete from appointment where mas_id in (" . implode(',', $mas_id) . ")");
        $affectedRows += $this->db->affected_rows();

        $updateMysqlReviewQry = $this->db->query("delete from passenger_rating where mas_id in (" . implode(',', $mas_id) . ")");
        $affectedRows += $this->db->affected_rows();

        $updateMysqlReviewQry = $this->db->query("delete from master_ratings where mas_id in (" . implode(',', $mas_id) . ")");
        $affectedRows += $this->db->affected_rows();

        $updateMysqlReviewQry = $this->db->query("delete from user_sessions where user_type = 1 and oid in (" . implode(',', $mas_id) . ")");
        $affectedRows += $this->db->affected_rows();

        echo json_encode(array('flag' => 0, 'affectedRows' => $deleteAllVehicles . $deleteVehicleTypes . $updateMysqlDriverQry));
    }

    function deletecountry() {
        $countryid = $this->input->post('countryid');

        $result = $this->db->query("delete from country where Country_Id ='" . $countryid . "'");
    }

    function deletepagecity() {
        $cityid = $this->input->post('cityid');

//        $result = $this->db->query("delete from city_available where City_Id ='" . $cityid . "'");

        $result = $this->db->query("select company_id from company_info where city = '" . $cityid . "'")->result();

        $companies = array();

        foreach ($result as $company) {
            $companies[] = $company->company_id;
        }

        $result1 = $this->db->query("select type_id from workplace_types where city_id = '" . $cityid . "'")->result();

        $vehicleTypes = array();

        foreach ($result1 as $company) {
            $vehicleTypes[] = $company->type_id;
        }
        $this->db->query("delete from city_available where City_Id = '" . $cityid . "'");

        $this->db->query("delete from company_info where company_id in (" . implode(',', $companies) . ")");

        $this->db->query("delete from dispatcher where city ='" . $cityid . "'");

        $this->db->query("delete from workplace where type_id in (" . implode(',', $vehicleTypes) . ")");

        $this->db->query("delete from workplace_types where type_id in (" . implode(',', $vehicleTypes) . ")");

        $this->db->query("delete from coupons where city_id ='" . $cityid . "'");

        $this->db->query("delete from master where company_id  in (" . implode(',', $companies) . ")");
    }

    function deletedriver() {
        $masterid = $this->input->post('masterid');

//        $result = $this->db->query("delete from master where mas_id ='" . $masterid . "'")->row_array();
        $this->load->library('mongo_db');
        $affectedRows = 0;

        foreach ($masterid as $row) {

            $getMasterDet = $this->db->query("select * from doctor where doc_id = '" . $row . "'")->row_array();

//        }
            if (!is_array($getMasterDet)) {

                echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'Driver not available'));
                return false;
            }
            $location = $this->mongo_db->delete('location', array('user' => (int) $row));
//
            $updateCarQry = $this->db->query("update workplace set status = 2 where workplace_id = '" . $getMasterDet['car_id'] . "'");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlDriverQry = $this->db->query("delete from doctor where doc_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

//            $updateMysqlApptQry = $this->db->query("delete from appointment where doc_id = '" . $getMasterDet['item_list'] . "'");
//            $affectedRows += $this->db->affected_rows();
//
//            $updateMysqlReviewQry = $this->db->query("delete from patient_rating where doc_id = '" . $getMasterDet['item_list'] . "'");
//            $affectedRows += $this->db->affected_rows();
//
//            $updateMysqlReviewQry = $this->db->query("delete from doctor_ratings where doc_id = '" . $getMasterDet['item_list'] . "'");
//            $affectedRows += $this->db->affected_rows();
//
//            $updateMysqlReviewQry = $this->db->query("delete from user_sessions where user_type = 1 and oid = '" . $getMasterDet['item_list'] . "'");
//            $affectedRows += $this->db->affected_rows();
            $updateMysqlApptQry = $this->db->query("delete from appointment where doc_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from patient_rating where doc_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from doctor_ratings where doc_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from user_sessions where user_type = 1 and oid = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();
        }

        echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'msg' => 'Process completed.'));
    }

    function makeonline() {
        $masterid = $this->input->post('masterid');
        $this->load->library('mongo_db');
        foreach ($masterid as $row) {
            $res = $this->mongo_db->update('location', array('status' => (int) 3), array('user' => (int) $row));
        }
        echo json_encode(array('flag' => 0, 'msg' => 'Status Updated'));
    }

    function makeOffline() {
        $masterid = $this->input->post('masterid');
        $this->load->library('mongo_db');
        foreach ($masterid as $row) {
            $res = $this->mongo_db->update('location', array('status' => (int) 4), array('user' => (int) $row));
        }
        echo json_encode(array('flag' => 0, 'msg' => 'Status Updated'));
    }

    function makeLogout() {
        $masterid = $this->input->post('masterid');
        $this->load->library('mongo_db');
        foreach ($masterid as $row) {
            $this->db->query("update user_sessions set loggedIn = '2' where oid = '" . $row . "' and user_type = '1'");
            $res = $this->mongo_db->update('location', array('status' => 5, 'login' => 2), array('user' => (int) $row));
        }
        echo json_encode(array('flag' => 0, 'msg' => 'Status Updated'));
    }

    function deletePatient() {
        $masterid = $this->input->post('masterid');
        $affectedRows = 0;
        foreach ($masterid as $row) {
            $getMasterDet = $this->db->query("select * from patient where patient_id = '" . $row . "'")->row_array();
            if (!is_array($getMasterDet)) {
                echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'patient not available'));
                return false;
            }

            $updateMysqlDriverQry = $this->db->query("delete from patient where patient_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

//            $updateMysqlApptQry = $this->db->query("delete from appointment where patient_id = '" . $getMasterDet['item_list'] . "'");
//            $affectedRows += $this->db->affected_rows();
//
//            $updateMysqlReviewQry = $this->db->query("delete from patient_rating where patient_id = '" . $getMasterDet['item_list'] . "'");
//            $affectedRows += $this->db->affected_rows();
//
//            $updateMysqlReviewQry = $this->db->query("delete from doctor_ratings where patient_id = '" . $getMasterDet['item_list'] . "'");
//            $affectedRows += $this->db->affected_rows();
//
//            $updateMysqlReviewQry = $this->db->query("delete from user_sessions where user_type = 2 and oid = '" . $getMasterDet['item_list'] . "'");
//            $affectedRows += $this->db->affected_rows();

            $updateMysqlApptQry = $this->db->query("delete from appointment where patient_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from patient_rating where patient_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from doctor_ratings where patient_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from user_sessions where user_type = 2 and oid = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();
        }
        echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'msg' => 'Process completed.'));
    }

    function deletemodal() {
        $modalid = $this->input->post('modalid');

        $result = $this->db->query("delete from vehiclemodel where id ='" . $modalid . "'");
    }

    function insert_modal() {
        $typeid = $this->input->post('typeid');

        $modal = $this->input->post('modal');

        $res = $this->db->query("insert into vehiclemodel(vehiclemodel,vehicletypeid) values('" . $modal . "','" . $typeid . "')");
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your  modal name added succesfully", 'flag' => 1));
            return;
        }
    }

    function deletevehicletype() {
        $val = $this->input->post('val');
        $this->load->library('mongo_db');
        foreach ($val as $row) {
//            $this->db->query("delete  from vehicleType where id = '" . $row . "' ");

            $affectedRows = 0;


            $deleteAllVehicles = $this->db->query("delete from workplace_types where type_id  = '" . $row . "'");

            $affectedRows += $this->db->affected_rows();

            if ($affectedRows <= 0) {

                echo json_encode(array('flag' => 1, 'affectedRows' => $affectedRows, 'msg' => 'Failed to delete'));
                return false;
            }

            $deleteAllVehicles = $this->db->query("delete from workplace where type_id = '" . $row . "'");
            $affectedRows += $this->db->affected_rows();




            $return[] = $this->mongo_db->delete('vehicleTypes', array('type' => (int) $row));

            $getAllDriversCursor = $this->mongo_db->get('location', array('type' => (int) $row));

            $mas_id = array();

            foreach ($getAllDriversCursor as $driver) {
                $mas_id[] = (int) $driver['user'];
            }

            $return[] = $this->mongo_db->delete('location', array('user' => array('$in' => $mas_id)));

            $updateMysqlDriverQry = $this->db->query("delete from master where mas_id in (" . implode(',', $mas_id) . ")");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlApptQry = $this->db->query("delete from appointment where mas_id in (" . implode(',', $mas_id) . ")");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from passenger_rating where mas_id in (" . implode(',', $mas_id) . ")");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from master_ratings where mas_id in (" . implode(',', $mas_id) . ")");
            $affectedRows += $this->db->affected_rows();

            $updateMysqlReviewQry = $this->db->query("delete from user_sessions where user_type = 1 and oid in (" . implode(',', $mas_id) . ")");
            $affectedRows += $this->db->affected_rows();
        }

        echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'msg' => 'Process completed.'));
    }

    function deletevehiclemodal() {
        $val = $this->input->post('val');
        foreach ($val as $row) {
            $this->db->query("delete  from vehiclemodel where id = '" . $row . "' ");
        }
    }

    function deletevehicletypemodel() {
        $val = $this->input->post('val');
        foreach ($val as $row) {
//            $this->db->query("delete  from vehiclemodel where id = '" . $row . "' ");
            $this->db->query("delete from vehicleType where id = '" . $row . "' ");
        }
//        if($this->db->num_rows() > 0){
//             echo json_encode(array('msg' => 'your vehicletype deleted', 'flag' => 0));
//            return;
//        } else {
//            echo json_encode(array('msg' => 'your not deleted', 'flag' => 1));
//            return;
//        }
//        
    }

    function editlonglat() {
        $val = $this->input->post('val');
        $lat = $this->input->post('lat');
        $lon = $this->input->post('lon');
//        foreach ($val as $rowid) {
        $this->db->query("update city_available set City_Lat = '" . $lat . "',City_Long = '" . $lon . "' where City_Id ='" . $val . "' ");
//        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => 'your latlong added successfully', 'flag' => 0));
            return;
        } else {
            echo json_encode(array('msg' => 'your latlong update failed', 'flag' => 1));
            return;
        }
    }

    function insert_city_available() {
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');
        $country = $this->input->post('country');
        $city = $this->input->post('city');

        $query = $this->db->query("select * from city_available where City_Id ='" . $city . "' ");

        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => "city  already exists", 'flag' => 0));
            return;
        } else {

            $selectCity = "select City_Name from city where City_Id = '" . $city . "'";

            $Result = $this->db->query($selectCity)->result_array();

            $this->db->query("insert into city_available(City_Id,Country_Id,City_Name,City_Lat,City_Long) values('" . $city . "','" . $country . "','" . $Result[0]['City_Name'] . "','" . $lat . "','" . $lng . "')");

            if ($this->db->affected_rows() > 0) {
                echo json_encode(array('msg' => "city added successfully", 'flag' => 1));
                return;
            }
        }
    }

    function city() {
        return $this->db->query("select City_Name,City_Id from city_available ORDER BY City_Name ASC ")->result();
    }

    function city_sorted() {
        return $this->db->query("select City_Name,City_Id from city_available ORDER BY City_Name ASC ")->result();
    }

    function get_driver() {
        return $this->db->query("select * from master ORDER BY last_name")->result();
    }

    function insert_company() {
        $companyname = $this->input->post('companyname');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $mobile = $this->input->post('mobilenumber');
        $city = $this->input->post('cityname');
        $state = $this->input->post('state');
        $postcode = $this->input->post('pincode');
        $vatnumber = $this->input->post('vatnumber');

        $status = 1;
//        $logo = "0";




        $companylogo = $_FILES["companylogo"]["name"];
        $extra = substr($companylogo, strrpos($companylogo, '.') + 1); //explode(".", $insurname);
        $logo = (rand(1000, 9999) * time()) . '.' . $extra;



        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/' . mainfolder . '/pics/';



        try {
            move_uploaded_file($_FILES['companylogo']['tmp_name'], $documentfolder . $logo);
        } catch (Exception $ex) {
            print_r($ex);
            return false;
        }

        $getcityname = $this->db->query("select * from company_info where  email = '" . $email . "'");
        if ($getcityname->num_rows() > 0) {


            echo json_encode(array('err' => 0));
        } else {

            $result['data'] = $this->db->query("insert into company_info(companyname,addressline1,city,state,postcode,vat_number,firstname,
                           lastname,email,mobile,userame,password,status,logo) values(
                           '" . $companyname . "',
                           '" . $address . "',
                           '" . $city . "',
                           '" . $state . "',
                           '" . $postcode . "',
                           '" . $vatnumber . "',
                           '" . $firstname . "',
                           '" . $lastname . "',
                           '" . $email . "',
                           '" . $mobile . "',

                           '" . $username . "',
                           '" . $password . "', '" . $status . "','" . $logo . "')");
            echo json_encode(array('err' => 1));
        }
    }

    function update_company($param) {

        $companyname = $this->input->post('companyname');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $mobile = $this->input->post('mobilenumber');
        $city = $this->input->post('cityname');
        $state = $this->input->post('state');
        $postcode = $this->input->post('pincode');
        $vatnumber = $this->input->post('vatnumber');

        $result['data'] = $this->db->query("update company_info set companyname='" . $companyname . "',
                                                                       addressline1='" . $address . "' ,
                                                                         city='" . $city . "',
                                                                                  state='" . $state . "',
                                                                                  vat_number='" . $vatnumber . "',
                                                                                  postcode='" . $postcode . "',
                                                                                  userame='" . $username . "',
                                                                                  firstname='" . $firstname . "',
                                                                                  lastname='" . $lastname . "',
                                                                                  email='" . $email . "',
                                                                                  mobile='" . $mobile . "',
                                                                                 password='" . $password . "' where company_id='" . $param . "'");


        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your company  updated successfully", 'flag' => 1));
            return;
        }
    }

    function get_passengerinfo($status) {
        $varToShowData = $this->db->query("select * from patient where status='" . $status . "'order by patient_id DESC")->result();

        return $varToShowData;
    }

    function inactivepassengers() {
        $val = $this->input->post('val');

        foreach ($val as $result) {
            $this->db->query("update patient set status=1 where patient_id='" . $result . "'");
            $this->db->query("update user_sessions set loggedIN=2 where user_type = 2 and oid='" . $result . "'");
        }
    }

//    function get_compaigns_data($status = '') {
//
//        return $this->db->query(" select cp.*,c.city_name,c.Currency as currency from coupons cp, city c where cp.city_id = c.city_id and cp.coupon_type = '" . $status . "' and cp.status = '0' and user_type = 2")->result();
//    }

    function get_compaigns_data($status = '') {
        $this->load->library('mongo_db');
        $db = $this->mongo_db->db;
        $selecttb = $db->selectCollection('coupons');

        if ($status == '1' || $status == '')
            $cond = array('coupon_type' => 1, 'coupon_code' => 'REFERRAL', 'user_type' => 2, 'status' => 0);
        else if ($status == '2')
            $cond = array('coupon_type' => 2, 'user_type' => 2, 'status' => 0);
        else if ($status == '3')
            $cond = array('coupon_type' => 3, 'user_type' => 1); //, 'expiry_date' => array('$gte' => time()));            
//        $find = $selecttb->find($cond);
//        print_r($cond); //$con;
        $find = $selecttb->find($cond)->sort(array('start_date' => 1));
        $allDocs = array();
        try {
            foreach ($find as $doc) {
                $allDocs[] = $doc;
            }
        } catch (Exception $e) {
            
        }
        if ($status == '2') {

            foreach ($allDocs as $key => $row) {
                $volume[$key] = $row['start_date'];
            }
            array_multisort($volume, SORT_DESC, $allDocs);
            return array_reverse($allDocs);
        }
        return $allDocs;
    }

//    function get_compaigns_data_ajax($for = '') {
////            $date =  date('Y-m-d');
//        $st = $this->input->post('value');
//
//        $this->load->library('mongo_db');
//
//        $db = $this->mongo_db->db;
//
//        $selecttb = $db->selectCollection('coupons');
//
//        if ($for == '1') {
//            $cond = array('coupon_type' => 1, 'coupon_code' => 'REFERRAL', 'user_type' => 2, 'status' => (int) $st);
//        } else if ($for == '2') {
//            if ($st == 0)
//                $cond = array('coupon_type' => 2, 'user_type' => 2, 'status' => 0, 'expiry_date' => array('$lte' => time()));
//            else if ($st = 10)
//                $cond = array('coupon_type' => 2, 'user_type' => 2,
//                    '$or' => array(
//                        array('status' => 1),
//                        array('status' => 0, 'expiry_date' => array('$lte' => time()))
//                    )
//                );
//        } else if ($for == '3') {
//            if ($st == 0)
//                $cond = array('coupon_type' => 3, 'user_type' => 1, 'status' => 1); //, 'expiry_date' => array('$gte' => time()));
//            else if ($st == 1)
//                $cond = array('coupon_type' => 3, 'user_type' => 1, 'status' => 0, 'expiry_date' => array('$gte' => time())); //, 'expiry_date' => array('$gte' => time()));
//            else if ($st == 2)
//                $cond = array('coupon_type' => 3, 'user_type' => 1, 'status' => 0, 'expiry_date' => array('$lte' => time())); //, 'expiry_date' => array('$gte' => time()));
//        }
//
//        $find = $selecttb->find($cond);
//
//        $allDocs = array();
//
//        foreach ($find as $doc) {
//            $doc['id'] = (string) $doc['_id'];
//            $doc['start_date'] = date('Y-m-d', $doc['start_date']);
//            $doc['expiry_date'] = date('Y-m-d', $doc['expiry_date']);
//            $allDocs[] = $doc;
//        }
//
////        print_r($allDocs);exit();
//        return json_encode(array('data' => $allDocs));
//    }

    function get_compaigns_data_ajax($for = '') {
//            $date =  date('Y-m-d');
        $st = $this->input->post('value');

        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        if ($for == '1')
            $cond = array('coupon_type' => 1, 'coupon_code' => 'REFERRAL', 'user_type' => 2, 'status' => (int) $st);
        else if ($for == '2')
            if ($st == 0)
                $cond = array('coupon_type' => 2, 'user_type' => 2, 'status' => 0);
            else if ($st = 10)
                $cond = array('coupon_type' => 2, 'user_type' => 2, 'status' => 1);


        $find = $selecttb->find($cond);

        $allDocs = array();

        foreach ($find as $doc) {
            $doc['id'] = (string) $doc['_id'];

            $doc['start_date'] = date('Y-m-d', $doc['start_date']);
            $doc['expiry_date'] = date('Y-m-d', $doc['expiry_date']);

            $allDocs[] = $doc;
        }

//        print_r($allDocs);exit();
        return json_encode(array('data' => $allDocs));
    }

    function deactivecompaigns() {
        $val = $this->input->post('val');


        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        foreach ($val as $row) {

            $selecttb->update(array('_id' => new MongoId($row)), array('$set' => array('status' => 1)));
        }
    }

    function activepassengers() {
        $val = $this->input->post('val');

        foreach ($val as $result) {
            $this->db->query("update patient set status=3 where patient_id='" . $result . "'");
        }
    }

    function insertdispatches() {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $phone = $this->input->post('phone');
        $username = $this->input->post('username');
        $status = 1;
        $res = $this->db->query("insert into dispatcher(dis_name,dis_phone,dis_email,dis_pass,dis_username,status) values('" . $name . "', '" . $phone . "', '" . $email . "','" . md5($password) . "','" . $username . "','" . $status . "')");

        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => '0'));
            return;
        } else {
            echo json_encode(array('msg' => '1'));
            return;
        }
    }

    function inactivedispatchers() {
        $status = $this->input->post('val');
        foreach ($status as $row) {
            $result = $this->db->query("update dispatcher set status=2 where dis_id='" . $row . "'");
        }
    }

    function activedispatchers() {
        $status = $this->input->post('val');
        foreach ($status as $row) {
            $result = $this->db->query("update dispatcher set status=1 where dis_id='" . $row . "'");
        }
    }

    function editdispatchers() {
        $dis_name = $this->input->post('dis_name');
        $dis_email = $this->input->post('dis_email');
        $dis_phone = $this->input->post('dis_phone');
        $dis_username = $this->input->post('dis_username');
        $val = $this->input->post('val');

        $this->db->query("update dispatcher set dis_phone = '" . $dis_phone . "', dis_username = '" . $dis_username . "', dis_name='" . $dis_name . "', dis_email='" . $dis_email . "' where dis_id='" . $val . "'");

        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => 'your Dispatcher edited successfully', 'flag' => 0));
            return;
        } else {
            echo json_encode(array('msg' => 'your Dispatcher update failed', 'flag' => 1));
            return;
        }
    }

    function editpass() {
        $newpass = $this->input->post('newpass');
        $val = $this->input->post('val');

//        $this->db->query("select * from dispatcher where dis_pass='" . $newpass . "' ")->result();

        $this->db->query("update dispatcher set dis_pass='" . $newpass . "' where dis_id = '" . $val . "' ");
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "this password already exists. Enter new password", 'flag' => 1));
            return;
        }
//         else {
//              $this->db->query("update dispatcher set dis_pass='" . $newpass . "' ");
//
//        }
    }

    function get_disputesdata($status) {
        $result = $this->db->query(" select mas.first_name as mas_fname,mas.last_name as mas_lname,mas.mas_id,slv.slave_id,slv.first_name as slv_name,slv.last_name as slv_lname,rep.report_msg,rep.report_id,rep.report_dt,rep.appointment_id from master mas,slave slv, reports rep where rep.mas_id = mas.mas_id   and rep.slave_id = slv.slave_id and rep.report_status = '" . $status . "' order by rep.report_id DESC")->result();

        return $result;
    }

    function resolvedisputes() {
        $value = $this->input->post('val');
        $mesage = $this->input->post('message');

        $this->db->query("update reports set report_status=2, report_msg='" . $mesage . "' where report_id='" . $value . "'");
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "your selected dispute resolved succesfully", 'flag' => 1));
            return;
        }
    }

    function driver() {
        $res = $this->db->query("select * from master order by first_name")->result();
        return $res;
    }

    function passenger() {
        $res = $this->db->query("select * from slave")->result();
        return $res;
    }

    function insertcampaigns() {

        //$coupon_type == '1'
        $city = $this->input->post('city');
        $coupon_type = $this->input->post('coupon_type');
        $discount = $this->input->post('discount');
        $discounttype = $this->input->post('discountradio');
        $referaldiscount = $this->input->post('referaldiscount');
        $refferaldiscounttype = $this->input->post('refferalradio');
        $message = $this->input->post('message');
        $title = $this->input->post('title');

//$coupon_type == '2'
        $codes = $this->input->post('codes');
        $citys = $this->input->post('citys');
        $discounts = $this->input->post('discounts');
        $messages = $this->input->post('messages');
        $discounttypes = $this->input->post('discounttypes');


        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        if ($coupon_type == '1') {
            $cond = array('coupon_type' => 1, 'coupon_code' => 'REFERRAL', 'city_id' => (int) $city, 'status' => 0);
            $find = $selecttb->findOne($cond);

            if (is_array($find)) {
                return json_encode(array('msg' => "Referral campaign already exists in this city ", 'flag' => 1));
            }
        }

        if ($coupon_type == '2') {
            $city = $citys;
            $cond = array('coupon_type' => 2, 'coupon_code' => $codes, 'city_id' => (int) $city, 'status' => 0, 'expiry_date' => array('$gt' => time()));
            $find = $selecttb->findOne($cond);

            if (is_array($find)) {
                return json_encode(array('msg' => "Same coupon already exists in this city", 'flag' => 1));
            }
        }

        $cityDet = $this->db->query("select ca.*,c.Currency from city_available ca,city c  where c.City_Id = ca.City_Id and ca.City_Id = '" . $city . "'")->result();

        if ($coupon_type == '1') {

            $insert = array(
                "coupon_code" => "REFERRAL",
                "coupon_type" => 1,
                "discount_type" => (int) $discounttype,
                "discount" => (float) $discount,
                "referral_discount_type" => (int) $refferaldiscounttype,
                "referral_discount" => (float) $referaldiscount,
                "message" => $message,
                "status" => 0,
                "title" => $title,
                "city_id" => (int) $city,
                "currency" => $cityDet[0]->Currency,
                "city_name" => $cityDet[0]->City_Name,
                "location" => array(
                    "longitude" => (double) $cityDet[0]->City_Long,
                    "latitude" => (double) $cityDet[0]->City_Lat
                ),
                "user_type" => 2
            );

            $selecttb->insert($insert);
        } else if ($coupon_type == '2') {
            $insert = array(
                "coupon_code" => $codes,
                "coupon_type" => 2,
                "start_date" => (int) strtotime($this->input->post('sdate')),
                "expiry_date" => (int) strtotime($this->input->post('edate')),
                "discount_type" => (int) $discounttypes,
                "discount" => (float) $discounts,
                "message" => $messages,
                "status" => 0,
                "title" => $title,
                "city_id" => (int) $city,
                "currency" => $cityDet[0]->Currency,
                "city_name" => $cityDet[0]->City_Name,
                "location" => array(
                    "longitude" => (double) $cityDet[0]->City_Long,
                    "latitude" => (double) $cityDet[0]->City_Lat
                ),
                "user_type" => 2
            );
            $selecttb->insert($insert);
        }
//         else{
        return json_encode(array('msg' => "Great! Your referrals has been added sucessfully for this city", 'flag' => 0, 'data' => $insert));
//            }
    }

    function updatecompaigns() {

        // for coupon types 1
        $coupon_type = $this->input->post('coupon_type');
        $discount = $this->input->post('discount');
        $discounttype = $this->input->post('discountradio');
        $referaldiscount = $this->input->post('referaldiscount');
        $refferaldiscounttype = $this->input->post('refferalradio');
        $message = $this->input->post('message');
        $title = $this->input->post('title');
        $cuponid = $this->input->post('val');

        // for coupon types 2
        $cuponids = $this->input->post('val2');
        $discounts = $this->input->post('discounts');
        $messages = $this->input->post('messages');
        $codes = $this->input->post('codes');
        $discounttypes = $this->input->post('discounttypes');

        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        if ($coupon_type == '1') {

            $selecttb->update(array('_id' => new MongoId($cuponid)), array(
                '$set' => array(
                    "discount_type" => (int) $discounttype,
                    "discount" => (float) $discount,
                    "referral_discount_type" => (int) $refferaldiscounttype,
                    "referral_discount" => (float) $referaldiscount,
                    "message" => $message,
                    "title" => $title,
                    "status" => 0
            )));
        } else if ($coupon_type == '2') {
            $selecttb->update(array('_id' => new MongoId($cuponids)), array(
                '$set' => array(
                    "coupon_code" => $codes,
                    "start_date" => (int) strtotime($this->input->post('sdate')),
                    "expiry_date" => (int) strtotime($this->input->post('edate')),
                    "discount_type" => (int) $discounttypes,
                    "discount" => (float) $discounts,
                    "message" => $messages,
                    "status" => 0,
                    "title" => $title,
                    "user_type" => 2
            )));
        }
    }

    function get_promo_details($id, $page) {

        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        $find = $selecttb->find(array('_id' => new MongoId($id)));

        $bookings = array();

        foreach ($find as $cur) {
            foreach ($cur['bookings'] as $dis) {
                $bookings[] = $dis['booking_id'];
            }
        }
        if (count($bookings) > 0) {
            $query = 'select a.appointment_dt,a.discount,a.inv_id,a.amount,a.sub_total,p.patient_id,p.email,a.appointment_id from appointment a,patient p where a.patient_id = p.patient_id and a.appointment_id IN(' . implode(',', $bookings) . ') and a.status = 7';
            return $this->db->query($query)->result_array();
        } else {
            return false;
        }
    }

    function get_referral_details($id, $page) {


        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;
        $selecttb = $db->selectCollection('coupons');
        $find = $selecttb->find(array('_id' => new MongoId($id)));
        $all = array();
        foreach ($find as $cur) {
            $all[] = $cur;
        }
        $all[0]['signups'] = array_reverse($all[0]['signups']);
        return $all;
    }

    function refered($code, $refCode, $page) {


        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        $find = $selecttb->find(array('_id' => new MongoId($code), 'signups.coupon_code' => $refCode), array('signups.$' => 1));

        $all = array();

        foreach ($find as $cur) {
            $all[] = $cur;
        }

//        print_r($all);

        return $all;
    }

    function editcompaigns() {
        $value = $this->input->post('val');

        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->selectCollection('coupons');

        $resu = $selecttb->findOne(array('_id' => new MongoId($value)));

//        print_r($resu);exit();
        $resu['start_date'] = date('Y-m-d', $resu['start_date']);
        $resu['expiry_date'] = date('Y-m-d', $resu['expiry_date']);


        echo json_encode($resu);
    }

    function insertpass() {
        $password = $this->input->post('newpass');
        $val = $this->input->post('val');

        $res = $this->db->query("update patient set password = md5('" . $password . "')  where patient_id='" . $val . "'");
//        return $res;
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "Password updated successfully", 'flag' => 1));
            return;
        }
    }

    function get_company_data($param) {
        $result = $this->db->query("select * from company_info where company_id='" . $param . "' ")->result();
        return $result;
    }

    function company_data() {
        $result = $this->db->query("select * from company_info")->result();
        return $result;
    }

    function get_dispatchers_data($status) {

        $res = $this->db->query("select * from dispatcher where status='" . $status . "'")->result();
        return $res;
    }

    function delete_dispatcher() {
        $var = $this->input->post('val');

        foreach ($var as $row) {
            $this->db->query("delete  from dispatcher where dis_id ='" . $row . "'");
        }
    }

    function get_country() {
        return $this->db->query("select * from country order by Country_Name")->result();
    }

    function datatable_cust_details($custid) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->datatables->select("sid,device,manf, dev_model,  (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) as type_img, dev_os, app_version, create_date", false)
                ->unset_column('sid')
                ->edit_column('type_img', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
                ->from("user_sessions")
                ->where("oid = " . $custid . " and user_type = 2");
        $this->db->order_by("sid", "desc");
        echo $this->datatables->generate();
    }

    function datatable_pro_details($custid) {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->datatables->select("sid,device,manf, dev_model, (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) as type_img, dev_os, app_version, create_date  ", false)
                ->unset_column('sid')
                ->edit_column('type_img', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
                ->from("user_sessions")
                ->where("oid = " . $custid . " and user_type = 1");
        $this->db->order_by("sid", "desc");
        echo $this->datatables->generate();
    }

    function pro_details_get($proid) {
        $data = $this->db->query("select * from doctor where doc_id ='" . $proid . "'")->row();
        return $data;
    }

    function cust_details_get($custid) {
        $data = $this->db->query("select * from patient where patient_id ='" . $custid . "'")->row();
        return $data;
    }

    function datatable_cities() {

        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select('ci.City_Id,co.Country_Name,ci.City_Name,ci.City_Lat,ci.City_Long')
//                ->add_column('select','<img src="$2">', 'ci.City_Id','co.Country_Name')
                ->unset_column('ci.City_Id')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'ci.City_Id')
                ->from('city_available ci,country co')
                ->where('ci.country_id = co.country_id'); //order by slave_id DESC ",false);
        $this->db->order_by("ci.City_Id", "desc");

        echo $this->datatables->generate();
    }

    function datatable_referrals() {
        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select('c.referred_user_id,c.user_id,s.created_dt,s.slave_id,s.first_name')
                ->unset_column('')
                ->add_column('')
                ->from('')
                ->where('');
        $this->db->order_by("", "desc");
        echo $this->datatables->generate();
    }

    function datatable_promodetails() {
        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select('c.')
                ->unset_column('')
                ->add_column('')
                ->from('')
                ->where('');
        $this->db->order_by("", "desc");
        echo $this->datatables->generate();
    }

    function datatable_companys($status = '') {

        $city = $this->session->userdata('city_id');
        if ($city != '0')
            $citylist = 'status ="' . $status . '"  and co.city = "' . $city . '"';
        else
            $citylist = 'status ="' . $status . '"';

        $this->load->library('Datatables');
        $this->load->library('table');


        $this->datatables->select('co.company_id,co.companyname,co.addressline1,(select City_Name from city  where City_Id = co.city) as cities,co.state,co.postcode,co.firstname,co.lastname,co.email,co.mobile', false)
//                ->unset_column('co.status')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value= "$1"/>', 'co.company_id')
                ->from('company_info co')
                ->where($citylist);
        $this->db->order_by("co.company_id", "desc");
        echo $this->datatables->generate();
//        echo json_encode(array('status' => $status));
    }

    function datatable_vehicletype($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $city = $this->session->userdata('city_id');

        $cityCond = "";

        if ($city != '0')
            $cityCond = ' and w.city_id = "' . $city . '"';

        $this->datatables->select(' w.type_id,w.type_name,w.max_size,w.basefare,w.min_fare,w.price_per_min,w.price_per_km,w.type_desc,cty.City_Name,w.cancilation_fee')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value= "$1"/>', 'w.type_id')
                ->from('workplace_types w, city_available cty')
                ->where('w.city_id = cty.City_Id' . $cityCond); //order by slave_id DESC ",false);
        $this->db->order_by("w.type_id", "desc");
        echo $this->datatables->generate();
    }

//     function datatable_payroll($status = '') {
//
//        $this->load->library('Datatables');
//        $this->load->library('table');
//
//        $this->datatables->select(' ')->from('')->where(''); //order by slave_id DESC ",false);
//
//        echo $this->datatables->generate();
//    }


    function documentgetdata() {
        $val = $this->input->post("val");
        /* \
         * [doc_ids] => 367
          [driverid] => 830
          [url] => 8204124114494.jpg
          [expirydate] => 2014-05-31
          [doctype] => 1
         */
        $return = array();
        foreach ($val as $row) {
            $data = $this->db->query("select * from docdetail where driverid = '" . $row . "'")->result();
        }
        foreach ($data as $doc) {
            $return[] = array('doctype' => $doc->doctype, 'url' => $doc->url, 'expirydate' => $doc->expirydate);
        }
        return $return;
    }

    function datatable_vehicles($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $city = $this->session->userdata('city_id');
        $company = $this->session->userdata('company_id');
        if (($city != '0') && ($company == '0'))
            $citylist = ' and wt.city_id = "' . $city . '"';
        else if (($city != '0') && ($company != '0'))
            $citylist = ' and w.company = "' . $company . '"';

//        $compCond = "";


        if ($status == '12')
            $status = '1,2';

        $this->datatables->select('w.workplace_id,w.uniq_identity,'
                        . '(select vehicletype from vehicleType where id = w.Title),'
                        . '(select vehiclemodel from vehiclemodel where id = w.Vehicle_Model),'
                        . '(select type_name from workplace_types  where type_id = w.type_id),'
                        . '(select companyname from company_info  where company_id = w.company),'
                        . 'w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Insurance_No,w.Vehicle_Color,'
                        . '(select City_Name from city where City_Id = wt.city_id)')
                ->unset_column('w.workplace_id')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value= "$1"/>', 'w.workplace_id')
                ->from('workplace w,vehicleType vt,vehiclemodel vm,workplace_types wt,company_info ci ')
                ->where('vt.id = w.title and w.company = ci.company_id and vm.id=w.Vehicle_Model and wt.type_id = w.type_id  and w.status IN (' . $status . ')' . $citylist); //order by slave_id DESC ",false);
        $this->db->order_by("w.workplace_id", "desc");


        echo $this->datatables->generate();
    }

    function loadAvailableCity() {
        $countryid = $this->input->post('country');
        $Result = $this->db->query("select c.* from city c where c.Country_Id = '" . $countryid . "' and c.City_Id not in (select City_Id from city_available where Country_Id = '" . $countryid . "')")->result();
        return $Result;
    }

//    function datatable_disputes($status = '') {
//
//        $this->load->library('Datatables');
//        $this->load->library('table');
//
//        $company_id = $this->session->userdata('company_id');
//        $compCond = "";
//        if ($company_id != 0)
//            $compCond = " and mas.company_id = '" . $company_id . "'";
//
//        $this->datatables->select("rep.report_id,slv.slave_id,slv.first_name,mas.mas_id,mas.first_name as name,rep.report_msg,rep.report_dt,rep.appointment_id")
//            ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rep.report_id')
//            ->from("master mas,slave slv, reports rep")
//            ->where("rep.mas_id = mas.mas_id   and rep.slave_id = slv.slave_id and rep.report_status = '" . $status . "'" . $compCond);
//
//        echo $this->datatables->generate();
//    }

    function datatable_disputes($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $company_id = $this->session->userdata('company_id');
        $compCond = "";
        if ($company_id != 0)
            $compCond = " and mas.company_id = '" . $company_id . "'";

        $this->datatables->select("rep.report_id,slv.slave_id,slv.first_name,mas.mas_id,mas.first_name as name,rep.report_msg,rep.report_dt,rep.appointment_id")
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rep.report_id')
                ->from("master mas,slave slv, reports rep")
                ->where("rep.mas_id = mas.mas_id   and rep.slave_id = slv.slave_id and rep.report_status = '" . $status . "'" . $compCond);

        echo $this->datatables->generate();
    }

    function validateCompanyEmail() {

        $query = $this->db->query("select company_id from company_info where email='" . $this->input->post('email') . "'");
        if ($query->num_rows() > 0) {

            echo json_encode(array('msg' => '1'));
            return;
        } else {
            echo json_encode(array('msg' => '0'));
        }
    }

    function datatable_vehiclemodels($status) {


        $this->load->library('Datatables');
        $this->load->library('table');

        if ($status == 1) {

            $this->datatables->select("id,vehicletype")
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'id')
                    ->from("vehicleType");
//             $this->db->order_by("id", "desc");//order by slave_id DESC ",false);
        } else if ($status == 2) {


            $this->datatables->select("vm.id,vm.*,vt.vehicletype")
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'vm.id')
                    ->from("vehiclemodel vm,vehicleType vt")
                    ->where("vm.vehicletypeid = vt.id");
//                   $this->db->order_by("vm.id", "desc");     //order by slave_id DESC ",false);
        }
        $this->db->order_by("id", "desc");
        echo $this->datatables->generate();
    }

//    function datatable_drivers($for = '', $status = '') {
//
//        $this->load->library('Datatables');
//        $this->load->library('table');
//        $company = $this->session->userdata('company_id');
//
//        if ($for == 'my') {
//
//
//            $meta = array('lastDoctor' => 'my/' . $status);
//
//            $whererc = "mas.status IN ('" . $status . "') ";
//
//            $whererc = "mas.status IN ('" . $status . "')";
//
//            if ($status == 1) {
//
////                      $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp,"
////                       . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
//
//                $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,concat(mas.country_code , mas.mobile), mas.email,mas.created_dt,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp,"
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id and user_type=1 order by oid DESC limit 0,1) as type_img", false)
//
////                     $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp,"
////                       . "(case us.type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions us where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
////                      ->unset_column('mas.company_id')
//                        ->unset_column('type_img')
//                        ->unset_column('pp')
//                        ->edit_column('mas.created_dt', "get_formatedDate/$1", 'mas.created_dt')
//                        ->add_column('PROFILE PIC', '<img src="' . base_url() . '../../pics/$1" width="50px" height="50px" class="imageborder">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
////                        ->add_column('VEHICLE IMAGE', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 'vehicleimage')
////                        ->add_column('LATITUDE', "get_lat/$1", 'rahul')
////                        ->add_column('LONGITUDE', "get_lon/$1", 'rahul')
//                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
////                        ->add_column('DETAILS', '<a href="'.base_url().'index.php/superadmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
//                        ->add_column('DETAILS', '<a href="' . base_url() . '/../../../PriveAdmin_v2/index.php/masteradmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
//                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
//                        ->from("doctor mas")
//                        ->where("mas.status IN ('" . $status . "')");
//            } else if ($status == 3 || $status == 4) {
//                $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp,"
////                                . "(select companyname from company_info where company_id = mas.company_id ) as companyname1,"
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
////                       . "(select Vehicle_Image from workplace where workplace_id = mas.workplace_id ) as vehicleimage", false)
////                           ->unset_column('vehicleimage')
//                        ->unset_column('type_img')
//                        ->unset_column('pp')
//                        ->edit_column('mas.created_dt', "get_formatedDate/$1", 'mas.created_dt')
//                        ->add_column('PROFILE PIC', '<img src="' . base_url() . '../../pics/$1" width="50px" height="50px" class="imageborder">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
////                        ->add_column('VEHICLE IMAGE', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 'vehicleimage')
////                        ->add_column('LATITUDE', "get_lat/$1", 'rahul')
////                        ->add_column('LONGITUDE', "get_lon/$1", 'rahul')
//                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
//                        //->add_column('DETAILS', '<a href="'.base_url().'index.php/superadmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
//                        ->add_column('DETAILS', '<a href="' . base_url() . '/../../../PriveAdmin_v2/index.php/masteradmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
//                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
//                        ->from("doctor mas")
//                        ->where($whererc);
//            } else if ($status == 6) {   //logout
//                $result = $this->db->query("select distinct oid from user_sessions u where u.loggedIn = 1  and u.user_type = 1")->result();
//                foreach ($result as $res) {
//                    $darray[] = $res->oid;
//                }
//                $mas_ids = implode(',', array_filter(array_unique($darray)));
//                if ($mas_ids == '')
//                    $mas_ids = 0;
//                $whererc = "mas.doc_id = u.oid and  (u.loggedIn = 2 or  u.loggedIn = 3)  and u.user_type = 1 and mas.doc_id NOT IN (" . $mas_ids . ")";
//                $this->datatables->select("distinct mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp,"
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
//                        ->unset_column('type_img')
//                        ->unset_column('pp')
//                        ->edit_column('mas.created_dt', "get_formatedDate/$1", 'mas.created_dt')
//                        ->add_column('PROFILE PIC', '<img src="' . base_url() . '../../pics/$1" width="50px" height="50px" class="imageborder">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
//                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
//                        ->add_column('DETAILS', '<a href="' . base_url() . '/../../../PriveAdmin_v2/index.php/masteradmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
//                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
//                        ->from("doctor mas, user_sessions u")
////                ->where($whererc);
//                        ->where($whererc);
//            }
//        } else if ($for == 'mo') {
//
//            $meta = array('lastDoctor' => 'mo/' . $status);
//
//            $this->load->library('mongo_db');
//
//            $db = $this->mongo_db->db;
//
//            $selecttb = $db->location;
//
//            $darray = $latlong = array();
//            if ($status == 3) { //online or free
//                $drivers = $selecttb->find(array('status' => (int) $status));
//
//                foreach ($drivers as $mas_id) {
//                    $darray[] = $mas_id['user'];
//                    $latlong[$mas_id['user']] = array($mas_id['location']['latitude'], $mas_id['location']['longitude']);
//                }
//                $mas_ids = implode(',', array_filter(array_unique($darray)));
//                if ($mas_ids == '')
//                    $mas_ids = 0;
//                $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp,"
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
//                        ->unset_column('type_img')
//                        ->unset_column('pp')
//                        ->add_column('PROFILE PIC', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
//                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
//                        ->add_column('DETAILS', '<button onclick="makeOffline(this)" class="btn btn-primary btn-cons" value="$1">MAKE OFFLINE</button>', 'rahul')
//                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
//                        ->unset_column('DEVICE TYPE')
//                        ->from("doctor mas")
//                        ->where("mas.doc_id IN (" . $mas_ids . ")");
//            } elseif ($status == 567) {//booked
//                $drivers = $selecttb->find(array('status' => 5)); //array('$in' => array(5, 6, 7))));
//                foreach ($drivers as $mas_id) {
//                    $darray[] = $mas_id['user'];
//
//                    $latlong[$mas_id['user']] = array($mas_id['location']['latitude'], $mas_id['location']['longitude']);
//                }
//                $mas_ids = implode(',', array_filter(array_unique($darray)));
//                if ($mas_ids == '')
//                    $mas_ids = 0;
//                $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp,"
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
//                        ->unset_column('type_img')
//                        ->unset_column('pp')
//                        ->add_column('PROFILE PIC', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
//                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
//                        ->add_column('DETAILS', '<a href="' . base_url() . 'index.php/superadmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
//                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
//                        ->unset_column('DEVICE TYPE')
//                        ->from("doctor mas")
//                        ->where("mas.doc_id IN (" . $mas_ids . ")");
//            } elseif ($status == 30) {//OFFLINE
//                $drivers = $selecttb->find(array('status' => array('$in' => array(1, 4))));
//                foreach ($drivers as $mas_id) {
//                    $darray[] = $mas_id['user'];
//                    $latlong[$mas_id['user']] = array($mas_id['location']['latitude'], $mas_id['location']['longitude']);
//                }
//                $mas_ids = implode(',', array_filter(array_unique($darray)));
//                if ($mas_ids == '')
//                    $mas_ids = 0;
//                $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp,"
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
//                        ->unset_column('type_img')
//                        ->unset_column('pp')
//                        ->add_column('PROFILE PIC', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
//                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
//                        ->add_column('DETAILS', '<button onclick="makeonline(this)" class="btn btn-primary btn-cons" value="$1">MAKE ONLINE</button>', 'rahul')
//                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
//                        ->unset_column('DEVICE TYPE')
//                        ->from("doctor mas")
//                        ->where("mas.doc_id IN (" . $mas_ids . ")");
//            }
//
////            $mas_ids = implode(',', array_filter(array_unique($darray)));
////            if ($mas_ids == '')
////                $mas_ids = 0;
////            $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp,"
////                            . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
////                    ->unset_column('type_img')
////                    ->unset_column('pp')
////                    ->add_column('PROFILE PIC', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 'pp')
////                    ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
////                    ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
////                    ->add_column('DETAILS', '<a href="' . base_url() . 'index.php/superadmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
////                    ->unset_column('DEVICE TYPE')
////                    ->from("doctor mas")
////                    ->where("mas.doc_id IN (" . $mas_ids . ")");
//        }
//
//        $this->session->set_userdata($meta);
//
////exit();
//        $this->db->order_by("rahul", "desc");
//        echo $this->datatables->generate();
//    }



    function datatable_drivers($for = '', $status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');
        $company = $this->session->userdata('company_id');

        if ($for == 'my') {


            $meta = array('lastDoctor' => 'my/' . $status);

            $whererc = "mas.status IN ('" . $status . "') ";

            $whererc = "mas.status IN ('" . $status . "')";

            if ($status == 1) {


//                $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,concat(mas.country_code , mas.mobile), mas.email,mas.created_dt,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp,"
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id and user_type=1 order by oid DESC limit 0,1) as type_img", false)

                $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,concat(mas.country_code , mas.mobile), mas.email,mas.created_dt,mas.doc_id as lsdate, mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp", false)

//                        ->unset_column('type_img')
                        ->unset_column('pp')
                        ->edit_column('mas.created_dt', "get_formatedDate/$1", 'mas.created_dt')
                        ->edit_column('lsdate', 'gte_schedule_date/$1', 'lsdate')
//                        ->edit_column('lsdate', 'gte_schedule_date/$1', 'lsdate')
                        ->add_column('PROFILE PIC', '<img src="$1" width="50px" height="50px" class="imageborder" onerror="http://privemd.net/pics/user.jpg">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
//                        ->add_column('VEHICLE IMAGE', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 'vehicleimage')
//                        ->add_column('LATITUDE', "get_lat/$1", 'rahul')
//                        ->add_column('LONGITUDE', "get_lon/$1", 'rahul')
                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
//                        ->add_column('DETAILS', '<a href="'.base_url().'index.php/superadmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
                        ->add_column('DETAILS', '<a href="' . base_url() . '/../../../PriveAdmin_v2/index.php/masteradmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
                        ->from("doctor mas")
                        ->where("mas.status IN ('" . $status . "')");
            } else if ($status == 3 || $status == 4) {
                $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt, mas.doc_id as lsdate,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp", false)
//                                . "(select companyname from company_info where company_id = mas.company_id ) as companyname1,"
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
//                       . "(select Vehicle_Image from workplace where workplace_id = mas.workplace_id ) as vehicleimage", false)
//                           ->unset_column('vehicleimage')
//                        ->unset_column('type_img')
                        ->unset_column('pp')
//                        ->edit_column('lsdate', 'gte_schedule_date/$1', 'lsdate')
                        ->edit_column('lsdate', 'gte_schedule_date/$1', 'lsdate')
                        ->edit_column('mas.created_dt', "get_formatedDate/$1", 'mas.created_dt')
                        ->add_column('PROFILE PIC', '<img src="$1" width="50px" height="50px" class="imageborder" onerror="http://privemd.net/pics/user.jpg">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
//                        ->add_column('VEHICLE IMAGE', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 'vehicleimage')
//                        ->add_column('LATITUDE', "get_lat/$1", 'rahul')
//                        ->add_column('LONGITUDE', "get_lon/$1", 'rahul')
                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
                        //->add_column('DETAILS', '<a href="'.base_url().'index.php/superadmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
                        ->add_column('DETAILS', '<a href="' . base_url() . '/../../../PriveAdmin_v2/index.php/masteradmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
                        ->from("doctor mas")
                        ->where($whererc);
            } else if ($status == 6) {   //logout
                $result = $this->db->query("select distinct oid from user_sessions u where u.loggedIn = 1  and u.user_type = 1")->result();
                foreach ($result as $res) {
                    $darray[] = $res->oid;
                }
                $mas_ids = implode(',', array_filter(array_unique($darray)));
                if ($mas_ids == '')
                    $mas_ids = 0;
                $whererc = "mas.doc_id = u.oid and  (u.loggedIn = 2 or  u.loggedIn = 3)  and u.user_type = 1 and mas.doc_id NOT IN (" . $mas_ids . ")";
                $this->datatables->select("distinct mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.doc_id as lsdate,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp", false)
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
//                        ->unset_column('type_img')
                        ->unset_column('pp')
                        ->edit_column('mas.created_dt', "get_formatedDate/$1", 'mas.created_dt')
                        ->edit_column('lsdate', 'gte_schedule_date/$1', 'lsdate')
                        ->add_column('PROFILE PIC', '<img src="$1" width="50px" height="50px" class="imageborder" onerror="http://privemd.net/pics/user.jpg">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
                        ->add_column('DETAILS', '<a href="' . base_url() . '/../../../PriveAdmin_v2/index.php/masteradmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
                        ->from("doctor mas, user_sessions u")
//                ->where($whererc);
                        ->where($whererc);
            }
        } else if ($for == 'mo') {

            $meta = array('lastDoctor' => 'mo/' . $status);

            $this->load->library('mongo_db');

            $db = $this->mongo_db->db;

            $selecttb = $db->location;

            $darray = $latlong = array();
            if ($status == 3) { //online or free
                $drivers = $selecttb->find(array('status' => (int) $status));

                foreach ($drivers as $mas_id) {
                    $darray[] = $mas_id['user'];
                    $latlong[$mas_id['user']] = array($mas_id['location']['latitude'], $mas_id['location']['longitude']);
                }
                $mas_ids = implode(',', array_filter(array_unique($darray)));
                if ($mas_ids == '')
                    $mas_ids = 0;
                $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.doc_id as lsdate,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp", false)
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
//                        ->unset_column('type_img')
                        ->unset_column('pp')
                        ->add_column('PROFILE PIC', '<img src="$1" width="50px" class="imageborder" onerror="http://privemd.net/pics/user.jpg">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
                        ->add_column('DETAILS', '<button onclick="makeOffline(this)" class="btn btn-primary btn-cons" value="$1">MAKE OFFLINE</button>', 'rahul')
                        ->edit_column('lsdate', 'gte_schedule_date/$1', 'lsdate')
                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
                        ->unset_column('DEVICE TYPE')
                        ->from("doctor mas")
                        ->where("mas.doc_id IN (" . $mas_ids . ")");
            } elseif ($status == 567) {//booked
                $drivers = $selecttb->find(array('status' => 5)); //array('$in' => array(5, 6, 7))));
                foreach ($drivers as $mas_id) {
                    $darray[] = $mas_id['user'];

                    $latlong[$mas_id['user']] = array($mas_id['location']['latitude'], $mas_id['location']['longitude']);
                }
                $mas_ids = implode(',', array_filter(array_unique($darray)));
                if ($mas_ids == '')
                    $mas_ids = 0;
                $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.doc_id as lsdate,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp", false)
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
//                        ->unset_column('type_img')
                        ->unset_column('pp')
                        ->add_column('PROFILE PIC', '<img src="$1" width="50px" class="imageborder">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
                        ->edit_column('lsdate', 'gte_schedule_date/$1', 'lsdate')
                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
                        ->add_column('DETAILS', '<a href="' . base_url() . 'index.php/superadmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
                        ->unset_column('DEVICE TYPE')
                        ->from("doctor mas")
                        ->where("mas.doc_id IN (" . $mas_ids . ")");
            } elseif ($status == 30) {//OFFLINE
                $drivers = $selecttb->find(array('status' => array('$in' => array(4)), 'accepted' => 1));
                foreach ($drivers as $mas_id) {
                    $darray[] = $mas_id['user'];
                    $latlong[$mas_id['user']] = array($mas_id['location']['latitude'], $mas_id['location']['longitude']);
                }
                $mas_ids = implode(',', array_filter(array_unique($darray)));
                if ($mas_ids == '')
                    $mas_ids = 0;
                $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.doc_id as lsdate,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp", false)
//                                . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
//                        ->unset_column('type_img')
                        ->unset_column('pp')
                        ->add_column('PROFILE PIC', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 'pp')
//                        ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
                        ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
                        ->edit_column('lsdate', 'gte_schedule_date/$1', 'lsdate')
                        ->add_column('DETAILS', '<button onclick="makeonline(this)" class="btn btn-primary btn-cons" value="$1">MAKE ONLINE</button>', 'rahul')
                        ->edit_column('rahul', '<a href=" ' . base_url() . '../dashboard/index.php/superadmin/pro_details/$1">$1</a>', 'rahul')
                        ->unset_column('DEVICE TYPE')
                        ->from("doctor mas")
                        ->where("mas.doc_id IN (" . $mas_ids . ")");
            }

//            $mas_ids = implode(',', array_filter(array_unique($darray)));
//            if ($mas_ids == '')
//                $mas_ids = 0;
//            $this->datatables->select("mas.doc_id as rahul,mas.first_name ,mas.last_name ,mas.mobile, mas.email,mas.created_dt,mas.zipcode,(select type_name from doctor_type where type_id = mas.type_id) as typename,mas.profile_pic as pp,"
//                            . "(select (case type when 2 then 'android_icon.png' when 1 then 'iphone-logo.png' else 'admin.jpg' END) from user_sessions where oid = mas.doc_id order by oid DESC limit 0,1) as type_img", false)
//                    ->unset_column('type_img')
//                    ->unset_column('pp')
//                    ->add_column('PROFILE PIC', '<img src="' . base_url() . '../../pics/$1" width="50px" class="imageborder">', 'pp')
//                    ->add_column('DEVICE TYPE', '<img src="' . base_url() . '../../pics/pp/$1" width="30px">', 'type_img')
//                    ->add_column('SELECT', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'rahul')
//                    ->add_column('DETAILS', '<a href="' . base_url() . 'index.php/superadmin/AuthenticateUser/$1" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
//                    ->unset_column('DEVICE TYPE')
//                    ->from("doctor mas")
//                    ->where("mas.doc_id IN (" . $mas_ids . ")");
        }

        $this->session->set_userdata($meta);

//exit();
        $this->db->order_by("rahul", "desc");
        echo $this->datatables->generate();
    }

    function uniq_val_chk() {

        $query = $this->db->query('select * from workplace where uniq_identity = "' . $this->input->post('uniq_id') . '"');
        if ($query->num_rows() > 0) {

            echo json_encode(array('msg' => "This vehicleId Is Already Allocated", 'flag' => '1'));
        } else {
            echo json_encode(array('msg' => "", 'flag' => '0'));
        }
        return;
    }

    function get_options($id) {

        if ($id != '')
            return '<img src="' . base_url() . '../../pics/' . $id . '" width="50px">';
        else
            return '<img src="' . base_url() . '../../superadmin/img/user.jpg" width="50px">';
    }

    function get_devicetype($id) {
//return $id;

        if ($id)
            return '<img src="' . base_url() . '../../superadmin/assets/' . $id . '" width="50px" class="imageborder" >';
        else
            return '<img src="' . base_url() . '../../superadmin/img/user.jpg" width="50px" class="imageborder">';
    }

    function datatable_bookings($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select("a.appointment_id,m.doc_id,m.first_name,s.first_name,a.address_line1,a.drop_addr1,a.appointment_dt,a.distance_in_mts")->from("appointment a,doctor m,patient s")->where("a.patient_id = s.patient_id and a.doc_id = m.doc_id"); //order by slave_id DESC ",false);

        echo $this->datatables->generate();
    }

    function datatable_dispatcher($status = '') {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->datatables->select('dis_id,dis_name,dis_email,dis_phone')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'dis_id')
                ->from('dispatcher')
                ->where('status = "' . $status . '"');
        echo $this->datatables->generate();
    }

    function dispatcherresetpass() {
        $status = $this->input->post('val');
        $newpass = $this->input->post('newpass');
        $newpass = md5($newpass);
        $this->db->query("update dispatcher set dis_pass = '" . $newpass . "' where dis_id='" . $status . "'");
    }

    function deletedispatchers() {
        $status = $this->input->post('val');
        foreach ($status as $row) {
            $result = $this->db->query("delete from dispatcher  where dis_id='" . $row . "'");
        }
    }

    function getEditDispatcherData() {
        $dis_id = $this->input->post('dis_id');
        $getQury = "select * from dispatcher where dis_id = '" . $dis_id . "'";
        return $this->db->query($getQury)->result();
    }

    function datatable_document($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

        $company = $this->session->userdata('company_id');

        if ($status == '1') {

            $this->datatables->select("d.doc_ids,c.first_name,c.last_name,d.expirydate,d.url")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<button type="button" name="view"  width="50px">'
                            . '<a target="_blank" href="' . base_url() . '../../pics/$1">view</a><a target="_blank" href="' . base_url() . '../../pics/$1"></button><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
                    ->from("master c,docdetail d")
                    ->where("c.mas_id = d.driverid and d.doctype=1" . ($company != 0 ? ' and c.company_id = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '2') {

            $this->datatables->select("d.doc_ids,c.first_name,c.last_name,d.expirydate,d.url")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">view</button></a><a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
                    ->from("master c,docdetail d")->where("c.mas_id = d.driverid and d.doctype=2" . ($company != 0 ? ' and c.company_id = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '3') {


            $this->datatables->select("d.docid,d.vechileid,(select companyname from company_info where company_id = w.company) as companyname,d.expirydate,d.url")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<button type="button" name="view"  width="50px"><a target="_blank" href="' . base_url() . '../../pics/$1">view</button></a><a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
//                     ->select("(select companyname from company_info where company_id = w.company) as companyname",false)
                    ->from("workplace w,vechiledoc d,vehicleType v")
                    ->where("w.title = v.id and w.workplace_id = d.vechileid and d.doctype = 2" . ($company != 0 ? ' and w.company = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '4') {

            $this->datatables->select("d.docid,d.vechileid,(select companyname from company_info where company_id = w.company) as companyname,d.url,d.expirydate")
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">view</button></a><a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
//                     ->select("(select companyname from company_info where company_id = w.company) as companyname",false)
                    ->from("workplace w,vechiledoc d,vehicleType v")
                    ->where("w.title = v.id and w.workplace_id = d.vechileid and d.doctype = 3" . ($company != 0 ? ' and w.company = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        } else if ($status == '5') {

            $this->datatables->select("d.docid,d.vechileid,(select companyname from company_info where company_id = w.company) as companyname,d.url,d.expirydate")
                    ->select("(select companyname from company_info where company_id = w.company) as companyname", false)
                    ->unset_column('d.url')
                    ->add_column('VIEW', '<a target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">view</button></a><a target="_blank" target="_blank" href="' . base_url() . '../../pics/$1"><button type="button" name="view"  width="50px">download</button></a>', 'd.url'
                    )
                    ->from("workplace w,vechiledoc d,vehicleType v")
                    ->where("w.title = v.id and w.workplace_id = d.vechileid and d.doctype = 1" . ($company != 0 ? ' and w.company = "' . $company . '"' : '')); //order by slave_id DESC ",false);
        }

        echo $this->datatables->generate();
    }

    function datatable_driverreview($status = '') {


        $this->load->library('Datatables');
        $this->load->library('table');

        $this->datatables->select("r.appointment_id,a.appointment_dt, d.first_name,r.patient_id,r.review, r.star_rating")
//                ->unset_column('$i')
//                ->add_column('sl.no','value="$1"', '$i++')
//                ->unset_column('r.appointment_id')
                ->edit_column('a.appointment_dt', "get_formatedDate/$1", 'a.appointment_dt')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'r.appointment_id')
                ->from("doctor_ratings r, doctor d, patient p,appointment a", false)
                ->where("r.patient_id = p.patient_id  AND r.doc_id = d.doc_id  AND r.status ='" . $status . "'AND r.review<>'' AND a.appointment_id = r.appointment_id"); //order by slave_id DESC ",false);
// ->where("r.slave_id = p.slave_id  AND r.mas_id = d.mas_id  AND r.status ='" . $status . "' AND a.appointment_id = r.appointment_id"); //order by slave_id DESC ",false);
        $this->db->order_by("r.appointment_id", "desc");
        echo $this->datatables->generate();
    }

    function editdispatchers_city() {
        $val = $this->input->post('val');

        $var = $this->db->query("select city from dispatcher where dis_id='" . $val . "'")->result();
        return $var;
    }

    function datatable_passengerrating() {

        $this->load->library('Datatables');
        $this->load->library('table');
        $status = 1;
        $this->datatables->select('p.patient_id, p.first_name ,p.email,IFNULL((select round(avg(rating),1)  from patient_rating where p.patient_id = patient_id), 0) as rating', false)
                ->from('patient p'); //->where('r.status =" ' . $status . '"'); //order by slave_id DESC ",false);
        $this->db->order_by("p.patient_id", "desc");
        echo $this->datatables->generate();
    }

    function datatable_compaigns($status) {

        $this->load->library('Datatables');
        $this->load->library('table');
        if ($status == 1) {
            $this->datatables->select("cp.id,cp.discount,cp.referral_discount,cp.message,c.city_name")
                    ->unset_column('cp.id')
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', ' cp.id')
                    ->from(" coupons cp, city c")
                    ->where('cp.city_id = c.city_id and cp.coupon_type = " ' . $status . ' " and cp.status = "0" and user_type = 2'); //order by slave_id DESC ",false);
        } elseif ($status == 2) {

            $this->datatables->select("cp.id,cp.coupon_code,cp.start_date,cp.expiry_date, cp.discount,cp.message,c.city_name")
                    ->unset_column('cp.id')
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', ' cp.id')
                    ->from(" coupons cp, city c")
                    ->where('cp.city_id = c.city_id and cp.coupon_type = " ' . $status . ' " and cp.status = "0" and user_type = 2'); //order by slave_id DESC ",false);
        } else if ($status == 3) {
            $this->datatables->select("cp.id,cp.coupon_code,cp.start_date,cp.expiry_date, cp.discount,cp.message,c.city_name")
                    ->unset_column('cp.id')
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', ' cp.id')
                    ->from(" coupons cp, city c")
                    ->where('cp.city_id = c.city_id and cp.coupon_type = " ' . $status . ' " and cp.status = "0" and user_type = 2'); //order by slave_id DESC ",false);
        }
        echo $this->datatables->generate();
    }

    function editNewVehicleData() {

        $vehicle_id = $this->input->post('vehicle_id');
        $title = $this->input->post('title');
        $vehiclemodel = $this->input->post('vehiclemodel');
        $vechileregno = $this->input->post('vechileregno');
        $licenceplaetno = $this->input->post('licenceplaetno');
        $vechilecolor = $this->input->post('vechilecolor');
        $type_id = $this->input->post('getvechiletype');
        $expirationrc = $this->input->post('expirationrc');
        $expirationinsurance = $this->input->post('expirationinsurance');
        $expirationpermit = $this->input->post('expirationpermit');
        $companyid = $this->input->post('company_id'); //$this->session->userdata('LoginId');

        $insuranceno = $this->input->post('Vehicle_Insurance_No'); //$_REQUEST['Vehicle_Insurance_No'];
//        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/roadyo_live/pics/';


        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/' . mainfolder . '/pics/';


        if ($_FILES["certificate"]["name"] != '' && $_FILES["certificate"]["size"] > 0) {
            $name = $_FILES["certificate"]["name"];
            $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice
            $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;
            move_uploaded_file($_FILES['certificate']['tmp_name'], $documentfolder . $cert_name);
            $this->db->query("update vechiledoc set expirydate = '" . $expirationrc . "', url = '" . $cert_name . "' where doctype = 1 and vechileid = '" . $vehicle_id . "'");
        } else {
            $this->db->query("update vechiledoc set expirydate = '" . $expirationrc . "' where doctype = 1 and vechileid = '" . $vehicle_id . "'");
        }
        if ($_FILES["insurcertificate"]["name"] != '' && $_FILES["insurcertificate"]["size"] > 0) {
            $name = $_FILES["insurcertificate"]["name"];
            $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice
            $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;
            move_uploaded_file($_FILES['insurcertificate']['tmp_name'], $documentfolder . $cert_name);
            $this->db->query("update vechiledoc set expirydate = '" . $expirationinsurance . "', url = '" . $cert_name . "' where doctype = 2 and vechileid = '" . $vehicle_id . "'");
        } else {
            $this->db->query("update vechiledoc set expirydate = '" . $expirationinsurance . "' where doctype = 2 and vechileid = '" . $vehicle_id . "'");
        }
        if ($_FILES["carriagecertificate"]["name"] != '' && $_FILES["carriagecertificate"]["size"] > 0) {
            $name = $_FILES["carriagecertificate"]["name"];
            $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice
            $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;
            move_uploaded_file($_FILES['carriagecertificate']['tmp_name'], $documentfolder . $cert_name);
            $this->db->query("update vechiledoc set expirydate = '" . $expirationpermit . "', url = '" . $cert_name . "' where doctype = 3 and vechileid = '" . $vehicle_id . "'");
        } else {
            $this->db->query("update vechiledoc set expirydate = '" . $expirationpermit . "' where doctype = 3 and vechileid = '" . $vehicle_id . "'");
        }

        if ($_FILES["imagefile"]["name"] != '' && $_FILES["imagefile"]["size"] > 0) {
            $name = $_FILES["imagefile"]["name"];
            $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice
            $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;
            move_uploaded_file($_FILES['imagefile']['tmp_name'], $documentfolder . $cert_name);
            $updateImageString = ", Vehicle_Image = '" . $cert_name . "'";
        }


//        try {
//
//            move_uploaded_file($_FILES['insurcertificate']['tmp_name'], $documentfolder . $insurance_name);
//            move_uploaded_file($_FILES['carriagecertificate']['tmp_name'], $documentfolder . $carriage_name);
//            move_uploaded_file($_FILES['imagefile']['tmp_name'], $documentfolder . $image_name);
//        } catch (Exception $ex) {
//            print_r($ex);
//            return false;
//        }

        $this->db->query("update workplace set type_id = '" . $type_id . "',Title = '" . $title . "',Vehicle_Model = '" . $vehiclemodel . "',Vehicle_Reg_No = '" . $vechileregno . "', License_Plate_No = '" . $licenceplaetno . "',Vehicle_Color = '" . $vechilecolor . "',company = '" . $companyid . "',Vehicle_Insurance_No = '" . $insuranceno . "'" . $updateImageString . " where workplace_id = '" . $vehicle_id . "'");

        if ($this->db->affected_rows > 0) {
            return true;
        } else {
            return false;
        }


        return;
    }

    function delete_vehicletype() {
        $val = $this->input->post('val');
        $this->load->library('mongo_db');
        foreach ($val as $row) {

            $this->db->query("delete from workplace_types where type_id='" . $row . "' ");

            $this->mongo_db->delete('vehicleTypes', array('type' => (int) $row));
        }
        if ($this->db->affected_rows() > 0) {

            echo json_encode(array('msg' => "vehicle type deleted successfully", 'flag' => 1));
            return;
        }
    }

    function delete_company() {

        $val = $this->input->post('val');
        foreach ($val as $row) {

            $this->db->query("delete from company_info where company_id='" . $row . "' ");

            $this->db->query("delete from master where company_id ='" . $row . "'");
        }
        if ($this->db->affected_rows() > 0) {
            echo json_encode(array('msg' => "company/companies deleted successfully", 'flag' => 1));
            return;
        }
    }

    function get_documentdata($status) {
        if ($status == 1) {
            $result = $this->db->query("select c.first_name,c.last_name,d.url,d.doc_ids,d.expirydate from master c,docdetail d where c.mas_id=d.driverid and d.doctype=1")->result();
            return $result;
        } else if ($status == 2) {
            $result = $this->db->query("select c.first_name,c.last_name,d.url,d.doc_ids,d.expirydate from master c,docdetail d where c.mas_id=d.driverid and d.doctype=2")->result();
            return $result;
        } else if ($status == 3) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        } else if ($status == 4) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=3")->result();
            return $result;
        } else if ($status == 5) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        } else if ($status == 5) {
            $result = $this->db->query("SELECT d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        } else if ($status == 6) {
            $result = $this->db->query("SELECT d.url,d.docid,v.vehicletype,d.expirydate,d.vechileid,d.url,w.company,(select companyname from company_info where company_id = w.company) as companyname FROM workplace w,vechiledoc d,vehicleType v where w.title=v.id and w.workplace_id = d.vechileid and d.doctype=2")->result();
            return $result;
        }
    }

    function setsessiondata($tablename, $LoginId, $res, $email, $password, $off) {

        $offset = -($off);

        $sessiondata = array(
            'emailid' => $email,
            'password' => $password,
            'LoginId' => $res->$LoginId,
            'profile_pic' => $res->logo,
            'first_name' => $res->companyname,
            'table' => $tablename,
            'city_id' => '0', 'company_id' => '0',
            'validate' => true,
            'admin' => 'super',
            'offset' => $offset
        );

        return $sessiondata;
    }

    function Drivers($status = '') {

        $quaery = $this->db->query("SELECT mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt,(select type from user_sessions where oid = mas.mas_id order by oid DESC limit 0,1) as dev_type FROM master mas where  mas.status IN (" . $status . ") and mas.company_id IN (" . $this->session->userdata('LoginId') . ") order by mas.mas_id DESC")->result();
        return $quaery;
    }

    function datatable($status = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

//        $explodeDateTime = explode(' ', date("Y-m-d H:s:i"));
//        $explodeDate = explode('-', $explodeDateTime[0]);
//        $weekData = $this->week_start_end_by_date(date("Y-m-d H:s:i"));
//        $this->datatables->query("select doc.mas_id,doc.first_name,doc.workplace_id, doc.last_name, doc.email, doc.license_num,doc.license_exp,
//                                          doc.board_certification_expiry_dt, doc.mobile, doc.status, doc.profile_pic,
//                                           (select count(appointment_id) from appointment where mas_id = doc.mas_id and status = 9) as cmpltApts,
//                                            (select sum(amount) from appointment where mas_id = doc.mas_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9) as today_earnings,
//                                             (select amount from appointment where mas_id = doc.mas_id and status = 9 order by appointment_id DESC limit 0, 1) as last_billed_amount,
//                                              (select sum(amount) from appointment where mas_id = doc.mas_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "') as week_earnings,
//                                              (select sum(amount) from appointment where mas_id = doc.mas_id and status = 9 and DATE_FORMAT(appointment_dt, '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "') as month_earnings,
//                                               (select sum(amount) from appointment where mas_id = doc.mas_id and status = 9) as total_earnings
//                                               from master doc where doc.company_id = '" . $this->session->userdata("LoginId") . "'");
//        $this->datatables->query('select * from city');
//        $this->datatables->select('doc.mas_id,doc.first_name,doc.workplace_id, doc.last_name, doc.email, doc.license_num,doc.license_exp,doc.board_certification_expiry_dt, doc.mobile, doc.status, doc.profile_pic')
//            ->select('(select count(appointment_id) from appointment where mas_id = doc.mas_id and status = 9) as cmpltApts')
//            ->select("(select sum(amount) from appointment where mas_id = doc.mas_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9) as today_earnings")
//            ->select('(select amount from appointment where mas_id = doc.mas_id and status = 9 order by appointment_id DESC limit 0'.','.'1) as last_billed_amount',false)
//            ->select("(select sum(amount) from appointment where mas_id = doc.mas_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "') as week_earnings")
//            ->select("(select sum(amount) from appointment where mas_id = doc.mas_id and status = 9 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ) as month_earnings",false)
//            ->select("(select sum(amount) from appointment where mas_id = doc.mas_id and status = 9) as total_earnings")
//            ->from('master doc');
//        $this->datatables->select('count(appointment_id) as cmpltApts')->from('appointment')->where('mas_id = doc.mas_id and status = 9');
//        $this->datatables->select('sum(amount) as today_earnings')->from('appointment')->where('mas_id = doc.mas_id DATE(appointment_dt) = "' . $explodeDateTime[0] . '"and status = 9');


        $this->datatables->select("*")->from('slave')->where('status', 3); //order by slave_id DESC ",false);

        echo $this->datatables->generate();
    }

    function validateEmail() {

        $query = $this->db->query("select doc_id from doctor where email='" . $this->input->post('email') . "'");
        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => '1'));
            return;
        } else {
            echo json_encode(array('msg' => '0'));
        }
    }

    function validatedispatchEmail() {

        $query = $this->db->query("select dis_id from dispatcher where dis_email='" . $this->input->post('email') . "'");
        if ($query->num_rows() > 0) {
            echo json_encode(array('msg' => '1'));
            return;
        } else {
            echo json_encode(array('msg' => '0'));
            return;
        }
    }

    function get_workplace() {
        $res = $this->db->query("select * from workplace_types")->result();
        return $res;
    }

    function get_cities() {
        $query = $this->db->query('select * from city_available')->result();
        return $query;
    }

    function loadcity() {
        $countryid = $this->input->post('country');
        $Result = $this->db->query("select * from city where Country_Id=" . $countryid . "")->result();
        return $Result;
    }

    function loadcompany() {
        $cityid = $this->input->post('city');
        $Result = $this->db->query("select * from company_info where city=" . $cityid . " and status = 3 ")->result_array();
        return $Result;
    }

    function get_city() {
        return $this->db->query("select ci.*,co.Country_Name from city_available ci,country co where ci.country_id = co.country_id ORDER BY ci.City_Name ASC")->result();
    }

    function get_companyinfo($status) {
        return $this->db->query("select * from company_info where status = '" . $status . "' ")->result();
    }

    function editdriver($status = '') {
//        $driverid = $this->input->post('val');

        $data['masterdata'] = $this->db->query("select * from doctor where doc_id ='" . $status . "' ")->result();

//        $data['masterdoc'] = $this->db->query("select * from docdetail where driverid ='" . $status . "' ")->result();

        return $data;
    }
    function editPatientdate($status = '') {
//        $driverid = $this->input->post('val');

        $data = $this->db->query("select * from patientnew where id ='" . $status . "' ")->result();

//        $data['masterdoc'] = $this->db->query("select * from docdetail where driverid ='" . $status . "' ")->result();

        return $data;
    }

    function getbooking_data($status = '') {

//        return $this->db->query("select a.*,m.first_name,m.last_name,s.first_name as sfirst_name,s.last_name as slast_name from appointment a,master m,slave s where a.slave_id = s.slave_id and a.mas_id = m.mas_id ")->result();
        $this->load->library('Datatables');
        $this->load->library('table');

//        $companyid = $this->session->userdata('company_id');
        if ($status == '11')
            $query = 'a.patient_id = s.patient_id and a.doc_id = m.doc_id';
        else
            $query = 'a.patient_id = s.patient_id and a.doc_id = m.doc_id and a.status = "' . $status . '"';
//        else if ($this->session->userdata('company_id') != '0' && $status != '11')
//            $query = 'a.slave_id = s.slave_id and a.mas_id = m.mas_id and a.status = "' . $status . '" and m.company_id = "' . $companyid . '" ';
//        else if ($this->session->userdata('company_id') == '0' && $status != '11')
//            $query = 'a.slave_id = s.slave_id and a.mas_id = m.mas_id and a.status = "' . $status . '" ';
//        else if ($status == '11' && $this->session->userdata('company_id') != '0')
//            $query = 'a.slave_id = s.slave_id and a.mas_id = m.mas_id and  m.company_id = "' . $companyid . '" ';


        $this->datatables->select("a.appointment_id,m.doc_id,(select type_name from doctor_type where type_id = m.type_id),(case a.booking_type when 1 then 'Now' when 2 then 'Later' else 'Unavailable.' END) as b_type,m.first_name,m.mobile,s.first_name as name,s.phone,a.address_line1,a.appointment_dt,
        (
    case a.status when 1 then 'Appointment Requested'
     when 2   then
    'Doctor accepted.'
     when 3  then
     'Doctor rejected.'
     when 4  then
    'Patient has cancelled with out fee.'
     when 5   then
    'Doctor is on the way.'
     when 6   then
    'Appointment started.'
     when 7  then
    'Appointment completed.'
     when 8   then
    'Appointment timed out.'
     when 9   then
    'Patient has cancelled with fee.'
     when 10   then
    'Doctor has cancelled.'

    else
    'Status Unavailable.'
    END) as status_result,accpt_time", false)
                ->edit_column('a.appointment_dt', "get_formatedDate/$1", 'a.appointment_dt')
                ->from('appointment a,doctor m,patient s')
                ->where($query);

        $this->db->order_by('a.appointment_id', 'DESC');
//            ->add_column('Actions', '<img src="asdf">', 'City_id')
        echo $this->datatables->generate();
    }

    public function getprofessiondata() {
        $data = $this->db->query('select type_id,type_name from doctor_type')->result();
        return $data;
    }

    public function getDatafromdate_for_all_bookings($stdate = '', $enddate = '', $status = '') {



        $this->load->library('Datatables');
        $this->load->library('table');

//            if($status == '11' && $company_id == '0')
//                $query = 'a.slave_id = s.slave_id and a.mas_id = m.mas_id and a.status';
//            else
        if ($status == '11')
            $query = 'a.patient_id = s.patient_id and a.doc_id = m.doc_id  and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '"';
        else
            $query = 'a.patient_id = s.patient_id and a.doc_id = m.doc_id and a.status = "' . $status . '" and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '"';
//        else if ($company_id != '0' && $status != '11')
//            $query = 'a.slave_id = s.slave_id and a.mas_id = m.mas_id and a.status = "' . $status . '" and m.company_id = "' . $company_id . '" and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '"';
//        else if ($status == '11' && $company_id != '0')
//            $query = 'a.slave_id = s.slave_id and a.mas_id = m.mas_id and m.company_id = "' . $company_id . '" and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '"';
//        else if ($status != '11' && $company_id == '0')
//            $query = 'a.slave_id = s.slave_id and a.mas_id = m.mas_id and a.status = "' . $status . '" and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '"';
//        else
//            $query = 'a.slave_id = s.slave_id and a.mas_id = m.mas_id and a.status = "' . $status . '" and m.company_id = "' . $company_id . '" and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '"';
//        $this->datatables->select("a.appointment_id,m.doc_id,m.first_name,s.first_name as name,a.address_line1,a.appointment_dt,
        $this->datatables->select("a.appointment_id,m.doc_id,(select type_name from doctor_type where type_id = m.type_id),(case a.booking_type when 1 then 'Now' when 2 then 'Later' else 'Unavailable.' END) as b_type,m.first_name,m.mobile,s.first_name as name,s.phone,a.address_line1,a.appointment_dt,            
        (
    case a.status when 1 then 'Request'
     when 2   then
    'Driver accepted.'
     when 3  then
     'Driver rejected.'
     when 4  then
    'Passenger has cancelled.'
     when 5   then
    'Driver has cancelled.'
     when 6   then
    'Driver is on the way.'
     when 7  then
    'Appointment started.'
     when 8   then
    'Driver arrived.'
     when 9   then
    'Appointment completed.'
    when 10 then
    'Appointment timed out.'
    else
    'Status Unavailable.'
    END) as status_result", false)
                ->edit_column('a.appointment_dt', "get_formatedDate/$1", 'a.appointment_dt')
                ->add_column('case a.status when 9 then', '<button class="btn btn-success btn-cons route_map" onclick="route_map()" style="min-width: 83px !important;" data="$1">Map</button>', 'a.appointment_id')
                ->unset_column('s.profile_pic')
                ->from('appointment a,doctor m,patient s')
                ->where($query);

        $this->db->order_by('a.appointment_id', 'DESC');
//            ->add_column('Actions', '<img src="asdf">', 'City_id')
        echo $this->datatables->generate();
    }

//    function payroll() {
//
//        $explodeDateTime = explode(' ', date("Y-m-d H:s:i"));
//        $explodeDate = explode('-', $explodeDateTime[0]);
//        $weekData = $this->week_start_end_by_date(date("Y-m-d H:s:i"));
//
////        echo $explodeDateTime;
////        echo $explodeDate;
////        echo $explodeDateTime[0];
////        echo $weekData;
////        exit();
//
//        $this->load->library('Datatables');
//        $this->load->library('table');
//        $wereclousetocome = ';';
////        if ($this->session->userdata('company_id') != '0') {
////            $wereclousetocome = "a.mas_id = doc.mas_id and  doc.company_id ='" . $this->session->userdata('company_id') . "'";
////
////
////            $this->datatables->select('doc.mas_id as masid,doc.first_name,'
//////                 .'(select count(appointment_id) from appointment where mas_id = doc.mas_id and status = 9) as cmpltApts ,'
////                            . "(case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9),2)  IS NULL then '--'  else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9),2) END) as TODAY_EARNINGS ,"
//////                 .'(select amount from appointment where mas_id = doc.mas_id and status = 9 order by appointment_id DESC limit 0'.','.'1) as last_billed_amount ,'
//////                    TODAY EARNINGS','WEEK EARNINGS','MONTH EARNINGS','LIFE TIME EARNINGS'
////                            . " (case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2)  IS NULL then '--' else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2) END) as WEEK_EARNINGS ,"
////                            . " (case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  IS NULL then '--' else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  END) as MONTH_EARNINGS,"
////                            . " (case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9),2)  IS NULL then '--'  else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9),2) END) as LIFE_TIME_EARNINGS,"
////                            . "(case  when TRUNCATE((select sum(pay_amount) from payroll where doc.mas_id = mas_id),2)  IS NULL then '--' else TRUNCATE((select sum(pay_amount) from payroll where doc.mas_id = mas_id),2) END) as PAID,"
////                            . "(case  when  TRUNCATE(((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and payment_status in (1,3))-(select sum(pay_amount) from payroll where doc.mas_id = mas_id)),2) IS NULL then '--' else TRUNCATE(((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9)-(select sum(pay_amount) from payroll where doc.mas_id = mas_id)),2)END) as DUE", false)
////                    ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/patientDetails/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">DETAILS</button></a>
////            <a href="' . base_url("index.php/superadmin/Driver_pay/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
////                    ->from('master doc,appointment a ', false)
////                    ->where($wereclousetocome);
////        } else {
//        $this->datatables->select('doc.doc_id as masid,(concat(doc.first_name,\'  \',doc.last_name)),'
////                 .'(select count(appointment_id) from appointment where mas_id = doc.mas_id and status = 7) as cmpltApts ,'
//                        . "(case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status in(7,9)),2)  IS NULL then '--'  else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status in(7,9)),2) END) as TODAY_EARNINGS ,"
//
//                        //                 //                 .'(select amount from appointment where mas_id = doc.mas_id and status = 7 order by appointment_id DESC limit 0'.','.'1) as last_billed_amount ,'
////                    TODAY EARNINGS','WEEK EARNINGS','MONTH EARNINGS','LIFE TIME EARNINGS'
//                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status in(7,9) and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2)  IS NULL then '--' else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status in(7,9) and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2) END) as WEEK_EARNINGS ,"
//                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status in(7,9) and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  IS NULL then '--' else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status in(7,9) and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  END) as MONTH_EARNINGS,"
//                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status in(7,9)),2)  IS NULL then '--'  else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status in(7,9)),2) END) as LIFE_TIME_EARNINGS,"
//                        . "(case  when TRUNCATE((select sum(pay_amount) from payroll where doc.doc_id = doc_id),2)  IS NULL then '--' else TRUNCATE((select sum(pay_amount) from payroll where doc.doc_id = mas_id),2) END) as PAID,"
//                        . "(case  when  TRUNCATE(((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 7 and payment_status in (1,3))-(select sum(pay_amount) from payroll where doc.doc_id = mas_id)),2) IS NULL then '--' else TRUNCATE(((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 7)-(select sum(pay_amount) from payroll where doc.doc_id = mas_id)),2)END) as DUE", false)
//                ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/patientDetails/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">DETAILS</button></a>
//            <a href="' . base_url("index.php/superadmin/Driver_pay/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
//                ->from('doctor doc', false)
//                ->where('doc.status = 3');
////        }
//        $this->db->order_by('doc.doc_id', 'DESC');
//        echo $this->datatables->generate();
//    }
    function pay_provider($proid = '') {
        $query1 = "select round(sum(pro_earning),2) as BookingEarning  from bookings where  pro_id = " . $proid . " and payment_status = 0 and payment_type = 2";
        $query2 = "select IFNULL(round((select closing_balance from payroll where mas_id = " . $proid . " order by payroll_id desc limit 1),2),0) as ClosingBal";
        $query3 = "select round(sum(app_earning),2) as appEarning  from bookings where  pro_id = " . $proid . " and payment_status = 0 and payment_type = 1";

        $res1 = $this->db->query($query1)->row();
        $res2 = $this->db->query($query2)->row();
        $res3 = $this->db->query($query3)->row();
        $duebalance = round(($res1->BookingEarning + $res2->ClosingBal) - $res3->appEarning, 2);
        $query = "select first_name from doctor where doc_id = '" . $proid . "'";
        $provider_name = $this->db->query($query)->result()[0]->first_name;
        $totalamountpaid = $this->db->query("SELECT sum(pay_amount) as totalamt from payroll WHERE  mas_id = '" . $proid . "'")->result()[0]->totalamt;
        $payrolldata = $this->db->query("SELECT * from payroll WHERE  mas_id = '" . $proid . "'")->result();

        $url = $this->serviceUrl . 'GetFinancialDataSuperadmin';
        $fields = array(
            'ent_pro_id' => $proid
        );
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $cycleData = json_decode($result, true);

        return array('provider_name' => $provider_name,
            'totalamountpaid' => $totalamountpaid,
            'duebalance' => $duebalance,
            'cycleData' => $cycleData);
    }
    function payroll() {

        $this->load->library('Datatables');
        $this->load->library('table');
        if ($this->input->post('sSearch') != '') {
            $this->datatables->select('doc.doc_id as masid,(concat(doc.first_name,\'  \',doc.last_name)),'
                            . " IFNULL(round((select p.closing_balance from payroll p,bookings b where  b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0) ,"
                            . " IFNULL((select count(*) from bookings b where b.pro_id = doc.doc_id and payment_Status = 0),0)  as BookingCycle ,"
                            . " IFNULL(round((select round(sum(b.pro_earning),2) from bookings b  where b.pro_id = doc.doc_id and payment_status = 0),2),0) as EarningCycle ,"
                            . " IFNULL(round((select round(sum(b.pro_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 2),2),0) - IFNULL(round((select round(sum(b.app_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 1),2),0) + IFNULL(round((select p.closing_balance from bookings b,payroll p  where b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0) as ClosingBal", false)
                    ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/pay_provider/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
                    ->from('doctor doc', false)
                    ->where('doc.status = 3');

            // - IFNULL(round((select round(sum(bo.app_earning),2) from bookings bo where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 1),2),0)
        } else {
            $this->datatables->select('doc.doc_id as masid,(concat(doc.first_name,\'  \',doc.last_name)),'
                            . " concat('" . CURRENCY . "',' ', IFNULL(round((select p.closing_balance from payroll p,bookings b where  b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0)) ,"
                            . " IFNULL((select count(*) from bookings b where b.pro_id = doc.doc_id and payment_Status = 0),0)  as BookingCycle ,"
                            . " concat('" . CURRENCY . "',' ',  IFNULL(round((select round(sum(b.pro_earning),2) from bookings b  where b.pro_id = doc.doc_id and payment_status = 0),2),0)) as EarningCycle ,"
                            . " concat('" . CURRENCY . "',' ', IFNULL(round((select round(sum(b.pro_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 2),2),0) - IFNULL(round((select round(sum(b.app_earning),2) from bookings b where  b.pro_id = doc.doc_id and payment_status = 0 and payment_type = 1),2),0) + IFNULL(round((select p.closing_balance from bookings b,payroll p  where b.pro_id = p.mas_id and b.pro_id = doc.doc_id order by payroll_id desc limit 1),2),0)) as ClosingBal", false)
                    ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/pay_provider/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
                    ->from('doctor doc', false)
                    ->where('doc.status = 3');
        }
        $this->db->order_by('doc.doc_id', 'DESC');
        echo $this->datatables->generate();
    }

// function payroll() {
//
//        $explodeDateTime = explode(' ', date("Y-m-d H:s:i"));
//        $explodeDate = explode('-', $explodeDateTime[0]);
//        $weekData = $this->week_start_end_by_date(date("Y-m-d H:s:i"));
//
//        $this->load->library('Datatables');
//        $this->load->library('table');
//        $wereclousetocome = ';';
//        if ($this->session->userdata('company_id') != '0') {
//            $wereclousetocome = "a.mas_id = doc.mas_id and  doc.company_id ='" . $this->session->userdata('company_id') . "'";
//
//
//            $this->datatables->select('doc.mas_id as masid,doc.first_name,'
////                 .'(select count(appointment_id) from appointment where mas_id = doc.mas_id and status = 9) as cmpltApts ,'
//                            . "(case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9),2)  IS NULL then '--'  else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9),2) END) as TODAY_EARNINGS ,"
////                 .'(select amount from appointment where mas_id = doc.mas_id and status = 9 order by appointment_id DESC limit 0'.','.'1) as last_billed_amount ,'
////                    TODAY EARNINGS','WEEK EARNINGS','MONTH EARNINGS','LIFE TIME EARNINGS'
//                            . " (case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2)  IS NULL then '--' else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2) END) as WEEK_EARNINGS ,"
//                            . " (case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  IS NULL then '--' else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  END) as MONTH_EARNINGS,"
//                            . " (case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9),2)  IS NULL then '--'  else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9),2) END) as LIFE_TIME_EARNINGS,"
//                            . "(case  when TRUNCATE((select sum(pay_amount) from payroll where doc.mas_id = mas_id),2)  IS NULL then '--' else TRUNCATE((select sum(pay_amount) from payroll where doc.mas_id = mas_id),2) END) as PAID,"
//                            . "(case  when  TRUNCATE(((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9)-(select sum(pay_amount) from payroll where doc.mas_id = mas_id)),2) IS NULL then '--' else TRUNCATE(((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9)-(select sum(pay_amount) from payroll where doc.mas_id = mas_id)),2)END) as DUE", false)
//                    ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/patientDetails/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">DETAILS</button></a>
//            <a href="' . base_url("index.php/superadmin/Driver_pay/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
//                    ->from('master doc,appointment a ', false)
//                    ->where($wereclousetocome);
//        } else {
//
//            $this->datatables->select('doc.mas_id as masid,doc.first_name,'
////                 .'(select count(appointment_id) from appointment where mas_id = doc.mas_id and status = 9) as cmpltApts ,'
//                            . "(case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9),2)  IS NULL then '--'  else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9),2) END) as TODAY_EARNINGS ,"
////                 .'(select amount from appointment where mas_id = doc.mas_id and status = 9 order by appointment_id DESC limit 0'.','.'1) as last_billed_amount ,'
////                    TODAY EARNINGS','WEEK EARNINGS','MONTH EARNINGS','LIFE TIME EARNINGS'
//                            . " (case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2)  IS NULL then '--' else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2) END) as WEEK_EARNINGS ,"
//                            . " (case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  IS NULL then '--' else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  END) as MONTH_EARNINGS,"
//                            . " (case  when TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9),2)  IS NULL then '--'  else TRUNCATE((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9),2) END) as LIFE_TIME_EARNINGS,"
//                            . "(case  when TRUNCATE((select sum(pay_amount) from payroll where doc.mas_id = mas_id),2)  IS NULL then '--' else TRUNCATE((select sum(pay_amount) from payroll where doc.mas_id = mas_id),2) END) as PAID,"
//                            . "(case  when  TRUNCATE(((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9)-(select sum(pay_amount) from payroll where doc.mas_id = mas_id)),2) IS NULL then '--' else TRUNCATE(((select sum(mas_earning) from appointment where mas_id = doc.mas_id and status = 9)-(select sum(pay_amount) from payroll where doc.mas_id = mas_id)),2)END) as DUE", false)
//                    ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/patientDetails/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">DETAILS</button></a>
//            <a href="' . base_url("index.php/superadmin/Driver_pay/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
//                    ->from('master doc', false);
//        }
//
//
////                $this->db->order_by('ap.appointment_id' ,'DESC');
//
//
//        echo $this->datatables->generate();
//    }

       
    function getmap_values() {

        $m = new MongoClient();
        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $bookingid = $this->input->post('mapval');


        $data = $this->mongo_db->get_one('booking_route', array('bid' => (int) $bookingid));

        echo json_encode($data["route"]);
    }

    function payroll_data_form_date($stdate = '', $enddate = '', $company_id = '') {

        $explodeDateTime = explode(' ', date("Y-m-d H:s:i"));
        $explodeDate = explode('-', $explodeDateTime[0]);
        $weekData = $this->week_start_end_by_date(date("Y-m-d H:s:i"));

        $this->load->library('Datatables');
        $this->load->library('table');

//        if ($company_id == '0')
//            $query = 'doc.mas_id = a.mas_id and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '"';
//        else
        $query = 'doc.doc_id = a.doc_id and  DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '"';

        $this->datatables->select('DISTINCT doc.doc_id as masid,doc.first_name,'
//                 .'(select count(appointment_id) from appointment where doc_id = doc.doc_id and status = 9) as cmpltApts ,'
                        . "(case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9),2)  IS NULL then '--'  else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 9),2) END) as TODAY_EARNINGS ,"
//                 .'(select amount from appointment where doc_id = doc.doc_id and status = 9 order by appointment_id DESC limit 0'.','.'1) as last_billed_amount ,'
//                    TODAY EARNINGS','WEEK EARNINGS','MONTH EARNINGS','LIFE TIME EARNINGS'
                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2)  IS NULL then '--' else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9 and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'),2) END) as WEEK_EARNINGS ,"
                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  IS NULL then '--' else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9 and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "' ),2)  END) as MONTH_EARNINGS,"
                        . " (case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9),2)  IS NULL then '--'  else TRUNCATE((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9),2) END) as LIFE_TIME_EARNINGS,"
                        . "(case  when TRUNCATE((select sum(pay_amount) from payroll where mas_id = doc.doc_id),2)  IS NULL then '--' else TRUNCATE((select sum(pay_amount) from payroll where mas_id = doc.doc_id),2) END) as PAID,"
                        . "(case  when  TRUNCATE(((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9 and payment_status in (1,3))-(select sum(pay_amount) from payroll where mas_id = doc.doc_id)),2) IS NULL then '--' else TRUNCATE(((select sum(doc_amount) from appointment where doc_id = doc.doc_id and status = 9)-(select sum(pay_amount) from payroll where mas_id = doc.doc_id)),2)END) as DUE", false)
                ->add_column('SHOW', '<a href="' . base_url("index.php/superadmin/patientDetails/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">DETAILS</button></a>
             <a href="' . base_url("index.php/superadmin/Driver_pay/$1") . '"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Pay</button>', 'masid')
                ->from(' doctor doc,appointment a ', false)
                ->where($query);


//                $this->db->order_by('ap.appointment_id' ,'DESC');


        echo $this->datatables->generate();
    }

    function addNewDriverData() {

        $datai['first_name'] = $this->input->post('firstname');
        $datai['type_id'] = $this->input->post('professionid');
        $datai['last_name'] = $this->input->post('lastname');
        $pass = $this->input->post('password');
        $datai['password'] = md5($pass);
//        $datai['created_dt'] = $this->input->post('current_dt');
        //   $datai['type_id'] = 1;
        $datai['status'] = 1;
        $datai['email'] = $this->input->post('email');
        $datai['mobile'] = $this->input->post('mobile');
        $datai['zipcode'] = $this->input->post('zipcode');
        date_default_timezone_set("Asia/Kolkata");
        $datai['created_dt'] = date("Y-m-d H:i:s");
        //$datai['created_dt'] = date("Y-m-d H:i:s");
        $expirationrc = $this->input->post('expirationrc');
//        $datai['company_id'] = $this->session->userdata('LoginId');

        $name = $_FILES["certificate"]["name"];
        $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice //1  doctype
        $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;

//        $insurname = $_FILES["photos"]["name"];
//        $ext1 = substr($insurname, strrpos($insurname, '.') + 1); //explode(".", $insurname);
//        $insurance_name = (rand(1000, 9999) * time()) . '.' . $ext1;

        $carriagecert = $_FILES["passbook"]["name"];
        $ext2 = substr($carriagecert, strrpos($carriagecert, '.') + 1); //explode(".", $carriagecert); 2 doctype
        $carriage_name = (rand(1000, 9999) * time()) . '.' . $ext2;
$insurance_name1 = '';
        if (isset($_FILES["photos"])) {
            
            $pro_pic = $_FILES["photos"]["name"];
            $size = $_FILES['photos']['size'];
            $tmp = $_FILES['photos']['tmp_name'];
            $pro_pic_ext = substr($pro_pic, strrpos($pro_pic, '.') + 1);
            $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");
            $msg = false;
             
            $bucket = 'privemdeast';
        $s3 = new S3('AKIAJ2MP7CZQJJHV4Q5Q', 'mye8glzEVpip1a0IPb3Mh95v5V2N5Ze0jq5uTdLR');
//          print_r("ds");die;
        $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
          
            
            if (strlen($pro_pic) > 0) {
                
                if (in_array($pro_pic_ext, $valid_formats)) {
                    if ($size < (1024 * 1024)) {
                        $profilepicw = (rand(1000, 9999) * time());
                        $actual_image_name =   $profilepicw . "." . $pro_pic_ext;
               
                        if ($s3->putObjectFile($tmp, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
                            $msg = true;
                            $s3file = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                           
                        } else
                            $msg = false;
                    } else
                        $msg = false;
                } else
                    $msg = false;
            } else
                $msg = true;

//            if ($msg == FALSE) {
//                die('hjhjgj');
//                return false;
//            }
            if (isset($s3file)) {
                $insurance_name1 = $s3file;
                
            }
        }
        $datai['profile_pic'] = $insurance_name1;
//             $datai['profile_pic']=$carriage_name;
        //LAXMAN
//        $getAmtQry = "select amount from doctor_type where type_id = '" . $datai['type_id'] . "' limit 0,1";
//        $amtData = $this->db->query($getAmtQry)->row_array();
//        $datai['amount'] = $amtData['amount'];
        //LAXMAN

        $this->db->insert('doctor', $datai);

        $newdriverid = $this->db->insert_id();

        $us['oid'] = $newdriverid;
        $us['user_type'] = 1;
        $us['type'] = 3;
        $us['loggedIn'] = 2;
        $us['create_date'] = date("Y-m-d H:i:s");
        $this->db->insert('user_sessions', $us);
//
//        $docdetail = array('url' => $cert_name, 'expirydate' => date("Y-m-d", strtotime($expirationrc)), 'doctype' => 1, 'driverid' => $newdriverid);
//        $this->db->insert('docdetail', $docdetail);
//        $docdetail1 = array('url' => $carriage_name, 'expirydate' => '0000-00-00', 'doctype' => 2, 'driverid' => $newdriverid);
//        $this->db->insert('docdetail', $docdetail1);
//        print_r($datai);
        $amount='';
        $type='';
        if($datai['type_id'] == 1){
            $amount = "299";
            $type = 1;
        }else if($datai['type_id'] == 2){
            $amount = "99";
            $type = 2;
        }
        $this->load->library('mongo_db');
        $curr_date = time();
        $curr_gmt_dates = gmdate('Y-m-d H:i:s', $curr_date);
        $curr_gmt_date = new MongoDate(strtotime($curr_gmt_dates));
        $mongoArr = array("radius" => 10, "type" => $type, "user" => (int) $newdriverid, "name" => $datai['first_name'], "lname" => $datai['last_name'],"amount"=>$amount,
            "location" => array(
                "longitude" => 0,
                "latitude" => 0
            ), "image" => $insurance_name1, 'accepted' => 0, 'later_status' => 1, "rating" => 0, 'status' => 1, 'email' => strtolower($datai['email']), 'dt' => $curr_gmt_date
        );

        $this->mongo_db->insert('location', $mongoArr);

        $config = new config();
        $mail = new sendAMail($config->getHostUrl());
        $mailArr = $mail->sendDocWelcomeMail($datai['email'], $datai['first_name']);

        $query = $this->db->query("SELECT * FROM superadmin");
        foreach ($query->result() as $row) {
            $mailres = $mail->sendMailToSuperAdmin($datai, $row->username, "Admin");
        }

        return true;
    }
    function editNewPatientData() {
 
        $id = $this->input->post('pid');
        
        $nonre = $this->input->post('nonre');
        if($nonre == "1")
        {
            $datai['is_eco']=$this->input->post('priveIDs');
        }else{
            $datai['is_eco']="Non-ACO";
        }
        $datai['tin'] = $this->input->post('tin');
        $datai['npi'] = $this->input->post('npi');
        $datai['hic'] = $this->input->post('hic');
        $datai['provider_name'] = $this->input->post('provider_name');
        
        $datai['physician_first_name'] = $this->input->post('physician_first_name');
        $datai['physician_last_name'] = $this->input->post('physician_last_name');
        $datai['risk_scor'] = $this->input->post('risk_scor');
        $datai['risk_band'] = $this->input->post('risk_band');
        $datai['patient_first_name'] = $this->input->post('patient_first_name');
        
        $datai['patient_last_name'] = $this->input->post('patient_last_name');
        $datai['gender'] = $this->input->post('gender');
        $datai['dob'] = $this->input->post('dob');
        $datai['age'] = $this->input->post('age');
        $datai['home_phone'] = $this->input->post('home_phone');
        
        $datai['mobile_phone'] = $this->input->post('mobile_phone');
        $datai['email'] = $this->input->post('email');
        $datai['email_alt'] = $this->input->post('email_alt');
        $datai['note'] = $this->input->post('note');
        $datai['mail_add_1'] = $this->input->post('mail_add_1');
        
        $datai['mail_add_2'] = $this->input->post('mail_add_2');
        $datai['mail_add_3'] = $this->input->post('mail_add_3');
        $datai['mail_city'] = $this->input->post('mail_city');
        $datai['mail_state'] = $this->input->post('mail_state');
        $datai['mail_zip_code'] = $this->input->post('mail_zip_code');
        
        $datai['mail_zip_code_4'] = $this->input->post('mail_zip_code_4');
        $datai['local_add_1'] = $this->input->post('local_add_1');
        $datai['local_add_2'] = $this->input->post('local_add_2');
        $datai['local_add_3'] = $this->input->post('local_add_3');
        $datai['location_city'] = $this->input->post('location_city');
        
        $datai['location_state'] = $this->input->post('location_state');
        $datai['location_zip_code'] = $this->input->post('location_zip_code');
        $datai['location_zip_code_4'] = $this->input->post('location_zip_code_4');
        $datai['date_of_deth'] = $this->input->post('date_of_deth');
        
        
         $this->db->where('id', $id);
        $this->db->update('patientnew', $datai);
        

        
        
 

        return true;
    }
    function AddNewPatientData() {
        
                $data = $this->db->query("SELECT id FROM PriveMd.patientnew order by id  DESC limit 0,1")->row()->id;
        $total=$data+1;        
        $datai['priveId'] = $this->input->post('priveIDs')."-" .$total;
        $nonre = $this->input->post('nonre');
        if($nonre == "1")
        {
            $datai['is_eco']=$this->input->post('priveIDs');
        }else{
            $datai['is_eco']="Non-ACO";
        }
        $datai['tin'] = $this->input->post('tin');
        $datai['npi'] = $this->input->post('npi');
        $datai['hic'] = $this->input->post('hic');
        $datai['provider_name'] = $this->input->post('provider_name');
        
        $datai['physician_first_name'] = $this->input->post('physician_first_name');
        $datai['physician_last_name'] = $this->input->post('physician_last_name');
        $datai['risk_scor'] = $this->input->post('risk_scor');
        $datai['risk_band'] = $this->input->post('risk_band');
        $datai['patient_first_name'] = $this->input->post('patient_first_name');
        
        $datai['patient_last_name'] = $this->input->post('patient_last_name');
        $datai['gender'] = $this->input->post('gender');
        $datai['dob'] = $this->input->post('dob');
        $datai['age'] = $this->input->post('age');
        $datai['home_phone'] = $this->input->post('home_phone');
        
        $datai['mobile_phone'] = $this->input->post('mobile_phone');
        $datai['email'] = $this->input->post('email');
        $datai['email_alt'] = $this->input->post('email_alt');
        $datai['note'] = $this->input->post('note');
        $datai['mail_add_1'] = $this->input->post('mail_add_1');
        
        $datai['mail_add_2'] = $this->input->post('mail_add_2');
        $datai['mail_add_3'] = $this->input->post('mail_add_3');
        $datai['mail_city'] = $this->input->post('mail_city');
        $datai['mail_state'] = $this->input->post('mail_state');
        $datai['mail_zip_code'] = $this->input->post('mail_zip_code');
        
        $datai['mail_zip_code_4'] = $this->input->post('mail_zip_code_4');
        $datai['local_add_1'] = $this->input->post('local_add_1');
        $datai['local_add_2'] = $this->input->post('local_add_2');
        $datai['local_add_3'] = $this->input->post('local_add_3');
        $datai['location_city'] = $this->input->post('location_city');
        
        $datai['location_state'] = $this->input->post('location_state');
        $datai['location_zip_code'] = $this->input->post('location_zip_code');
        $datai['location_zip_code_4'] = $this->input->post('location_zip_code_4');
        $datai['date_of_deth'] = $this->input->post('date_of_deth');
        
        

            $this->db->insert('patientnew', $datai);

        
        
 

        return true;
    }
     function new_ajx() {

        

        $this->load->library('Datatables');
        $this->load->library('table');
      
 
        $this->datatables->select('mas.id as rahul,mas.tin,mas.patient_first_name,mas.patient_last_name,mas.gender,mas.dob,mas.mobile_phone,mas.email')
                ->add_column('DETAILS', '<a href="'. base_url("index.php/superadmin/patientdetailData/$1") .'" target="_blank"> <button class="btn btn-primary btn-cons">DETAILS </button></a>', 'rahul')
                ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value= "$1"/>', 'rahul')
                ->from('patientnew mas', false);
//        }
       
        echo $this->datatables->generate();
    }
    function editdriverdata() {

        $driverid = $this->input->post('driver_id');

        $first_name = $this->input->post('firstname');
        $last_name = $this->input->post('lastname');
        $password = $this->input->post('password');

        $created_dt = date('Y-m-d H:i:s', time());
        $type_id = 1;
        $profile_pic1 = "";
         if (isset($_FILES["photos"])) {
            
            $pro_pic = $_FILES["photos"]["name"];
            $size = $_FILES['photos']['size'];
            $tmp = $_FILES['photos']['tmp_name'];
            $pro_pic_ext = substr($pro_pic, strrpos($pro_pic, '.') + 1);
            $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");
            $msg = false;
             
            $bucket = 'privemdeast';
        $s3 = new S3('AKIAJ2MP7CZQJJHV4Q5Q', 'mye8glzEVpip1a0IPb3Mh95v5V2N5Ze0jq5uTdLR');
        $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
           
            
            if (strlen($pro_pic) > 0) {
                
                if (in_array($pro_pic_ext, $valid_formats)) {
                    if ($size < (1024 * 1024)) {
                        $profilepicw = (rand(1000, 9999) * time());
                        $actual_image_name =  $profilepicw . "." . $pro_pic_ext;
               
                        if ($s3->putObjectFile($tmp, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
                            $msg = true;
                            $s3file = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                           
                        } else
                            $msg = false;
                    } else
                        $msg = false;
                } else
                    $msg = false;
            } else
                $msg = true;

//            if ($msg == FALSE) {
//                die('hjhjgj');
//                return false;
//            }
            if (isset($s3file)) {
                $profile_pic1 = $s3file;
                
            }
        }
       
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $zipcode = $this->input->post('zipcode');
        $expirationrc = $this->input->post('expirationrc');
//        $datai['company_id'] = $this->session->userdata('LoginId');
       
        $name = $_FILES["certificate"]["name"];
        $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice //1  doctype
        $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;

//        $insurname = $_FILES["photos"]["name"];
//        $ext1 = substr($insurname, strrpos($insurname, '.') + 1); //explode(".", $insurname);
//        $profilepic = (rand(1000, 9999) * time()) . '.' . $ext1;
 
        $carriagecert = $_FILES["passbook"]["name"];
        $ext2 = substr($carriagecert, strrpos($carriagecert, '.') + 1); //explode(".", $carriagecert); 2 doctype
        $carriage_name = (rand(1000, 9999) * time()) . '.' . $ext2;
       
       
//        $profile_pic='';
//
//        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/' . mainfolder . '/pics/';
//
//        try {
//            move_uploaded_file($_FILES['certificate']['tmp_name'], $documentfolder . $cert_name);
//            if (move_uploaded_file($_FILES['photos']['tmp_name'], $documentfolder . $profilepic)) {
//                $this->uploadimage_diffrent_redulation($documentfolder . $profilepic, $profilepic, $_SERVER['DOCUMENT_ROOT'] . '/' . mainfolder . '/', $ext1);
//                $profile_pic = $profilepic;
//            }
//            move_uploaded_file($_FILES['passbook']['tmp_name'], $documentfolder . $carriage_name);
//        } catch (Exception $ex) {
//            print_r($ex);
//            return false;
//        }

        $license_pic = $cert_name;
        
//             $datai['profile_pic']=$carriage_name;
        if ($insurname != '') {
            
            if ($password == "******") {
                $driverdetails = array('first_name' => $first_name, 'last_name' => $last_name, 'profile_pic' => $profile_pic1,
                    'created_dt' => $created_dt, 'type_id' => $type_id, 'mobile' => $mobile, 'zipcode' => $zipcode);
            } else {
                $driverdetails = array('first_name' => $first_name, 'last_name' => $last_name, 'profile_pic' => $profile_pic1,
                    'created_dt' => $created_dt, 'type_id' => $type_id, 'mobile' => $mobile, 'zipcode' => $zipcode,  'password' => md5($password));
            }
        } else {
            if ($password == "******") {
                $driverdetails = array('first_name' => $first_name, 'last_name' => $last_name,
                    'created_dt' => $created_dt, 'type_id' => $type_id, 'mobile' => $mobile, 'zipcode' => $zipcode, );
            } else {
                $driverdetails = array('first_name' => $first_name, 'last_name' => $last_name,
                    'created_dt' => $created_dt, 'type_id' => $type_id, 'mobile' => $mobile, 'zipcode' => $zipcode,  'password' => md5($password));
            }
        }
        if($profile_pic1 != '')
        {
             $driverdetails['profile_pic'] = $profile_pic1 ;
        } 
       
        
        $this->db->where('doc_id', $driverid);
        $this->db->update('doctor', $driverdetails);
       
//        $data = $this->db->query("select * from docdetail where driverid = '" . $driverid . "' and doctype = 1");
//
//        if ($data->num_rows() > 0) {
//
//            $docdetail = array('url' => $license_pic, 'expirydate' => date("Y-m-d", strtotime($expirationrc)));
//            $this->db->where('driverid', $driverid);
//            $this->db->where('doctype', 1);
//            $this->db->update('docdetail', $docdetail);
//        } else {
//            $this->db->insert('docdetail', array('doctype' => 1, 'driverid' => $driverid, 'url' => $license_pic, 'expirydate' => date("Y-m-d", strtotime($expirationrc))));
//        }
//        $data = $this->db->query("select * from docdetail where driverid = '" . $driverid . "' and doctype = 2");
//
//        if ($data->num_rows > 0) {
//            $docdet = array('url' => $carriage_name, 'expirydate' => '0000-00-00');
//            $this->db->where('driverid', $driverid);
//            $this->db->where('doctype', 2);
//            $this->db->update('docdetail', $docdet);
//        } else {
//            $this->db->insert('docdetail', array('doctype' => 1, 'driverid' => $driverid, 'url' => $carriage_name, 'expirydate' => '0000-00-00'));
//        }
//       
//
        $this->load->library('mongo_db');
        $curr_date = time();
        $curr_gmt_dates = gmdate('Y-m-d H:i:s', $curr_date);
        $curr_gmt_date = new MongoDate(strtotime($curr_gmt_dates));
        $mongoArr=array();
        if ($insurname != '')
        {
            if($profile_pic1 == ''){
                $mongoArr = array("name" => $first_name, "lname" => $last_name, "image" => $insurname,);
            }else{
                $mongoArr = array("name" => $first_name, "lname" => $last_name, "image" => $insurname,"image" => $profile_pic1);
            }
        }
            
        else
        {
             if($profile_pic1 == ''){
                $mongoArr = array("name" => $first_name, "lname" => $last_name);
            }else{
                $mongoArr = array("name" => $first_name, "lname" => $last_name,"image" => $profile_pic1);
            }
        }
            
        
        $this->mongo_db->update('location', $mongoArr, array('user' => (int)$driverid));

//        $mail = new sendAMail($db1->host);
//        $err = $mail->sendMasWelcomeMail(strtolower($email), ucwords($firstname));


        return true;
    }

    function documentgetdatavehicles() {
        $val = $this->input->post("val");

        $vehicleImage = array();

        $return = $data = array();
        foreach ($val as $row) {
            $data = $this->db->query("select * from vechiledoc where vechileid = '" . $row . "'")->result();
//            return $data;
        }
        foreach ($data as $vehicle) {


            $return[] = array('doctype' => $vehicle->doctype, 'url' => $vehicle->url, 'expirydate' => $vehicle->expirydate);
        }
        $vehicleImage = $this->db->query("select Vehicle_Image from workplace where workplace_id = '" . $val[0] . "'")->row_array();
        $return[] = array('doctype' => '99', 'urls' => $vehicleImage['Vehicle_Image'], 'expirydate' => "");

        return $return;
    }

    function uploadimage_diffrent_redulation($file_to_open, $imagename, $servername, $ext) {


        list($width, $height) = getimagesize($file_to_open);

        $ratio = $height / $width;



        /* mdpi 36*36 */
        $mdpi_nw = 36;
        $mdpi_nh = $ratio * 36;

        $mtmp = imagecreatetruecolor($mdpi_nw, $mdpi_nh);

        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($mtmp, $new_image, 0, 0, 0, 0, $mdpi_nw, $mdpi_nh, $width, $height);

        $mdpi_file = $servername . 'pics/mdpi/' . $imagename;

        imagejpeg($mtmp, $mdpi_file, 100);

        /* HDPI Image creation 55*55 */
        $hdpi_nw = 55;
        $hdpi_nh = $ratio * 55;

        $tmp = imagecreatetruecolor($hdpi_nw, $hdpi_nh);

        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($tmp, $new_image, 0, 0, 0, 0, $hdpi_nw, $hdpi_nh, $width, $height);

        $hdpi_file = $servername . 'pics/hdpi/' . $imagename;

        imagejpeg($tmp, $hdpi_file, 100);

        /* XHDPI 84*84 */
        $xhdpi_nw = 84;
        $xhdpi_nh = $ratio * 84;

        $xtmp = imagecreatetruecolor($xhdpi_nw, $xhdpi_nh);

        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($xtmp, $new_image, 0, 0, 0, 0, $xhdpi_nw, $xhdpi_nh, $width, $height);

        $xhdpi_file = $servername . 'pics/xhdpi/' . $imagename;

        imagejpeg($xtmp, $xhdpi_file, 100);

        /* xXHDPI 125*125 */
        $xxhdpi_nw = 125;
        $xxhdpi_nh = $ratio * 125;

        $xxtmp = imagecreatetruecolor($xxhdpi_nw, $xxhdpi_nh);

        if ($ext == "jpg" || $ext == "jpeg") {
            $new_image = imagecreatefromjpeg($file_to_open);
        } else if ($ext == "gif") {
            $new_image = imagecreatefromgif($file_to_open);
        } else if ($ext == "png") {
            $new_image = imagecreatefrompng($file_to_open);
        }
        imagecopyresampled($xxtmp, $new_image, 0, 0, 0, 0, $xxhdpi_nw, $xxhdpi_nh, $width, $height);

        $xxhdpi_file = $servername . 'pics/xxhdpi/' . $imagename;

        imagejpeg($xxtmp, $xxhdpi_file, 100);
    }

    function AddNewVehicleData() {

//        'expirydate' => date("Y-m-d", strtotime($expirationrc)),

        $title = $this->input->post('title');
        $vehiclemodel = $this->input->post('vehiclemodel');
        $vechileregno = $this->input->post('vechileregno');
        $licenceplaetno = $this->input->post('licenceplaetno');
        $vechilecolor = $this->input->post('vechilecolor');
        $type_id = $this->input->post('getvechiletype');
        $expirationrc = $this->input->post('expirationrc');

        $expirationinsurance = $this->input->post('expirationinsurance');
        $expirationpermit = $this->input->post('expirationpermit');
        $companyname = $this->input->post('company_select'); //$this->session->userdata('LoginId');
        $vehicleid = $this->input->post('vehicleid'); //$this->session->userdata('LoginId');

        $insuranceno = $_REQUEST['Vehicle_Insurance_No'];


        $name = $_FILES["certificate"]["name"];
        $ext = substr($name, strrpos($name, '.') + 1); //explode(".", $name); # extra () to prevent notice
        $cert_name = (rand(1000, 9999) * time()) . '.' . $ext;

        $insurname = $_FILES["insurcertificate"]["name"];
        $ext1 = substr($insurname, strrpos($insurname, '.') + 1); //explode(".", $insurname);
        $insurance_name = (rand(1000, 9999) * time()) . '.' . $ext1;

        $carriagecert = $_FILES["carriagecertificate"]["name"];
        $ext2 = substr($carriagecert, strrpos($carriagecert, '.') + 1); //explode(".", $carriagecert);
        $carriage_name = (rand(1000, 9999) * time()) . '.' . $ext2;

        $vehicleimage = $_FILES["imagefile"]["name"];
        $text3 = substr($vehicleimage, strrpos($vehicleimage, '.') + 1);
        $image_name = (rand(1000, 999) * time()) . '.' . $text3;



        $documentfolder = $_SERVER['DOCUMENT_ROOT'] . '/' . mainfolder . '/pics/';

        try {
            move_uploaded_file($_FILES['certificate']['tmp_name'], $documentfolder . $cert_name);
            move_uploaded_file($_FILES['insurcertificate']['tmp_name'], $documentfolder . $insurance_name);
            move_uploaded_file($_FILES['carriagecertificate']['tmp_name'], $documentfolder . $carriage_name);
            move_uploaded_file($_FILES['imagefile']['tmp_name'], $documentfolder . $image_name);
        } catch (Exception $ex) {
            print_r($ex);
            return false;
        }

        $selectPrefixQry = $this->db->query("select (select LEFT(companyname,2) from company_info where company_id = '" . $companyname . "') as company_prefix,(select LEFT(type_name,2) from workplace_types where type_id = '" . $type_id . "') as type_prefix from dual")->result();

        $vehiclePrefix = strtoupper($selectPrefixQry->company_prefix) . strtoupper($selectPrefixQry->type_prefix);

        $get_last_inserted_id = $this->insertQuery($vehiclePrefix, $type_id, $title, $vehiclemodel, $vechileregno, $licenceplaetno, $vechilecolor, $companyname, $insuranceno, $image_name, $vehicleid);

//        if(!$get_last_inserted_id){
//            return false;
//        }

        $insert_doc = $this->db->query("INSERT INTO `vechiledoc`(`url`, `expirydate`, `doctype`,`vechileid`) VALUES ('" . $insurance_name . "','" . (date("Y-m-d", strtotime($expirationinsurance))) . "','2','" . $get_last_inserted_id . "'),
	('" . $cert_name . "','" . (date("Y-m-d", strtotime($expirationrc))) . "','1','" . $get_last_inserted_id . "'),
	('" . $carriage_name . "','" . (date("Y-m-d", strtotime($expirationpermit))) . "','3','" . $get_last_inserted_id . "')");



        return;
    }

    function insertQuery($vehiclePrefix, $type_id, $title, $vehiclemodel, $vechileregno, $licenceplaetno, $vechilecolor, $companyname, $insuranceno, $image_name, $vehicleid) {

        if ($vehicleid != '') {
            $uniq_id = $vehicleid;
        } else {
            $rand = rand(100000, 999999);
            $uniq_id = $vehiclePrefix . $rand; //str_pad($rand, 6, '0', STR_PAD_LEFT);
        }

        $this->db->query("INSERT INTO workplace(uniq_identity,type_id,Title,Vehicle_Model,Vehicle_Reg_No, License_Plate_No,Vehicle_Color,company,Status,Vehicle_Insurance_No,Vehicle_Image) VALUES ('" . $uniq_id . "','" . $type_id . "','" . $title . "','" . $vehiclemodel . "','" . $vechileregno . "','" . $licenceplaetno . "','" . $vechilecolor . "','" . $companyname . "','5','" . $insuranceno . "','" . $image_name . "')");

        if ($this->db->_error_number() == 1586) {
            if ($vehicleid != '') {
                return false;
            }
            return $this->insertQuery($uniq_id, $type_id, $title, $vehiclemodel, $vechileregno, $licenceplaetno, $vechilecolor, $companyname, $insuranceno, $vehicleid);
        } else {
            return $this->db->insert_id();
        }
    }

    function getTransectionData() {
        $this->load->library('Datatables');
        $this->load->library('table');

//        if ($this->session->userdata('company_id') == '0')
        $query = 'ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and ap.status in(7,9,4) and payment_status in(1,3)';
//        else
//            $query = 'd.company_id = c.company_id and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 and payment_status in(1,3) and d.company_id = "' . $this->session->userdata('company_id') . '"';
//            $query = 'ap.status = 9 and ap.payment_status in(1,3)';

        $this->datatables->select("ap.appointment_id,ap.doc_id, ap.appointment_dt,(case ap.status when 7 then round((ap.discount + ap.amount),2) when 9 then ap.cancel_amount END),ap.discount,ap.amount,ap.app_commision,round(ap.pg_commision,2),ap.doc_amount,ap.tr_id,(
     case ap.status when 1 then 'Request'
     when 2   then
    'Doctor accepted.'
     when 3  then
     'Doctor rejected.'
     when 4  then
    'Patient has cancelled.'
     when 5   then
    'Doctor is on the way.'
     when 6   then
    'Appointment started.'
     when 7  then
    'Appointment completed.'
     when 8   then
    'Appointment timed out.'
     when 9   then
    'Patient has cancelled with fee.'
     when 10   then
    'Doctor has cancelled.'
    else
    'status Unavailable.'
    END) as status_result, (case ap.payment_type  when 1 then 'card' when 2 then 'cash' END)", false)
                ->edit_column('ap.appointment_dt', "get_formatedDate/$1", 'ap.appointment_dt')
//                ->edit_column('fare', "get_fare/$1", 'fare')
                ->add_column('Download', '<a href="' . base_url() . '../../getPDF.php?apntId=$1" target="_blank"> <button class="btn btn-primary btn-cons">Download </button></a>', 'ap.appointment_id')
                ->from('appointment ap,doctor d,patient p')
                ->where($query);

        $this->db->order_by('ap.appointment_id', 'DESC');

//            ->add_column('Actions', '<img src="asdf">', 'City_id')
        echo $this->datatables->generate();
    }

    function transection_data_form_date($stdate = '', $enddate = '') {


        $this->load->library('Datatables');
        $this->load->library('table');

//            if($status == '11' && $company_id == '0')
//                $query = 'a.slave_id = s.slave_id and a.mas_id = m.mas_id and a.status';
//            else
//        if($company_id == '0')
//            $query = 'a.slave_id = s.slave_id and a.mas_id = m.mas_id and a.status = "'.$status.'" and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)).'"';
//        else
//            $query = 'a.slave_id = s.slave_id and a.mas_id = m.mas_id and a.status = "'.$status.'" and m.company_id = "'.$company_id.'" and DATE(a.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)).'"';
//        if ($status != 0 && $company_id != 0) {
//            $query = "d.company_id = c.company_id and d.company_id = '" . $company_id . "' and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 and payment_status in(1,3) and ap.payment_type = '" . $status . "' and DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($stdate)) . "' AND '" . date('Y-m-d', strtotime($enddate)) . "'";
//        } else if ($status == 0 && $company_id != 0)
//            $query = "d.company_id = c.company_id and d.company_id = '" . $company_id . "' and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 and payment_status in(1,3) and DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($stdate)) . "' AND '" . date('Y-m-d', strtotime($enddate)) . "'";
//        else if ($status != 0 && $company_id == 0)
//            $query = "d.company_id = c.company_id and  ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 and payment_status in(1,3) and DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($stdate)) . "' AND '" . date('Y-m-d', strtotime($enddate)) . "' ";
//        else
        $query = "ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and ap.status = 7 and payment_status in(1,3) and DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($stdate)) . "' AND '" . date('Y-m-d', strtotime($enddate)) . "' ";

//echo $query;
//        exit();

        $this->datatables->select("ap.appointment_id,ap.doc_id,ap.appointment_dt,round((ap.discount + ap.amount),2),ap.discount,ap.amount, ap.app_commision, ap.pg_commision,ap.doc_amount, ap.tr_id,(
    case ap.status when 1 then 'Request'
     when 2   then
    'Doctor accepted.'
     when 3  then
     'Doctor rejected.'
     when 4  then
    'Patient has cancelled.'
     when 5   then
    'Doctor is on the way.'
     when 6   then
    'Appointment started.'
     when 7  then
    'Appointment completed.'
     when 8   then
    'Appointment timed out.'
     when 9   then
    'Patient has cancelled.'

    else
    'Status Unavailable.'
    END) as status_result, (case ap.payment_type  when 1 then 'card' when 2 then 'cash' END)", false)
                ->edit_column('ap.appointment_dt', "get_formatedDate/$1", 'ap.appointment_dt')
                ->add_column('Download', '<a href="' . base_url() . '../../getPDF.php?apntId=$1" target="_blank"> <button class="btn btn-primary btn-cons">Download </button></a>', 'ap.appointment_id')
                ->from('appointment ap,doctor d,patient p')
                ->where($query);
//        if($status == '2')
//            $this->datatables->unset_columns('Payment Gateway commission ('.currency.')');
        $this->db->order_by('ap.appointment_id', 'DESC');

//            ->add_column('Actions', '<img src="asdf">', 'City_id')
        echo $this->datatables->generate();
    }

    function getDataSelected($selectdval = '') {

//        $query = $this->db->query("select ap.appointment_dt,ap.payment_type,ap.appointment_id,ap.inv_id,ap.txn_id as tr_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname,c.company_id from appointment ap,master d,slave p,company_info c where c.company_id = d.company_id and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.payment_type = '" . $selectdval . "' order by ap.appointment_id DESC LIMIT 200")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
//        return $query;


        $this->load->library('Datatables');
        $this->load->library('table');
        if ($selectdval != '0') {
//        $query = 'c.company_id = d.company_id and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.payment_type = "'.$selectdval .'" order by ap.appointment_id';
            $query = 'ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 7 and payment_status in(1,3) and ap.payment_type = "' . $selectdval . '"';
        } else if ($selectdval == '0' && $this->session->userdata('company_id') == '0') {
            $query = 'd.company_id = c.company_id and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 and payment_status in(1,3)';
        } else if ($selectdval != '0' && $this->session->userdata('company_id') == '0') {
            $query = 'd.company_id = c.company_id and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 and payment_status in(1,3) and ap.payment_type = "' . $selectdval . '" ';
        } else {
            $query = 'ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and ap.status = 9 and payment_status in(1,3) and d.company_id="' . $this->session->userdata('company_id') . '"';
        }


        $this->datatables->select("ap.appointment_id,ap.mas_id,ap.appointment_dt,round((ap.discount + ap.amount),2),ap.discount,ap.amount,ap.app_commission, ap.pg_commission,ap.mas_earning, ap.txn_id,(
    case ap.status when 1 then 'Request'
     when 2   then
    'Driver accepted.'
     when 3  then
     'Driver rejected.'
     when 4  then
    'Passenger has cancelled.'
     when 5   then
    'Driver has cancelled.'
     when 6   then
    'Driver is on the way.'
     when 7  then
    'Appointment started.'
     when 8   then
    'Driver arrived.'
     when 9   then
    'Appointment completed.'
    when 10 then
    'Appointment timed out.'
    else
    'Status Unavailable.'
    END) as status_result, (case ap.payment_type  when 1 then 'card' when 2 then 'cash' END)", false)
                ->edit_column('ap.appointment_dt', "get_formatedDate/$1", 'ap.appointment_dt')
                ->add_column('Download', '<a href="' . base_url() . '../../getPDF.php?apntId=$1" target="_blank"> <button class="btn btn-primary btn-cons">Download </button></a>', 'ap.appointment_id')
                ->from('appointment ap,master d,slave p,company_info c')
                ->where($query);
        $this->db->order_by('ap.appointment_id', 'DESC');

//            ->add_column('Actions', '<img src="asdf">', 'City_id')
        echo $this->datatables->generate();
    }

    function passenger_rating() {
        $status = 1;
        $query = $this->db->query(" SELECT p.patient_id, p.first_name ,p.email,r.rating FROM patient_rating r, patient p WHERE r.patient_id = p.patient_id  AND r.status = 1")->result();


        return $query;
    }

    function driver_review($status) {


        $query = $this->db->query(" SELECT r.review, r.status,r.star_rating, r.review_dt,r.appointment_id, r.doc_id, d.first_name AS mastername, p.patient_id,a.appointment_dt  FROM doctor_ratings r, doctor d, patient p,appointment a WHERE r.patient_id = p.patient_id  AND r.doc_id = d.doc_id  AND r.status ='" . $status . "' AND r.review <>'' AND a.appointment_id = r.appointment_id ")->result();
        return $query;
    }

    function patientDetails($mas_id = '') {
//        $query = $this->db->query("select ap.appointment_dt,ap.appointment_id,ap.inv_id,ap.txn_id as tr_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname,c.company_id from appointment ap,master d,slave p,company_info c where d.company_id = c.company_id and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and d.mas_id ='" . $mas_id . "'  order by ap.appointment_id DESC")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
//        return $query;

        $this->load->library('Datatables');
        $this->load->library('table');

//        if ($this->session->userdata('company_id') != '0')
//            $query = "ap.status = 9 and ap.payment_status in(1,3) and p.slave_id = ap.slave_id and ap.mas_id = doc.mas_id and ap.mas_id='" . $mas_id . "' and  doc.company_id ='" . $this->session->userdata('company_id') . "'";
//        else
        $query = 'ap.status in(7,9) and ap.payment_status in(1,3) and p.patient_id = ap.patient_id and ap.doc_id = doc.doc_id and ap.doc_id="' . $mas_id . '"';

        $this->datatables->select('ap.appointment_id,p.first_name,(case ap.status when 7 then ap.amount when 9 then ap.cancel_amount else "N/A" end) as amount, ap.app_commision, ap.pg_commision,ap.doc_amount', false)
                ->from(' doctor doc,appointment ap,patient p', false)
                ->where($query);


//                $this->db->order_by('ap.appointment_id' ,'DESC');


        echo $this->datatables->generate();
    }

    function patientDetails_form_Date($stdate = '', $enddate = '', $doc_id = '') {

        $this->load->library('Datatables');
        $this->load->library('table');

//        if ($company_id == '0')
//            $query = 'ap.status = 9 and ap.payment_status in(1,3) and p.slave_id = ap.slave_id and ap.mas_id="' . $mas_id . '" and DATE(ap.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '" and ap.mas_id=doc.mas_id';
//        else
        $query = 'ap.status = 9 and ap.payment_status in(1,3) and p.patient_id = ap.patient_id and ap.doc_id="' . $doc_id . '" and DATE(ap.appointment_dt) BETWEEN "' . date('Y-m-d', strtotime($stdate)) . '" AND "' . date('Y-m-d', strtotime($enddate)) . '" and ap.doc_id=doc.doc_id';

        $this->datatables->select('ap.appointment_id,p.first_name,ap.amount, ap.app_commision, ap.pg_commision,ap.doc_amount', false)
                ->from(' doctor doc,appointment ap,patient p', false)
                ->where($query);


//                $this->db->order_by('ap.appointment_id' ,'DESC');


        echo $this->datatables->generate();
    }

    function inactivedriver_review() {
        $val = $this->input->post('val');

        foreach ($val as $row) {
            $values = explode(",", $row);
            $query = $this->db->query("update doctor_ratings set status = 2 where appointment_id= '" . $row . "'");
        }
    }

    function activedriver_review() {
        $val = $this->input->post('val');

        foreach ($val as $row) {
            $values = explode(",", $row);
            $query = $this->db->query("update doctor_ratings set status=1 where  appointment_id= '" . $row . "'");
        }
    }

    function get_Drivers_from_mongo($status) {

        $m = new MongoClient();
        $this->load->library('mongo_db');

        $db = $this->mongo_db->db;

        $selecttb = $db->location;
        $darray = array();
        if ($status == 3) { //online or free
            $drivers = $selecttb->find(array('status' => (int) $status));

            foreach ($drivers as $mas_id) {
                $darray[] = $mas_id['user'];
            }
        } elseif ($status == 567) {//booked
            $drivers = $selecttb->find(array('status' => array('$in' => array(5, 6, 7))));
            foreach ($drivers as $mas_id) {
                $darray[] = $mas_id['user'];
            }
        } elseif ($status == 30) {//OFFLINE
            $drivers = $selecttb->find(array('status' => (int) 4));
            foreach ($drivers as $mas_id) {
                $darray[] = $mas_id['user'];
            }
        }

        $mas_ids = implode(', ', $darray);

        $quaery = $this->db->query("SELECT mas.mas_id, mas.first_name ,mas.zipcode, mas.profile_pic, mas.last_name, mas.email, mas.mobile, mas.status,mas.created_dt,(select type from user_sessions where oid = mas.mas_id order by oid DESC limit 0,1) as dev_type FROM master mas where  mas.mas_id IN (" . $mas_ids . ")  order by mas.mas_id DESC")->result();
        return $quaery;

//        print_r($mas_ids);
    }

    function getDtiverDetail() {

        $did = $this->input->post("did");
        $queryM = $this->db->query("select * from doctor where doc_id ='" . $did . "'")->result();
        $queryapp = $this->db->query("select appointment_id,appointment_dt,address_line1 from appointment  where doc_id='" . $did . "' and status  in(1,2,6,7,8) ORDER BY appointment_id DESC")->result();
        foreach ($queryM as $master) {
            $name = $master->first_name . "  " . $master->last_name;
            $mobile = $master->mobile;
            $email = $master->email;
            $profile = $master->profile_pic;
        }

        if ($profile) {
            $img = base_url() . "../../pics/" . $profile;
        } else {
            $img = base_url() . "../../pics/aa_default_profile_pic.gif";
        }
        $html = '<script>
              function CloseWindow(){
                  document.getElementById("quickview").style.display = "none";
                }
            </script><div id="quickview" class="quickview-wrapper open" data-pages="quickview" style="max-height: 487px;margin-top: 39px;">
            <ul class="nav nav-tabs" style="padding: 0 14px;">
                <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                                                                             <span class="col-xs-height col-middle">
                                                                            <span class="thumbnail-wrapper d32 circular bg-success">
                                                                                <img width="34" height="34" alt="" data-src-retina="' . $img . '" data-src="' . $img . '" src="' . $img . '" class="col-top">
                                                                            </span>
                                                                            </span>
                    <p class="p-l-20 col-xs-height col-middle col-xs-12">
                        <span class="text-master" style="color: #ffffff !important;">' . $name . '</span>
                        <span class="block text-master hint-text fs-12" style="color: #ffffff !important;">+91' . $mobile . '</span>
                    </p>
                </a>
            </ul>
            <p class="close_quick"> <a onClick="CloseWindow()" class="btn-link quickview-toggle"><i class="pg-close" style="color: #ffffff ! important;" ></i></a></p>
            <div class="tab-content" style="top: 21px !important;">
            <div class="list-view-group-container" >
            <ul>
                <li class="chat-user-list clearfix">
                    <div class="form-control">
                        <label class="col-sm-5 control-label">Email</label><label class="col-sm-7 control-label">' . $email . '</label>
                    </div>
                </li>
            </ul>
            <div class="list-view-group-container" style="overflow-y: scroll;max-height: 314px;">
            <div class="list-view-group-header text-uppercase" style="background-color: #f0f0f0;padding: 10px;">
                        APPOINTMENTS</div>';
        foreach ($queryapp as $result) {
            $html.='<div style="overflow: auto;background: #fff;">
                <ul style="margin-top: 15px;">
                    <li class="chat-user-list clearfix">
                        <div class="item share share-self col1" data-social="item" style="border: 2px solid #e5e8e9;">
                            <div class="pull-right" style="margin: 5px 5px 0px 11px;width: 157px;">
                            ' . date("M d Y g:i A", strtotime($result->appointment_dt)) . '
                        </div>
                            <div class="item-header clearfix" style="margin: 5px 8px 11px 12px;">

                            ' . $result->appointment_id . '
                        </div>
                            <div class="item-description" style="">
                                <ul>
                                    <li class="chat-user-list clearfix">
                                         <div class=""  style="border: 1px solid rgba(0, 0, 0, 0.07);">
                                         <p style="padding: 8px;">' . $result->address_line1 . '</p>
                                        </div>
                                    </li>
                                    <li class="chat-user-list clearfix">
                                    <div class="" style="border: 1px solid rgba(0, 0, 0, 0.07);">
                                         <p style="padding: 8px;">' . $result->drop_addr1 . '</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>';
        }
        $html.='</div></div></div></div>';
        echo json_encode(array('html' => $html));
    }

//    function Driver_pay($masid = '') {
//
////      $query = "select * from payroll wehre company_id='".$this->session->userdata('LoginId')."'";
//
//        $query = "select sum(a.amount) as total,m.first_name from appointment a,master m where a.mas_id = '" . $masid . "' and a.mas_id = m.mas_id and a.status = 9 and (a.cancel_status not in(1,2,7) or a.cancel_status is null)";
//        return $this->db->query($query)->result();
//    }



    function Driver_pay($masid = '') {

//      $query = "select * from payroll wehre company_id='".$this->session->userdata('LoginId')."'";

        $query = "select sum(a.doc_amount) as total,m.first_name,"
                . "(select count(settled_flag) from appointment where settled_flag = 0 and doc_id = a.doc_id and doc_amount != 0 and status = 7 and payment_status IN (1,3)) as unsettled_amount_count,"
                . "(select sum(app_commision) from appointment where settled_flag = 0 and payment_type = 2 and doc_id = a.doc_id and doc_amount != 0 and status = 7 ) as driver_hasTo_pay,"
                . "(select sum(doc_amount) from appointment where settled_flag = 0 and  payment_type = 1 and doc_id = a.doc_id and doc_amount != 0 and status = 7) as app_owner_hasTo_pay,"
                . "(select appointment_id from appointment where settled_flag = 0 and doc_id = a.doc_id and status = 7 and payment_status IN (1,3) order by appointment_id DESC limit 0,1) as last_unsettled_appointment_id from appointment a,doctor m where a.doc_id = '" . $masid . "' and a.doc_id = m.doc_id and settled_flag = 0 and a.status = 7 and a.payment_status in (1,3)";
        return $this->db->query($query)->result();
    }

//    function get_payrolldata($id = '') {
//        $quaery = $this->db->query("SELECT * from payroll WHERE  mas_id = '" . $id . "'")->result();
////        $quaery = $this->db->query("SELECT due_amount,closing_balance,pay_date,pay_date,opening_balance,mas_id,trasaction_id,payroll_id,sum(pay_amount) as totalpaid from payroll  WHERE  mas_id = '" . $id . "'")->result();
//        return $quaery;
//    }
//
//    function Totalamountpaid($id = '') {
//        $quaery = $this->db->query("SELECT sum(pay_amount) as totalamt from payroll WHERE  mas_id = '" . $id . "'")->result();
////        $quaery = $this->db->query("SELECT due_amount,closing_balance,pay_date,pay_date,opening_balance,mas_id,trasaction_id,payroll_id,sum(pay_amount) as totalpaid from payroll  WHERE  mas_id = '" . $id . "'")->result();
//        return $quaery;
//    }

    function get_payrolldata($id = '') {
        $quaery = $this->db->query("SELECT * from payroll WHERE  mas_id = '" . $id . "'")->result();
//        $quaery = $this->db->query("SELECT due_amount,closing_balance,pay_date,pay_date,opening_balance,mas_id,trasaction_id,payroll_id,sum(pay_amount) as totalpaid from payroll  WHERE  mas_id = '" . $id . "'")->result();
        return $quaery;
    }

    function Totalamountpaid($id = '') {
//        $quaery = $this->db->query("SELECT sum(pay_amount) as totalamt from payroll WHERE  mas_id = '" . $id . "'")->result();
        $quaery = $this->db->query("select round(sum(doc_amount),2) as tt from appointment where doc_id = '".$id."' and status in (7,9) and payment_status in (1,3) ")->row()->tt;
        $quaeryz = $this->db->query("(select sum(pay_amount) as zz from payroll where '".$id."' = mas_id) ")->row()->zz;
//        print_r($quaeryz);
        $q = $quaery-$quaeryz;
        return $q;
    }

    function get_all_data($stdate, $enddate) {

        if ($stdate || $enddate) {
            $query = $this->db->query("select ap.app_commision,ap.pg_commision,ap.doc_amount,ap.coupon_code,ap.discount,ap.appt_long,ap.appt_lat,ap.address_line1,ap.address_line2,ap.appointment_dt as appointment_date,ap.appointment_id as Bookin_Id,ap.inv_id,ap.tr_id,ap.status,ap.amount,d.email as Driver_email,d.first_name as Driver_First_Name,d.last_name as Driver_Last_Name,p.email as Passenger_email,p.first_name as Passenger_fname,p.last_name as Passenger_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.patient_id = p.patient_id and DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($stdate)) . "' AND '" . date('Y-m-d', strtotime($enddate)) . "' order by ap.appointment_id DESC");
        } else {
            $query = $this->db->query("select ap.app_commision,ap.pg_commision,ap.doc_amount,ap.coupon_code,ap.discount,ap.appt_long,ap.appt_lat,ap.address_line1,ap.address_line2,ap.appointment_dt as appointment_date,ap.appointment_id as Bookin_Id,ap.inv_id,ap.tr_id,ap.status,ap.amount,d.email as Driver_email,d.first_name as Driver_First_Name,d.last_name as Driver_Last_Name,p.email as Passenger_email,p.first_name as Passenger_fname,p.last_name as Passenger_lname from appointment ap,doctor d,patient p  where ap.doc_id = d.doc_id and ap.patient_id = p.patient_id order by ap.appointment_id DESC");
        }


        foreach ($query->result_array() as $row) {

            if ($row['status'] == '1')
                $status = 'Appointment requested';
            else if ($row['status'] == '2')
                $status = 'Doctor accepted.';
            else if ($row['status'] == '3')
                $status = 'Doctor rejected.';
            else if ($row['status'] == '4')
                $status = 'Patient has cancelled.';
            else if ($row['status'] == '5')
                $status = 'Doctor on the way';
            else if ($row['status'] == '6')
                $status = 'Doctor has arrived';
            else if ($row['status'] == '7')
                $status = 'Appointment Completed';
            else if ($row['status'] == '8')
                $status = 'Appointment expired';
            else if ($row['status'] == '9')
                $status = 'Patient cancelled with fee';
            else
                $status = 'Status unavailable.';

            $now = new DateTime($row['complete_dt']);
            $ref = new DateTime($row['start_dt']);
            $diff = $now->diff($ref);

            $data[] = array(
                'Booking_Id' => $row['Bookin_Id'],
                'appointment_Date' => $row['appointment_date'],
                'Amount' => '$' . $row['amount'],
                'App_Commission' => '$' . $row['app_commision'],
                'Payment_Gateway_Commission' => '$' . $row['pg_commision'],
                'Doctor_Earning' => '$' . $row['doc_amount'],
                'Booking_Status' => $status,
                'Doctor_Name' => $row['Driver_First_Name'],
                'Appointment_address' => $row['address_line1'] . $row['address_line2'],
                'Appointment_latitude' => $row['appt_lat'],
                'Appointment_longitude' => $row['appt_long'],
                'Start_date_time' => $row['start_dt'],
//                'Destination' => $row['drop_addr1'] . $row['drop_addr2'],
//                'Drop_latitude' => $row['drop_lat'],
//                'Drop_longitude' => $row['drop_long'],
//                'Drop_date_time' => $row['complete_dt'],
                'Patient_Name' => $row['Passenger_fname'],
//                'Date_time_to_pickup_point' => $row['arrive_dt'],
//                'Waiting_time_minute' => $row['waiting_mts'],
//                'Journey_Duration' => $diff->h . 'hours,' . $diff->i . 'minutes',
//                'Toll_Fee' => $row['toll_fee'],
//                'parking_fee' => $row['parking_fee'],
//                'airport_fee' => $row['airport_fee'],
//                'tip_amount' => $row['tip_amount'],
                'discount' => $row['discount'],
                'discount_code' => $row['coupon_code'],
            );
        }

        return $data;
    }

    function getDatafromdate($stdate, $enddate) {
        $query = $this->db->query("select ap.appointment_dt,ap.appointment_id,ap.inv_id,ap.txn_id as tr_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname,c.company_id from appointment ap,master d,slave p,company_info c where c.company_id = d.company_id and ap.mas_id = d.mas_id and ap.slave_id = p.slave_id and DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($stdate)) . "' AND '" . date('Y-m-d', strtotime($enddate)) . "' order by ap.appointment_id DESC LIMIT 200")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function getuserinfo() {
        $query = $this->db->query("select * from company_info  ")->row();
        return $query;
    }

//    function getPassangerBooking() {
//        $query = $this->db->query("select a.appointment_id,a.complete_dt,a.amount,a.inv_id,a.distance_in_mts,a.appointment_dt,a.drop_addr1,a.drop_addr2,a.mas_id,a.slave_id,d.first_name as doc_firstname,d.profile_pic as doc_profile,d.last_name as doc_lastname,p.first_name as patient_firstname,p.last_name as patient_lastname,a.address_line1,a.address_line2,a.status from appointment a,master d,slave p where a.slave_id=p.slave_id and d.mas_id=a.mas_id and a.status IN (9) and a.slave_id='" . $this->session->userdata("LoginId") . "' order by a.appointment_id desc")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
//        return $query;
//    }
//    function addservices() {
//        $data = $this->input->post('servicedata');
//        $this->db->insert('services', $data);
//    }
//
//    function updateservices($table = '') {
//        $formdataarray = $this->input->post('editservicedata');
//        $id = $this->input->post('id');
//        $this->db->update($table, $formdataarray, array('service_id' => $id));
//    }
//
//    function deleteservices($table = '') {
//        $id = $this->input->post('id');
//        $this->db->where('service_id', $id);
//        $this->db->delete($table);
//    }
//    function getActiveservicedata() {
//        $query = $this->db->query("select * from services")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
//        return $query;
//    }

    function Vehicles($status = '') {
        $quaery = $this->db->query("select w.workplace_id,w.uniq_identity,w.Title,w.Vehicle_Model,w.type_id,w.Vehicle_Reg_No,w.License_Plate_No,w.Vehicle_Insurance_No,w.Vehicle_Color,vt.vehicletype,vm.vehiclemodel,wt.type_id,wt.type_name,ci.companyname FROM workplace w,vehicleType vt,vehiclemodel vm,workplace_types wt,company_info ci where vt.id=w.title and w.company = ci.company_id and vm.id=w.Vehicle_Model and wt.type_id =w.type_id  and w.status ='" . $status . "' order by w.workplace_id desc")->result();
        return $quaery;
    }

    function week_start_end_by_date($date, $format = 'Y-m-d') {

        //Is $date timestamp or date?
        if (is_numeric($date) AND strlen($date) == 10) {
            $time = $date;
        } else {
            $time = strtotime($date);
        }

        $week['week'] = date('W', $time);
        $week['year'] = date('o', $time);
        $week['year_week'] = date('oW', $time);
        $first_day_of_week_timestamp = strtotime($week['year'] . "W" . str_pad($week['week'], 2, "0", STR_PAD_LEFT));
        $week['first_day_of_week'] = date($format, $first_day_of_week_timestamp);
        $week['first_day_of_week_timestamp'] = $first_day_of_week_timestamp;
        $last_day_of_week_timestamp = strtotime($week['first_day_of_week'] . " +6 days");
        $week['last_day_of_week'] = date($format, $last_day_of_week_timestamp);
        $week['last_day_of_week_timestamp'] = $last_day_of_week_timestamp;

        return $week;
    }

    function updateDataProfile() {

        $formdataarray = $this->input->post('fdata');
        $this->db->update('company_info', $formdataarray, array('company_id' => $this->session->userdata("LoginId")));

        $this->session->set_userdata(array('profile_pic' => $formdataarray['logo'],
            'first_name' => $formdataarray['first_name'],
            'last_name' => $formdataarray['last_name']));
    }

    function updateMasterBank() {

        $config = new config();

        $stripe = new StripeModule($config->getStripeSecretKey());

        $checkStripeId = $this->db->query("SELECT stripe_id from master where mas_id = " . $this->session->userdata("LoginId"))->row();

//        if (!is_array($checkStripeId)) {
//            return array('flag' => 2);
//        }

        $userData = $this->input->post('fdata');

        if ($checkStripeId->stripe_id == '') {
            $createRecipientArr = array('name' => $userData['name'], 'type' => 'individual', 'email' => $userData['email'], 'tax_id' => $userData['tax_id'], 'bank_account' => $userData['account_number'], 'routing_number' => $userData['routing_number'], 'description' => 'For ' . $userData['email']);
            $recipient = $stripe->apiStripe('createRecipient', $createRecipientArr);
        } else {
            $updateRecipientArr = array('name' => $userData['name'], 'email' => $userData['email'], 'tax_id' => $userData['tax_id'], 'bank_account' => $userData['account_number'], 'routing_number' => $userData['routing_number'], 'description' => 'For ' . $userData['email']);
            $recipient = $stripe->apiStripe('updateRecipient', $updateRecipientArr);
        }
        if (isset($recipient['error']))
            return array('flag' => 1, 'message' => $recipient['err']['error']['message'], 'data' => $userData); //, 'args' => $recipient);
        else if ($recipient['verified'] === FALSE)
            return array('flag' => 1, 'message' => "Need your full, legal name, you can check the details with the below link<br>https://support.stripe.com/questions/how-do-i-verify-transfer-recipients", 'link' => 'https://support.stripe.com/questions/how-do-i-verify-transfer-recipients', 'data' => $userData);
        else if ($recipient['verified'] === TRUE)
            return array('flag' => 0, 'message' => "Updated bank details successfully", 'data' => $userData);
    }

    function Getdashboarddata() {

//        $offset = $this->session->userdata('offset');
//        $currTime = strtotime($offset . " minutes", time());

        date_default_timezone_set('America/Los_Angeles');
        $currTime = time();

        $todayone = $this->db->query("SELECT a.appointment_id,m.doc_id FROM appointment a,doctor m  WHERE a.doc_id = m.doc_id   and a.appointment_dt like '" . date('Y-m-d') . "%' and a.status = 7 ");
//        $today
        //this week completed booking
        $weekArr = $this->week_start_end_by_date($currTime);
        $week = $this->db->query("SELECT  a.appointment_id,m.doc_id FROM appointment a,doctor m WHERE a.doc_id = m.doc_id  and a.status = 7 and DATE(a.appointment_dt) >= '" . $weekArr['first_day_of_week'] . "'");


        // this month completed booking

        $currMonth = date('n', $currTime);
        $currYear = date('Y', $currTime);
        $month = $this->db->query("SELECT a.appointment_id,m.doc_id FROM appointment a,doctor m WHERE a.doc_id = m.doc_id and a.status = 7  and  MONTH(a.appointment_dt) = '" . $currMonth . "' and YEAR(a.appointment_dt) = '" . $currYear . "'");


        // lifetime completed booking
        $lifetime = $this->db->query("SELECT a.appointment_id,m.doc_id FROM appointment a,doctor m,patient p WHERE a.doc_id = m.doc_id and a.status = 7 and p.patient_id = a.patient_id");

        // total booking uptodate
        $totaluptodate = $this->db->query("SELECT  a.appointment_id,m.doc_id FROM appointment a,doctor m  WHERE a.doc_id = m.doc_id ");



        //today earnings
//
        $todayearning = $this->db->query("SELECT sum(a.amount) as totamount,a.appointment_id,m.doc_id FROM appointment a,doctor m WHERE a.doc_id = m.doc_id  and a.appointment_dt  like '" . date('Y-m-d') . "%' and a.status = 7 ");

//
//
//        //this week completed booking
//
        $weekearning = $this->db->query("SELECT sum(a.amount) as totamount,a.appointment_id,m.doc_id FROM appointment a,doctor m WHERE a.doc_id = m.doc_id  and a.status = 7 and DATE(a.appointment_dt) >= '" . $weekArr['first_day_of_week'] . "'");
//
//
//        // this month completed booking
//
//
        $monthearning = $this->db->query("SELECT sum(a.amount) as totamount,a.appointment_id,m.doc_id FROM appointment a,doctor m WHERE a.doc_id = m.doc_id  and a.status = 7  and  MONTH(a.appointment_dt) = '" . $currMonth . "' ");
//
//
//        // lifetime completed booking
        $lifetimeearning = $this->db->query("SELECT sum(a.amount) as totamount, a.appointment_id,m.doc_id FROM appointment a,doctor m WHERE a.doc_id = m.doc_id and a.status = 7 ");
//
//        // total booking uptodate
        $totaluptodateearning = $this->db->query("SELECT  sum(a.amount) as totalearning, a.appointment_id,m.doc_id FROM appointment a,doctor m WHERE a.doc_id = m.doc_id ");


        $t = $todayearning->row();
        $w = $weekearning->row();
        $m = $monthearning->row();
        $l = $lifetimeearning->row();
        $te = $totaluptodateearning->row();


        $data = array('today' => $todayone->num_rows(), 'week' => $week->num_rows(), 'month' => $month->num_rows(), 'lifetime' => $lifetime->num_rows(), 'total' => $totaluptodate->num_rows(),
            'todayearning' => (float) (($t->totamount - ($t->totamount * (10 / 100)) - (float) (($t->totamount * (2.9 / 100)) + 0.3))), 'weekearning' => (float) (($w->totamount - ($w->totamount * (10 / 100)) - (float) (($w->totamount * (2.9 / 100)) + 0.3))), 'monthearning' => (float) (($m->totamount - ($m->totamount * (10 / 100)) - (float) (($m->totamount * (2.9 / 100)) + 0.3))), 'lifetimeearning' => (float) (($l->totamount - ($l->totamount * (10 / 100)) - (float) (($l->totamount * (2.9 / 100)) + 0.3))), 'totalearning' => $te->totalearning
        );
        return $data;
    }

    function updateData($IdToChange = '', $databasename = '', $db_field_id_name = '') {
        $formdataarray = $this->input->post('fdata');
        $this->db->update($databasename, $formdataarray, array($db_field_id_name => $IdToChange));
    }

    function LoadAdminList() {
        $db = new MongoClient();
        $mongoDB = $db->db_Ryland_Insurence;
        $collection = $mongoDB->Col_Manage_Admin;
        $cursor = $collection->find(array('Role' => "SubAdmin"));
//        $db->close();
        return $cursor;
    }

    function issessionset() {

        if ($this->session->userdata('emailid') && $this->session->userdata('password')) {

            return true;
        }
        return false;
    }

}

?>
