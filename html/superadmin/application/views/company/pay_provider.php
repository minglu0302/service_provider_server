<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
    .exportOptions{
        display: none;
    }
    .jumbotron p {
        font-family: "Segoe UI", Arial, sans-serif;
        font-size: 11px;
        font-weight: normal;
        margin-bottom: inherit;
        color: #626262;
    }
</style>
<?php
$this->load->library('session');
$pay_error = $this->session->userdata('pay_error');
if ($pay_error != '') {
    ?> 
    <script>alert("<?php echo $pay_error; ?>");</script>
    <?php
}
$meta = array('pay_error' => '');
$this->session->set_userdata($meta);
date_default_timezone_set('UTC');

error_reporting(0);
?>

<script>
    $(document).ready(function () {
        $('.payroll').addClass('active');
        $('.payroll').attr('src', "<?php echo base_url(); ?>/theme/icon/payroll_on.png");
        $('#payamount').click(function () {
            var colonneSelected = $('#due_amt').html();
            if (colonneSelected == 0)
                alert("Sorry  there is no due amount for this Provider");
            else if (colonneSelected < 0) {
                $('#collectModal').modal('show');
            } else if (colonneSelected > 0) {
                $('#myModal').modal('show');
//                if (colonneSelected == 0) {
//                    $('.r_amount,.cr_amount,.p_amount').hide();
//                } else {
//                    $('.r_amount,.cr_amount,.p_amount').show();
//                }
            }
        });

        $("#releasedamt").blur(function () {
            var firstamt = parseFloat($('#due_amt').html());
            var secondamt = parseFloat($('#releasedamt').val());
            if (firstamt >= secondamt) {
                $('#error_msg').html("");
            } else {
                $('#error_msg').html("Released Amount Is grater then Due Amount ! ");
                $('#releasedamt').focus();
            }
        });


        $('#submit_form').click(function () {
            $('#error_msg_collect').html('');
            $('#error_msg_chk').html('');

            var firstamt = parseInt($('#releasedamt').val());
            var secondamt = parseInt($('#creleasedamt').val());


            if (!$('input[name=payment_mode]:checked').length <= 0) {
                if (firstamt == secondamt) {
                    if (firstamt > 0 || secondamt > 0) {
                        $('#error_msg_chk').html("");
                        var currentdate = new Date();
                        var datetime = currentdate.getFullYear() + "-"
                                + (currentdate.getMonth() + 1) + "-"
                                + currentdate.getDate() + " "
                                + currentdate.getHours() + ":"
                                + currentdate.getMinutes() + ":"
                                + currentdate.getSeconds();
                        $('#hdate').val(datetime);
                        var colonneSelected = $("#tableWithSearch tr:last").find('.close_bal').text();
                        $('#currunEarnigs').val(colonneSelected);
                        $('#form-work').submit();
                    } else {
                        $('#error_msg_chk').html("Amount Must be Greater then 0!");
                    }

                } else {

                    $('#error_msg_chk').html("Amount does't match !");
                }
            } else {
                $('#error_msg_pay').html("Selecte payment mode ");
            }

        });
        $('#submit_collect').click(function () {
            $('#error_msg_pay').html('');
            $('#error_msg_coll').html('');

            var firstamt = parseInt($('#collectamt').val());
            var secondamt = parseInt($('#ccollectamt').val());
            if (!$('input[name=payment_mode]:checked').length <= 0) {
                if (firstamt == secondamt) {
                    if (firstamt > 0 || secondamt > 0) {
                        $('#error_msg_coll').html("");
                        var currentdate = new Date();
                        var datetime = currentdate.getFullYear() + "-"
                                + (currentdate.getMonth() + 1) + "-"
                                + currentdate.getDate() + " "
                                + currentdate.getHours() + ":"
                                + currentdate.getMinutes() + ":"
                                + currentdate.getSeconds();
                        $('#colldate').val(datetime);
                        var colonneSelected = $("#tableWithSearch tr:last").find('.close_bal').text();
                        $('#currunEarnigs').val(colonneSelected);
                        $('#form-collect').submit();
                    } else {
                        $('#error_msg_coll').html("Amount Must be Greater then 0!");
                    }
                } else {
                    $('#error_msg_coll').html("Amount does't match !");
                }
            } else {
                $('#error_msg_coll').html("Selecte payment mode ");
            }

        });


    });

    function validate(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault)
                theEvent.preventDefault();
        }
    }
</script>
<div class="page-content-wrapper">
    <div class="content">
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <br/>
                <div class="inner" style="-webkit-transform: translateY(0px); opacity: 1;">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url() ?>index.php/superadmin/payroll" class="">Payroll</a>
                        </li>
                        <li><a href="#" class="active">Provider(<?php echo $provider_name; ?>)</a>
                        </li>
                    </ul>
                </div>

                <div class="panel panel-transparent ">
                    <div class="tab-content">
                        <div class="container-fluid container-fixed-lg bg-white">
                            <div class="panel panel-transparent">
                                <div class="panel-heading">
                                    <div class="row clearfix">
                                        <div class="pull-left">
                                            <div class="col-md-6">
                                                <button id="payamount" class="btn btn-primary btn-cons m-b-10 m-l-10 pull-left" type="button" ><i class="pg-form"></i> <span class="bold">PAY</span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="pull-right">                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive"><table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending" style="width: 68px;">SLNO</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">CYCLE START DATE</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">BOOKINGS THIS CYCLE</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">OPENING BALANCE (<?php echo CURRENCY; ?>)</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PROVIDER CARD EARNINGS(<?php echo CURRENCY; ?>)</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">CASH DUE TO APP(<?php echo CURRENCY; ?>)</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PROVIDER RECEIVABLE(<?php echo CURRENCY; ?>)</th> 
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PAID AMOUNT(<?php echo CURRENCY; ?>)</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">PAY DATE</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">CLOSING BALANCE</th>                                         
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">Txn ID</th>                                     
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PAYMENT GATEWAY TXN ID</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PAYMENT STATUS</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">AVG RATING</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">AVG ACCEPTANCE RATE</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $slno = 1;
                                                    ?>
                                                <td class="v-align-middle sorting_1"> <p><?php echo $slno ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo $cycleData['currentCycle']['start_date'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><b><a href=" <?php echo base_url("index.php/superadmin/patientDetails/0/" . $mas_id) ?> "><?php echo $cycleData['currentCycle']['booking_cycle'] ?></a></b></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' . $cycleData['currentCycle']['opening_balance'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' . $cycleData['currentCycle']['booking_earn'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' . $cycleData['currentCycle']['cash_due_app'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' . $cycleData['currentCycle']['pro_receivable'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p>--</p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo $cycleData['currentCycle']['pay_date'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' . $cycleData['currentCycle']['closing_balance'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p>--</p></td>
                                                <td class="v-align-middle sorting_1"> <p>--</p></td>
                                                <td class="v-align-middle sorting_1"> <p>--</p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo $cycleData['currentCycle']['rev_rate'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo $cycleData['currentCycle']['acpt_rate'] ?> %</p></td>
                                                <?php
                                                $slno += 1;
                                                foreach ($cycleData['pastCycle'] as $result) {
                                                    ?>
                                                    <tr role="row"  class="gradeA odd">
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $slno; ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['start_date'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><b><a href=" <?php echo base_url("index.php/superadmin/patientDetails/" . $result['payroll_id'] . "/" . $mas_id) ?> "><?php echo $result['no_of_bookings'] ?></a></b></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['opening_balance'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['booking_earn'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['cash_due_app'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['pro_receivable'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['pay_amount'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['pay_date'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['closing_balance'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo "TXN-PAY-00" . $result['payroll_id'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['trasaction_id'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p>Success</p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['rev_rate'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['acpt_rate'] ?></p></td>
                                                    </tr>
                                                    <?php
                                                    $slno++;
                                                }
                                                ?>
                                                </tbody>
                                            </table></div><div class="row">
                                        </div></div>
                                </div>
                            </div>                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade stick-up in" id="myModal" tabindex="-1" role="dialog" aria-hidden="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Payment <span class="semi-bold">Information</span></h5>
            </div>
            <div class="modal-body">
                <form id="form-work" class="form-horizontal" role="form" autocomplete="off" novalidate="novalidate" action="<?php echo base_url() ?>index.php/superadmin/pay_driver_amount/<?php echo $mas_id ?>" method="post">
                    <div class="form-group-attached">
                        <input type='hidden' name='ctime' id="hdate" value==''>
                        <input type='hidden' id="dueamount" name='dueamount' value='<?php echo $duebalance; ?>'>
                        <div class="form-group">
                            <label for="position" class="col-sm-3 control-label">Due Amount</label>
                            <div class="col-sm-9">
                                <?php echo CURRENCY ?> <span id="due_amt"><?php echo $duebalance; ?></span>
                            </div>
                        </div>                      
                        <div class="form-group">
                            <label for="position" class="col-sm-3 control-label">Payment Mode</label>                           
                            <div class="col-sm-1">
                                <input type="radio" id="cash_pay" value="1" name="payment_mode" class="">
                            </div>
                            <label for="can_after_pro_on" class="col-sm-3 control-label">
                                CASH
                            </label>                          
                            <div class="col-sm-1">
                                <input type="radio" id="card_pay" value="2" name="payment_mode" class="">
                            </div>
                            <label for="can_after_pro_on" class="col-sm-3 control-label">
                                CARD
                            </label>
                            <br>
                            <span id="error_msg_pay" style="color: red"></span>
                        </div>
                        <div class="form-group r_amount">
                            <label for = "position" class = "col-sm-3 control-label">Enter Amount To Be Paid </label>
                            <div class = "col-sm-9">
                                <input type = "text" min = "1" class = "form-control " id = "releasedamt" placeholder = "Collect Amount" onkeypress = 'validate(event)' required = "" aria-required = "true" aria-invalid = "true" >
                                <span id = "error_msg" style = "color: red"></span>
                            </div>
                        </div>
                        <div class = "form-group cr_amount">
                            <label for = "position" class = "col-sm-3 control-label">Re-Confirm Amount</label>
                            <div class = "col-sm-9">
                                <input type = "text" min = "1" class = "form-control " id = "creleasedamt" placeholder = "Confirm Amount" onkeypress = 'validate(event)' required = "" aria-required = "true" aria-invalid = "true" name = "paid_amount">
                                <span id = "error_msg_chk" style = "color: red"></span>
                            </div>
                        </div>
                    </div>
                </form>
                <div class = "row">
                    <div class = "col-sm-8">
                        <div class = "p-t-20 clearfix p-l-10 p-r-10">
                            <div class = "pull-left">
                            </div>
                        </div>
                    </div>
                    <div class = "col-sm-4 m-t-10 sm-m-t-10 p_amount">
                        <button type = "button" id = "submit_form" class = "btn btn-primary btn-block m-t-5">Pay Now</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<div class = "modal fade stick-up in" id = "collectModal" tabindex = "-1" role = "dialog" aria-hidden = "false" >
    <div class = "modal-dialog modal-lg">
        <div class = "modal-content">
            <div class = "modal-header clearfix text-left">
                <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true"><i class = "pg-close fs-14"></i>
                </button>
                <h5>Payment <span class = "semi-bold">Information</span></h5>
            </div>
            <div class = "modal-body">
                <form id = "form-collect" class = "form-horizontal" role = "form" autocomplete = "off" novalidate = "novalidate" action = "<?php echo base_url() ?>index.php/superadmin/collect_driver_amount/<?php echo $mas_id ?>" method = "post">
                    <div class = "form-group-attached">
                        <input type = 'hidden' name = 'ctime' id = "colldate" value==''>
                        <input type = 'hidden' id = "dueamount" name = 'dueamount' value = '<?php echo $duebalance; ?>'>
                        <div class = "form-group">
                            <label for = "position" class = "col-sm-3 control-label">Due Amount</label>
                            <div class = "col-sm-9">
                                <?php echo CURRENCY
                                ?> <span id="due_amt"><?php echo $duebalance; ?></span>
                            </div>
                        </div>                      
                        <div class="form-group">
                            <label for="position" class="col-sm-3 control-label">Payment Mode</label>                           
                            <div class="col-sm-1">
                                <input type="radio" id="cash_pay" value="1" name="payment_mode" class="">
                            </div>
                            <label for="can_after_pro_on" class="col-sm-3 control-label">
                                CASH
                            </label>                          
                            <div class="col-sm-1">
                                <input type="radio" id="card_pay" value="2" name="payment_mode" class="">
                            </div>
                            <label for="can_after_pro_on" class="col-sm-3 control-label">
                                CARD
                            </label>
                            <br>
                            <span id="error_msg_collect" style="color: red"></span>
                        </div>

                        <div class="form-group r_amount">
                            <label for="position" class="col-sm-3 control-label">Enter Amount To Be Collect </label>
                            <div class="col-sm-9">
                                <input type="text" min="1" class="form-control " id="collectamt" placeholder="Amount" onkeypress='validate(event)' required="" aria-required="true" aria-invalid="true" >
                                <span id="error_msg" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group cr_amount">
                            <label for="position" class="col-sm-3 control-label">Re-Confirm Amount</label>
                            <div class="col-sm-9">
                                <input type="text" min="1" class="form-control " id="ccollectamt" placeholder="Confirm Amount" onkeypress='validate(event)' required="" aria-required="true" aria-invalid="true" name="collect_amount">
                                <span id="error_msg_coll" style="color: red"></span>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="p-t-20 clearfix p-l-10 p-r-10">
                            <div class="pull-left">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 m-t-10 sm-m-t-10 coll_amount">
                        <button type="button" id="submit_collect" class="btn btn-primary btn-block m-t-5">Collect Now</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>