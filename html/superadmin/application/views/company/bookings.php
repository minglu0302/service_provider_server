<style>
    .panel-body{
        overflow-x: auto;
    }
    .div-table{
        display:table;         
        width:auto;         
        background-color:#eee;         
        border:1px solid  #666666;         
        border-spacing:5px;/*cellspacing:poor IE support for  this*/
    }
    .div-table-row{
        display:table-row;
        width:auto;
        clear:both;
    }
    .div-table-col{
        float:left;/*fix for  buggy browsers*/
        display:table-column;         
        width:200px;         
        background-color:#ccc;  
    }
    .select2-results {
        max-height: 192px;
    }
    td.details-control {
        background: url('<?php echo base_url() ?>theme/pages/img/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('<?php echo base_url() ?>theme/pages/img/details_close.png') no-repeat center center;
    }
    .table.table-hover tbody tr:hover td.details-control{
        background: url('<?php echo base_url() ?>theme/pages/img/details_open.png') no-repeat center center !important;
    }
    .table.table-hover tbody tr.details:hover td.details-control{
        background: url('<?php echo base_url() ?>theme/pages/img/details_close.png') no-repeat center center !important;
    }
</style>
<script type="text/javascript">
    // request permission on page load
    document.addEventListener('DOMContentLoaded', function () {
        if (Notification.permission !== "granted")
            Notification.requestPermission();
    });
    function bookingcame() {
        var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
        snd.play();
        if (!Notification) {
            alert('Desktop notifications not available in your browser. Try Chromium.');
            return;
        }
        if (Notification.permission !== "granted")
            Notification.requestPermission();
        else {
            var iconPath = "<?php echo small_logo ?>";
            var notification = new Notification('Booking Requested', {
//                icon: 'http://www.iserve.ind.in/pics/iserve_admin_logo.png',
                icon: iconPath,
                body: "Hey there! Someone Requested Booking!",
            });
            notification.onclick = function () {
//                window.open("http://iserve.ind.in/");
                window.open("<?php echo website; ?>");
            };
        }
    }
    $(document).ready(function () {
        var socket = io('<?php echo socketpath; ?>');
        socket.on('LiveBookingClient', function (data) {
            bookingcame();
            var tdata = "<tr style='background-color: yellow' >";
            tdata += "<td>" + data.bid + "</td>";
            tdata += "<td>" + data.dt + "</td>";
            tdata += "<td>" + data.cid + "</td>";
            tdata += "<td>" + data.cname + "</td>";
            tdata += "<td>" + data.cphone + "</td>";
            tdata += "<td>" + data.address + "</td>";
//            tdata += "<td>" + data.catname + "</td>";
//            tdata += "<td>" + data.dtype + "</td>";
            tdata += "<td>" + data.btype + "</td>";
            tdata += "<td>" + data.status + "</td>";
            tdata += "<td>---</td>";
            tdata += "<td>---</td>";
            tdata += "<td class='details-control'></td>";
            tdata += "</tr>";
            $('#big_table').prepend(tdata);
            $('#big_table').find('tr:first').find('th:first').trigger('click');
        });
        socket.on('LiveBookingResponce', function (data) {
            $('#big_table').find('tr:first').find('th:first').trigger('click');
        });
    });</script>
<div id="message"></div>
<!--<script src="http://iserve.ind.in:9999/socket.io/socket.io.js"></script>-->
<script src="<?php echo socketpath ?>socket.io/socket.io.js"></script>

<script src="http://momentjs.com/downloads/moment.js"></script>

<script>

    $(document).ready(function () {
        $("#define_page").html("Bookings");
        $('.bookings').addClass('active');
        $('.bookings').attr('src', "<?php echo base_url(); ?>/theme/icon/all_booking_on.png");
        $('#searchData').click(function () {
            if ($("#start").val() && $("#end").val())
            {
                var dateObject = $("#start").datepicker("getDate"); // get the date object
                var st = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() 
                var dateObject = $("#end").datepicker("getDate"); // get the date object
                var end = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() 
                var table = $('#big_table');
                 $('#big_table_processing').show();
                var settings = {
                    "autoWidth": false,
                    "sDom": "<'table-responsive't><'row'<p i>>",
                    "destroy": true,
                    "scrollCollapse": true,
                    "iDisplayLength": 20,
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/Get_dataformdate_for_all_bookingspg/' + st + '/' + end + '/' + $('#Sortby').val() ,
//                    "bJQueryUI": true,
//                    "sPaginationType": "full_numbers",
                    "iDisplayStart ": 20,
                             "columns": [
                {"data": 0},
                {"data": 1},
                {"data": 2},
                {"data": 3},
                {"data": 4},
                {"data": 5},
                {"data": 6},
                {"data": 7},
                {"data": 8},
                {"data": 9},
                {"data": 10},
                {
                    "class": "details-control",
                    "orderable": false,
                    "data": null,
                    "defaultContent": ""
                }],
                    "oLanguage": {
                        "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
                    },
                    "fnInitComplete": function () {
                        //oTable.fnAdjustColumnSizing();
                         $('#big_table_processing').hide();
                    },
                             "drawCallback": function () {
                $('#big_table_processing').hide();
            },
                    'fnServerData': function (sSource, aoData, fnCallback)
                    {
                        $.ajax
                                ({
                                    'dataType': 'json',
                                    'type': 'POST',
                                    'url': sSource,
                                    'data': aoData,
                                    'success': fnCallback
                                });
                    }
                };

                table.dataTable(settings);

                // search box for table
                $('#search-table').keyup(function () {
                 $('#big_table_processing').show();
                    table.fnFilter($(this).val());
                });
            } else
            {
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodels');
                if (size == "mini")
                {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#confirmmodels').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdatas").text(<?php echo json_encode(POPUP_DRIVERS_DEACTIVAT_DATEOFBOOKING); ?>);

                $("#confirmeds").click(function () {
                    $('.close').trigger('click');
                });
            }
        });

        $('#search_by_select').change(function () {


            $('#atag').attr('href', '<?php echo base_url() ?>index.php/superadmin/search_by_select/' + $('#search_by_select').val());

            $("#callone").trigger("click");
        });



        $("#chekdel").click(function () {
            var val = [];
            $('.checkbox:checked').each(function (i) {
                val[i] = $(this).val();
            });

            if (val.length > 0) {
                if (confirm("Are you sure to Delete " + val.length + " Vehicle")) {
                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/deleteVehicles",
                        type: "POST",
                        data: {val: val},
                        dataType: 'json',
                        success: function (result) {
                            alert(result.affectedRows)

                            $('.checkbox:checked').each(function (i) {
                                $(this).closest('tr').remove();
                            });
                        }
                    });
                }

            } else {
                alert("Please mark any one of options");
            }

        });


    });



    function route_map($dis) {

        var val = $dis;
        var mapval = $("#bookingid").val();
        markers = [];

        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/getmap_values",
            type: "POST",
            data: {mapval: val},
            dataType: 'json',
            success: function (response) {
                alert(JSON.stringify(response));
                $.each(response, function (index, row) {

//                            markers.push({'lat':row.latitude,'lng' : row.longitude}) 
                    markers.push(new google.maps.LatLng(row.latitude, row.longitude));
                });

                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#myModalmap');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show');
                } else {
                    $('#myModalmap').modal('show');
                    setTimeout(initialize, 300);
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }


            }
        });


    }


    function format(d) {
        var html = '<div class="div-table" align="center"><div class="div-table-row">\n\
               <div class="div-table-col" >PROVIDER ID</div> \n\
                <div  class="div-table-col">PROVIDER NAME</div>\n\
                <div  class="div-table-col">PROVIDER PHONE</div>\n\
                <div  class="div-table-col">TIME</div>\n\
                <div  class="div-table-col">RESPONSE</div>\n\
             </div>';

        $.each(d.dispatched, function (index, value) {

            if (value.mobile == 'null')
                var mobile = '--';
            else
                var mobile = value.mobile;
            if (value.receiveDt == '')
                var receiveDt = '--';
            else
                var receiveDt = value.receiveDt;

            html += ' <div class="div-table-row">\n\
                <div class="div-table-col">' + value.pid + '</div>\n\
                <div class="div-table-col">' + value.name + '</div>\n\
                <div class="div-table-col">' + mobile + '</div>\n\
                <div class="div-table-col">' + value.sent_dt + '</div>\n\
                <div class="div-table-col">' + value.status_msg + '</div>\n\
            </div>';
        });
        html += '</div>';
        return html;
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#datepicker-component').on('changeDate', function () {
            $(this).datepicker('hide');
        });
        var table = $('#big_table');
         $('#big_table_processing').show();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/superadmin/bookings_data_ajax/',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
//            "oLanguage": {
//                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
//            },
            "columns": [
                {"data": 0},
                {"data": 1},
                {"data": 2},
                {"data": 3},
                {"data": 4},
                {"data": 5},
                {"data": 6},
                {"data": 7},
                {"data": 8},
                {"data": 9},
                {"data": 10},
                {
                    "class": "details-control",
                    "orderable": false,
                    "data": null,
                    "defaultContent": ""
                }],
            "fnInitComplete": function () {
                 $('#big_table_processing').hide();
                //oTable.fnAdjustColumnSizing();
            },
                     "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                console.log("data");
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);
         
        // search box for table
        $('#search-table').keyup(function () {
             $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });


        var detailRows = [];

        $('#big_table tbody').on('click', 'tr td.details-control', function () {
            var tr = $(this).closest('tr');
            var bid = tr.find('td:first').html();
            console.log(bid);

            $.ajax({
                url: "<?php echo base_url('index.php/superadmin') ?>/getBookingDetails",
                type: "POST",
                data: {bid: bid},
                dataType: 'json',
                success: function (result) {
                    var row = table.api().row(tr);
                    var idx = $.inArray(tr.attr('id'), detailRows);

                    if (row.child.isShown()) {
                        tr.removeClass('details');
                        row.child.hide();

                        // Remove from the 'open' array
                        detailRows.splice(idx, 1);
                    } else {
                        tr.addClass('details');
                        row.child(format(result)).show();

                        // Add to the 'open' array
                        if (idx === -1) {
                            detailRows.push(tr.attr('id'));
                        }
                    }
                }
            });


        });

        // On each draw, loop over the `detailRows` array and show any child rows
        table.on('draw', function () {
            $.each(detailRows, function (i, id) {
                $('#' + id + ' td.details-control').trigger('click');
            });
        });


        


        $('#Sortby').change(function () {
             $.each(detailRows, function (i, id) {
                  
                         

                        // Remove from the 'open' array
                        detailRows.splice(id, 1);
            });
            var table = $('#big_table');
             $('#big_table_processing').show();
            var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/superadmin/bookings_data_ajax/' + $(this).val(),
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
//            "oLanguage": {
//                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
//            },
            "columns": [
                {"data": 0},
                {"data": 1},
                {"data": 2},
                {"data": 3},
                {"data": 4},
                {"data": 5},
                {"data": 6},
                {"data": 7},
                {"data": 8},
                {"data": 9},
                {"data": 10},
                {
                    "class": "details-control",
                    "orderable": false,
                    "data": null,
                    "defaultContent": ""
                }],
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
                     "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                console.log("data");
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };
           

            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
                $('#big_table_processing').show();
                table.fnFilter($(this).val());
            });

        });

    });


    function refreshTableOnCityChange() {

        var table = $('#big_table');
        var url = '';

        if ($('#start').val() != '' || $('#end').val() != '') {

            var dateObject = $("#start").datepicker("getDate"); // get the date object
            var st = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() 
            var dateObject = $("#end").datepicker("getDate"); // get the date object
            var end = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date()

            url = '<?php echo base_url() ?>index.php/superadmin/Get_dataformdate_for_all_bookingspg/' + st + '/' + end + '/' + $('#Sortby').val();

        } else {
            url = '<?php echo base_url(); ?>index.php/superadmin/bookings_data_ajax/' + $('#Sortby').val() ;
        }
        alert(url);
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": url,
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
                     "columns": [
                {"data": 0},
                {"data": 1},
                {"data": 2},
                {"data": 3},
                {"data": 4},
                {"data": 5},
                {"data": 6},
                {"data": 7},
                {"data": 8},
                {"data": 9},
                {"data": 10},
                {
                    "class": "details-control",
                    "orderable": false,
                    "data": null,
                    "defaultContent": ""
                }],
                
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

    }
</script>

<style>
    /*    .dataTables_wrapper .dataTables_info{
            padding: 0;
        }*/
    .exportOptions{
        display: none;
    }
     .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
</style>


<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">LIVE BOOKINGS FEED</a>
            </li>
             
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
<!--                    <li class="active">
                        <a href="#hlp_txt" data-toggle="tab" role="tab" aria-expanded="false">LIVE BOOKINGS FEED</a>
                    </li>-->
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif"></div>
                                 <div class="pull-left">
                                            <select class="full-width select2-offscreen" id="Sortby" data-init-plugin="select2" tabindex="-1" title="select" style="width: 238px;">
                                                <option value="7">JOB COMPLETED</option>
                                                <option value="5" >PROVIDER ON THE WAY</option>
                                                <option value="21" >PROVIDER ARRIVED</option>
                                                <option value="6" >JOB STARTED</option>
                                                <option value="9" >CANCELED BY CUSTOMER</option>
                                                <option value="3" >PROVIDER REJECTED</option>
                                                <option value="10" >CANCELED BY PROVIDER</option>
                                                <option value="2" >PROVIDER ACCEPTED</option>
                                                <option value="22" >INVOICE RAISED</option>
                                                <option value=""  selected>  ALL   </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="" aria-required="true">

                                                <div class="input-daterange input-group" id="datepicker-component">
                                                    <input type="text" class="input-sm form-control" name="start" id="start" placeholder="From">
                                                    <span class="input-group-addon">to</span>
                                                    <input type="text" class="input-sm form-control" name="end" id="end" placeholder="To">

                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-sm-1">
                                            <div class="">
                                                <button class="btn btn-primary" type="button" id="searchData"><i class="fa fa-search text-white"></i> Search</button>
                                            </div>
                                        </div>
                                        <div class="row clearfix pull-right" >


                                            <div class="col-sm-12">
                                                <div class="searchbtn" >

                                                    <div class="pull-right"><input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                                                </div>
                                            </div>
                                        </div>
                               
                                    
                                </div>
                               
                                <div class="panel-body no-padding">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 

 


    <div class="modal fade stick-up" id="myModalmap" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">

                    <div class="modal-header">

                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>

                        </div>
                        <h3> <?php echo LIST_MAP; ?></h3>
                    </div>


                    <div class="modal-body">

                        <div id="googleMap" style="width:500px;height:380px;"></div>
                        <BR>

                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4 error-box" id="errorpass"></div>
                            <div class="col-sm-4" >
                                <button type="button" class="btn btn-primary pull-right" id="documentok" ><?php echo BUTTON_OK; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


    </div>




    <div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">
                    <div class="row">

                        <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                    </div>
                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_OK; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
