<?php
date_default_timezone_set('UTC');
$rupee = "$";
?>
<style>
    .ui-state-default{
        font-size : 10px !important; 
    }
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    function LoadCategory()
    {
        var cityid = $("#cityid option:selected").val();
        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/GetCategoryByCity",
            type: "POST",
            data: {cid: cityid},
            dataType: 'JSON',
            beforeSend: function () {
                $('#catloading').show();
            },
            complete: function () {
                $('#catloading').hide();
            },
            success: function (response)
            {
                $('#catlist').html('');
                $('#catlist').append('<option value="0">Select Category</option>');
                $.each(response, function (key, value)
                {
                    $('#catlist').append('<option value="' + value['id'] + '">' + value['cat_name'] + '</option>');
                });
            }
        });
    }
    
    function LoadServiceGroup()
    {
        var catlist = $("#catlist option:selected").val();
        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/GetGroupByCategory",
            type: "POST",
            data: {cid: catlist},
            dataType: 'JSON',
            beforeSend: function () {
                $('#grploading').show();
            },
            complete: function () {
                $('#grploading').hide();
            },
            success: function (response)
            {
                $('#grouplist').html('');
                $('#grouplist').append('<option value="0">Select Group</option>');
                $.each(response, function (key, value)
                {
                    $('#grouplist').append('<option value="' + value['id'] + '">' + value['group_name'] + '</option>');
                });
            }
        });
    }
    $(document).ready(function () {
        $('.provisionen').addClass('active');
        $('.provisionen_thumb').attr('src', "<?php echo base_url(); ?>/theme/icon/comission_on copy.png");
        $('.provisionen_thumb').addClass("bg-success");
        $('#search_by_select').change(function () {
            $('#atag').attr('href', '<?php echo base_url() ?>index.php/superadmin/search_by_select/' + $('#search_by_select').val());
            $("#callone").trigger("click");
        });
        var table = $('#tableWithSearch1');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "order": [[0, "desc"]]
        };
        table.dataTable(settings);
        $('#search-table1').keyup(function () {
            table.fnFilter($(this).val());
        });

        $('#btnStickUpSizeToggler').click(function () {
            $("#display-data").text("");
            var size = $('input[name=stickup_toggler]:checked').val();
            var modalElem = $('#sub_cat_mod');
            if (size == "mini") {
                $('#modalStickUpSmall').modal('show')
            } else {
                $('#sub_cat_mod').modal('show')
                if (size == "default") {
                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                } else if (size == "full") {
                    modalElem.children('.modal-dialog').addClass('modal-lg');
                }
            }
        });

        $("#insertsubcat").click(function () {
            $("#addsubcat").text("");
            var cityid = $("#cityid").val();
            var catlist = $("#catlist").val();
            var grouplist = $("#grouplist").val();
            var s_name = $("#s_name").val();
            var s_desc = $("#s_desc").val();
            var fixed_price = $("#fixed_price").val();
            if (cityid == "0") {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_CITY); ?>);
            } else if (catlist == "0") {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_CATEGORY); ?>);
            } else if (s_name == "" || s_name == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_SUB_CATEGORY); ?>);
            } else if (fixed_price == "" || fixed_price == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_FIXED_PRICE); ?>);
            } else
            {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/AddService",
                    type: 'POST',
                    data: {
                        cid: cityid,
                        catid: catlist,
                        group_id: grouplist,
                        s_name: s_name,
                        s_desc: s_desc,
                        fp: fixed_price
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $(".close").trigger("click");
                        location.reload();
                    }
                });
            }

        });
       
        $("#cityid").change(function (){
         var cityid = $("#cityid option:selected").val();
         
         $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/GetCurrncy",
            type: "POST",
            data: {cityid: cityid},
            dataType: 'JSON',
            
            success: function (response)
            {
               
               $("#curncy").val(response[0].Currency)
            }
        });
        });


    });

</script>
<style>
    #active{
        display:none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        var status = '<?php echo $status; ?>';
        if (status == 1) {
            $('#inactive').show();
            $('#active').hide();
            $('#btnStickUpSizeToggler').show();
        }
        $('#big_table_processing').show();
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_cservices',
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {

                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };
        table.dataTable(settings);
        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });
    });
</script>


<style>
    .exportOptions{
        display: none;
    }
</style>
<div class="page-content-wrapper"style="padding-top: 20px;">
    <div class="content">
        <div class="brand inline" style="  width: auto;font-size: 16px;color: gray;margin-left: 30px;padding-top: 20px;">
            <strong style="color:#0090d9;">SERVICES</strong>
        </div>
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="panel panel-transparent ">
                    <ul class="nav nav-tabs nav-tabs-fillup  bg-white">
                        <li class="active" style="cursor:pointer">
                            <a  data = "<?php echo base_url(); ?>index.php/superadmin/datatable_cities/2"><span><?php echo LIST_SERVICES; ?></span></a>
                        </li>
                        <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons" id="btnStickUpSizeToggler"><span><?php echo BUTTON_ADD_SERVICE; ?></button></a></div>                    
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="container-fluid container-fixed-lg bg-white">
                            <div class="panel panel-transparent">
                                <div class="panel-heading">
                                    <div class="error-box" id="display-data" style="text-align:center"></div>
                                    <div id="big_table_processing" class="dataTables_processing" style=""><img src="http://www.ahmed-samy.com/demos/datatables_2/assets/images/ajax-loader_dark.gif"></div>
                                    <div class="searchbtn row clearfix pull-right" style=" padding-right: 18px;">
                                        <div class="pull-right">
                                            <input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH; ?>"> </div>
                                    </div>
                                </div>
                                <br>
                                <div class="panel-body">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                                <div class="row">
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END PANEL -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade stick-up" id="sub_cat_mod" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3> <?php echo ADD_SERVICE_TEXT; ?></h3>
                </div>
                <br>
                <div class="modal-body">
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_VEHICLE_SELECTCITY; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="cityid" name="city_select"  class="form-control error-box-class" onchange="LoadCategory()">
                                <option value="0">Select City</option>
                                <?php
                                foreach ($citylist as $result) {
                                    echo "<option value=" . $result->City_Id . ">" . $result->City_Name . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-1" id="catloading" style="display: none;">
                            <img src="<?php echo base_url() ?>../../pics/loadingimg.gif" /> 
                        </div>
                    </div>
                    <br>
                    <br>
                     <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="fname" class="col-sm-4 control-label">Currency</label>
                            <div class="col-sm-8">
                                <input name="curncy" type="text" id="curncy" style="  width: 219px;line-height: 2;" class="form-control error-box-class" readonly/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="catlist" name="fdata[cat]"  class="form-control error-box-class" onchange="LoadServiceGroup()">                                                          
                            </select>
                        </div>
                        <div class="col-sm-1" id="grploading" style="display: none;">
                            <img src="<?php echo base_url() ?>../../pics/loadingimg.gif" /> 
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <label for="grouplist" class="col-sm-4 control-label" ><?php echo FIELD_SELECT_GROUP; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="grouplist" name="fdata[grouplist]"  class="form-control error-box-class">                                                          
                            </select>
                        </div>
                    </div>
                    <br>
                    <br>

                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_SERVICE_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[s_name]" type="text" id="s_name"  placeholder="service Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_SERVICE_DESC; ?></label>
                        <div class="col-sm-6">
                            <input type="text"  id="s_desc" name="fdata['s_desc']"  class="form-control error-box-class" placeholder="Service Desc">
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="form-group" class="formex">
                        <label for="fixed_price" class="col-sm-4 control-label"> <?php echo FIELD_FIXED_PRICE; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"  id="fixed_price" name="fdata[fixed_price]" class="form-control error-box-class" placeholder="Fxied Price">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="addsubcat"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="insertsubcat" ><?php echo BUTTON_ADD; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>
