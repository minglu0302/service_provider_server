<?php
date_default_timezone_set('UTC');
$rupee = "$";
?>
<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
   .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<style>
    .exportOptions{
        display: none;
    }
</style>
<script>
    $(document).ready(function () {
        $('.provisionen').addClass('active');
        $('.provisionen_thumb').attr('src', "<?php echo base_url(); ?>/theme/icon/comission_on copy.png");
        $('.provisionen_thumb').addClass("bg-success");
        $('#search_by_select').change(function () {
            $('#atag').attr('href', '<?php echo base_url() ?>index.php/superadmin/search_by_select/' + $('#search_by_select').val());
            $("#callone").trigger("click");
        });
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "order": [[0, "desc"]]
        };
        table.dataTable(settings);
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });



    });

</script>
<style>
    #active{
        display:none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {       
        $('#big_table_processing').show();
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_proreview/<?php echo $proid ?>',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {

                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };



        table.dataTable(settings);
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

        $('.changeMode').click(function () {

            $('#big_table_processing').show();
            var table = $('#big_table');
            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_proreview/<?php echo $proid ?>',
//                "bJQueryUI": true,
//                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {

                    $('#big_table_processing').hide();
                },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            };

            $('.tabs_active').removeClass('active');
            $(this).parent().addClass('active');
            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
                table.fnFilter($(this).val());
            });

        });
        $('.tabs_active').removeClass('active');
        $(this).parent().addClass('active');

        table.dataTable(settings);
        $('#ecityid').change(function () {
            table.fnFilter($(this).val());
        });
    });
</script>
<div class="page-content-wrapper">
    <div class="content">
        <div class="inner">
                
                 <ul class="breadcrumb" style="margin-left: 20px;">
                    <li><a href="<?php echo base_url(); ?>index.php/superadmin/Drivers/my/1" class="">PROVIDER RATING</a>
                    </li>
                    
                    <li><a href="" class="active"><?= $name ?></a>
                    </li>
                </ul>

             
                       
              
            </div>
<!--        <div class="brand inline" style="  width: auto;
             font-size: 16px;
             color: gray;
             margin-left: 30px;padding-top: 20px;">
            <strong style="color:#0090d9;">PROVIDER RATING</strong>
        </div>-->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="panel panel-transparent ">
                    
                    <div class="tab-content">
                        <div class="container-fluid container-fixed-lg bg-white">
                            <div class="panel panel-transparent">
                                <div class="panel-heading">
                                    <div class="error-box" id="display-data" style="text-align:center"></div>
                                    <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif"></div>
                                    <div class="searchbtn row clearfix pull-right" style=" padding-right: 18px;">
                                        <div class="pull-right">
                                            <input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH; ?>"> </div>
                                    </div>                                                                       
                                </div>
                                <br>
                                <div class="panel-body">
                                    <?php echo $this->table->generate(); ?>
                                    </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
 
