<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
    .exportOptions{
        display: none;
    }
</style>

<script>
</script>
<div class="page-content-wrapper">
    <div class="content">
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <br/>
                <div class="inner" style="-webkit-transform: translateY(0px); opacity: 1;">
                    <ul class="breadcrumb">
                        <ul class="breadcrumb">
                            <li><a href="<?php echo base_url('index.php/superadmin/payroll') ?>" class="">Payroll</a>
                            </li>
                            <li><a href="<?php echo base_url('index.php/superadmin/pay_provider/'.$mas_id) ?>" class="">Provider(<?php echo $provider_name ?>)</a>
                            </li>
                            <li><a href="<?php echo base_url('index.php/superadmin/patientDetails/'.$pay_id.'/'.$mas_id) ?>" class="">Payment Cycle(<?php echo $start_date ?>)</a>
                            </li>
                            <li><a href="#" class="active">Acceptance Rate</a>
                            </li>
                        </ul>
                    </ul>
                </div>

                <div class="panel panel-transparent ">
                    <div class="tab-content">
                        <div class="container-fluid container-fixed-lg bg-white">
                            <div class="panel panel-transparent">
                                <div class="panel-heading">
                                    <div class="row clearfix">
                                        <div class="pull-left">
                                            <div class="col-md-6">                                                
                                            </div>
                                        </div>
                                        <div class="pull-right">                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
    <div class="col-md-4">
        <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="panel-heading  top-left top-right">
                            <div class="panel-title text-black">
                                <!--                                                <span class="font-montserrat fs-11 all-caps">Weekly Sales <i class="fa fa-chevron-right"></i>-->
                                
                            </div>
                            <div class="panel-controls">
                                <ul>
                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="p-l-20">
                            <h5 class="no-margin p-b-5 text-white">TOTAL BOOKINGS</h5>
                            <!--                                            <a href="#" class="btn-circle-arrow text-white"><i class="pg-arrow_minimize"></i>-->
                            <!--                                            </a>-->

                            <div style="font-size: 42px;margin-left: 10%;">
                               <?php echo $totalBoookings ?>
                            </div>
                            <!--                                            <span class="label  font-montserrat m-r-5">--><!--</span>-->
                        </div>

                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-bottom">
                        <div class="progress progress-small m-b-20">
                            <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                            <div class="progress-bar progress-bar-white" data-percentage="3.030303030303%" style="width: 3.0303%;"></div>
                            <!-- END BOOTSTRAP PROGRESS -->
                        </div>
                    </div>
                </div>
            </div>
            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;"></div>
    </div>
    <div class="col-md-4">
        <div class="widget-9 panel no-border bg-success no-margin widget-loader-bar">
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="panel-heading  top-left top-right">
                            <div class="panel-title text-black">
                                <!--                                                <span class="font-montserrat fs-11 all-caps">Weekly Sales <i class="fa fa-chevron-right"></i>-->
                                
                            </div>
                            <div class="panel-controls">
                                <ul>
                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="p-l-20">
                            <h5 class="no-margin p-b-5 text-white">TOTAL ACCEPTED BOOKINGS</h5>
                            <!--                                            <a href="#" class="btn-circle-arrow text-white"><i class="pg-arrow_minimize"></i>-->
                            <!--                                            </a>-->

                            <div style="font-size: 42px;margin-left: 10%;">
                               <?php echo $totalAccepted ?>
                            </div>
                            <!--                                            <span class="label  font-montserrat m-r-5">--><!--</span>-->
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-bottom">
                        <div class="progress progress-small m-b-20">
                            <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                            <div class="progress-bar progress-bar-white" data-percentage="66.666666666667%" style="width: 66.6667%;"></div>
                            <!-- END BOOTSTRAP PROGRESS -->
                        </div>
                    </div>
                </div>
            </div>
            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;"></div>
    </div>
    <div class="col-md-4">
        <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="panel-heading  top-left top-right">
                            <div class="panel-title text-black">
                                <!--                                                <span class="font-montserrat fs-11 all-caps">Weekly Sales <i class="fa fa-chevron-right"></i>-->
                                
                            </div>
                            <div class="panel-controls">
                                <ul>
                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="p-l-20">
                            <h5 class="no-margin p-b-5 text-white">TOTAL REJECTED BOOKINGS</h5>
                            <!--                                            <a href="#" class="btn-circle-arrow text-white"><i class="pg-arrow_minimize"></i>-->
                            <!--                                            </a>-->

                            <div style="font-size: 42px;margin-left: 10%;">
                               <?php echo $totalRejected ?>
                            </div>
                            <!--                                            <span class="label  font-montserrat m-r-5">--><!--</span>-->
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-bottom">
                        <div class="progress progress-small m-b-20">
                            <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                            <div class="progress-bar progress-bar-white" data-percentage="93.939393939394%" style="width: 93.9394%;"></div>
                            <!-- END BOOTSTRAP PROGRESS -->
                        </div>
                    </div>
                </div>
            </div>
            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;"></div>
    </div>
                                    </div><br><br>
                                    <div class="row">
    <div class="col-md-4">
        <div class="widget-9 panel no-border btn-complete no-margin widget-loader-bar">
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="panel-heading  top-left top-right">
                            <div class="panel-title text-black">
                                <!--                                                <span class="font-montserrat fs-11 all-caps">Weekly Sales <i class="fa fa-chevron-right"></i>-->
                                
                            </div>
                            <div class="panel-controls">
                                <ul>
                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="p-l-20">
                            <h5 class="no-margin p-b-5 text-white">TOTAL CANCELLED BOOKINGS</h5>
                            <!--                                            <a href="#" class="btn-circle-arrow text-white"><i class="pg-arrow_minimize"></i>-->
                            <!--                                            </a>-->

                            <div style="font-size: 42px;margin-left: 10%;">
                               <?php echo $tot_cancel ?>
                            </div>
                            <!--                                            <span class="label  font-montserrat m-r-5">--><!--</span>-->
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-bottom">
                        <div class="progress progress-small m-b-20">
                            <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                            <div class="progress-bar progress-bar-white" data-percentage="96.969696969697%" style="width: 96.9697%;"></div>
                            <!-- END BOOTSTRAP PROGRESS -->
                        </div>
                    </div>
                </div>
            </div>
            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;"></div>
    </div>
    <div class="col-md-4">
        <div class="widget-9 panel no-border bg-success no-margin widget-loader-bar">
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="panel-heading  top-left top-right">
                            <div class="panel-title text-black">
                                <!--                                                <span class="font-montserrat fs-11 all-caps">Weekly Sales <i class="fa fa-chevron-right"></i>-->
                                
                            </div>
                            <div class="panel-controls">
                                <ul>
                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="p-l-20">
                            <h5 class="no-margin p-b-5 text-white">TOTAL IGNORED BOOKINGS</h5>
                            <!--                                            <a href="#" class="btn-circle-arrow text-white"><i class="pg-arrow_minimize"></i>-->
                            <!--                                            </a>-->

                            <div style="font-size: 42px;margin-left: 10%;">
                               <?php echo $totalIgnored ?>
                            </div>
                            <!--                                            <span class="label  font-montserrat m-r-5">--><!--</span>-->
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-bottom">
                        <div class="progress progress-small m-b-20">
                            <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                            <div class="progress-bar progress-bar-white" data-percentage="66.666666666667%" style="width: 66.6667%;"></div>
                            <!-- END BOOTSTRAP PROGRESS -->
                        </div>
                    </div>
                </div>
            </div>
            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;"></div>
    </div>
    <div class="col-md-4">
        <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="panel-heading  top-left top-right">
                            <div class="panel-title text-black">
                                <!--                                                <span class="font-montserrat fs-11 all-caps">Weekly Sales <i class="fa fa-chevron-right"></i>-->
                                
                            </div>
                            <div class="panel-controls">
                                <ul>
                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="p-l-20">
                            <h5 class="no-margin p-b-5 text-white">ACCEPTANCE RATE</h5>
                            <!--                                            <a href="#" class="btn-circle-arrow text-white"><i class="pg-arrow_minimize"></i>-->
                            <!--                                            </a>-->

                            <div style="font-size: 42px;margin-left: 10%;">
                               <?php echo $acpt_rate ?> %
                            </div>
                            <!--                                            <span class="label  font-montserrat m-r-5">--><!--</span>-->
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-bottom">
                        <div class="progress progress-small m-b-20">
                            <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                            <div class="progress-bar progress-bar-white" data-percentage="93.939393939394%" style="width: 93.9394%;"></div>
                            <!-- END BOOTSTRAP PROGRESS -->
                        </div>
                    </div>
                </div>
            </div>
            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;"></div>
    </div>
                                        </div>
    

<!--                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer">
                                        <div class="table-responsive">
                                        </div>
                                        <div class="row">
                                            <h5><b>Total Bookings</b> : <b><?php echo $totalBoookings ?></b></h5>
                                            <br/>
                                            <h5><b>Total bookings accepted</b> :  <b><?php echo $totalAccepted ?></b></h5>
                                            <br/>
                                            <h5><b>Total bookings rejected</b> : <b><?php echo $totalRejected ?></b></h5> 
                                            <br/>
                                            <h5><b>Total bookings cancelled</b>: <b><?php echo $tot_cancel ?></b></h5>
                                            <br/>
                                            <h5><b>Total Ignored Bookings</b>: <b><?php echo $totalIgnored ?></b></h5>
                                            <br/>
                                            <h5><b>ACCEPTANCE RATE</b>: <b><?php echo $acpt_rate ?></b></h5>
                                            <br/>
                                        </div>
                                    </div>-->
                                </div>
                            </div>                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>