<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
    .exportOptions{
        display: none;
    }
</style>
<?php
$this->load->library('session');
$pay_error = $this->session->userdata('pay_error');
if ($pay_error != '') {
    ?> 
    <script>alert("<?php echo $pay_error; ?>");</script>
    <?php
}
$meta = array('pay_error' => '');
$this->session->set_userdata($meta);
date_default_timezone_set('UTC');
$rupee = "$";
error_reporting(0);
$total = $unsettled_amount_count = 0;
foreach ($driverdata as $result) {
    $drivername = $result->first_name;
    $total = round($result->total, 2);
    $unsettled_amount_count = $result->unsettled_amount_count;
    $last_unsettled_appointment_id = $result->last_unsettled_appointment_id;
}
echo 'unsettled_amount_count' . $unsettled_amount_count;
$driverpaid = 0;
foreach ($totalamountpaid as $result) {
    $driverpaid = $result->totalamt;
}
$ClosingAmount = round(($total - $driverpaid), 2);
?>

<script>
    $(document).ready(function () {
        $('.payroll').addClass('active');
        $('.payroll').attr('src', "<?php echo base_url(); ?>/theme/icon/payroll_on.png");
        $('#payamount').click(function () {
            $('#myModal').modal('show');
            var colonneSelected = $('#due_amt').html();// $("#tableWithSearch tr:last").find('.close_bal').text();
            if (colonneSelected == 0) {
                $('.r_amount,.cr_amount,.p_amount').hide();
            } else {
                $('.r_amount,.cr_amount,.p_amount').show();
            }
        });

        $("#releasedamt").blur(function () {
            var firstamt = parseFloat($('#due_amt').html());
            var secondamt = parseFloat($('#releasedamt').val());
            if (firstamt >= secondamt) {
                $('#error_msg').html("");
            } else {
                $('#error_msg').html("Released Amount Is grater then Due Amount ! ");
                $('#releasedamt').focus();
            }
        });

        $('#submit_form').click(function () {
            var firstamt = parseInt($('#releasedamt').val());
            var secondamt = parseInt($('#creleasedamt').val());
            if (firstamt == secondamt) {
                if (firstamt > 0 || secondamt > 0) {
                    $('#error_msg_chk').html("");
                    var currentdate = new Date();
                    var datetime = currentdate.getFullYear() + "-"
                            + (currentdate.getMonth() + 1) + "-"
                            + currentdate.getDate() + " "
                            + currentdate.getHours() + ":"
                            + currentdate.getMinutes() + ":"
                            + currentdate.getSeconds();
                    $('#hdate').val(datetime);
                    var colonneSelected = $("#tableWithSearch tr:last").find('.close_bal').text();
                    $('#currunEarnigs').val(colonneSelected);
                    $('#form-work').submit();
                } else {
                    $('#error_msg_chk').html("Amount Must be Greater then 0!");
                }
            } else {
                $('#error_msg_chk').html("Amount does't match !");
            }

        });

    });

    function validate(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault)
                theEvent.preventDefault();
        }
    }
</script>
<div class="page-content-wrapper">
    <div class="content">
        <div class="inner">
                    <!-- START BREADCRUMB -->
                   <ul class="breadcrumb" style="margin-left: 20px;font-size: 16px;color:#0090d9;">
                      
                      <li><a href="<?php echo base_url() ?>index.php/superadmin/payroll" class="">Payroll</a>
                        </li>
                        <li><a href="#" class="active"><?php echo $drivername; ?></a>
                        </li>
                    </ul>
                    <!-- END BREADCRUMB -->
                </div>
        
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                

                <div class="panel panel-transparent ">
                    <div class="tab-content">
                        <div class="container-fluid container-fixed-lg bg-white">
                            <div class="panel panel-transparent">
                                <div class="panel-heading">
                                    <div class="row clearfix">
                                        <div class="pull-left">
                                            <div class="col-md-6">
                                                <button id="payamount" class="btn btn-primary btn-cons m-b-10 m-l-10 pull-left" type="button" ><i class="pg-form"></i> <span class="bold">PAY</span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="pull-right">                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive"><table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending" style="width: 68px;">SLNO</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">BOOKING THIS CYCLE</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">EARNING THIS CYCLE</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">OPENING BALANCE (<?php echo $rupee; ?>)</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PAID AMOUNT(<?php echo $rupee; ?>)</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PAID DATE</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">GATEWAY FEES(<?php echo $rupee; ?>)</th> 
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">NET PAID</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 50px;">CLOSING BALANCE (<?php echo $rupee; ?>)</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">TXN ID</th>                                               
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PAYMENT GATEWAY TXN ID</th>                                                        
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">STATUS</th>         

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $slno = 1;
                                                    $lastDue = 0;
                                                    if (empty($payrolldata)) {
                                                        ?>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <td class="v-align-middle sorting_1"> <p>-</p></td>
                                                    <?php
                                                    $slno += 1;
                                                } else {
                                                    foreach ($payrolldata as $result) {
                                                        ?>
                                                        <tr role="row"  class="gradeA odd">
                                                            <!--<td class="v-align-middle sorting_1"> <p><?php echo $slno; ?></p></td>-->
                                                            <td class="v-align-middle sorting_1"><a href=" <?php echo base_url("index.php/superadmin/patientDetails/" . $result->payroll_id) ?> "><?php echo $slno; ?></a></td>
                                                            <td class="v-align-middle"><?php echo $result->booking_cycle; ?></td>
                                                            <td class="v-align-middle"><?php echo $result->booking_earning; ?></td>
                                                            <td class="v-align-middle sorting_1"> <p><?php echo $result->opening_balance; ?></p></td>
                                                            <td class="v-align-middle"><?php echo $result->pay_amount; ?></td>
                                                            <td class="v-align-middle"><?php echo date("M d Y g:i:s A", strtotime($result->pay_date)); ?></td>
                                                            <td class="v-align-middle"><?php echo $result->gateway_fees; ?></td>
                                                            <td class="v-align-middle"><?php echo $result->net_paid; ?></td>
                                                            <td class="v-align-middle close_bal"><?php echo $result->closing_balance; ?></td>
                                                            <td class="v-align-middle"><?php echo "TXN-CRED-0000-00" . $result->payroll_id; ?></td>
                                                            <td class="v-align-middle"><?php echo $result->trasaction_id; ?></td>
                                                            <td class="v-align-middle">Success</td>
                                                        </tr>
                                                        <?php
                                                        $lastDue = $result->closing_balance;
                                                        $slno++;
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                            </table></div><div class="row">
                                        </div></div>
                                </div>
                            </div>                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 


<div class="modal fade stick-up in" id="myModal" tabindex="-1" role="dialog" aria-hidden="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Payment <span class="semi-bold">Information</span></h5>
            </div>
            <div class="modal-body">
                <form id="form-work" class="form-horizontal" role="form" autocomplete="off" novalidate="novalidate" action="<?php echo base_url() ?>index.php/superadmin/pay_driver_amount/<?php echo $mas_id ?>" method="post">
                    <div class="form-group-attached">
                        <input type='hidden' name='ctime' id="hdate" value==''>
                        <input type='hidden' id="dueamount" name='dueamount' value='<?php echo $duebalance; ?>'>
                        <input type='hidden' name='last_unsettled_appointment_id' id="last_unsettled_appointment_id" value='<?php echo $last_unsettled_appointment_id; ?>'>
                        <div class="form-group">
                            <label for="position" class="col-sm-3 control-label">Due Amount</label>
                            <div class="col-sm-9">
                                <?php echo $rupee ?> <span id="due_amt"><?php echo $duebalance; ?></span>
                            </div>
                        </div>
                        <div class="form-group r_amount">
                            <label for="position" class="col-sm-3 control-label">Enter Amount To Be Released </label>
                            <div class="col-sm-9">
                                <input type="text" min="1" class="form-control " id="releasedamt" placeholder="Released Amount" onkeypress='validate(event)' required="" aria-required="true" aria-invalid="true" >
                                <span id="error_msg" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group cr_amount">
                            <label for="position" class="col-sm-3 control-label">Re-Confirm Amount</label>
                            <div class="col-sm-9">
                                <input type="text" min="1" class="form-control " id="creleasedamt" placeholder="Confirm Amount" onkeypress='validate(event)' required="" aria-required="true" aria-invalid="true" name="paid_amount">
                                <span id="error_msg_chk" style="color: red"></span>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="p-t-20 clearfix p-l-10 p-r-10">
                            <div class="pull-left">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 m-t-10 sm-m-t-10 p_amount">
                        <button type="button" id="submit_form" class="btn btn-primary btn-block m-t-5">Pay Now</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>