<style>

    .div-table{
        display:table;         
        width:auto;         
        background-color:#eee;         
        border:1px solid  #666666;         
        border-spacing:5px;/*cellspacing:poor IE support for  this*/
    }
    .div-table-row{
        display:table-row;
        width:auto;
        clear:both;
    }
    .div-table-col{
        float:left;/*fix for  buggy browsers*/
        display:table-column;         
        width:200px;         
        background-color:#ccc;  
    }

    .select2-results {
        max-height: 192px;
    }
    td.details-control {
        background: url('<?php echo base_url() ?>theme/pages/img/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('<?php echo base_url() ?>theme/pages/img/details_close.png') no-repeat center center;
    }
       .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
</style>
<div id="message"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/superadmin/datatable_cust_details/<?php echo $status ?>',
                        "iDisplayStart ": 20,
                        "oLanguage": {
                            "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
                        },
                        "fnInitComplete": function () {
                        },
                        'fnServerData': function (sSource, aoData, fnCallback)
                        {
                            $.ajax
                                    ({
                                        'dataType': 'json',
                                        'type': 'POST',
                                        'url': sSource,
                                        'data': aoData,
                                        'success': fnCallback
                                    });
                        }
                    };
                    table.dataTable(settings);
                    $('#search-table').keyup(function () {
                        table.fnFilter($(this).val());
                    });
                    var detailRows = [];
                    table.on('draw', function () {
                        $.each(detailRows, function (i, id) {
                            $('#' + id + ' td.details-control').trigger('click');
                        });
                    });
                });
</script>
<style>
    .exportOptions{
        display: none;
    }
</style> 


<div class="content">
 <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url(); ?>index.php/superadmin/patients/3" class="">CUSTOMERS</a>
            </li>
            <li><a href="#" class="active"> <?= $name ?></a>
            </li>

        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"></div>
                                 <div class="error-box" id="display-data" style="text-align:center"></div>
                                <div class='pull-right cls110'>
                                     
                                    <div class="pull-right">
                                            <input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH; ?>"> </div>
                                    </div>
                                    
                                </div>
                                <br>
                                <div class="panel-body no-padding">
                                     <?php
                                                $this->table->function = 'htmlspecialchars';
                                                echo $this->table->generate();
                                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
