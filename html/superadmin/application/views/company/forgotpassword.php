<link href="<?php echo base_url(); ?>theme/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>theme/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>theme/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>theme/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url(); ?>theme/assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url(); ?>theme/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />

<!-- BEGIN Pages CSS-->
<link href="<?php echo base_url(); ?>theme/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="<?php echo base_url(); ?>theme/pages/css/pages.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>theme/assets/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url(); ?>theme/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>theme/assets/plugins/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
//        alert('hi');
        $('.showmsg').click(function () {
            if (confirm('Password updated !')){
                $('#form-register').submit();
            }
        });
    });
</script>
<div class="register-container full-height sm-p-t-30">
    <div class="container-sm-height full-height">
        <div class="row row-sm-height">
            <div class="col-sm-12 col-sm-height col-middle">
                <img style="height: 100px;width: 102px;float: right " src="<?php echo base_url() ?>theme/assets/img/logo.png" alt="logo" data-src="<?php echo base_url() ?>theme/assets/img/logo.png" data-src-retina="<?php echo base_url() ?>theme/assets/img/logo.png" width="78" height="22">
                <h3><?php echo Appname; ?> </h3>
                <p>
                    <small>
                        Reset your password
                    </small>
                </p>
                <form id="form-register" class="p-t-42" role="form" action="<?php echo base_url() ?>index.php/superadmin/varifyandsave/<?php echo $vlink ?>" method="post">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Password</label>
                                <input type="password" name="password" placeholder="Minimum of 4 Charactors" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Confirm Password</label>
                                <input type="password" name="pass" placeholder="Minimum of 4 Charactors" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-primary btn-cons m-t-10 showmsg" type="button">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>