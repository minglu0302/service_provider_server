<?php
date_default_timezone_set('UTC');
$rupee = "$";
?>
<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    $(document).ready(function () {
        $('.provisionen').addClass('active');
        $('.provisionen_thumb').attr('src', "<?php echo base_url(); ?>/theme/icon/comission_on copy.png");
        $('.provisionen_thumb').addClass("bg-success");
        $('#search_by_select').change(function () {
            $('#atag').attr('href', '<?php echo base_url() ?>index.php/superadmin/search_by_select/' + $('#search_by_select').val());
            $("#callone").trigger("click");
        });
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "order": [[0, "desc"]]
        };
        table.dataTable(settings);
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

        $('#editgateway').click(function () {
            var pay_id = $('#pay_id').val();
            var name = $('#name').val();
            var percent = $('#percent').val();
            var fixed = $('#fixed').val();

            if (name == "" || name == null) {
                $("#clearerror").text("ENTER PAYMENT GATEWAY NAME");
            } else if (percent == "" || percent == null) {
                $("#clearerror").text("ENTER PAYMENT GATEWAY PERCENT");
            } else if (fixed == "" || fixed == null) {
                $("#clearerror").text("ENTER PAYMENT GATEWAY FIXED");
            } else {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/EditPaymentGateWay",
                    type: 'POST',
                    data: {
                        pay_id: pay_id,
                        name: name,
                        percent: percent,
                        fixed: fixed
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $('.close').trigger('click');
                        window.location = "<?php echo base_url(); ?>index.php/superadmin/pgateway";
                    }
                });
            }
        });


        $('#insertsgroup').click(function () {
            $('.clearerror').text("");
            var fixed = $('#a_fixed').val();
            var name = $('#a_name').val();
            var percent = $('#a_percent').val();

            if (fixed == '' || fixed == null)
            {
                $("#addsubcat").text('ENTER FIXED');
            } else if (percent == "" || percent == null) {
                $("#addsubcat").text('ENTER PERCENT');
            } else if (name == "" || name == null) {
                $("#addsubcat").text('Enter Name');
            } else {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/AddPaymentGateway",
                    type: 'POST',
                    data: {
                        name: name,
                        percent: percent,
                        fixed: fixed
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $('.close').trigger('click');
                        window.location = "<?php echo base_url(); ?>index.php/superadmin/pgateway";
                    }
                });
            }
        });

        $('#add_group').click(function () {
            $('#add_model').modal('show');
        });

        $('#edit').click(function () {
            $('#modalHeading').html('EDIT PAYMENT GATEWAY');
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 1)
            {
                var data = $('.checkbox:checked').map(function () {
                    return $(this).attr("data");
                }).get();
                var dataarr = data[0].split(",");
                $('#editModal').modal('show');
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/EditGetPaymentGateWay",
                    type: 'POST',
                    data: {
                        payid: dataarr[0]
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $('#pay_id').val(response._id);
                        $("#name").val(response.name);
                        $("#percent").val(response.percent);
                        $("#fixed").val(response.fixed);
                    }
                });
            } else {
                $("#display-data").text('SELECT ONE RECORD TO EDIT');
            }
        });

        $('#fdelete').click(function () {
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length < 0 || val.length == 0) {

                $("#display-data").text("SELECT ONE OR MORE RECORD TO DELETE");

            } else if (val.length == 1 || val.length > 1)
            {
                $("#display-data").text("");
                var BusinessId = val;
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodel');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#confirmmodel').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }

                $("#confirmed").click(function () {
                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/DeletePaymentGateway",
                        type: "POST",
                        data: {
                            val: val
                        },
                        dataType: 'json',
                        success: function (response)
                        {
                            $(".close").trigger("click");
                            window.location = "<?php echo base_url(); ?>index.php/superadmin/pgateway";

                        }
                    });

                });
            }
        });


    });

</script>
<style>
    #active{
        display:none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
//        var status = '<?php // echo $status; ?>';
//        if (status == 1) {
//            $('#inactive').show();
//            $('#active').hide();
//            $('#btnStickUpSizeToggler').show();
//        }
        $('#big_table_processing').show();
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_pgateway',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {

                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };



        table.dataTable(settings);
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

        $('.changeMode').click(function () {

            $('#big_table_processing').show();
            var table = $('#big_table');
            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_pgateway',
//                "bJQueryUI": true,
//                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {

                    $('#big_table_processing').hide();
                },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            };

            $('.tabs_active').removeClass('active');

            $(this).parent().addClass('active');



            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
                table.fnFilter($(this).val());
            });

        });
        $('.tabs_active').removeClass('active');
        $(this).parent().addClass('active');

        table.dataTable(settings);
        $('#ecityid').change(function () {
            table.fnFilter($(this).val());
        });
    });
</script>


<style>
    .exportOptions{
        display: none;
    }
         .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
</style>

<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">PAYMENT GATEWAY</a>
            </li>
             
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white whenclicked li" role="tablist" data-init-reponsive-tabs="collapse">
                    <li id= "1" class="active" style="cursor: auto;">
                            <a  class="" style="cursor: auto;" ><span style="cursor: auto;">PAYMENT GATEWAY</span></a>
                        </li>
                        <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons" id="add_group"><i class="fa fa-plus text-white"></i> <?php echo BUTTON_ADD; ?></button></div>
                        <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons" id="edit"><i class="fa fa-edit text-white"></i> <?php echo BUTTON_EDIT; ?></button></div>
                        <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons" id="fdelete"><span><i class="fa fa-trash text-white"></i> <?php echo BUTTON_DELETE; ?></button></a></div>
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                 <div class="error-box" id="display-data" style="text-align:center"></div>
                                        <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif"></div>

                                
                                            <div class="searchbtn row clearfix pull-right" >

                                                <div class="pull-right" style="margin-right: 15px;" ><input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH;?>"> </div>
                                            </div>
                                       
                                            <div class="dltbtn">
                               
                                    
                                </div>
                                <br>
                                <div class="panel-body no-padding">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
 

<div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center">
                        ARE YOU  SURE YOU WANT TO DELETE PAYMENT GATEWAY
                    </div>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4">

                    </div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo NO ?></button>
                        <button type="button" class="btn btn-primary pull-right" id="confirmed" >
                            <?php echo YES ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
            </div>  
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo NO ?></button>
                    </div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_YES; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade stick-up" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3 id='modalHeading'>EDIT PAYMENT GATEWAY</h3>
                </div>
                <br/>
                <input name="fdata[pay_id]" type="hidden" id="pay_id"/> 
                <div class="modal-body">                   
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="name" class="col-sm-4 control-label">PAYMENT GATEWAY NAME<span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[name]" type="text" id="name"  placeholder="Payment Gateway Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>                    
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="percent" class="col-sm-4 control-label">PERCENT<span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[percent]" type="text" id="percent"  placeholder="PERCENT" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>                    
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="fixed" class="col-sm-4 control-label">FIXED<span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[fixed]" type="text" id="fixed"  placeholder="FIXED" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="clearerror"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="editgateway" ><?php echo BUTTON_ADD; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>


<div class="modal fade stick-up" id="add_model" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3>ADD PAYMENT GATEWAY</h3>
                </div>
                <br>
                <div class="modal-body">                          
                    <br>
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="group_name" class="col-sm-4 control-label">PAYMENT GATEWAY NAME<span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[name]" type="text" id="a_name"  placeholder="PAYMENT GATEWAY NAME Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>                    
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="a_percent" class="col-sm-4 control-label">PAYMENT GATEWAY PERCENT<span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[name]" type="text" id="a_percent"  placeholder="PERCENT" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>                    
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="a_fixed" class="col-sm-4 control-label">PAYMENT GATEWAY FIXED<span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[fixed]" type="text" id="a_fixed"  placeholder="FIXED" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="addsubcat"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="insertsgroup" ><?php echo BUTTON_ADD; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>