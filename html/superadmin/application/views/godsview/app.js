var express = require('express');
var app = express();
var path = require("path");
var mongoose = require('mongoose');

var mysql = require('mysql');
var connection = mysql.createConnection({
	host: '139.59.191.126',
	user: 'root',
	password: 'WGhbMv3c0212tyO',
	database: 'postmates'
});
connection.connect();

//connecting mongodb database
mongoose.connect("mongodb://jd_admin:3Embed#pass@139.59.191.126:27017/PostMates", function (err) {
	if (err) {
		console.log({ error: 'error in mongoose connect', err: err });
	} else {
		db = mongoose.connection;
		console.log('connected to database');
	}
});

//cross origin access
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

// var favicon = require('serve-favicon');

// app.use(favicon("http://ideliver.mobi/iDeliver/sadmin//theme/icon/fevicon.png"));

app.get('/', function (req, res) {
	res.sendFile(path.join(__dirname + '/home.html'));
});

//city type data from mysql database
app.get("/citydata", function (req, res) {
	connection.query('SELECT * FROM postmates.city_available ', function (err, rows, fields) {
		if (!err) {
			res.send(rows);
		}
		else
			console.log('Error while performing Query. citydata');
	});
});

//appointment details for selected driver from mysql
app.get("/appointmentdata/:id/:apid", function (req, res) {
	var id = req.params.id;
	var apid = Number(req.params.apid);

	connection.query('select s.*,a.* from postmates.master m, postmates.appointment a, postmates.slave s where m.mas_id = a.mas_id and a.slave_id = s.slave_id and a.appointment_id=' + apid + ' and  m.email="' + id + '"', function (err, rows, fields) {
		if (!err) {
			var collection1 = db.collection('ProviderData');
			var collection = db.collection('appointment');
			collection.find({ appointment_id: apid }).toArray(function (err, result) {
				if (!err) {
					var bid = mongoose.Types.ObjectId(result[0].BusinessId);
					collection1.find({ "_id": bid }).toArray(function (err, result1) {
						if (!err) {
							res.send({
								'app': rows,
								'sname': result1[0].ProviderName
							});
						}
					});
				}
			});
		}
	});
});

app.listen(8080, function () {
	console.log('Example app listening on port 8080!');
});