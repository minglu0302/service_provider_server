<?php
require_once '../Models/config.php';
$dType = array(
        '0' => 'text',
        '1' => 'date'
    );
$cat = $_GET['cat'];

$mongo_hostbase = 'localhost';
$mongo_database = 'iserve';
$mongo_username = 'iserve_user';
$mongo_password = '#iserve_user';
$con = new MongoClient('mongodb://' . $mongo_hostbase . ':27017');
$mongo = $con->selectDB($mongo_database);
if ($mongo_username != '' && $mongo_password != '')
    $mongo->authenticate($mongo_username, $mongo_password);

//$con = new Mongo("mongodb://iserve_user:#iserve_user@localhost:27017");
//$mongo = $con->selectDB('iserve');

$hlp_txt = $mongo->selectCollection('support_txt');
$helpText = $hlp_txt->findOne(array('_id' => new MongoId($cat)));

if($helpText['has_scat']){
    if (isset($_GET['scat'])) {
        $scat = $_GET['scat'];
        foreach ($helpText['sub_cat'] as $val) {
            if ($val['scat_id'] == $scat) {
                $helpText = $val;
                break;
            }
        }
    }
}
$lan = 0;
if (isset($_GET['lan']))
    $lan = $_GET['lan'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?= APP_NAME?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>
            @font-face {
                font-family: 'MyWebFont';
                src: url('ClanPro-WideNews.otf'); /* IE9 Compat Modes */
                src: 
                    url('ClanPro-WideNews') format('otf'), /* Super Modern Browsers */
                    url('webfont.woff') format('woff'), /* Pretty Modern Browsers */
                    url('webfont.ttf')  format('truetype') /* Safari, Android, iOS */
            }
            body,p,input,label,textarea {
                font-family: 'ClanPro-WideNews', sans-serif!important;
            }
            input{
                border: none;
                outline:0px;
                border-bottom:1px solid #d8d8d8;
                padding-bottom:8px;
                width:100%;
                font-size:12px;
            }
            input:focus,textarea:focus { 
                border-bottom:1px solid #16bbcc;
            }
            textarea{border:none;
                     border-bottom:1px solid #d8d8d8;
                     width:100%;
                     outline:0px;
                     overflow:hidden;
                     font-size:12px;
                     resize: none;}

            p{color:#949494;
              font-size:12px}

            h2 {
                font-size: 15px;
            }
            button{outline:0px;width:100%;
                   letter-spacing:1px}
            span {
                font-size: 10px;
                font-weight: 400;
                color: #949494;
                font-family: 'ClanPro-WideNews', sans-serif!important;
            }
            .btn-primary{
                color: #fff;
                background-color: #949494;
                border-color: #949494;
                outline:0px;
                padding-top: 12px;
                padding-bottom: 12px;
            }
            .btn-primary:hover{
                color: #fff;
                background-color: #949494;
                border-color: #949494;
            }
            .btn-primary:active{
                color: #fff;
                background-color: #949494;
                border-color: #949494;
            }
            span.required::after{
                content:" *";
                color:red;
            }
        </style>
    </head>

    <body>

        <div class="container">

        </div>
        <div class="container">
            <h2><?= $helpText['name'][$lan] ?></h2>
            <p><?= $helpText['desc'][$lan] ?></p>
       </div>

    </body>
</html>

