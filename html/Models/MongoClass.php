<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MongoClass {

    private $db;
    public $selects = array();
    public $wheres = array();
    public $sorts = array();
    public $updates = array();
    public $limit = FALSE;
    public $offset = FALSE;
    public $_inserted_id = FALSE;

    public function __construct($db) {

        $this->db = $db;
    }

    public function get($collection = "", $limit = FALSE, $offset = FALSE) {
        if (empty($collection)) {
            $return = array("flag" => 1, "msg" => "In order to retreive documents from MongoDB, a collection name must be passed");
        }
        $cursor = $this->db->selectCollection($collection)->find($this->wheres, $this->selects); //->find($this->wheres, $this->selects);

        foreach ($cursor as $data) {
            $return[] = $data;
        }
        $return = array("flag" => 0, "data" => $return);
        return $return;
    }

    public function get_where($collection = "", $where = array(), $limit = FALSE, $offset = FALSE) {
        if (empty($collection)) {
            $return = array("flag" => 1, "msg" => "In order to retreive documents from MongoDB, a collection name must be passed");
        }
        return $this->where($where)->get($collection, $limit, $offset);
    }

    public function get_one($collection = "", $where = array()) {
        if (empty($collection)) {
            $return = array("flag" => 1, "msg" => "In order to retreive documents from MongoDB, a collection name must be passed");
        }
        $data = $this->db->selectCollection($collection)->findOne($where, $this->selects);
        if (count($data) <= 0) {
            return array("flag" => 1, "msg" => "No data foud");
        }
        return $data;
    }

    public function where($wheres = array(), $native = FALSE) {
        if ($native === TRUE && is_array($wheres)) {
            $this->wheres = $wheres;
        } elseif (is_array($wheres)) {
            foreach ($wheres as $where => $value) {
                $this->_where_init($where);
                $this->wheres[$where] = $value;
            }
        }
        return $this;
    }

    public function _where_init($param) {
        if (!isset($this->wheres[$param])) {
            $this->wheres[$param] = array();
        }
    }

    /**
     *
     * 	Count the documents based upon the passed parameters
     *
     *  @since v1.0.0
     */
    public function count($collection = "", $operation = array()) {
        if (empty($collection)) {
            $return = array("flag" => 1, "msg" => "In order to retreive documents from MongoDB, a collection name must be passed");
        }
        $cursor = $this->db->selectCollection($collection)->find($operation);
        return $cursor->count();
    }

    /**
     * 
     * get aggrigation query 
     * 
     */
    public function aggrigation($collection = "", $SubArrayName = "", $operation = array(), $otherData = NULL) {

        if (empty($collection)) {
            $return = array("flag" => 1, "msg" => "In order to retreive documents from MongoDB, a collection name must be passed");
        }
        $data = $this->db->selectCollection($collection)->aggregate(
                array('$match' => $operation), array('$unwind' => '$' . $SubArrayName), array('$match' => $operation)
        );

        if (empty($data['result'][0]['status']))
            return array("flag" => 1, "msg" => "status is not available");

        if ($otherData == NULL)
            $return = $data['result'][0]['status'];
        else
            $return = $data['result'][0];

        return $return;
    }

    /**
     *
     * 	Insert a new document into the passed collection
     *
     *  @since v1.0.0
     */
    public function insert($collection = "", $insert = array()) {
        if (empty($collection)) {
            return array("flag" => 1, "msg" => "In order to retreive documents from MongoDB, a collection name must be passed");
        }

        if (count($insert) == 0) {
            return array("flag" => 1, "msg" => "In order to retreive documents from MongoDB, a collection name must be passed");
        }
        $this->_inserted_id = FALSE;
        try {
            $insertResult = $this->db->selectCollection($collection)->insert($insert);
//            return $insertResult;
                return array("flag" => 0, "msg" => "not inserted");
        } catch (MongoException $e) {
            return array("flag" => 1, "msg" => "Insert of data into MongoDB failed: {$e->getMessage()}");
        } catch (MongoCursorException $e) {
            return array("flag" => 1, "msg" => "Insert of data into MongoDB failed: {$e->getMessage()}");
        }
    }

    /**
     *
     * Update a single document
     *
     *   @since v1.0.0
     */
    public function update($collection = "", $data = array(), $options = array()) {
        if (empty($collection)) {
            return array("flag" => 1, "msg" => "No Mongo collection selected to update");
        }
        if (is_array($data) && count($data) > 0) {
            $this->_update_init('$set');
            $this->updates['$set'] += $data;
        }
        if (count($this->updates) == 0) {
            return array("flag" => 1, "msg" => "Nothing to update in Mongo collection or update is not an array");
        }
        try {
            $data = $this->db->selectCollection($collection)->update($options, array('$set' => $data), array('multiple' => true));
            return array("flag" => 0, "data" => $data);
        } catch (MongoCursorException $e) {
            return array("flag" => 1, "msg" => "Update of data into MongoDB failed: {$e->getMessage()}");
        } catch (MongoCursorException $e) {
            return array("flag" => 1, "msg" => "Update of data into MongoDB failed: {$e->getMessage()}");
        } catch (MongoCursorTimeoutException $e) {
            return array("flag" => 1, "msg" => "Update of data into MongoDB failed: {$e->getMessage()}");
        }
    }

    public function updatewithpush($collection = "", $data = array(), $options = array()) {
        $data = $this->db->selectCollection($collection)->update($options, array('$push' => $data), array("upsert" => true));
        return array("flag" => 0, "data" => $data);
    }

    public function _update_init($method) {
        if (!isset($this->updates[$method])) {
            $this->updates[$method] = array();
        }
    }

    public function getLastIdInCollection($collection = "", $field = "") {
        $cursor = $this->db->selectCollection($collection)->find(array(),array($field))->sort(array((string)$field => -1))->limit(1);
        foreach ($cursor as $data) {
            $return[] = $data;
        }
        return array("flag" => 0, "data" => $return[0][$field]);
    }

    /**
     *
     * Delete document from the passed collection based upon certain criteria
     *
     *   @since v1.0.0
     */
    public function delete($collection = "", $options = array()) {
        if (empty($collection)) {
            return array("flag" => 1, "msg" => "No Mongo collection selected to update");
        }
        try {
            $options = array_merge($options);
            if ($options)
                $data = $this->db->selectCollection($collection)->remove($options);
            return array("flag" => 0, "data" => $data);
        } catch (MongoCursorException $e) {
            return array("flag" => 1, "msg" => "Update of data into MongoDB failed: {$e->getMessage()}");
        } catch (MongoCursorTimeoutException $e) {
            return array("flag" => 1, "msg" => "Update of data into MongoDB failed: {$e->getMessage()}");
        }
    }

}

?>