<?php
error_reporting(-1);
ini_set('display_errors', 'On');
require_once 'Models/config.php';
require_once 'Models/ConDBServices.php';
$config = new config();
$db = new ConDBServices();

//$con = new Mongo();
//$mongodb = $con->iserve;
$mongodb = $db->mongo;
$citylist = $devType = array();
$Mileage_type = array();
$Hourly_type = array();
$Fixed_type = array();
$pcol = $mongodb->selectCollection('Category');
$cursor = $pcol->find(array(), array('cat_name' => 1, 'fee_type' => 1));
foreach ($cursor as $cur) {
    $cur['type_id'] = (string) $cur['_id'];
    if ($cur['fee_type'] == "Mileage") {
        $Mileage_type[] = $cur;
    } else if ($cur['fee_type'] == "Hourly") {
        $Hourly_type[] = $cur;
    } else if ($cur['fee_type'] == "Fixed") {
        $Fixed_type[] = $cur;
    }
}
$getTypesQry = "select * from dev_type";
$getTypesRes = mysql_query($getTypesQry, $db->conn);
while ($type = mysql_fetch_assoc($getTypesRes)) {
    $devType[] = $type;
}

$getCityQry = "select City_Id,City_Name from city_available";
$getCityRes = mysql_query($getCityQry, $db->conn);
while ($type = mysql_fetch_assoc($getCityRes)) {
    $citylist[] = $type;
}
$processFile = 'services.php/';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title></title>
        <style>
            .form_row{height: 40px;}
            .form_label{display: inline-block;width: 250px;text-align: right;}
            .form_field{display: inline;}
            .div_service{margin:3% 0;}
            .list_1 li{padding: 10px 0;}
            .list_1 li a{color: #1383e4;font-weight: bold;font-family: verdana;}
        </style>
        <script>
            var typearr = [];
            function selectType(val)
            {
                if (val.checked)
                {
                    typearr.push(val.value);
                } else {
                    var index = typearr.indexOf(val.value);
                    typearr.splice(index, 1);
                }
                document.getElementById('ent_device_type_arr').value = typearr;
            }
        </script>
    </head>
    <body>
        <div id="top">&nbsp;</div>
        <div style="position:fixed;right: 5%;top:40%;">
            <a href="#top"><input type="button" value="Top"/></a>
        </div>
        <h2>Mobile <?php echo APP_NAME ?> app services</h2>
        <div style="width:35%;text-align: left;float: left;">
            <h3>Provider app (p)</h3>
            <ol type="1" class="list_1">

                <li><a style="background: yellow" href="#doctor_signup1_service">Provider Signup</a> (masterSignup)</li>
                <li><a style="background: yellow" href="#getCityList">Get City List</a> (getCityList)</li>
                <li><a style="background: yellow" href="#getTypeBYCityId">Get Pro By City Id </a> (getTypeBYCityId)</li>
                <li><a style="background: yellow" href="#doctor_login_service">Provider Login</a> (masterLogin)<br><br></li>
                <li><a style="background: yellow" href="#get_appointments_service">Get appointments</a> (getMasterAppointments)(HOME + HISTORY SCREEN)</li>
                <li><a style="background: yellow" href="#getMasterAppointmentsHome">Get appointments Home</a> (getMasterAppointmentsHome)(HOME)</li>
                <li><a style="background: yellow" href="#getMasterAppointmentsHistory">Get appointments History</a> (getMasterAppointmentsHistory)(HISTORY)</li>                
                <li><a style="background: yellow" href="#respond_to_request_service">Respond to appointment request</a> (respondToAppointment)</li>                
                <li><a style="background: yellow" href="#update_status_service">Update appointment status to Customer</a> (updateApptStatus)<br><br></li>
                <li><a style="background: yellow" href="#update_status_service">Update appointment Timer</a> (UpdateApptTimer)<br><br></li>                         
                <li><a style="background: yellow" href="#get_master_profile_service">Get master profile</a> (getMasterProfile)</li>
                <li><a style="background: yellow" href="#updateMasterProfile">Update master profile</a> (updateMasterProfile)</li>                
                <li><a style="background: yellow" href="#updateMasterJobImages">Update master JOB IMAGES</a> (updateMasterJobImages)</li>                
                <li><a style="background: yellow" href="#reser_pass_service">Reset password</a> (resetPassword)</li>
                <li><a style="background: yellow"  href="#update_presence_service">Update Provider as active or inactive</a> (updateMasterStatus)<br><br></li>   
                <li><a style="background: yellow" href="#abort_appointment_service">Abort appointment</a> (abortAppointment)<br><br></li>
                <li><a style="background: yellow" href="#raise_invoice">Raise Invoice</a> (raiseInvoice)<br><br></li>
                <li><a style="background: yellow" href="#GetFinancialData">GetFinancialData</a> (GetFinancialData)<br><br></li>
                <li><a style="background: yellow" href="#CanIStartJob">CanIStartJob</a> (CanIStartJob)<br><br></li>
                <li><a style="background : yellow" href="#AddProviderAddress">Add Provider address</a> (AddProviderAddress)<br><br></li>                
                <li><a style="background : yellow" href="#DeleteProviderAddress">Delete Provider address</a> (DeleteProviderAddress)<br><br></li>
                <li><a style="background : yellow" href="#GetProviderAddress">Get Provider address</a> (GetProviderAddress)<br><br></li>
                <li><a style="background : yellow" href="#addslot">ADD SLOT</a> (addslot)<br><br></li>
                <li><a style="background : yellow" href="#removeSlot">REMOVE SLOT</a> (removeSlot)<br><br></li>
                <li><a style="background : yellow" href="#GetAllSlot">GET ALL SLOT</a> (GetAllSlot)<br><br></li>
                <li><a style="background : yellow" href="#GetBookedSlotDetail">GET BOOKED SLOT DETAIL</a> (GetBookedSlotDetail)<br><br></li>
                <li><a style="background : yellow" href="#GetCategoryDetail">GET CATEGORY DETAIL</a> (GetCategoryDetail)<br><br></li>
                <li><a style="background : yellow" href="#UpdateCategoryDetail">Update CATEGORY DETAIL</a> (UpdateCategoryDetail)<br><br></li>
                <li><a style="background : yellow" href="#UpdateBookingReminder">Update Booking Reminder</a> (UpdateBookingReminder)<br><br></li>
                <li><a style="background : yellow" href="#ProviderCheckMobile">ProviderCheckMobile</a> (ProviderCheckMobile)<br><br></li>

                <li><a style="background : yellow" href="#AddBankDetails">AddBankDetails</a> (AddBankDetails)<br><br></li>
                <li><a style="background : yellow" href="#getMasterBankData">getMasterBankData</a> (getMasterBankData)<br><br></li>
                <li><a style="background : yellow" href="#deleteMasterBank">deleteMasterBank</a> (deleteMasterBank)<br><br></li>
                <li><a style="background : yellow" href="#BankAccountDefault">BankAccountDefault</a> (BankAccountDefault)<br><br></li>
            </ol>
        </div>
        <div style="width:35%;text-align: left;float: left;">
            <h3>CUSTOMER app (C)</h3>
            <ol type="1" class="list_1">
                <li><a style="background : yellow" href="#patient_signup_service">Customer Signup</a> (slaveSignup)</li>                
                <li><a  style="background : yellow" href="#patient_login_service">Customer Login</a> (slaveLogin)<br><br></li>
                <li><a  style="background : yellow" href="#get_doctor_det_service">Get Master details</a> (getMasterDetails)(Return Master Slot, Details, Review)</li>
                <li><a  style="background : yellow" href="#get_doctor_reviews_service">Get Provider reviews</a> (getMasterReviews)(return all review for master id )</li>
                <li><a  style="background : yellow" href="#look_booking_service">Live booking</a> (liveBooking)(Live , Later Booking)</li>
                <li><a style="background : yellow" href="#get_apnt_det_service">Get appointment details</a> (getAppointmentDetails)(single appointment detail if ent_inv 1  then all invoice data else appointment data)</li>
                <li><a  style="background : yellow" href="#update_review_service">Give Appointment review</a> (updateSlaveReview)(Customer will submit review for appointment)</li>
                <li><a style="background : yellow" href="#get_slave_apnts_service">Get Customer appointments new</a> (getSlaveAppts)(Return All Appointments ongoing and past)</li>
                <li><a style="background : yellow" href="#getAllOngoingBookings">Get all Ongoing Bookings</a> (getAllOngoingBookings)(Return Current Booking BID & all completed but not Reviewed BID Array)</li>
                <li><a style="background : yellow" href="#cancel_appointment_service">Cancel appointment</a> (cancelAppointment)(Befor Accept, After Accept & Before OntheWay)<br><br></li>
                <li><a style="background : yellow" href="#get_profile_service">Get profile data</a> (getProfile)(get Customer Data)</li>
                <li><a style="background : yellow" href="#update_profile_service">Update profile data</a> (updateProfile)(update Customer Data)<br><br></li>
                <li><a style="background : yellow" href="#add_card_service">Add a credit card</a> (addCard)</li>
                <li><a  style="background : yellow" href="#get_cards_service">Get cards</a> (getCards)</li>          
                <li><a style="background : yellow" href="#remove_card_service">Remove a credit cards</a> (removeCard)</li>                
            </ol>
        </div>
        <div>
            <h3>CUSTOMER app (C)</h3>
            <ol type="1" class="list_1">
                <li><a  style="background : yellow" href="#check_email_service">Validate email and zipcode</a> (validateEmailZip)</li>
                <li><a style="background : yellow" href="#forgot_pass_service">Forgot password</a> (resetPassword)</li>                       
                <li><a style="background : yellow" href="#logout_service">Logout user</a> (logout)<br><br></li>
                <li><a style="background : yellow" href="#check_coupon_service">Check coupon available or not</a> (checkCoupon)<br><br></li>
                <li><a style="background : yellow" href="#verify_coupon_service">Verify coupon available or not</a> (verifyCode)<br><br></li>
                <li><a style="background : yellow" href="#AddCustomerAddress">Add Customer address</a> (AddCustomerAddress)<br><br></li>
                <li><a style="background : yellow" href="#DeleteCustomerAddress">Delete Customer address</a> (DeleteCustomerAddress)<br><br></li>
                <li><a style="background : yellow" href="#GetCustomerAddress">Get Customer address</a> (GetCustomerAddress)<br><br></li>
                <li><a style="background : yellow" href="#getAllServices">Get All Services</a> (getAllServices)<br><br></li>
                <li><a style="background : yellow" href="#UpdateAppVersion">Update App Version</a> (UpdateAppVersion)<br><br></li>
                <li><a style="background : yellow" href="#verify_code_service">Verify the code</a> (verifyPhone)</li>
                <li><a  style="background : yellow" href="#verify_mobile_service">Verify mobile available or not</a> (checkMobile)<br><br></li>
                <li><a  style="background : yellow" href="#get_code_service">Get mobile verification code</a> (getVerificationCode)</li>
                <li><a  style="background : yellow" href="#reportDispute">Report Dispute</a> (reportDispute)</li>                
                <li><a  style="background : yellow" href="#support">Support</a> (support)</li>
                <li><a  style="background : yellow" href="#getCancellationReson">getCancellationReson</a> (getCancellationReson)</li>                
                <li><a  style="background : yellow" href="#ForgotPasswordWithOtp">ForgotPasswordWithOtp</a> (ForgotPasswordWithOtp)</li>                
                <li><a  style="background : yellow" href="#updatePasswordForUser">updatePasswordForUser</a> (updatePasswordForUser)</li>                
                <li><a  style="background : yellow" href="#testfcmpush">TEST FCM PUSH</a> (testfcmpush)</li>
                <li><a  style="background : yellow" href="#pushFromAdmin">Push FROM Admin</a> (pushFromAdmin)</li>
            </ol>
        </div>
        <div style="clear:both;"></div>
        <div  id="push" class="div_service">

        </div>
        <div id="doctor_signup1_service" class="div_service">
            <form action="<?php echo $processFile; ?>masterSignup" method="post">
                <h3>(p) Provider Signup</h3>
                <div class="form_row">
                    <div class="form_label">First Name *: </div>
                    <div class="form_field"><input type="text" name="ent_first_name" />name= "ent_first_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Last Name : </div>
                    <div class="form_field"><input type="text" name="ent_last_name" />name="ent_last_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Password *: </div>
                    <div class="form_field"><input type="text" name="ent_password" />name="ent_password",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Country Code *: </div>
                    <div class="form_field"><input type="text" name="ent_country_code" />name="ent_country_code",type="string"</div>
                </div>                
                <div class="form_row">
                    <div class="form_label">Mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_mobile" />name="ent_mobile",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Zipcode *: </div>
                    <div class="form_field"><input type="text" name="ent_zipcode" />name="ent_zipcode",type="string"</div>
                </div> 
                <div class="form_row">
                    <div class="form_label">License Num *: </div>
                    <div class="form_field"><input type="text" name="ent_license_num" />name="ent_license_num",type="string"</div>
                </div> 
                <div class="form_row">
                    <div class="form_label">Latitude : </div>
                    <div class="form_field"><input type="text" name="ent_latitude" />name="ent_latitude",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude : </div>
                    <div class="form_field"><input type="text" name="ent_longitude" />name="ent_longitude",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Profile Pic : </div>
                    <div class="form_field"><input type="text" name="ent_pro_image" />name="ent_pro_image",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">App Version : </div>
                    <div class="form_field"><input type="text" name="ent_app_version" />name="ent_app_version",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device OS : </div>
                    <div class="form_field"><input type="text" name="ent_dev_os" />name="ent_dev_os",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Model : </div>
                    <div class="form_field"><input type="text" name="ent_dev_model" />name="ent_dev_model",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Fees Group : </div>
                    <div class="form_field"><input type="text" name="ent_fees_group" />name="ent_fees_group",type="Integer" 1-Milage, 2-Hourly, 3-fixed</div>
                </div>
                <div class="form_row">
                    <div class="form_label">City Id *: </div>
                    <div class="form_field">
                        <select name="ent_city_id">
                            <?php foreach ($citylist as $city) { ?> ,
                                <option value="<?php echo $city['City_Id']; ?>"><?php echo $city['City_Name']; ?></option>
                            <?php } ?>
                        </select>                     
                        name="ent_city_id"
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label"> Milage Type *: </div>
                    <div class="form_field">
                        <?php foreach ($Mileage_type as $type) { ?>
                            <input type="checkbox" onchange="selectType(this)" value="<?php echo $type['type_id']; ?>" id="id_chk7" class="cbtype" value="7" /><?php echo $type['cat_name']; ?>                     
                        <?php } ?>
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label"> Hourly Type *: </div>
                    <div class="form_field">
                        <?php foreach ($Hourly_type as $type) { ?>
                            <input type="checkbox" onchange="selectType(this)" value="<?php echo $type['type_id']; ?>" id="id_chk7" class="cbtype" value="7" /><?php echo $type['cat_name']; ?>                     
                        <?php } ?>
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label"> Fixed Type *: </div>
                    <div class="form_field">
                        <?php foreach ($Fixed_type as $type) { ?>
                            <input type="checkbox" onchange="selectType(this)" value="<?php echo $type['type_id']; ?>" id="id_chk7" class="cbtype" value="7" /><?php echo $type['cat_name']; ?>                     
                        <?php } ?>
                    </div>
                </div> name="ent_device_type_arr" 
                <input type="hidden" id="ent_device_type_arr" name="ent_device_type_arr"/>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" /> &nbsp;&nbsp;&nbsp; name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Push Token *: </div>
                    <div class="form_field"><input type="text" name="ent_push_token" />name="ent_push_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device type *: </div>
                    <div class="form_field">
                        <select name="ent_device_type">
                            <?php foreach ($devType as $dev) { ?>
                                <option value="<?php echo $dev['dev_id']; ?>"><?php echo $dev['name'] . '(' . $dev['dev_id'] . ')'; ?></option>
                            <?php } ?>
                        </select>name="ent_device_type",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="doctor_login_service" class="div_service">
            <form action="<?php echo $processFile; ?>masterLogin" method="post">
                <h3>(P) Provider Login</h3>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Password *: </div>
                    <div class="form_field"><input type="text" name="ent_password" />name="ent_password",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Push Token *: </div>
                    <div class="form_field"><input type="text" name="ent_push_token" />name="ent_push_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device type *: </div>
                    <div class="form_field">
                        <select name="ent_device_type">
                            <?php foreach ($devType as $dev) { ?>
                                <option value="<?php echo $dev['dev_id']; ?>"><?php echo $dev['name'] . '(' . $dev['dev_id'] . ')'; ?></option>
                            <?php } ?>
                        </select>name="ent_device_type"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">App Version : </div>
                    <div class="form_field"><input type="text" name="ent_app_version" />name="ent_app_version",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device OS : </div>
                    <div class="form_field"><input type="text" name="ent_dev_os" />name="ent_dev_os",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Model : </div>
                    <div class="form_field"><input type="text" name="ent_dev_model" />name="ent_dev_model",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="patient_signup_service" class="div_service">
            <form action="<?php echo $processFile; ?>slaveSignup" method="post">
                <h3>(C) Customer Signup</h3>
                <div class="form_row">
                    <div class="form_label">First Name *: </div>
                    <div class="form_field"><input type="text" name="ent_first_name" />name= "ent_first_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Last Name : </div>
                    <div class="form_field"><input type="text" name="ent_last_name" />name="ent_last_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Password *: </div>
                    <div class="form_field"><input type="text" name="ent_password" />name="ent_password",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Country Code *: </div>
                    <div class="form_field"><input type="text" name="ent_country_code" />name="ent_country_code",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_mobile" />name="ent_mobile",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Zipcode *: </div>
                    <div class="form_field"><input type="text" name="ent_zipcode" />name="ent_zipcode",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Latitude: </div>
                    <div class="form_field"><input type="text" name="ent_latitude" />name="ent_latitude",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude: </div>
                    <div class="form_field"><input type="text" name="ent_longitude" />name="ent_longitude",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Referral code: </div>
                    <div class="form_field"><input type="text" name="ent_referral_code" />name="ent_referral_code",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Profile Pic: </div>
                    <div class="form_field"><input type="text" name="ent_profile_pic" />name="ent_profile_pic",type="string"</div>
                </div>

                <div class="form_row">
                    <div class="form_label">Access token : </div>
                    <div class="form_field"><input type="text" name="ent_token" />name="ent_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Terms and conditions *: </div>
                    <div class="form_field"><input type="text" name="ent_terms_cond" />name="ent_terms_cond",type="boolean"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Pricing conditions *: </div>
                    <div class="form_field"><input type="text" name="ent_pricing_cond" />name="ent_pricing_cond",type="boolean"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Push Token *: </div>
                    <div class="form_field"><input type="text" name="ent_push_token" />name="ent_push_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device type *: </div>
                    <div class="form_field">
                        <select name="ent_device_type">
                            <?php foreach ($devType as $dev) { ?>
                                <option value="<?php echo $dev['dev_id']; ?>"><?php echo $dev['name'] . '(' . $dev['dev_id'] . ')'; ?></option>
                            <?php } ?>
                        </select>name="ent_device_type",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">App Version : </div>
                    <div class="form_field"><input type="text" name="ent_app_version" />name="ent_app_version",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">ent_signup_type : </div>
                    <div class="form_field"><input type="text" name="ent_signup_type" />name="ent_signup_type",type="1-NORMAL 2-FB"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device OS : </div>
                    <div class="form_field"><input type="text" name="ent_dev_os" />name="ent_dev_os",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Model : </div>
                    <div class="form_field"><input type="text" name="ent_dev_model" />name="ent_dev_model",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="patient_login_service" class="div_service">
            <form action="<?php echo $processFile; ?>slaveLogin" method="post">
                <h3>(C) Customer Login</h3>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Password *: </div>
                    <div class="form_field"><input type="text" name="ent_password" />name="ent_password",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Latitude: </div>
                    <div class="form_field"><input type="text" name="ent_latitude" />name="ent_latitude",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude: </div>
                    <div class="form_field"><input type="text" name="ent_longitude" />name="ent_longitude",type="double"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Push Token *: </div>
                    <div class="form_field"><input type="text" name="ent_push_token" />name="ent_push_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device type *: </div>
                    <div class="form_field">
                        <select name="ent_device_type">
                            <?php foreach ($devType as $dev) { ?>
                                <option value="<?php echo $dev['dev_id']; ?>"><?php echo $dev['name'] . '(' . $dev['dev_id'] . ')'; ?></option>
                            <?php } ?>
                        </select>name="ent_device_type"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">App Version : </div>
                    <div class="form_field"><input type="text" name="ent_app_version" />name="ent_app_version",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device OS : </div>
                    <div class="form_field"><input type="text" name="ent_dev_os" />name="ent_dev_os",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Model : </div>
                    <div class="form_field"><input type="text" name="ent_dev_model" />name="ent_dev_model",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">ent_signup_type : </div>
                    <div class="form_field"><input type="text" name="ent_signup_type" />name="ent_signup_type",type="1-NORMAL 2-FB"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="getCityList" class="div_service">
            <form action="<?php echo $processFile; ?>getCityList" method="post">
                <h3>(P) Get City List </h3>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="getTypeBYCityId" class="div_service">
            <form action="<?php echo $processFile; ?>getTypeBYCityId" method="post">
                <h3>(P) Get Pro By City Id </h3>
                <div class="form_row">
                    <div class="form_label">City Id *: </div>
                    <div class="form_field"><input type="text" name="ent_city_id" />name="ent_city_id",type="datetime""</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>




        <div id="get_doctor_det_service" class="div_service">
            <form action="<?php echo $processFile; ?>getMasterDetails" method="post">
                <h3>(C) Get Provider details</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Provider id *: </div>
                    <div class="form_field"><input type="text" name="ent_pro_id" />name="ent_pro_id",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Slot date: </div>
                    <div class="form_field"><input type="text" name="ent_slot_date" />name="ent_slot_date",type="MM-DD-YYYY"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="get_doctor_reviews_service" class="div_service">
            <form action="<?php echo $processFile; ?>getMasterReviews" method="post">
                <h3>(C) Get Master reviews</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Doc email *: </div>
                    <div class="form_field"><input type="text" name="ent_doc_email" />name="ent_doc_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Page number : </div>
                    <div class="form_field"><input type="text" name="ent_page" />name="ent_page",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>



        <div id="get_appointments_service" class="div_service">
            <form action="<?php echo $processFile; ?>getMasterAppointments" method="post">
                <h3>(P) Get Appointments</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment date *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="date",format="YYYY-MM-DD" or "YYYY-MM"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="getMasterAppointmentsHome" class="div_service">
            <form action="<?php echo $processFile; ?>getMasterAppointmentsHome" method="post">
                <h3>(P) Get Appointments Home</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="getMasterAppointmentsHistory" class="div_service">
            <form action="<?php echo $processFile; ?>getMasterAppointmentsHistory" method="post">
                <h3>(P) Get Appointments History</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment Month *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="date",format="YYYY-MM"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>



        <div id="look_booking_service" class="div_service">
            <form action="<?php echo $processFile; ?>liveBooking" method="post">
                <h3>(C) Live booking:</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Customer Id *: </div>
                    <div class="form_field"><input type="text" name="ent_custid" />name= "ent_custid",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label"> Booking Type </div>
                    <div class="form_field"><input type="text" name="ent_btype" />name="ent_btype",type="Integer" , 1 For Now with Dispatcher , 2 For Later Booking, 3 For Now Without Dispatcher</div>
                </div>
                <div class="form_row">
                    <div class="form_label"> Provider Id </div>
                    <div class="form_field"><input type="text" name="ent_proid" />name="ent_proid",type="Integer" , if Booking Type is 2 Then</div>
                </div>
                <div class="form_row">
                    <div class="form_label"> Slot Id </div>
                    <div class="form_field"><input type="text" name="ent_slot_id" />name="ent_slot_id",type="String" , if Booking Type is 2 Then</div>
                </div>
                <div class="form_row">
                    <div class="form_label"> Device Type </div>
                    <div class="form_field"><input type="text" name="ent_dtype" />name="ent_dtype",type="Integer" , 1 For Android , 2 For IOS</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Address 1 *: </div>
                    <div class="form_field"><input type="text" name="ent_a1" />name="ent_a1",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Address 2 *: </div>
                    <div class="form_field"><input type="text" name="ent_a2" />name="ent_a2",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Latitude *: </div>
                    <div class="form_field"><input type="text" name="ent_lat" />name="ent_lat",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude *: </div>
                    <div class="form_field"><input type="text" name="ent_long" />name="ent_long",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Cat Id *: </div>
                    <div class="form_field"><input type="text" name="ent_cat_id" />name="ent_cat_id",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Cat name *: </div>
                    <div class="form_field"><input type="text" name="ent_cat_name" />name="ent_cat_name",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Card Id *: </div>
                    <div class="form_field"><input type="text" name="ent_card_id" />name="ent_card_id",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Services ID List : </div>
                    <div class="form_field"><input type="text" name="ent_service_id" />name="ent_service_id",type="coma saprated String String "</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Job Images *: </div>
                    <div class="form_field"><input type="text" name="ent_job_imgs" />name="ent_job_imgs",type="Integer" Number Of Images</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Job Details (Notes)*: </div>
                    <div class="form_field"><input type="text" name="ent_job_details" />name="ent_job_details",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Cash OR Card *: </div>
                    <div class="form_field"><input type="text" name="ent_pymt" />name="ent_pymt",type="Integer" 1 For CASH, 2 for CARD</div>
                </div>        
                <div class="form_row">
                    <div class="form_label">Zipcode *: </div>
                    <div class="form_field"><input type="text" name="ent_zipcode" />name="ent_zipcode",type="int"</div>
                </div>            
                <div class="form_row">
                    <div class="form_label">Coupon : </div>
                    <div class="form_field"><input type="text" name="ent_coupon" />name="ent_coupon",type="string"</div>
                </div> 

                <div class="form_row">
                    <div class="form_label">Discount *: </div>
                    <div class="form_field"><input type="text" name="ent_discount" />name="ent_discount",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Hourly Total Amount : </div>
                    <div class="form_field"><input type="text" name="ent_hourly_amt" />name="ent_hourly_amt",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Total *: </div>
                    <div class="form_field"><input type="text" name="ent_total" />name="ent_total",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>



        <div id="get_apnt_det_service" class="div_service">
            <form action="<?php echo $processFile; ?>getAppointmentDetails" method="post">
                <h3>(C) Get appointment details</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking Id *: </div>
                    <div class="form_field"><input type="text" name="ent_bid" />name="ent_bid",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appt Status *: </div>
                    <div class="form_field"><input type="text" name="ent_inv" />name="ent_inv",type="Integer" 0-Noraml, 1-Invoice</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:mm:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>



        <div id="update_review_service" class="div_service">
            <form action="<?php echo $processFile; ?>updateSlaveReview" method="post">
                <h3>(C) Give Appointment review</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label"> Booking Id *: </div>
                    <div class="form_field"><input type="text" name="ent_bid" />name="ent_bid",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Rating (1 to 5) *: </div>
                    <div class="form_field"><input type="text" name="ent_rating_num" />name="ent_rating_num",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Review message : </div>
                    <div class="form_field"><input type="text" name="ent_review_msg" />name="ent_review_msg",type="string"</div>
                </div>                   
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>



        <div id="getAllOngoingBookings" class="div_service">
            <form action="<?php echo $processFile; ?>getAllOngoingBookings" method="post">
                <h3>(c) Get Customer All Ongoing appointments</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Custome Id *: </div>
                    <div class="form_field"><input type="text" name="ent_cust_id" />name="ent_cust_id",type="Interger"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="get_slave_apnts_service" class="div_service">
            <form action="<?php echo $processFile; ?>getSlaveAppts" method="post">
                <h3>(C) Get Customer appointments new</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Page Index *: </div>
                    <div class="form_field"><input type="text" name="ent_page_index" />name="ent_page_index",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="cancel_appointment_service" class="div_service">
            <form action="<?php echo $processFile; ?>cancelAppointment" method="post">
                <h3>(C) Cancel appointment</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking Id *: </div>
                    <div class="form_field"><input type="text" name="ent_bid" />name="ent_bid",type="integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Reason *: </div>
                    <div class="form_field"><input type="text" name="ent_reason" />name="ent_reason",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="get_profile_service" class="div_service">
            <form action="<?php echo $processFile; ?>getProfile" method="post">
                <h3>(C) Get profile data</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>



        <div id="update_profile_service" class="div_service">
            <form action="<?php echo $processFile; ?>updateProfile" method="post">
                <h3>(C) Update profile data</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Customer Id*: </div>
                    <div class="form_field"><input type="text" name="ent_cid" />name="ent_cid",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">First name *: </div>
                    <div class="form_field"><input type="text" name="ent_first_name" />name="ent_first_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Last name : </div>
                    <div class="form_field"><input type="text" name="ent_last_name" />name="ent_last_name",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="add_card_service" class="div_service">
            <form action="<?php echo $processFile; ?>addCard" method="post">
                <h3>(C) Add a credit card</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Customer Id : </div>
                    <div class="form_field"><input type="text" name="ent_cust_id" />name="ent_cust_id",type="Int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Access token : </div>
                    <div class="form_field"><input type="text" name="ent_token" />name="ent_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="get_cards_service" class="div_service">
            <form action="<?php echo $processFile; ?>getCards" method="post">
                <h3>(C) Get cards</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Customer Id : </div>
                    <div class="form_field"><input type="text" name="ent_cust_id" />name="ent_cust_id",type="Int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>




        <div id="remove_card_service" class="div_service">
            <form action="<?php echo $processFile; ?>removeCard" method="post">
                <h3>(C) Remove a credit card</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Customer Id : </div>
                    <div class="form_field"><input type="text" name="ent_cust_id" />name="ent_cust_id",type="Int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Card Id : </div>
                    <div class="form_field"><input type="text" name="ent_token" />name="ent_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>




        <div id="verify_mobile_service" class="div_service">
            <form action="<?php echo $processFile; ?>checkMobile" method="post">
                <h3>Check mobile available or not :</h3>
                <div class="form_row">
                    <div class="form_label">Mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_phone" />name="ent_phone"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>

        <div id="get_code_service" class="div_service">
            <form action="<?php echo $processFile; ?>getVerificationCode" method="post">
                <h3>(C) Get mobile verification code </h3>

                <div class="form_row">
                    <div class="form_label">Phone with country code *: </div>
                    <div class="form_field"><input type="text" name="ent_phone" />name="ent_phone",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="reportDispute" class="div_service">
            <form action="<?php echo $processFile; ?>reportDispute" method="post">
                <h3>(C) Report Dispute </h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking Id *: </div>
                    <div class="form_field"><input type="text" name="ent_bid" />name="ent_bid",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Dispute Message *: </div>
                    <div class="form_field"><input type="text" name="ent_dispute_msg" />name="ent_dispute_msg",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="support" class="div_service">
            <form action="<?php echo $processFile; ?>support" method="post">
                <h3>(C) support </h3>
                <div class="form_row">
                    <div class="form_label">Language Id *: </div>
                    <div class="form_field"><input type="text" name="ent_lan" />name="ent_lan",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>



        <div id="getCancellationReson" class="div_service">
            <form action="<?php echo $processFile; ?>getCancellationReson" method="post">
                <h3>get Cancellation Reson </h3>
                <div class="form_row">
                    <div class="form_label">Language Id *: </div>
                    <div class="form_field"><input type="text" name="ent_lan" />name="ent_lan",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User Type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type",type="string" formate : 1-PROVIDER, 2-CUSTOMER</div>
                </div>                
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>




        <div id="updatePasswordForUser" class="div_service">
            <form action="<?php echo $processFile; ?>updatePasswordForUser" method="post">
                <h3>updatePasswordForUser </h3>
                <div class="form_row">
                    <div class="form_label">ent_mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_mobile" />name="ent_mobile",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">ent_pass *: </div>
                    <div class="form_field"><input type="text" name="ent_pass" />name="ent_pass",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">ent_user_type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type",type="string" formate : 1-PROVIDER, 2-CUSTOMER</div>
                </div>

                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="testfcmpush" class="div_service">
            <form action="<?php echo $processFile; ?>testfcmpush" method="post">
                <h3>testfcmpush</h3>
                <div class="form_row">
                    <div class="form_label">ent_ios_token : </div>
                    <div class="form_field"><input type="text" name="ent_ios_token" />name="ent_ios_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">ent_and_token : </div>
                    <div class="form_field"><input type="text" name="ent_and_token" />name="ent_and_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>  



        <div id="pushFromAdmin" class="div_service">
            <form action="<?php echo $processFile; ?>pushFromAdmin" method="post">
                <h3>pushFromAdmin</h3>
                <div class="form_row">
                    <div class="form_label">ent_message : </div>
                    <div class="form_field"><input type="text" name="ent_message" />name="ent_message",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">ent_notify : </div>
                    <div class="form_field"><input type="text" name="ent_notify" />name="ent_notify",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">ent_usertype : </div>
                    <div class="form_field"><input type="text" name="ent_usertype" />name="ent_usertype",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">ent_date_time : </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>    


        <div id="ForgotPasswordWithOtp" class="div_service">
            <form action="<?php echo $processFile; ?>ForgotPasswordWithOtp" method="post">
                <h3>ForgotPasswordWithOtp </h3>
                <div class="form_row">
                    <div class="form_label">ent_mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_mobile" />name="ent_mobile",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">ent_date_time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">ent_user_type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type",type="string" formate : 1-PROVIDER, 2-CUSTOMER</div>
                </div>

                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>




        <div id="verify_code_service" class="div_service">
            <form action="<?php echo $processFile; ?>verifyPhone" method="post">
                <h3>(C) Verify the code </h3>

                <div class="form_row">
                    <div class="form_label">Phone with country code *: </div>
                    <div class="form_field"><input type="text" name="ent_phone" />name="ent_phone",type="string" if Doing Signup then send Country Code, if doing Forgot Password Don't Send Country Code</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Verification code *: </div>
                    <div class="form_field"><input type="text" name="ent_code" />name="ent_code",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User Type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type",type="string" 1-pro 2-cust</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Service Type *: </div>
                    <div class="form_field"><input type="text" name="ent_service_type" />name="ent_service_type",type="string" 1-Signup 2-forgot</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="respond_to_request_service" class="div_service">
            <form action="<?php echo $processFile; ?>respondToAppointment" method="post">
                <h3>(P) Respond to appointment request</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking Id *: </div>
                    <div class="form_field"><input type="text" name="ent_bid" />name="ent_bid",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Provider Id *: </div>
                    <div class="form_field"><input type="text" name="ent_proid" />name="ent_proid",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Customer email *: </div>
                    <div class="form_field"><input type="text" name="ent_pat_email" />name="ent_pat_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment datetime *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:mm:ss"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Response *: </div>
                    <div class="form_field"><input type="text" name="ent_response" />name="ent_response",type="int",2 -> Accept & 3 -> reject</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking type *: </div>
                    <div class="form_field"><input type="text" name="ent_book_type" />name="ent_book_type",type="int",1 -> Now & 2 -> Later</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="update_status_service" class="div_service">
            <form action="<?php echo $processFile; ?>updateApptStatus" method="post">
                <h3>(P) Update appointment status to Customer</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Customer email *: </div>
                    <div class="form_field"><input type="text" name="ent_pat_email" />name="ent_pat_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Appointment datetime *: </div>
                    <div class="form_field"><input type="text" name="ent_appnt_dt" />name="ent_appnt_dt",type="datetime",format="YYYY-MM-DD HH:mm:ss"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking Id *: </div>
                    <div class="form_field"><input type="text" name="ent_bid" />name="ent_bid",type -> Integer </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Response *: </div>
                    <div class="form_field"><input type="text" name="ent_response" />name="ent_response",type="int",5 -> On the way, 6 -> Arrived / appointment start, 7 -> Appointment completed </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Doc Notes : </div>
                    <div class="form_field"><input type="text" name="ent_doc_remarks" />name="ent_doc_remarks",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Signature URL : </div>
                    <div class="form_field"><input type="text" name="ent_signature_url" />name="ent_signature_url",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">TOTAL : </div>
                    <div class="form_field"><input type="text" name="ent_total_pro" />name="ent_total_pro",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Timer : </div>
                    <div class="form_field"><input type="text" name="ent_timer" />name="ent_timer",type="string" when status 22</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="update_status_service" class="div_service">
            <form action="<?php echo $processFile; ?>UpdateApptTimer" method="post">
                <h3>(P) Update Appointment Timer</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking Id *: </div>
                    <div class="form_field"><input type="text" name="ent_bid" />name="ent_bid",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Timer : *</div>
                    <div class="form_field"><input type="text" name="ent_timer" />name="ent_timer",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Timer Status : *</div>
                    <div class="form_field"><input type="text" name="ent_timer_status" />name="ent_timer_status",type="Integer" , 1-start , 2-pause , 3 - stop</div>
                </div>

                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="UpdateAppVersion" class="div_service">
            <form action="<?php echo $processFile; ?>UpdateAppVersion" method="post">
                <h3>(P)(C)UpdateAppVersion</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">APP VERSION : *</div>
                    <div class="form_field"><input type="text" name="ent_app_version" />name="ent_app_version",type="STRING"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User Type : *</div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type",type="Integer" , 1-Provider , 2-Customer</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>



        <div id="get_master_profile_service" class="div_service">
            <form action="<?php echo $processFile; ?>getMasterProfile" method="post">
                <h3>(P) Get master profile</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="updateMasterProfile" class="div_service">
            <form action="<?php echo $processFile; ?>updateMasterProfile" method="post">
                <h3>(P) Update master profile</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Number Of Job Images : </div>
                    <div class="form_field"><input type="text" name="ent_job_img" />name="ent_job_img",type="Array of job images URL"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">About : </div>
                    <div class="form_field"><input type="text" name="ent_about" />name="ent_about",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">First Name : </div>
                    <div class="form_field"><input type="text" name="ent_name" />name="ent_name",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Last Name : </div>
                    <div class="form_field"><input type="text" name="ent_last_name" />name="ent_last_name",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Languages : </div>
                    <div class="form_field"><input type="text" name="ent_lang" />name="ent_lang",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">expertise : </div>
                    <div class="form_field"><input type="text" name="ent_expertise" />name="ent_expertise",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">profile image : </div>
                    <div class="form_field"><input type="text" name="ent_profile_img" />name="ent_profile_img",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>



        <div id="updateMasterJobImages" class="div_service">
            <form action="<?php echo $processFile; ?>updateMasterJobImages" method="post">
                <h3>(P) Update master Job Images</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Job Images : </div>
                    <div class="form_field"><input type="text" name="ent_num_job_img" />name="ent_job_img",type="Array of job images URL"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>




        <div id="reser_pass_service" class="div_service">
            <form action="<?php echo $processFile; ?>resetPassword" method="post">
                <h3>(P) Reset password</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="reser_pass_service" class="div_service">
            <form action="<?php echo $processFile; ?>resetPassword" method="post">
                <h3>(P) Reset password</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="update_presence_service" class="div_service">
            <form action="<?php echo $processFile; ?>updateMasterStatus" method="post">
                <h3>(P) Update Provider as active or inactive</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Provider status *: </div>
                    <div class="form_field"><input type="text" name="ent_status" />name="ent_status",type="int",3->active,4->inactive</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="abort_appointment_service" class="div_service">
            <form action="<?php echo $processFile; ?>abortAppointment" method="post">
                <h3>(P) Abort appointment</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking Id *: </div>
                    <div class="form_field"><input type="text" name="ent_bid" />name="ent_bid",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Reason *: </div>
                    <div class="form_field"><input type="text" name="ent_reason" />name="ent_reason",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="raise_invoice" class="div_service">
            <form action="<?php echo $processFile; ?>raiseInvoice" method="post">
                <h3>(P) Raise Invoice</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking Id *: </div>
                    <div class="form_field"><input type="text" name="ent_bid" />name="ent_bid",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Minuts *: </div>
                    <div class="form_field"><input type="text" name="ent_minutes" />name="ent_minutes",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div id="GetFinancialData" class="div_service">
            <form action="<?php echo $processFile; ?>GetFinancialData" method="post">
                <h3>(P) Get Financial Data</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Provider Id *: </div>
                    <div class="form_field"><input type="text" name="ent_pro_id" />name="ent_pro_id",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <div id="CanIStartJob" class="div_service">
            <form action="<?php echo $processFile; ?>CanIStartJob" method="post">
                <h3>(P) Can I Start Job</h3>
                <div class="form_row">
                    <div class="form_label">Session token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking Id *: </div>
                    <div class="form_field"><input type="text" name="ent_bid" />name="ent_bid",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>


        <!-- Start Provider Address -->
        <div id="AddProviderAddress" class="div_service">
            <form action="<?php echo $processFile; ?>AddProviderAddress" method="post">
                <h3>(P) Add Provider Address :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Provider Id *: </div>
                    <div class="form_field"><input type="text" name="ent_pro_id" />name="ent_pro_id",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Location name *: </div>
                    <div class="form_field"><input type="text" name="ent_locName" />name="ent_locName",type="String"</div>
                </div>

                <div class="form_row">
                    <div class="form_label">Address 1*: </div>
                    <div class="form_field"><input type="text" name="ent_address1" />name="ent_address1",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Address 2: </div>
                    <div class="form_field"><input type="text" name="ent_address2" />name="ent_address2",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Latitude : </div>
                    <div class="form_field"><input type="text" name="ent_lat" />name="ent_lat",type="Float"</div>
                </div>     
                <div class="form_row">
                    <div class="form_label">Longitude : </div>
                    <div class="form_field"><input type="text" name="ent_lng" />name="ent_lng",type="Float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>


        <div id="DeleteProviderAddress" class="div_service">
            <form action="<?php echo $processFile; ?>DeleteProviderAddress" method="post">
                <h3>(P) Delete Provider Address :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Provider Id *: </div>
                    <div class="form_field"><input type="text" name="ent_pro_id" />name="ent_pro_id",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Address Id *: </div>
                    <div class="form_field"><input type="text" name="ent_addressid" />name="ent_addressid",type="string"</div>
                </div>        
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>

        <div id="GetProviderAddress" class="div_service">
            <form action="<?php echo $processFile; ?>GetProviderAddress" method="post">
                <h3>(P) Get Provider Address :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Provider Id *: </div>
                    <div class="form_field"><input type="text" name="ent_pro_id" />name="ent_pro_id",type="Integer"</div>
                </div>        
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>
        <!-- End Provider Address -->

        <div id="addslot" class="div_service">
            <form action="<?php echo $processFile; ?>addslot" method="post">
                <h3>(P) ADD  Slot :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Provider Id *: </div>
                    <div class="form_field"><input type="text" name="ent_pro_id" />name="ent_pro_id",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Location  Id *: </div>
                    <div class="form_field"><input type="text" name="ent_location" />name="ent_location",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Start Time *: </div>
                    <div class="form_field"><input type="text" name="ent_start_time" />name="ent_start_time",type="String" Format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Radius *: </div>
                    <div class="form_field"><input type="text" name="ent_radius" />name="ent_radius",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Price *: </div>
                    <div class="form_field"><input type="text" name="ent_price" />name="ent_price",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Slot Duration *: </div>
                    <div class="form_field"><input type="text" name="ent_duration" />name="ent_duration",type="Integer" In Minuts</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Number of Slot *: </div>
                    <div class="form_field"><input type="text" name="ent_num_slot" />name="ent_num_slot",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Option *: </div>
                    <div class="form_field"><input type="text" name="ent_option" />name="ent_option",type="Integer" Format = "1=NOT-REPEAT, 2-REPEAT"</div>
                </div>

                FOR REPEAT SLOT                
                <div class="form_row">
                    <div class="form_label">Repeat Day *: </div>
                    <div class="form_field"><input type="text" name="ent_repeatday" />name="ent_repeatday",type="Integer" Format: 1=EveryDay, 2=Weak-Day, 3=WEEK-END  </div>
                </div>
                <div class="form_row">
                    <div class="form_label">End Date *: </div>
                    <div class="form_field"><input type="text" name="ent_end_date" />name="ent_end_date",type="Date" Format = "YYYY-MM-DATE"</div>
                </div> 


                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>



        <div id="removeSlot" class="div_service">
            <form action="<?php echo $processFile; ?>removeSlot" method="post">
                <h3>(P) REMOVE  Slot :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Provider Id *: </div>
                    <div class="form_field"><input type="text" name="ent_pro_id" />name="ent_pro_id",type="Integer"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Slot Id *: </div>
                    <div class="form_field"><input type="text" name="ent_slot_id" />name="ent_slot_id",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>




        <div id="GetBookedSlotDetail" class="div_service">
            <form action="<?php echo $processFile; ?>GetBookedSlotDetail" method="post">
                <h3>(P) GET BOOKED Slot DETAILS:</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Slot Id *: </div>
                    <div class="form_field"><input type="text" name="ent_slot_id" />name="ent_slot_id",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>



        <div id="GetCategoryDetail" class="div_service">
            <form action="<?php echo $processFile; ?>GetCategoryDetail" method="post">
                <h3>(P) GET CATEGORY DETAILS:</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Category Id *: </div>
                    <div class="form_field"><input type="text" name="ent_cat_id" />name="ent_cat_id",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Provider Id *: </div>
                    <div class="form_field"><input type="text" name="ent_pro_id" />name="ent_pro_id",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>



        <div id="UpdateCategoryDetail" class="div_service">
            <form action="<?php echo $processFile; ?>UpdateCategoryDetail" method="post">
                <h3>(P) UPDATE CATEGORY DETAILS:</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>  
                <div class="form_row">
                    <div class="form_label">Provider Id *: </div>
                    <div class="form_field"><input type="text" name="ent_pro_id" />name="ent_pro_id",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Category Id *: </div>
                    <div class="form_field"><input type="text" name="ent_cat_id" />name="ent_cat_id",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Price Per Minut *: </div>
                    <div class="form_field"><input type="text" name="ent_price_min" />name="ent_price_min",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>



        <div id="UpdateBookingReminder" class="div_service">
            <form action="<?php echo $processFile; ?>UpdateBookingReminder" method="post">
                <h3>(P)(C) Update Booking Reminder:</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User Type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type",type="Integer" 1- Provider , 2- Customer</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Booking Id *: </div>
                    <div class="form_field"><input type="text" name="ent_bid" />name="ent_bid",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Reminder Id *: </div>
                    <div class="form_field"><input type="text" name="ent_rem_id" />name="ent_rem_id",type="String"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>


        <div id="ProviderCheckMobile" class="div_service">
            <form action="<?php echo $processFile; ?>ProviderCheckMobile" method="post">
                <h3>Provider Check mobile available or not :</h3>
                <div class="form_row">
                    <div class="form_label">Mobile *: </div>
                    <div class="form_field"><input type="text" name="ent_phone" />name="ent_phone"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>


        <div id="AddBankDetails" class="div_service">
            <form action="<?php echo $processFile; ?>AddBankDetails" method="post">
                <h3>AddBankDetails :</h3>
                <div class="form_row">
                    <div class="form_label">ent_sess_token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token"</div>
                </div>

                <div class="form_row">
                    <div class="form_label">ent_dev_id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">routing_number *: </div>
                    <div class="form_field"><input type="text" name="routing_number" />name="routing_number"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">account_number *: </div>
                    <div class="form_field"><input type="text" name="account_number" />name="account_number"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">account_holder_name *: </div>
                    <div class="form_field"><input type="text" name="account_holder_name" />name="account_holder_name"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">dob *: </div>
                    <div class="form_field"><input type="text" name="dob" />name="dob" format="mm/dd/yyyy" </div>
                </div>
                <div class="form_row">
                    <div class="form_label">personal_id *: </div>
                    <div class="form_field"><input type="text" name="personal_id" />name="personal_id"</div>
                </div>

                <div class="form_row">
                    <div class="form_label">state *: </div>
                    <div class="form_field"><input type="text" name="state" />name="state"</div>
                </div>

                <div class="form_row">
                    <div class="form_label">postal_code *: </div>
                    <div class="form_field"><input type="text" name="postal_code" />name="postal_code"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">city *: </div>
                    <div class="form_field"><input type="text" name="city" />name="city"</div>
                </div>

                <div class="form_row">
                    <div class="form_label">address *: </div>
                    <div class="form_field"><input type="text" name="address" />name="address"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">IdProof *: </div>
                    <div class="form_field"><input type="text" name="IdProof" />name="IdProof" Format="Amazon Link"</div>
                </div>               
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>

        <div id="getMasterBankData" class="div_service">
            <form action="<?php echo $processFile; ?>getMasterBankData" method="post">
                <h3>getMasterBankData :</h3>
                <div class="form_row">
                    <div class="form_label">ent_sess_token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token"</div>
                </div>                
                <div class="form_row">
                    <div class="form_label">ent_dev_id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id"</div>
                </div>             
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>

        <div id="deleteMasterBank" class="div_service">
            <form action="<?php echo $processFile; ?>deleteMasterBank" method="post">
                <h3>deleteMasterBank :</h3>
                <div class="form_row">
                    <div class="form_label">ent_sess_token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token"</div>
                </div>                
                <div class="form_row">
                    <div class="form_label">ent_dev_id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id"</div>
                </div>             
                <div class="form_row">
                    <div class="form_label">bank_token *: </div>
                    <div class="form_field"><input type="text" name="bank_token" />name="bank_token"</div>
                </div>             
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>
        <div id="BankAccountDefault" class="div_service">
            <form action="<?php echo $processFile; ?>BankAccountDefault" method="post">
                <h3>BankAccountDefault :</h3>
                <div class="form_row">
                    <div class="form_label">ent_sess_token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name="ent_sess_token"</div>
                </div>                
                <div class="form_row">
                    <div class="form_label">ent_dev_id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id"</div>
                </div>             
                <div class="form_row">
                    <div class="form_label">bank_token *: </div>
                    <div class="form_field"><input type="text" name="bank_token" />name="bank_token"</div>
                </div>             
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>




        <div id="GetAllSlot" class="div_service">
            <form action="<?php echo $processFile; ?>GetAllSlot" method="post">
                <h3>(P) GET ALL  Slots :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Provider Id *: </div>
                    <div class="form_field"><input type="text" name="ent_pro_id" />name="ent_pro_id",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Month  *: </div>
                    <div class="form_field"><input type="text" name="ent_month" />name="ent_month",type="String" Format="YYYY-MM" </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>



        <div id="check_email_service" class="div_service">
            <form action="<?php echo $processFile; ?>validateEmailZip" method="post">
                <h3>Validate email and zipcode :</h3>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Zipcode *: </div>
                    <div class="form_field"><input type="text" name="zip_code" />name="zip_code",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type", 1- Provider, 2- Customer</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>

        <!--        <div id="check_zip_service" class="div_service">
                    <form action="<?php echo $processFile; ?>checkZip" method="post">
                        <h3>Check Zip :</h3>
                        <div class="form_row">
                            <div class="form_label">Zipcode *: </div>
                            <div class="form_field"><input type="text" name="zip_code" />name="zip_code",type="int"</div>
                        </div>
                        <div class="form_row">
                            <div class="form_label">&nbsp;</div>
                            <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                        </div>
                    </form>
                </div>-->

        <div id="forgot_pass_service" class="div_service">
            <form action="<?php echo $processFile; ?>forgotPassword" method="post">
                <h3>Forgot password :</h3>
                <div class="form_row">
                    <div class="form_label">Email *: </div>
                    <div class="form_field"><input type="text" name="ent_email" />name="ent_email",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type", 1- Provider, 2- Customer</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>        



        <div id="logout_service" class="div_service">
            <form action="<?php echo $processFile; ?>logout" method="post">
                <h3>Logout the user :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">User type *: </div>
                    <div class="form_field"><input type="text" name="ent_user_type" />name="ent_user_type", 1- Provider, 2- Customer</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>

        <div id="check_coupon_service" class="div_service">
            <form action="<?php echo $processFile; ?>checkCoupon" method="post">
                <h3>Check coupon available or not :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Coupon *: </div>
                    <div class="form_field"><input type="text" name="ent_coupon" />name="ent_coupon"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Lat *: </div>
                    <div class="form_field"><input type="text" name="ent_lat" />name="ent_lat"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Long *: </div>
                    <div class="form_field"><input type="text" name="ent_long" />name="ent_long"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Date time *: </div>
                    <div class="form_field"><input type="text" name="ent_date_time" />name="ent_date_time",type="datetime",format="YYYY-MM-DD HH:MM:SS"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>


        <div id="verify_coupon_service" class="div_service">
            <form action="<?php echo $processFile; ?>verifyCode" method="post">
                <h3>Check coupon available or not :</h3>
                <div class="form_row">
                    <div class="form_label">Coupon *: </div>
                    <div class="form_field"><input type="text" name="ent_coupon" />name="ent_coupon"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Lat *: </div>
                    <div class="form_field"><input type="text" name="ent_lat" />name="ent_lat"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Long *: </div>
                    <div class="form_field"><input type="text" name="ent_long" />name="ent_long"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>



        <div id="AddCustomerAddress" class="div_service">
            <form action="<?php echo $processFile; ?>AddCustomerAddress" method="post">
                <h3>(C) Add Customer Address :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Customer Id *: </div>
                    <div class="form_field"><input type="text" name="ent_cust_id" />name="ent_cust_id",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Address 1*: </div>
                    <div class="form_field"><input type="text" name="ent_address1" />name="ent_address1",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Address 2: </div>
                    <div class="form_field"><input type="text" name="ent_address2" />name="ent_address2",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">suite Number : </div>
                    <div class="form_field"><input type="text" name="ent_suite_num" />name="ent_suite_num",type="string"</div>
                </div>     
                <div class="form_row">
                    <div class="form_label">Tag Address : </div>
                    <div class="form_field"><input type="text" name="ent_tag_address" />name="ent_tag_address",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Zip Code *: </div>
                    <div class="form_field"><input type="text" name="ent_zipcode" />name="ent_zipcode",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Latitude *: </div>
                    <div class="form_field"><input type="text" name="ent_latitude" />name="ent_latitude",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Longitude *: </div>
                    <div class="form_field"><input type="text" name="ent_longitude" />name="ent_longitude",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>


        <div id="DeleteCustomerAddress" class="div_service">
            <form action="<?php echo $processFile; ?>DeleteCustomerAddress" method="post">
                <h3>(C) Delete Customer Address :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Customer Id *: </div>
                    <div class="form_field"><input type="text" name="ent_cust_id" />name="ent_cust_id",type="Integer"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Address Id *: </div>
                    <div class="form_field"><input type="text" name="ent_addressid" />name="ent_addressid",type="string"</div>
                </div>        
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>

        <div id="GetCustomerAddress" class="div_service">
            <form action="<?php echo $processFile; ?>GetCustomerAddress" method="post">
                <h3>(C) Get Customer Address :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Customer Id *: </div>
                    <div class="form_field"><input type="text" name="ent_cust_id" />name="ent_cust_id",type="Integer"</div>
                </div>        
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>


        <div id="getAllServices" class="div_service">
            <form action="<?php echo $processFile; ?>getAllServices" method="post">
                <h3>(C) Get All Services :</h3>
                <div class="form_row">
                    <div class="form_label">Session Token *: </div>
                    <div class="form_field"><input type="text" name="ent_sess_token" />name= "ent_sess_token",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Device Id *: </div>
                    <div class="form_field"><input type="text" name="ent_dev_id" />name="ent_dev_id",type="string"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Category Id *: </div>
                    <div class="form_field"><input type="text" name="ent_catid" />name="ent_catid",type="String"</div>
                </div>        
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>


        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>
        <h1>&nbsp;</h1>




        <div class="div_service">
            <form action="<?php echo $processFile; ?>assignDoctorSlots" method="post">
                <h3>Assign Provider slots:</h3>
                <div class="form_row">
                    <div class="form_label">Provider id *: </div>
                    <div class="form_field"><input type="text" name="ent_doc_id" />name= "ent_doc_id"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Days (comma separated 1(Monday) - 7(Sunday)) *: </div>
                    <div class="form_field"><input type="text" name="ent_days" />name="ent_days"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Start time *: </div>
                    <div class="form_field"><input type="text" name="ent_start" />name= "ent_start",24 hour format</div>
                </div>
                <div class="form_row">
                    <div class="form_label">End time *: </div>
                    <div class="form_field"><input type="text" name="ent_end" />name="ent_end",24 hour format</div>
                </div>   
                <div class="form_row">
                    <div class="form_label">Duration (in minutes)*: </div>
                    <div class="form_field"><input type="text" name="ent_duration" />name="ent_duration"</div>
                </div>    
                <div class="form_row">
                    <div class="form_label">Interval between slot (in minutes)*: </div>
                    <div class="form_field"><input type="text" name="ent_interval" />name="ent_interval"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="ent_submit" value="Submit" /></div>
                </div>
            </form>
        </div>



        <div>
            <form action="<?php echo $processFile; ?>testUpdateLoc" method="post">
                <h3>(T) Update Provider latlong</h3>
                <div class="form_row">
                    <div class="form_label">Doc *: </div>
                    <div class="form_field"><input type="text" name='ent_doc' />name="ent_doc",type="int"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Lat : </div>
                    <div class="form_field"><input type="text" name='ent_lat' />name="ent_lat",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Long : </div>
                    <div class="form_field"><input type="text" name='ent_long' />name="ent_token",type="float"</div>
                </div>
                <div class="form_row">
                    <div class="form_label">Now/later : </div>
                    <div class="form_field"><input type="text" name='ent_status' />name="ent_status",3->now, 4-> later</div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

        <div>
            <form action="services_v5.php/testPushWoosh" method="post">
                <h3>(T) test push woosh</h3>
                <div class="form_row">
                    <div class="form_label">type *: </div>
                    <div class="form_field">
                        <select name="type">
                            <option value="1">Driver</option>
                            <option value="2">Passenger</option>
                        </select>                     
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label">msg *: </div>
                    <div class="form_field">
                        <input type="text" name="message" />
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label">token *: </div>
                    <div class="form_field">
                        <input type="text" name="token" />
                    </div>
                </div>
                <div class="form_row">
                    <div class="form_label">*-marked are mandatory</div>
                    <div class="form_field"><input type="submit" name='/"ent_submit/"' value="Submit" />name="ent_submit"</div>
                </div>
            </form>
        </div>

    </body>
</html>
