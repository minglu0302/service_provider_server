#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://guest:guest@localhost:5672', function (err, conn) {
//amqp.connect('amqp://localhost', function(err, conn) {
//    console.log(conn);
    conn.createChannel(function (err, ch) {
        var q = 'hello';
        var msg = 'Hello World!';

        ch.assertQueue(q, {durable: false});
        // Note: on Node 6 Buffer.from(msg) should be used
        ch.sendToQueue(q, new Buffer(msg));
        console.log(" [x] Sent %s", msg);
    });
    setTimeout(function () {
        conn.close();
        process.exit(0)
    }, 500);
});



//conn.on('error', function(e) {
//  conn.log("Error from amqp: ", e);
//});
